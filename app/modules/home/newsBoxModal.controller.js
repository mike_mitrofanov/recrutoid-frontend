(function ()
{
    'use strict';

    function NewsBoxModalController($modalInstance, newsBoxData, gettext)
    {
        var ctrl = this;

        this.closeModal = function(){
            $modalInstance.dismiss();
        };


        (function init()
        {
            ctrl.data = newsBoxData;

        })();
    }

    angular.module('recrutoid.common').controller('NewsBoxModalController',
        ['$modalInstance', 'newsBoxData', 'gettext', NewsBoxModalController]);
})();
