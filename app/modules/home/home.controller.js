(function ()
{
    'use strict';

    function HomeCtrl($location, $routeParams, $scope, $rootScope, Authenticator, CurrentUser, gettext,
                      CommonDAO, UserDAO, recrutoidModals, recrutoidPopup, VacanciesDAO, SearchSuggestions)
    {
        var ctrl = this;
        this.search = {};


		this.onCreateCVClick = function()
        {
            function redirectOrShowWarning(user)
            {
                if ('employer' === user.role) {
                    var warning = gettext('Warning');
                    var text = gettext('Message_To_Create_Cv_Your_Account_Has_To_Be_Job_Seeker_Type');
                    recrutoidPopup(warning, text, 'warning');
                }
                else {
                    $location.url('/user/settings/cv-page?tab=my_cv');
                }
            }

            CurrentUser.resolve().then(function (user)
            {
                if(!user.id) {
                    recrutoidModals.entireFlow({
                        register: {
                            initialUserType: {
                                type: 'normal'
                            },
                            hideUserType: true
                        }
                    }).then(redirectOrShowWarning);
                }
                else {
                    redirectOrShowWarning(user);
                }
            });
        };

		this.onPublishClick = function()
        {
			function redirectOrShowWarning(user)
            {
                if ('normal' === user.role) {
                    var warning = gettext('Warning');
                    var text = gettext('Message_To_Post_New_Vacancy_Your_Account_Has_To_Be_Employer_Type');
                    recrutoidPopup(warning, text, 'warning');
                }
                else {
                    if ($rootScope.media == 'mobile'){
                        text = gettext('Message_Please_Use_Desktop_Or_Tablet_To_Use_All_The_Features');
                        recrutoidPopup({
                            type: 'warning',
                            title: gettext('Warning!'),
                            text: text
                        });
                        return;
                    }
                    $location.url('/user/settings/myVacancies');
                }
            }

            CurrentUser.resolve().then(function (user)
            {
                if(!user.id) {
                    recrutoidModals.entireFlow({
                        login: {
                            disableSocialOptions: true
                        },
                        register: {
                            initialUserType: {
                                type: 'employer'
                            },
                            hideUserType: true
                        }
                    }).then(redirectOrShowWarning);
                }
                else {
                    redirectOrShowWarning(user);
                }
            });
        };

		this.onCVDatabaseClick = function()
        {
			function redirectOrShowWarning(user)
            {
                if ('normal' === user.role) {
                    var warning = gettext('Warning');
                    var text = gettext('Message_To_Post_New_Vacancy_Your_Account_Has_To_Be_Employer_Type');
                    recrutoidPopup(warning, text, 'warning');
                }
                else {
                    $location.url('/users');
                }
            }

            CurrentUser.resolve().then(function (user)
            {
                if(!user.id) {
                    recrutoidModals.entireFlow({
                        register: {
                            initialUserType: {
                                type: 'normal'
                            }
                        }
                    }).then(redirectOrShowWarning);
                }
                else {
                    redirectOrShowWarning(user);
                }
            });
        };


        this.createCV = function()
        {
            function redirectOrShowWarning(user)
            {
                if('employer' === user.role) {
                    var warning = gettext('Warning');
                    var text = gettext('Message_To_Create_Cv_Your_Account_Has_To_Be_Job_Seeker_Type');
                    recrutoidPopup(warning, text, 'warning');
                }
                else {
                    $location.url('/user/settings/edit-cv');
                }
            }

            CurrentUser.resolve().then(function (user)
            {
                if(!user.id) {
                    recrutoidModals.entireFlow({
                        register: {
                            initialUserType: {
                                type: 'normal'
                            }
                        }
                    }).then(redirectOrShowWarning);
                }
                else {
                    redirectOrShowWarning(user);
                }
            });
        };

        this.postVacancy = function()
        {
            function redirectOrShowWarning(user)
            {
                if(user) {
                    if('normal' === user.role) {
                        var warning = gettext('Warning');
                        var text = gettext('Message_To_Post_New_Vacancy_Your_Account_Has_To_Be_Employer_Type');
                        recrutoidPopup(warning, text, 'warning');
                    }
                    else {
                        $location.url('/user/settings/myVacancies?addVacancy');
                    }
                }
            }

            CurrentUser.resolve().then(function (user)
            {
                if(!user.id) {
                    recrutoidModals.entireFlow({
                        login: {
                            disableSocialOptions: true
                        },
                        register: {
                            initialUserType: {
                                type: 'employer'
                            }
                        }
                    }).then(redirectOrShowWarning);
                }
                else {
                    redirectOrShowWarning(user);
                }
            });
        };

        this.doSearch = function (isEmployer)
        {

            var filter = {};
            if (ctrl.search.title) {
                if(isEmployer) {
                    filter.resumeTitle = ctrl.search.title;
                } else {
                    filter.vacancyTitle = ctrl.search.title;
                }
            }
            if (ctrl.selectedCity) {
                filter.city = ctrl.selectedCity.id;
            }
            if (ctrl.selectedCategory) {
                filter.category = ctrl.selectedCategory.id;
            }

            $location.search(filter);

            CurrentUser.resolve().then(function (user)
            {
                if(user && 'employer' === user.role) {
                    $location.path('/users');
                }
                else {
                    $location.path('/vacancies');
                }
            });
        };

        function refreshOffers()
        {
            var filter = {
                size: 20,
                publishMode: 'website',
                verified: true
            };

            ctrl.resultData = [];

            // reloading because is user was logged in as employer we do not want to serve list of vacancies to him.
            return CurrentUser.reload().then(function (user)
            {
                if(user && 'employer' === user.role) {
                    return;
                }

                VacanciesDAO.query(filter).then(function (data)
                {
                    angular.forEach(data.results, function (elem)
                    {
                        elem.displayingTitle = elem.title;
                        elem.link = 'vacancy/' + elem.id;
                    });
                    ctrl.resultData.push(data.results.slice(0, 10));
                    ctrl.resultData.push(data.results.slice(10, data.results.length));
                });
            });
        }
        
        SearchSuggestions.setContext(ctrl, '', 'title');
        SearchSuggestions.setCtrlField('search');
        $scope.suggestionsHandler = SearchSuggestions;
        
        
        function clearFilters() {
            var clearableFilters = [
                'category',
                'city',
                'resumeTitle',
                'vacancyTitle'
            ];
            
            var params = $location.search();
            
            for(var i in clearableFilters) {
                console.log(clearableFilters[i]);
                   $location.search(clearableFilters[i], null);
                
            } 
            
        }
        
        function init()
        {
            if($location.search().hasOwnProperty('login')) {
                recrutoidModals.entireFlow();
            }
            
            clearFilters();
            
            refreshOffers();

            CommonDAO.getHomeData().then(function (data)
            {
                ctrl.categories = data.categories;
                ctrl.categoriesForSubscription = angular.copy(data.categories);
                ctrl.categoriesForSubscription.unshift({name: {en: 'All categories', ru: 'Все категории'}});
                ctrl.cities = data.cities;
                ctrl.citiesForSubscription = data.citiesForSubscription;
                ctrl.citiesForSubscription.unshift({name: {en: 'All cities', ru: 'Все города'}});
                ctrl.salaryRanges = data.salaryRanges;
                ctrl.salaryRangesForSubscription = angular.copy(data.salaryRanges);
                ctrl.salaryRangesForSubscription.unshift({name: {en: 'All salary ranges', ru: 'Все диапазоны окладов'}});
                ctrl.vacanciesCount = data.statistics.vacancies;
                ctrl.usersCount = data.statistics.users;
                ctrl.news = data.news;
            });

        }

        if ($routeParams.token) {
            if(-1 < $location.path().indexOf('activate')) {
                UserDAO.activate({token: $routeParams.token}).then(function (data)
                {
                    var text;
                    // variable data.publishOnWebsite is available here
                    if (data.vacancyActivation && data.publishOnWebsite) {
                        text = gettext('Message_Your_Account_Has_Been_Created_And_Vacancy_Published');
                    }
                    else if (data.vacancyActivation) {
                        text = gettext('Message_Your_Account_Has_Been_Created');
                    }
                    else {
                        text = gettext('Message_Your_Account_Has_Been_Activated');
                    }

                    recrutoidPopup(gettext('Congratulations!'), text, 'success');

                    if(data.token) {
                        Authenticator.setToken(data.token);
                        CurrentUser.reload();
                        $location.path('/user/settings/editProfile');
                    }
                }).catch(function (error)
                {
                    if (405 === error.status) {
                        recrutoidPopup('warning', gettext('Banned'), gettext('Message_You_Have_Been_Banned_Contact_Support_To_Get_More_Info'));
                    }
                    else if (error.data) {
                        recrutoidPopup('warning', gettext('Warning!'), error.data);
                    }
                });
            }
            else if(-1 < $location.path().indexOf('confirm-email')) {
                UserDAO.confirmEmail({token: $routeParams.token}).then(function ()
                {
                    $location.path('/user/settings/editProfile');
                    recrutoidPopup(gettext('Success!'), gettext('Message_Email_Successfully_Changed'), 'success');
                });
            }
        }
        init();

        $scope.$on('event:auth-loggedOut', refreshOffers);
        $scope.$on('event:auth-loggedIn', refreshOffers);
    }

    angular.module('recrutoid.subscription').controller('HomeCtrl',
            ['$location', '$routeParams', '$scope', '$rootScope', 'Authenticator', 'CurrentUser', 'gettext',
                'CommonDAO', 'UserDAO', 'recrutoidModals', 'recrutoidPopup', 'VacanciesDAO', 'SearchSuggestions',  HomeCtrl]);
})();
