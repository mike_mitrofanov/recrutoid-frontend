(function ()
{
    'use strict';

    function homeCarousel($interval)
    {
        function link(scope, elem) {}

        return {
            restrict: 'A',
            controller: 'HomeCarouselController as homeCarousel',
            link: link,
            templateUrl: 'modules/home/carousel.tpl.html'
        };

    }

    function HomeCarouselController($location, $scope, $rootScope, $timeout, CurrentUser, gettext, recrutoidModals, recrutoidPopup, siteSettings, UserDAO)
    {

        $scope.awesomeThings = ['HTML5', 'AngularJS', 'Karma', 'Slick', 'Bower', 'Coffee'];

        var ctrl = this;
        $scope.carouselInit = true;
        var w = angular.element(window);


        this.showResume = function (userId) {
            $location.path('/user/' + userId + '/profile');
        };

        this.goToVacancySearch = function(companyName) {
            if (companyName) {
                $location.path('/vacancies/').search({companyName: companyName});
            } else {
                $location.path('/vacancies/');
            }
        };

        this.goToCVSearch = function(companyId) {
            if (companyId) {
                $location.path('/users/').search({company: companyId});
            } else {
                $location.path('/users/');
            }
        };

        this.shouldDisplayOnDesktop = function(index) {
            return $rootScope.media === 'desktop' && index === 3;
        };

        this.shouldDisplayOnTablet = function(index) {
            return $rootScope.media === 'tablet' && index === 3;
        };

        this.shouldDisplayOnMobile = function(index) {
            return $rootScope.media === 'mobile' && index === 0;
        };

        $scope.slickConfig = {
            dots: true,
            infinite: false,
            speed: 300,
            slidesToShow: 4,
            slidesToScroll: 4,
            prevArrow: '<div class="slick-prev"><i class="fa fa-chevron-left"></i></div>',
            nextArrow: '<div class="slick-next"><i class="fa fa-chevron-right"></i></div>',
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        };

        $rootScope.$on('cfpLoadingBar:completed', function(event, message) {
            $scope.carouselInit = true;
            $scope.carouselLoaded = true;
        });

        function loadCarousel() {
            $scope.carouselLoaded = false;
            UserDAO.carousel().then(function (data)
            {
                ctrl.carouselData = [];
                ctrl.carouselDataMobile = [];
                var number_slides = data.results.count;

                var item = [];
                data.results.data.forEach(function(value, index){
                    item.push(value);

                    if ( ((index + 1 - 3) % 4 == 0 && index > 3) || index == 2) {
                        ctrl.carouselDataMobile.push(item);
                        item = [];
                    }

                    ctrl.carouselData.push(value);
                });

                $scope.number_slides = number_slides;
                //$scope.initArrows();
            });
        }

        (function init()
        {
            loadCarousel();

            $scope.$root.$on('event:auth-loggedOut', function ()
            {
                $scope.carouselInit = false;
                loadCarousel();
            });

            $scope.$root.$on('event:auth-loggedIn', function ()
            {
                $scope.carouselInit = false;
                loadCarousel();
            });
        })();
    }

    angular.module('recrutoid').directive('homeCarousel', ['$interval', homeCarousel]);
    angular.module('recrutoid').controller('HomeCarouselController',
        ['$location', '$scope', '$rootScope', '$timeout', 'CurrentUser', 'gettext', 'recrutoidModals', 'recrutoidPopup', 'siteSettings', 'UserDAO', HomeCarouselController]);
})();
