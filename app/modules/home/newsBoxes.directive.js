(function()
{
    'use strict';

    function newsBoxes()
    {
        return {
            restrict: 'A',
            scope: {
                newsBoxes: '='
            },
            templateUrl: 'modules/home/newsBoxes.tpl.html',
            controller: function($scope, $modal) {

                $scope.openModal = function(newsBoxData) {
                    $modal.open({
                        size: 'sm',
                        templateUrl: 'modules/home/newsBoxModal.tpl.html',
                        windowClass: 'recrutoid-newsbox-window',
                        controller: 'NewsBoxModalController as newsBox',
                        resolve: {
                            newsBoxData: function()
                            {
                                return newsBoxData;
                            }
                        }
                    })
                };
            }
        }
    }

    angular.module('recrutoid.home').directive('newsBoxes', [newsBoxes])
})();
