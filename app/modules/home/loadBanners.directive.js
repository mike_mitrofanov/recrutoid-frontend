(function ()
{
    'use strict';

    function loadBanners($document, BannersDAO)
    {
        function linkFn()
        {
            BannersDAO.getPublished().then(function (data)
            {
                var banner;
                angular.forEach(data.results, function (elem)
                {
                    banner = $document.find('.banner-' + elem.location);
                    banner.find('img').attr('src', elem.url);
                    banner.css('display', 'block');
                });
            });
        }

        return {
            restrict: 'A',
            link: linkFn
        };
    }

    angular.module('recrutoid').directive('loadBanners', ['$document', 'BannersDAO', loadBanners]);
})();
