(function ()
{
    'use strict';

    function uploadCvModalController($modalInstance, $modal, $scope, gettext, CurrentUser, recrutoidPopup, UserResumeDAO, SalaryRangesDAO, CategoriesDAO, CitiesDAO)
    {
        var ctrl = this;
		ctrl.cv = {};
        ctrl.errors = {};

        this.close = function ()
        {
			$modalInstance.dismiss();
        };
        

        this.send = function ()
        {
            var text;
            var successDisplay;

            if(isFormValid()){
                var fileExtension = getFileExtension(ctrl.currentFile);
                UserResumeDAO.uploadCv(ctrl.currentFile, ctrl.cv, fileExtension).then(function(results){
                    $modalInstance.close();
                    $scope.$emit('event:cv-uploaded', true);
                });
            }else if(!ctrl.currentFile){
                text = gettext('Message_You_Have_To_Choose_File_To_Upload');
                successDisplay = gettext('Warning');
                recrutoidPopup(successDisplay, text, 'warning');
            }else{
                text = gettext('Message_Some_fields_require_your_attention');
                successDisplay = gettext('Warning');
                recrutoidPopup(successDisplay, text, 'warning');
            }

        };


//		function handleErrors() {
//            ctrl.errors = {};
//
//            if (!ctrl.feedback.message) {
//                ctrl.errors.message = true;
//            }
//
//            if (!validateEmail(ctrl.feedback.email)) {
//                ctrl.errors.email = true;
//            }
//        }

        function isFormValid() {
            var requiredMessage = "Please_fill_out_this_field";

            var isValid = true;
            if (!ctrl.cv.title || !ctrl.cv.title.length) {
                ctrl.errors.title = requiredMessage;
                isValid = false;
            }

            if(!ctrl.cv.category) {
                ctrl.errors.category = requiredMessage;
                isValid = false;
            }

            if(!ctrl.cv.city) {
                ctrl.errors.city = requiredMessage;
                isValid = false;
            }
            if(!ctrl.cv.desiredSalaryRange) {
                ctrl.errors.desiredSalaryRange = requiredMessage;
                isValid = false;
            }

            return isValid;
        }

		this.uploadCv = function (files)
		{
			if (!files || !files.length) {
				return;
			}

			if(isUploadValid(files)){
                ctrl.currentFileName = files[0].name;
                ctrl.currentFile = files;
            }
		};

		function isUploadValid(files) {
            if(files[0].type !== 'application/pdf' && files[0].name.indexOf('docx') == -1 && files[0].name.indexOf('doc') == -1){
                var text = gettext('Message_Wrong_File_Type_selected');
                var successDisplay = gettext('Warning');
                recrutoidPopup(successDisplay, text, 'warning');
                return false;
            }
            return true;
        }

        function getFileExtension(files){
            if(files[0].type == 'application/pdf') {
                return 'pdf'
            }else if(files[0].name.indexOf('docx') !== -1){
                return 'docx'
            }else{
                return 'doc'
            }
        }


		(function init()
        {
            CitiesDAO.query().then(function (cities)
            {
                ctrl.cities = cities;
                return CategoriesDAO.query();
            }).then(function (categories)
            {
                ctrl.categories = categories;
                return SalaryRangesDAO.query();
            }).then(function (salaryRanges)
            {
                ctrl.salaryRanges = salaryRanges;
            });
        })();

    }

    angular.module('recrutoid.common').controller('uploadCvModalController',
            ['$modalInstance', '$modal', '$scope', 'gettext', 'CurrentUser', 'recrutoidPopup', 'UserResumeDAO', 'SalaryRangesDAO', 'CategoriesDAO', 'CitiesDAO', uploadCvModalController]);
})();
