(function ()
{
    'use strict';

    function uploadCvModal($modal)
    {
        function open()
        {
            return $modal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: 'modules/user/upload-cv-modal/uploadCvModal.tpl.html',
                controller: 'uploadCvModalController as ctrl',
                windowClass: 'upload-cv-modal-window'
            }).result;
        }

        return {
            open: open
        };
    }

    angular.module('recrutoid.common').service('uploadCvModal', ['$modal', uploadCvModal]);

})();
