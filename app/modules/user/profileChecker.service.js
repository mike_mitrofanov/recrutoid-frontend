(function ()
{
    'use strict';

    function profileChecker($filter, $timeout, gettext, $rootScope)
    {

		function isSeekerProfileActive(user) {

            return !!(user.resume.title && user.resume.title.length && user.categories.length && user.city &&
                user.firstName && user.firstName.length &&
                user.lastName && user.lastName.length && user.resume.education.length);
        }

		function isEmployerProfileActive(user) {
            return user.phone &&
                user.firstName && user.firstName.length &&
                user.lastName && user.lastName.length;
        }


        function isActive(user)
        {
			if (user.role === 'employer') {
				return Boolean(isEmployerProfileActive(user));
			}
			else if (user.role === 'normal') {

				return Boolean(isSeekerProfileActive(user));
			}

			return true;
        }

//		return CommonDAO.isProfileActive().then(function (data)
//            {
//                return data.isActive;
//            });


        return {
            isActive: isActive
        };
    }

    angular.module('recrutoid.user').service('profileChecker', ['$filter', '$timeout', 'gettext', '$rootScope', profileChecker]);
})();


