(function ()
{
    'use strict';

    function CropImageController($modalInstance, $scope, $timeout, CurrentUser, file)
    {
        var ctrl = this;

        this.image = '';
        this.resultImage = '';

        function dataURLtoBlob(dataurl) {
            var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
                    bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
            while(n--){
                u8arr[n] = bstr.charCodeAt(n);
            }
            return new Blob([u8arr], {type:mime});
        }

        this.save = function (fullSize)
        {
            var newFile = fullSize ? dataURLtoBlob(ctrl.image) : dataURLtoBlob(ctrl.resultImage);
            $modalInstance.close(newFile);
        };

        (function init()
        {
            var reader = new FileReader();
            reader.onload = function (evt) {
                $scope.$apply(function()
                {
                    $timeout(function ()
                    {
                        ctrl.image = evt.target.result;
                    }, 1000);
                });
            };
            reader.readAsDataURL(file);
            CurrentUser.resolve().then(function (user)
            {
                ctrl.isEmployer = user && user.role && 'employer' === user.role;
            });
        })();
    }

    angular.module('recrutoid.user').controller('CropImageController', ['$modalInstance', '$scope', '$timeout', 'CurrentUser', 'file', CropImageController]);
})();
