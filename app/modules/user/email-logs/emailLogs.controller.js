(function ()
{
    'use strict';

    function EmailLogsCtrl($location, $modal, $q, $scope, LogsDAO, Notification, paginationSupport, recrutoidPopup, BugreportsDAO,
                                        vacancyDetails, gettext)
    {
        var ctrl = this;
        var refresh = angular.noop;

        var defaultFilter = {
            log: null,
            size: 20,
            from: 0
        };
        this.filter = angular.copy(defaultFilter);

	    this.remove = function (bugreports)
        {
            var title = gettext('Warning');
            var text = gettext('message_are_you_sure');
            var confirm = gettext('popup_yes_i_am_button');
            recrutoidPopup({
                title: title,
                text: text,
                html: true,
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: confirm,
                closeOnConfirm: true
            }, function (isConfirm)
            {
                if (isConfirm) {
                    LogsDAO.remove(bugreports.id).then(function ()
                    {
                        refresh();
                        var text = gettext('Bug deleted.');
                        Notification.success(text);
                    });
                }
            });
        };


        this.view = function (user_message)
        {
            var title = gettext('Bug report');
            recrutoidPopup({
                title: title,
                text: user_message,
                html: true,
                type: 'warning',
                showCancelButton: false,
                confirmButtonText: 'Cancel',
                closeOnConfirm: true
            });
        };

		(function init()
        {




                refresh = paginationSupport(ctrl, function (callback)
                {
                    LogsDAO.query(ctrl.filter).then(function (results)
                    {
                        ctrl.results = results.results;
                        ctrl.total = results.total;
                        ctrl.showTotal = true;
                        callback(results.total);
                    });
                });
                refresh();


        })();
    }

    angular.module('recrutoid.vacancy').controller('EmailLogsCtrl',
            ['$location', '$modal', '$q', '$scope', 'LogsDAO', 'Notification', 'paginationSupport', 'recrutoidPopup', 'BugreportsDAO',
             'vacancyDetails', 'gettext', EmailLogsCtrl]);
})();
