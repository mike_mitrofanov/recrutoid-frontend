(function ()
{
    'use strict';

    function SearchUsersController($location, $window, $timeout, $scope, $http, $rootScope,
            CommonDAO, CurrentUser, searchColumnMover, paginationSupport,
            UserResumeDAO, UserDAO, localStorageService, SearchSuggestions, SearchHelper)
    {
        var ctrl = this;
        var refresh;
        var year = 31556952000;

        var filtersHandledBySelect2 = [
            'category',
            'city'
        ];
        var languageFilters = ['l0', 'l1', 'l2'];
        var availableFilters = [
            'resumeTitle',
            'workExperienceGTE',
            'workExperienceLTE',
            'language',
            'languageProficiency',
            'education',
            'days',
            'foreignDegree',
            'expat',
            'companyName'
        ].concat(filtersHandledBySelect2).concat(languageFilters);


        var clearableFilters = [
            'expat',
            'foreignDegree',
            'days',
            'workExperienceLTE',
            'workExperienceGTE',
            'language',
            'languageProficiency',
            'education'
        ];

        var companyExcludeFilters = [
        ].concat(clearableFilters).concat(filtersHandledBySelect2);

        var previousResumeTitle = '';


        this.languageProficiencies = ['Proficiency', 'Basic', 'Intermediate', 'Advanced'];
        this.educationOptions = ['Bachelors', 'Masters', 'PhD'];

        this.selectedLanguages = {};

        ctrl.filter = {
            size: 20,
            from: 0
        };
        languageFilters.map(function (prop)
        {
            ctrl.filter[prop] = [];
        });

        ctrl.select2Models = {};

        ctrl.activeRow = 0;
        ctrl.mobileFilter = ctrl.filter;

        SearchSuggestions.setContext(ctrl, '/api/autocomplete/cv?q=', 'resumeTitle');

        $scope.suggestionsHandler = SearchSuggestions;
        $rootScope.suggestionsHandler = SearchSuggestions;
        $rootScope.companyKeypress = function (event) {
            $window.scrollBy(0, 1000);

            if (event.which === 13) {
                ctrl.setFilter('companyName', ctrl.filter.companyName);
            }
        };

        this.resetRequired = function () {
            return ctrl.filter.resumeTitle !== previousResumeTitle;
        };


        this.details = function (user)
        {
            var url = '/user/' + user.id + '/profile';

            if ($rootScope.media !== 'desktop') {
                $location.path(url).search({});
            } else {
                window.open(url);
            }

        };

        this.encodeString = function (string)
        {
            return encodeURI(string);
        };

        function dealWithFilters()
        {

            var searchParams = $location.search();

            if (ctrl.filter.resumeTitle !== searchParams.resumeTitle) {
                searchParams.resumeTitle = ctrl.filter.resumeTitle || null;
            }
            if (ctrl.filter.category !== searchParams.category) {
                searchParams.category = ctrl.filter.category || null;
            }
            if (ctrl.filter.city !== searchParams.city) {
                searchParams.city = ctrl.filter.city || null;
            }
            if (ctrl.filter.workExperienceGTE !== searchParams.workExperienceGTE) {
                searchParams.workExperienceGTE = ctrl.filter.workExperienceGTE || null;
            }
            if (ctrl.filter.workExperienceLTE !== searchParams.workExperienceLTE) {
                searchParams.workExperienceLTE = ctrl.filter.workExperienceLTE || null;
            }
            if (ctrl.filter.languageProficiency !== searchParams.languageProficiency) {
                searchParams.languageProficiency = ctrl.filter.languageProficiency || null;
            }
            if (ctrl.filter.education !== searchParams.education) {
                searchParams.education = ctrl.filter.education || null;
            }
            if (ctrl.filter.days !== searchParams.days) {
                searchParams.days = ctrl.filter.days || null;
            }
            if (ctrl.filter.foreignDegree !== searchParams.foreignDegree) {
                searchParams.foreignDegree = ctrl.filter.foreignDegree || null;
            }
            if (ctrl.filter.expat !== searchParams.expat) {
                searchParams.expat = ctrl.filter.expat || null;
            }

            if (ctrl.filter.companyName !== searchParams.companyName) {
                searchParams.companyName = ctrl.filter.companyName || null;


            }



            if (ctrl.currentPage !== searchParams.currentPage) {

                if (ctrl.currentPage != 1) {
                    searchParams['currentPage'] = ctrl.currentPage;
                } else {
                    searchParams['currentPage'] = null;
                }
            }


            SearchSuggestions.updateShortened();

            languageFilters.map(function (prop)
            {
                searchParams[prop] = ctrl.filter[prop];
            });



            $location.search(searchParams);



        }

        this.dealWithFilters = dealWithFilters;

        /**
         * [with-preview, no-preview]
         */
        this.listingMode = function ()
        {
            return (localStorageService.get('listingMode') !== null) ? localStorageService.get('listingMode') : 'with-preview';
        };

        this.setListingMode = function (mode)
        {
            localStorageService.set('listingMode', mode);
        };




        this.resetFilters = function (excludeCompany)
        {



            for (var i = 0; i < clearableFilters.length; i++) {

                var item = clearableFilters[i];

                delete ctrl.filter[item];
            }



            if (!excludeCompany) {
                this.setFilter('companyName', null);
            }

            this.resetLanguageFilter(false);


            dealWithFilters();
            refresh();

        };

        $rootScope.resetLanguageFilter = this.resetLanguageFilter = function (reload) {

            ctrl.selectedLanguages = {};
            ctrl.setFilter('language', null);

            if (reload) {
                dealWithFilters();
                refresh();
            }

        };


        this.companyResetFilters = function ()
        {
            ctrl.selectedLanguages = {};

            for (var i = 0; i < companyExcludeFilters.length; i++) {

                var item = companyExcludeFilters[i];

                delete ctrl.filter[item];
            }

            this.setFilter('language', null);
            delete ctrl.select2Models.category;
            delete ctrl.select2Models.city;
        };



        this.applyMobileFilters = function ()
        {
            ctrl.filter = ctrl.mobileFilter;

            $scope.showMobileFilters = false;

            dealWithFilters();
            refresh();
        };

        this.hasSelectedFilters = function ()
        {
            for (var i = 0, len = clearableFilters.length; i < len; i++) {
                var item = clearableFilters[i];
                if (this.isFilterSelected(item)) {
                    return true;
                }
            }

            if (this.isFilterSelected('companyName')) {
                return true;
            }

            return false;
        };

        this.isFilterSelected = function (item)
        {
            if (item === 'language') {
                if (ctrl.filter['l0'] && ctrl.filter['l0'].length || ctrl.filter['l1'] && ctrl.filter['l1'].length || ctrl.filter['l2'] && ctrl.filter['l2'].length) {
                    return true;
                }

                return false;
//            } else if (item === 'companyName') {

            } else if (ctrl.filter[item] !== null && ctrl.filter[item] !== undefined) {
                if (Array.isArray(ctrl.filter[item]) && ctrl.filter[item].length === 0) {
                    return false;
                }

                return true;
            }

            return false;
        };

        this.isFilterSet = function (item, value)
        {
            if (this.isFilterSelected(item)) {
                if (Array.isArray(ctrl.filter[item]) && ctrl.filter[item].indexOf(value) !== -1) {
                    return true;
                } else if (value === ctrl.filter[item]) {
                    return true;
                }
            }

            return false;
        };

        this.setFilter = function (type, value)
        {

            if (-1 === availableFilters.indexOf(type)) {
                console.log('not found  filter ' + type);

                return null;
            }


            if (ctrl.filter.companyName && type !== 'companyName') {
                return null;
            }


            if (type === 'companyName' && value) {
                this.companyResetFilters();
            }


            if (type === 'language' && value === null) {

                languageFilters.map(function (elem) {

                    delete ctrl.selectedLanguages[ctrl.filter[elem]];

                    ctrl.filter[elem] = [];

                });
            }

            ctrl.filter[type] = value;

            if ((type === 'expat' || type === 'foreignDegree') && value === false) {
                delete ctrl.filter[type];
            }

            if (!$scope.showMobileFilters) {
                dealWithFilters();
                refresh();
            }
        };



        function getWorkExperienceRange(number)
        {
            switch (number)
            {
                case 0:
                    return [null, year * 1.1];
                case 1:
                    return [year, year * 3.1];
                case 2:
                    return [year * 3, year * 6.1];
                case 3:
                    return [year * 6, null];
                default:
                    return [null, null];
            }
        }

        this.setWorkExperienceFilter = function (number)
        {
            if (ctrl.filter.companyName) {
                return null;
            }

            var range = getWorkExperienceRange(number);
            ctrl.filter.workExperienceGTE = range[0];
            ctrl.filter.workExperienceLTE = range[1];

            if (!$scope.showMobileFilters) {
                dealWithFilters();
                refresh();
            }
        };

        this.isWorkExperienceFilterSet = function (number)
        {
            var range = getWorkExperienceRange(number);
            return ctrl.filter.workExperienceGTE == range[0] && ctrl.filter.workExperienceLTE == range[1];
        };

        this.toggleLanguage = function (lang)
        {
            // console.log(lang);
            if (ctrl.filter.companyName) {
                return null;
            }

            var proficiency;
            if (ctrl.selectedLanguages.hasOwnProperty(lang)) {
                proficiency = ctrl.selectedLanguages[lang];
                delete ctrl.selectedLanguages[lang];

            } else {
                ctrl.selectedLanguages[lang] = ctrl.languageProficiencies[0];

            }

            ctrl.langProficiencyChanged(lang, proficiency);
        };

        this.langProficiencyChanged = function (lang, proficiency)
        {


            if (ctrl.filter.companyName) {
                return null;
            }

            var selectedLanguage = ctrl.selectedLanguages[lang];

            proficiency = proficiency || selectedLanguage;
            var profNum = ctrl.languageProficiencies.indexOf(proficiency) - 1;
            // console.log('profNum', profNum, proficiency);
            if (profNum === -1) {
                return;
            }
            var prop = 'l' + profNum;
            var otherProps = angular.copy(languageFilters);
            otherProps.splice(profNum, 1);

            if (-1 === ctrl.filter[prop].indexOf(lang)) {
                ctrl.filter[prop].push(lang);

                otherProps.map(function (elem)
                {
                    if (-1 < ctrl.filter[elem].indexOf(lang)) {
                        ctrl.filter[elem].splice(ctrl.filter[elem].indexOf(lang), 1);
                    }
                });
            } else {
                ctrl.filter[prop].splice(ctrl.filter[prop].indexOf(lang), 1);
            }


            if (!$scope.showMobileFilters) {
                dealWithFilters();
                refresh();

            }
        };

        this.setMultipleFilter = function (type, value)
        {
            if (ctrl.filter.companyName && type !== 'companyName') {
                return null;
            }

            // init array
            if (ctrl.filter[type] === undefined) {
                ctrl.filter[type] = [];
            }


            if (value === undefined) {
                ctrl.filter[type] = [];
            } else if (-1 === ctrl.filter[type].indexOf(value)) {
                ctrl.filter[type].push(value);
            } else {
                ctrl.filter[type].splice(ctrl.filter[type].indexOf(value), 1);
            }


            if (!$scope.showMobileFilters) {
                dealWithFilters();
                refresh();
            }
        };

        this.toggleJob = function (jobTitle)
        {
            if (ctrl.selectedJob === jobTitle) {
                delete ctrl.selectedJob;
            } else {
                ctrl.selectedJob = jobTitle;
            }

            ctrl.setFilter('resumeTitle', jobTitle);
        };


//        this.isSelect2Cleared = function (fieldName) {
//
//            if (!ctrl.select2Models[fieldName]) {
//                dealWithFilters();
//                this.search();
//            }
//        };
//
//        this.isInputCleared = function (fieldName) {
//
//        };

        this.search = function ()
        {

            if (ctrl.filter.companyName) {
                delete ctrl.filter.category;
                delete ctrl.filter.city;
                delete ctrl.select2Models.category;
                delete ctrl.select2Models.city;
            }

            ctrl.filter.category = ctrl.select2Models.category && ctrl.select2Models.category.id || null;
            ctrl.filter.city = ctrl.select2Models.city && ctrl.select2Models.city.id || null;

            if (this.resetRequired()) {
                this.resetFilters(true);
            }

            if (!$scope.showMobileFilters) {
                dealWithFilters();
                refresh();
            }
        };

        this.searchWithCompany = function () { //TODO: check and remove
            console.log('searchWithCompany');
            this.search();
        };

        var timer;
        this.toggleTwoColumns = function (turnOn) {
            $timeout.cancel(timer);
            if (turnOn) {
                $scope.twoColumnsMode = true;

                $scope.hideVacancyPreview = true;
                timer = $timeout(function () {
                    $scope.wideSearchResults = true;
                }, 220);
            } else {
                $scope.twoColumnsMode = false;

                $scope.wideSearchResults = false;
                timer = $timeout(function () {
                    $scope.hideVacancyPreview = false;
                }, 220);
            }
        };

        this.setActiveRow = function (index, user) {
            ctrl.activeRow = index;
            ctrl.activeUser = user;
        };

        function goHomeJobSeeker()
        {
            CurrentUser.resolveOrReject().then(function (user)
            {
                if ('normal' === user.role) {
                    $location.path('/');
                }
            });
        }

        function formatWorkExperience(workExperience)
        {
            var wExperience = moment.duration(workExperience), finalExp = "";


            if (wExperience._data.years) {
                finalExp += wExperience._data.years + ' years ';
            }

            if (wExperience._data.months) {
                finalExp += wExperience._data.months + ' months ';
            }

            return finalExp;
        }

        var notInited = true;

        refresh = paginationSupport(this, function (callback)
        {
            var jobTitlesMap = {};
            var jobTitlesMapKey;

            if (ctrl.lastPage == ctrl.currentPage) {
                ctrl.filter.size = 20;
                ctrl.filter.from = 0;
                // ctrl.currentPage = 1;

                if (!ctrl.currentPage || !notInited) {
                    ctrl.currentPage = 1;
                }
            }

            return UserResumeDAO.find(ctrl.filter).then(function (result)
            {

                ctrl.results = result.results;
                callback(result.total);

                angular.forEach(ctrl.results, function (value)
                {
                    value.workExperience = formatWorkExperience(value.workExperience);

                    if (value.resume.workExperience.length) {
                        if (value.resume.workExperience[value.resume.workExperience.length - 1].title) {
                            value.previewSubtitle = value.resume.workExperience[value.resume.workExperience.length - 1].title;
                        }
                        if (value.resume.workExperience[value.resume.workExperience.length - 1].company) {
                            value.previewSubtitle += value.previewSubtitle ? ' - ' : '';
                            value.previewSubtitle = value.resume.workExperience[value.resume.workExperience.length - 1].company;
                        }
                    }

                    value.resume.workExperience.map(function (elem)
                    {
                        if (elem.title) {
                            jobTitlesMapKey = elem.title.toLowerCase();
                            jobTitlesMap[jobTitlesMapKey] = (jobTitlesMap[jobTitlesMapKey] || 0) + 1;
                        }
                    });
                });

                angular.forEach(ctrl.results, function (value)
                {
                    jobTitlesMapKey = value.resume.title.toLowerCase();
                    if (!isNaN(jobTitlesMap[jobTitlesMapKey])) {
                        jobTitlesMap[jobTitlesMapKey]++;
                    }
                });

                var jobTitles = [];
                angular.forEach(jobTitlesMap, function (count, title)
                {
                    jobTitles.push({
                        count: count,
                        title: title
                    });
                });
                jobTitles.sort(function (a, b)
                {
                    return b.count - a.count;
                });
                ctrl.jobTitlesFilterData = jobTitles.slice(0, 10);

//                ctrl.currentPage = 1;
                ctrl.setActiveRow(0, ctrl.results[0]);
                dealWithFilters();

                $window.scrollTo(0, 0);




                if (notInited) {
                    SearchSuggestions.refocus();
                    notInited = false;
                }
            }).then(function ()
            {
                markNewAndVisited();
                ctrl.lastPage = ctrl.currentPage;
            });
        });


        function markNewAndVisited() {

            CurrentUser.resolve().then(function (user) {
                setTimeout(function () {
                    if($location.path() == '/users')
                        searchColumnMover.init();
                }, 500);


                var idsToCheckIfNew = [];
                var usersCopyForIsNewMarking = {};

                angular.forEach(ctrl.results, function (value)
                {
                    idsToCheckIfNew.push(value.id);
                    usersCopyForIsNewMarking[value.id] = value;
                });


                if (!user.id || !idsToCheckIfNew.length) {
                    return;
                }

                UserDAO.searchVisited(user);

                if (idsToCheckIfNew.length) {
                    UserResumeDAO.isNew(idsToCheckIfNew).then(function (result)
                    {
                        angular.forEach(result, function (value, key)
                        {
                            if ('$' !== key[0] && usersCopyForIsNewMarking[key]) {
                                usersCopyForIsNewMarking[key].applicationNew = value;
                            }
                        });
                    });

                    UserDAO.isVisitedList(idsToCheckIfNew).then(function (result)
                    {
                        angular.forEach(result, function (value, key)
                        {
                            if ('$' !== key[0] && usersCopyForIsNewMarking[key]) {
                                usersCopyForIsNewMarking[key].applicationVisited = value;
                            }
                        });
                    });
                }


                if (ctrl.allowScroll) {
                    $scope.$root.$emit('scrollTo', 'searchUsers');
                    delete ctrl.allowScroll;
                }
            });
        }

        function dealWithQueryString()
        {
            var availableLanguages = ctrl.languages.map(function (lang)
            {
                return lang.id;
            });
            angular.forEach($location.search(), function (value, name)
            {
                var setOfSelect2DataToSearch;
                if (-1 < availableFilters.indexOf(name)) {
                    if (-1 < name.indexOf('workExperience')) {
                        value = parseInt(value, 10);
                    }

                    if (-1 < languageFilters.indexOf(name)) {
                        var prof = ctrl.languageProficiencies[parseInt(name[1], 10)];
                        value = Array.isArray(value) ? value : [value];
                        value.map(function (lang)
                        {
                            if (-1 === availableLanguages.indexOf(lang)) {
                                return;
                            }
                            ctrl.selectedLanguages[lang] = prof;
                        });
                    }

                    if (-1 < name.indexOf('education')) {
                        value = Array.isArray(value) ? value : [value];
                    }

                    if (-1 < filtersHandledBySelect2.indexOf(name)) {
                        setOfSelect2DataToSearch = 'category' === name ? ctrl.categories : ctrl.cities;
                        for (var i = 0; i < setOfSelect2DataToSearch.length; i++) {
                            if (value === setOfSelect2DataToSearch[i].id) {
                                ctrl.select2Models[name] = setOfSelect2DataToSearch[i];
                            }
                        }
                    }

                    ctrl.filter[name] = value;
                }
            });
        }



        CurrentUser.onreload(function (user)
        {

            if ($rootScope.lastReloadId !== user.id) {
                $rootScope.lastReloadId = user.id;
                refresh();
            }

        });


        SearchHelper.init(ctrl, refresh, dealWithQueryString, availableFilters, filtersHandledBySelect2);
        $scope.searchHelper = SearchHelper;

        (function init()
        {
            goHomeJobSeeker();

            delete ctrl.vacancyTitlte;

            CommonDAO.getSearchUsersData().then(function (data)
            {

                ctrl.categories = data.categories;
                ctrl.languages = data.languages;
                ctrl.cities = data.cities;
            })
                    .then(dealWithQueryString)
                    .then(function () {
                        if ($location.search().currentPage) {
                            ctrl.currentPage = $location.search().currentPage;
                        }
                    })
                    .then(refresh)
                    ;
        })();

        var destroyJobSeekerHunter = $scope.$root.$on('event:auth-loggedOut', goHomeJobSeeker);

        $scope.$on('$destroy', function ()
        {
            destroyJobSeekerHunter();
            searchColumnMover.destroy();
        });
    }

    angular.module('recrutoid.user')
            .controller('SearchUsersController',
                    [
                        '$location',
                        '$window',
                        '$timeout',
                        '$scope',
                        '$http',
                        '$rootScope',
                        'CommonDAO',
                        'CurrentUser',
                        'searchColumnMover',
                        'paginationSupport',
                        'UserResumeDAO',
                        'UserDAO',
                        'localStorageService',
                        'SearchSuggestions',
                        'SearchHelper',
                        SearchUsersController
                    ]);
})();
