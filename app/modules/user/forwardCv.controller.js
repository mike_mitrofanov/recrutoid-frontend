(function ()
{
    'use strict';

    function ForwardCv($rootScope, recrutoidPopup, gettext, emailValidator, UserResumeDAO)
    {
        var ctrl = this;
        $rootScope.siteLoaded = true;
        ctrl.errors = {};

        this.send = function ()
        {
            if(!ctrl.email || !ctrl.email.length) {
                ctrl.errors.email = gettext('Please_fill_out_this_field');
                return;
            }
            if (!emailValidator.isValid(ctrl.email)) {
                ctrl.errors.email = gettext('Email_is_invalid');
                return;
            }

            ctrl.forwarding = true;
            UserResumeDAO.forwardCv(ctrl.email).then(function ()
            {
                recrutoidPopup(gettext('Success'), gettext('Message_Your_CV_Has_Been_Sent_To_Provided_Email'), 'success');
            }).finally(function ()
            {
                ctrl.forwarding = false;
            });
        };
    }

    angular.module('recrutoid.user').controller('ForwardCv',
        ['$rootScope', 'recrutoidPopup', 'gettext', 'emailValidator', 'UserResumeDAO', ForwardCv]);
})();
