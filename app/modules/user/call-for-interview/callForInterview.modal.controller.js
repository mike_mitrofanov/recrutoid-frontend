(function ()
{
    'use strict';

    function CallForInterview($modalInstance, data, UserResumeDAO)
    {
        var ctrl = this;

        this.call = function (id)
        {
            UserResumeDAO.callForInterview(data.cvId, id).then($modalInstance.close).catch($modalInstance.dismiss);
        };

        (function init()
        {
            ctrl.vacancies = data.vacancies;
            ctrl.user = data.user;

			ctrl.userOffersIds = ctrl.user.offers.map(function (offer) {
				return offer.vacancyId;
			});

            angular.forEach(ctrl.vacancies, function (elem)
            {
                elem.expDate = moment(elem.expDate).fromNow();
            });

            console.log('init');
        })();
    }

    angular.module('recrutoid.user').controller('CallForInterview',
            ['$modalInstance', 'data', 'UserResumeDAO', CallForInterview]);
})();
