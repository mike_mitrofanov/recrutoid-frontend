(function ()
{
    'use strict';

    function UserSettingsCtrl($modal, $q, $scope, $rootScope, $routeParams, $timeout, $location, CurrentUser, UserDAO, UserResumeDAO, UserOffersDAO, UserSettingsChangePage, VacanciesDAO,
                              recrutoidPopup, gettext, uploadAvatar)
    {
        var ctrl = this;
        this.user = {};
        this.site = $routeParams.site.split('?')[0];
        $rootScope.currentSite = $routeParams.site.split('?')[0];

        var errorDisplay = gettext('Error');
        var successDisplay = gettext('Success');

        var instantLoadedPages = [
            // 'myVacanciesApplications'
        ];

        var hideDashboardButtonPages = [
            'myVacanciesApplications'
        ];

        this.filter = {
            from: 0,
            size: 5
        };
		

        function canProceed()
        {
            if (UserSettingsChangePage.isCallback()) {
                return UserSettingsChangePage.callback();
            }
            var defer = $q.defer();
            defer.resolve();
            return defer.promise;
        }

        var notificationsIntervalLock = false;
        this.notificationsIntervalChanged = function ()
        {
            function update()
            {
                UserDAO.applicationsNotifications(ctrl.user.applicationsNotificationsInterval).then(function ()
                {
                    recrutoidPopup(successDisplay, gettext('Message_Your_Change_Has_Been_Applied'), 'success');
                }).catch(function ()
                {
                    recrutoidPopup(errorDisplay, gettext('Message_Something_Went_Wrong_And_Your_Change_Hasnt_Been_Applied'), 'error');
                });
                notificationsIntervalLock = false;
            }

            if (!notificationsIntervalLock) {
                notificationsIntervalLock = true;
                $timeout(update, 500);
            }
        };

        this.removeVacancy = function remove()
        {
            var text = gettext('message_are_you_sure');
            recrutoidPopup({
                title: warning,
                text: text,
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: confirm,
                closeOnConfirm: true
            }, function (isConfirm)
            {
                if (isConfirm) {
                    VacanciesDAO.remove(ctrl.vacancyID).then(function ()
                    {
                        refresh();
                    }).catch(function (error)
                    {
                        recrutoidPopup(errorDisplay, error, 'error');

                    });
                }
            });
        };

        this.forwardCv = function ()
        {
            $modal.open({
                size: 'sm',
                controller: 'ForwardCvController as forwardCv',
                templateUrl: 'modules/user/forward-cv-modal/forwardCv.tpl.html'
            });
        };

        this.edit = function ()
        {
            return $modal.open({
                size: 'lg',
                controller: 'PublishNewVacancyController as ctrl',
                templateUrl: 'modules/user/publish-new-vacancy/publishNewVacancy.modal.tpl.html',
                windowClass: 'publish-new-vacancy-modal',
                backdrop: 'static',
                resolve: {
                    vacancyData: function ()
                    {
                        return ctrl.vacancyDetails;
                    }
                }
            }).result.then(function ()
            {
                var text = gettext('Message_Changes_Successfully_Saved');
                recrutoidPopup('Success', text, 'success');
                refresh();
            });
        };

        this.isCvEmpty = function() {
            var user = ctrl.user;
            return !user || !user.resume || !user.resume.title;
        };

        this.refreshNewApplicationsCount = function ()
        {
            return VacanciesDAO.getNewApplicationsCount().then(function (applicationsCount)
            {
                ctrl.user.applicationsCount = applicationsCount.count;
                $scope.$emit('event:applications-seen', applicationsCount);
            });
        };

        function refreshNewResumeViewsCount($e, resumeViewsCount)
        {
            ctrl.newResumeViews = resumeViewsCount.count;
        }

        function refreshNewOffersViewsCount($e, offersViewsCount) {
            ctrl.user.newOffers = offersViewsCount.count;
        }

        function refreshAccountDeleted($e, resumeViewsCount)
        {
            ctrl.newResumeViews = resumeViewsCount.count;
        }

        var refresh = function ()
        {
            UserDAO.getMe().then(function (data)
            {
                ctrl.user = data;
                ctrl.user.newOffers = 0;
                ctrl.user.offers.map(function (offer)
                {
                    if (!offer.seen) {
                        ctrl.user.newOffers++;
                    }
                });
                $scope.$emit('event:interview-seen', ctrl.user.newOffers);

                ctrl.newResumeViews = 0;
                angular.forEach(data.resumeViews, function (view)
                {
                    if(!view.seen) {
                        ctrl.newResumeViews++;
                    }
                });

                if ('employer' === data.role) {
                    ctrl.refreshNewApplicationsCount();
                }

                if (ctrl.user.thumbnail === undefined) {
					ctrl.user.thumbnail = '/modules/user/avatar-company.png';
				}

                return UserDAO.getMyProfile();
            }).then(function (profile){
                ctrl.profile = profile;

				if (ctrl.profile.thumbnail === undefined) {
					ctrl.profile.thumbnail = '/modules/user/avatar-company.png';
				}
            });
        };

        this.uploadAvatar = function (files, event, isJobSeeker)
        {
            if ($rootScope.media == 'media' && !isJobSeeker) {
                return;
            }

            if (!files || !files.length) {
                return;
            }

            ctrl.uploading = true;
            uploadAvatar.upload(files).then(function (url)
            {
                $scope.$emit('event:avatar-changed', url);
                ctrl.profile.thumbnail = url;
            }).finally(function ()
            {
                ctrl.uploading = false;
            });
        };

        this.setActiveSite = function (site, vacancyId, applicants)
        {
            if (site == $rootScope.currentSite) {
                return;
            }

            if ($rootScope.siteBlocked) {
                return;
            }

            return canProceed().then(function ()
            {
                ctrl.vacancyID = vacancyId;
                if (vacancyId) {
                    VacanciesDAO.get(vacancyId).then(function (data)
                    {
                        ctrl.vacancyDetails = data;
                    });
                }

                $scope.showDashboard = false;
                return updatePageInfo(site, applicants, vacancyId);
            });
        };

        this.getCvPdf = function (siteUrl) {
            window.open(siteUrl+'/api/users/'+ctrl.profile.id+'/resume/pdf');
        };

        function updatePageInfo(site, applicants, vacancyId){
            $rootScope.siteLoaded = false;
            ctrl.showLoader = false;
            ctrl.menuSite = site;

            return $timeout(function(){
                if(applicants && vacancyId){
                    $location.search({'appliers': vacancyId});
                }

                if (vacancyId === undefined) {
                    $location.search({});
                }

                ctrl.site = ctrl.menuSite;
                $rootScope.currentSite = site;
                ctrl.showLoader = true;
                handleInstantLoadAndDashboardButton(site);
                return UserSettingsChangePage.clearCallback();
            },200);
        }

        var destroyListener = {
            newResumeViewsCount: $scope.$root.$on('event:resume-views-seen', refreshNewResumeViewsCount),
            InterviewSeen: $scope.$root.$on('event:interview-seen', refreshNewOffersViewsCount),
            reloadAvatar: $scope.$on('event:reloadAvatar', function ($e, url)
            {
                ctrl.user.thumbnail = url;
            }),
            dataChanged: $scope.$on('event:userDataHasChanged', function ()
            {
                refresh();
            }),
            currentSite: $scope.$watch('currentSite', function(newValue, oldValue) {
                $location.update_path('/user/settings/'+newValue);
                ctrl.menuSite = newValue;
                handleInstantLoadAndDashboardButton(newValue);
            }),
            uploadedCvChange: $scope.$root.$on('event:cv-uploaded', handleUploadedCv),
            pathChange: $rootScope.$watch(function() {
                    return $location.path();
                },
                function(a){
                    var basePath = '/user/settings/';
                    if(a.indexOf(basePath) !== -1) {
                        if(a.split(basePath).length > 1) {
                            ctrl.setActiveSite(a.split(basePath)[1]);
                        }
                    }
                })
        };

        function handleInstantLoadAndDashboardButton(site){
            if(instantLoadedPages.indexOf(site) !== -1){
                $rootScope.siteLoaded = true;
            }

            $scope.hideDashboardButton = hideDashboardButtonPages.indexOf(site) !== -1;
        }


        function handleUploadedCv(event, added) {
            if(added) {
                ctrl.user.uploadedResume.data = {url:'fake'};
            }
            else {
                ctrl.user.resume = {};
                delete ctrl.user.uploadedResume.data;
            }
        }

        $scope.$on('$destroy', function ()
        {
            destroyListener.newResumeViewsCount();
            destroyListener.InterviewSeen();
            destroyListener.reloadAvatar();
            destroyListener.dataChanged();
            destroyListener.currentSite();
            destroyListener.uploadedCvChange();
            destroyListener.pathChange();
        });



        (function init()
        {
            CurrentUser.resolveOrReject().then(function (user)
            {
                if('normal' === user.role) {
                    UserResumeDAO.getNewResumeViewsCount().then(function (resumeViewsCount)
                    {
                        refreshNewResumeViewsCount(null, resumeViewsCount);

                        return UserOffersDAO.getNewOffersViewsCount();
                    }).then(function (offersViewsCount){
                        refreshNewOffersViewsCount(null, offersViewsCount);

                        return UserResumeDAO.getNewResumeViewsCount(ctrl.user.id);
                    }).then(function(resumeViewsCount){
                        refreshNewResumeViewsCount(null, resumeViewsCount);
                    });
                }
            });

            refresh();
        })();
    }

    angular.module('recrutoid.user').controller('UserSettingsCtrl',
            ['$modal',
             '$q',
             '$scope',
             '$rootScope',
             '$routeParams',
             '$timeout',
             '$location',
             'CurrentUser',
             'UserDAO',
             'UserResumeDAO',
             'UserOffersDAO',
             'UserSettingsChangePage',
             'VacanciesDAO',
             'recrutoidPopup',
             'gettext',
             'uploadAvatar',
             UserSettingsCtrl]);
})();