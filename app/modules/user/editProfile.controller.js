(function ()
{
    'use strict';

    function EditUserProfile($q, $scope, $rootScope, $location, $routeParams, CitiesDAO, CurrentUser, lookForChanges, recrutoidModals,
                             UserDAO, UserSettingsChangePage, uploadAvatar, recrutoidPopup, unsavedChangesPopup, gettext, CategoriesDAO,
							profileChecker, emailValidator)
    {
        var ctrl = this;

        this.password = {};
        this.passwordTmp = {};
        this.errors = {};
        var text;
        var confirm = gettext('Yes');
        var warningDisplay = gettext('Warning!');
        var successDisplay = gettext('Success!');
        var propertiesToIgnoreWhenLookingForChanges = ['thumbnail'];

        var initialProfileData;
        var shouldLookForChanges = true;

        this.delete = function ()
        {
            text = gettext('Message_Do_You_Really_Want_To_Delete_This_Account');
            recrutoidPopup({
                title: warningDisplay,
                text: text,
                html: true,
                type: 'warning',
                confirmButtonText: confirm,
                showCancelButton: true,
                closeOnConfirm: true
            }, function (isConfirm)
            {
                if (isConfirm) {

                    var loggedInUserDuringDeletion;
                    shouldLookForChanges = false;
                    CurrentUser.resolveOrReject().then(function (user)
                    {
                        var promise;
                        if ('admin' !== user.role) {
                            promise = recrutoidModals.login.open({
                                initialEmail: user.email,
                                disableReload: true,
                                disableFooterOptions: true,
                                disableSocialOptions: 'employer' === user.role,
                                customErrorMessage: {
                                    error: 401,
                                    message: gettext('Message_Incorrect_Credentials_Account_Has_Not_Been_Deleted'),
                                    close: true
                                }
                            }).then(function (loggedInUser)
                            {
                                loggedInUserDuringDeletion = loggedInUser;
                                return UserDAO.remove(user.id);
                            });
                        } else {
                            promise = UserDAO.remove($routeParams.id);
                        }

                        promise.then(function ()
                        {
                            $rootScope.daysLeftToDeletion = 7;
                        }).catch(function (error)
                        {
                            if (403 === error.status) {
                                text = gettext('Message_Account_That_You_Tried_To_Remove_Is_Different_Then_Account_That_You_Has_Been_Logged_In');
                                recrutoidPopup({
                                    title: warningDisplay,
                                    text: text,
                                    html: true,
                                    type: 'warning'
                                });
                            }
                        });
                    });
                }
            });
        };

        this.revertDeletion = function() {
            CurrentUser.resolveOrReject().then(function (user)
            {
                UserDAO.unremove(user.id || $routeParams.id).then(function() {
                    $rootScope.daysLeftToDeletion = false;
                });
            });
        };

        this.uploadAvatar = function (files)
        {
            if (!files || !files.length) {
                return;
            }

            ctrl.uploading = true;
            shouldLookForChanges = true;
            uploadAvatar.upload(files).then(function (url)
            {
                ctrl.profile.thumbnail = url;
            }).finally(function ()
            {
                ctrl.uploading = false;
            });
        };

        this.save = function (remotelyInvokedSave)
        {
            if (!!ctrl.profile.newEmail) {
                ctrl.profile.email = ctrl.profile.newEmail;
            }

            if(!validateForm()){
                return;
            }

            var payload = {};
            payload.profile = angular.copy(ctrl.profile);
            if (payload.profile.city) {
                payload.profile.city = payload.profile.city.id;
            }

            shouldLookForChanges = false;
            UserDAO.saveMyProfile(payload).then(function (res)
            {
                shouldLookForChanges = true;
                $scope.$emit('event:userDataHasChanged', payload.profile);
                if (initialProfileData.email !== ctrl.profile.email) {
                    ctrl.profile.newEmail = ctrl.profile.email;
                    text =
                      gettext('Message_Profile_Successfully_Updated_The_Confirmation_Link_Was_Sent_To_Your_New_Email');
                }
                else {
                    text = gettext('Message_Profile_Successfully_Updated');
                }
                initialProfileData = angular.copy(ctrl.profile);
                recrutoidPopup(successDisplay, text, 'success');
                if(remotelyInvokedSave) {
                    remotelyInvokedSave.resolve();   // invoking save method from popup about unsaved changes
                }

                $scope.$emit('event:cv-active', ctrl.profile.hiddenOnSearch);
                $scope.$emit('event:unconfirmed-email', !!ctrl.profile.newEmail);
                CurrentUser.reload();
                $scope.editPassword = false;
            }).catch(function (error)
            {
                if (460 === error.status) {
                    text = gettext('Message_You_Cant_Use_This_Email_Because_It_Is_On_Blacklist');
                    ctrl.errors.email = 'Message_Please_Enter_Valid_Email';
                } else if (409 === error.status) {
                    text = gettext('Message_This_Email_Is_Already_Taken');
                    ctrl.errors.email = 'Message_This_Email_Is_Already_Taken';
                } else {
                    text = gettext('Message_Something_Went_Wrong');
                }
                recrutoidPopup(warningDisplay, text, 'warning');
                if(remotelyInvokedSave) {
                    remotelyInvokedSave.reject();
                }
            });
        };

        this.savePassword = function(remotelyInvokedSave) {
            if(!validatePassword()){
                return;
            }


            var payload = {};
            payload.profile = {id: ctrl.profile.id};
            if (ctrl.password.current && ctrl.password.new && ctrl.password.new === ctrl.password.newConfirm) {
                payload.password = {
                    current: ctrl.password.current,
                    new: ctrl.password.new
                };
            } else {
                delete ctrl.password.current;
                delete ctrl.password.new;
                delete ctrl.password.newConfirm;
                text = gettext('Message_You_Didnt_Pass_Current_Password_Or_Fields');
                recrutoidPopup({
                    title: warningDisplay,
                    text: text,
                    html: true,
                    type: 'warning',
                    confirmButtonText: confirm,
                    closeOnConfirm: true
                });
                if(remotelyInvokedSave) {
                    remotelyInvokedSave.reject();
                }
                return;
            }

            UserDAO.saveMyProfile(payload).then(function ()
            {
                shouldLookForChanges = true;
                $scope.$emit('event:userDataHasChanged', payload.profile);

                text = gettext('Message_Profile_Successfully_Updated');

                initialProfileData = angular.copy(ctrl.profile);
                recrutoidPopup(successDisplay, text, 'success');
                if(remotelyInvokedSave) {
                    remotelyInvokedSave.resolve();   // invoking save method from popup about unsaved changes
                }

                CurrentUser.reload();
                $scope.editPassword = false;
            }).catch(function (error)
            {
                if (412 === error.status) {
                    text = gettext('Message_Wrong_Current_Password_Try_Again');
                    ctrl.errors.currentPassword = 'Message_Wrong_Current_Password';
                } else {
                    text = gettext('Message_Something_Went_Wrong');
                }
                recrutoidPopup(warningDisplay, text, 'warning');
                if(remotelyInvokedSave) {
                    remotelyInvokedSave.reject();
                }
            });
        };

        function validateForm() {
            var isValid = true;
            var text = false;
            var requiredMessage = "Please_fill_out_this_field";
            ctrl.errors = {};

            if (ctrl.profile.role !== 'normal' && !ctrl.profile.firstName) {
                console.log('1');
                ctrl.errors.firstName = requiredMessage;
                isValid = false;
            }

            if (ctrl.profile.role !== 'normal' && !ctrl.profile.lastName) {
                console.log('2');
                ctrl.errors.lastName = requiredMessage;
                isValid = false;
            }

            if (ctrl.profile.role !== 'normal' && !ctrl.profile.phone) {
                console.log('3');
                ctrl.errors.phone = requiredMessage;
                isValid = false;
            }

            if (!ctrl.profile.email) {
                console.log('4');
                ctrl.errors.email = requiredMessage;
                isValid = false;
            }else if(!emailValidator.isValid(ctrl.profile.email)){
                ctrl.errors.email = 'Message_Please_Enter_Valid_Email';
                isValid = false;
            }

            if(!isValid){
                recrutoidPopup(warningDisplay, gettext('Message_Some_fields_require_your_attention'), 'warning');
            }

            return isValid;
        }

        function validatePassword() {
            var isValid = true;
            var text = false;
            var requiredMessage = "Please_fill_out_this_field";
            ctrl.errors = {};

            if (!ctrl.password.current) {
                ctrl.errors.currentPassword = requiredMessage;
                isValid = false;
            }

            if (!ctrl.password.new) {
                ctrl.errors.newPassword = requiredMessage;
                isValid = false;
            }

            if (!ctrl.password.newConfirm) {
                ctrl.errors.newPasswordConfirm = requiredMessage;
                isValid = false;
            }

            if(ctrl.password.new && ctrl.password.newConfirm){
                if(ctrl.password.new !== ctrl.password.newConfirm) {
                    ctrl.errors.newPassword = 'Message_Passwords_Are_Not_Equal';
                    ctrl.errors.newPasswordConfirm = 'Message_Passwords_Are_Not_Equal';
                    isValid = false;
                }
            }

            if(!isValid){
                recrutoidPopup(warningDisplay, gettext('Message_Some_fields_require_your_attention'), 'warning');
            }

            return isValid;
        }

        function registerWatchOnCategories()
        {
            var allCategoriesSelected = false;
            $scope.$watch(function ()
            {
                return ctrl.profile.subscription.categories;
            }, function (newV)
            {
                if (newV.length && null === newV[newV.length - 1].id) {
                    allCategoriesSelected = true;
                    newV.splice(0, newV.length - 1);
                }
                else if (allCategoriesSelected) {
                    newV.splice(0, 1);
                    allCategoriesSelected = false;
                }
            });
        }

        (function init()
        {
            CitiesDAO.query().then(function (cities)
            {
                ctrl.cities = cities;
                ctrl.citiesSubscription = angular.copy(cities);
                ctrl.citiesSubscription.unshift({
                    name: {
                        ru: 'Все города',
                        en: 'All cities'
                    },
                    id: null
                });
                return CategoriesDAO.query();
            }).then(function (categories)
            {
                ctrl.categories = categories;
                ctrl.categories.unshift({id: null, name: {en: 'All categories', ru: 'Все категории'}});
                return UserDAO.getMyProfile();
            }).then(function (profile)
            {
                ctrl.profile = profile;
                ctrl.showPasswordBox = !profile.socialOnly;
                if (ctrl.profile.subscription) {
                    if (null === ctrl.profile.subscription.city) {
                        ctrl.profile.subscription.city = ctrl.citiesSubscription[0];
                    }
                    if (-1 < ctrl.profile.subscription.categories.indexOf(null)) {
                        ctrl.profile.subscription.categories[0] = ctrl.categories[0];
                    }
                    registerWatchOnCategories();
                }

                if ('employer' === ctrl.profile.role) {
                    ctrl.isEmployer = true;
                }

				if (ctrl.profile.thumbnail === undefined) {
					ctrl.profile.thumbnail = '/modules/user/avatar-company.png';
				}

                ctrl.loaded = true;
                delete ctrl.profile.$promise;
                initialProfileData = angular.copy(ctrl.profile);
                $rootScope.siteLoaded = true;
            }).catch(function ()
            {
                $location.path('/');
            });
        })();

        var unregisterChangeLocationListener = $rootScope.$on('$locationChangeStart', function ($e, newUrl, oldUrl)
        {
            if (shouldLookForChanges && lookForChanges(ctrl.profile, initialProfileData, propertiesToIgnoreWhenLookingForChanges)) {
                $e.preventDefault();

                unsavedChangesPopup.open(function (saving)
                {
                    if (saving) {
                        var remotelyInvokedSave = $q.defer();
                        ctrl.save(remotelyInvokedSave);
                        remotelyInvokedSave.promise.then(function ()
                        {
                            unregisterChangeLocationListener();
                            $location.href = newUrl;
                        });
                    }
                    else {
                        unregisterChangeLocationListener();
                        $location.href = newUrl;
                    }
                });
            }
            else {
                unregisterChangeLocationListener();
            }
        });

        var reloadAvatar = $rootScope.$on('event:avatar-changed', function ($e, url)
        {
            ctrl.profile.thumbnail = url;
        });



        $scope.$on('$destroy', function ()
        {
            unregisterChangeLocationListener();
            reloadAvatar();
        });

        function employerEmptyProfilePopUp(){
            var text;
            text = gettext('Message_You_Cant_Use_Page_Until_You_Fill_Up_Your_Company_Profile');
            recrutoidPopup({
                type: 'warning',
                title: gettext('Warning!'),
                text: text
            }, function (confirmed)
            {
                if(confirmed) {
                    $location.path('/user/settings/editProfile');
                }
            });
        }

        UserSettingsChangePage.callback = function ()
        {
            var defer = $q.defer();
			CurrentUser.resolve().then(function (currentUser)
            {
                if (currentUser.id && currentUser.role === 'employer' && !profileChecker.isActive(currentUser)) {
                    employerEmptyProfilePopUp();
                    defer.reject();
                }else{
                    if (lookForChanges(ctrl.profile, initialProfileData, propertiesToIgnoreWhenLookingForChanges)) {
                        unsavedChangesPopup.open(function (saving)
                        {
                            if (saving) {
                                var remotelyInvokedSave = $q.defer();
                                ctrl.save(remotelyInvokedSave);
                                remotelyInvokedSave.promise.then(function ()
                                {
                                    unregisterChangeLocationListener();
                                    defer.resolve();
                                }).catch(function ()
                                {
                                    defer.reject();
                                });
                            }
                            else {
                                unregisterChangeLocationListener();
                                defer.resolve();
                            }
                        });
                    }
                    else {
                        unregisterChangeLocationListener();
                        defer.resolve();
                    }
                }
			});

            return defer.promise;
        };
    }

    angular.module('recrutoid.user').controller('EditUserProfile',
      ['$q', '$scope', '$rootScope', '$location', '$routeParams', 'CitiesDAO', 'CurrentUser', 'lookForChanges', 'recrutoidModals',
          'UserDAO', 'UserSettingsChangePage', 'uploadAvatar', 'recrutoidPopup', 'unsavedChangesPopup', 'gettext', 'CategoriesDAO',
		  'profileChecker', 'emailValidator', EditUserProfile]);
})();
