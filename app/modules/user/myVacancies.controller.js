(function ()
{
    'use strict';

    function MyVacanciesCtrl($location, $modal, $timeout, $scope, $rootScope, CurrentUser,
                             siteSettings, recrutoidPopup, VacanciesDAO, paginationSupport, gettext,
                             $filter, $window)
    {
        var ctrl = this;
        var text;
        var refresh;

        ctrl.filter = {
            myAll: true,
            size: -1,   // displaying all in one page.
            publishMode: 'website',
            from: 0,
            newCount: true
        };

        var warning = gettext('Warning');
        var errorDisplay = gettext('Error');
        var successDisplay = gettext('Success');
        var confirm = gettext('popup_yes_i_am_button');


        $scope.showme='published';
        $scope.selectedTab='published';
        $scope.switchingTab = true;

        $scope.$watch('vacanciesList.vacancies', function(items){
            var selectedItems = 0;
            angular.forEach(items, function(item){
              selectedItems += item.checked ? 1 : 0;
            });
            $scope.selectedItems = selectedItems;
        }, true);

        this.alertMessage = function() {
            text = gettext('Message_You_Cant_See_Not_Approved_Vacancy');
            recrutoidPopup(warning, text, 'warning');
        };

        this.remove = function remove(id)
        {
            text = gettext('message_are_you_sure');
            recrutoidPopup({
                title: warning,
                text: text,
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: confirm,
                closeOnConfirm: true
            }, function (isConfirm)
            {
                if (isConfirm) {
                    VacanciesDAO.remove(id).then(function ()
                    {
                        refresh();
                    }).catch(function (error)
                    {
                        recrutoidPopup(errorDisplay, error, 'error');

                    });
                }
            });
        };

        this.removeSelected = function removeSelected()
        {
            var toRemove = [];

            angular.forEach(ctrl.vacancies, function (elem)
            {
                if(elem.checked) {
                    toRemove.push(elem.id);
                }
            });

            if (0 === toRemove.length) {
                 text = gettext('Message_You_Havent_Checked_Anything');
                recrutoidPopup(errorDisplay, text, 'error');
            } else {
                text = gettext('Message_Are_You_Sure_You_Want_To_Remove_All_Selected_Vacancies');
                recrutoidPopup({
                    title: warning,
                    text: text,
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: confirm,
                    closeOnConfirm: true
                }, function (isConfirm)
                {
                    if (isConfirm) {
                        VacanciesDAO.removeArray(toRemove).then(function ()
                        {
                            text = gettext('You have deleted ' + toRemove.length + ' vacancies.');
                            recrutoidPopup(successDisplay, text, 'success');
                            refresh();
                        }).catch(function (error)
                        {
                            recrutoidPopup(errorDisplay, error, 'error');
                        });
                    }
                });
            }

        };

        this.selectAll = function ()
        {
            ctrl.selectValue = !ctrl.selectValue;
            angular.forEach(ctrl.vacancies, function (elem)
            {
                elem.checked = ctrl.selectValue;    
            });
        };

        function publishVacancyModal(vacancyData, callback)
        {
            return $modal.open({
                size: 'lg',
                controller: 'PublishNewVacancyController as ctrl',
                templateUrl: 'modules/user/publish-new-vacancy/publishNewVacancy.modal.tpl.html',
                windowClass: 'publish-new-vacancy-modal',
                backdrop: 'static',
                resolve: {
                    vacancyData: function()
                    {
                        return vacancyData;
                    }
                }
            }).result.then(callback);
        }

        this.edit = function (id)
        {
            $scope.$parent.settings.setActiveSite('addNewVacancy').then(function(){
                $location.search({'id': id});
            });
        };

        this.showVacancy = function (id)
        {
            var path = '/vacancy/'+id;

            if($rootScope.media == 'mobile'){
                $location.search({myVac: true});
                $location.path(path);
            }else{
                path+='?myVac';
                $window.open(path, '_blank');
            }

        };

        this.changeTab = function (tab) {
            $scope.selectedTab = tab;
            $scope.switchingTab = true;
            if ($scope.showme !== tab) {
                $timeout(function(){
                    ctrl.filter.publishMode = tab == 'published'?'website':'email';
                    ctrl.filter.from = 0;
                    ctrl.currentPage = 1;
                    refresh();
                },200).then(function(){
                    $scope.showme = tab;
                });
            }
        };

        this.add = function ()
        {
            CurrentUser.resolveOrReject().then(siteSettings.requireVacancyPermission).then(function (user)
            {
                if(!user.firstName) {
                    text = gettext('Message_Please_Complete_Your_Company_Name_In_Your_Profile_Settings');
                    recrutoidPopup('Warning', text, 'warning');
                    return;
                }

                return publishVacancyModal({
                    email: user.email,
                    companyName: user.firstName
                }, function ()
                {
                    text = gettext('Message_Your_New_Vacancy_Has_Been_Published');
                    recrutoidPopup('Success', text, 'success');
                    refresh();
                });
            });
        };

        this.getDaysToDeletion = function(extendDate)
        {
            return Math.ceil( 7 - ( (+moment() - extendDate) / 86400000 ) );
        };

        refresh = paginationSupport(ctrl, function (callback)
        {
            var vacancies = {
                email: [],
                website: []
            };

            VacanciesDAO.query(ctrl.filter).then(function (results)
            {
                for (var i = 0; i < results.results.length; i++) {
                    if(results.results[i].publishViaEmail) {
                        delete results.results[i].expDate;
                    }
                    else {
                        results.results[i].expDate = moment(results.results[i].expDate).fromNow();
                    }

                    if(results.results[i].applicants.length){
                        results.results[i].newApplicantsCount = $filter('filter')(results.results[i].applicants, {new: true }).length;
                    }

                }
                ctrl.vacancies = results.results;
                if(ctrl.allowScroll) {
                    $scope.$root.$emit('scrollTo', 'myVacanciesList');
                    delete ctrl.allowScroll;
                }

                $rootScope.siteLoaded = true;
                callback(results.total);
                $scope.switchingTab = false;
                ctrl.selectValue = false;
            });
        });

        (function init()
        {
            refresh();
        })();
    }

    angular.module('recrutoid.user').controller('MyVacanciesCtrl',
            ['$location', '$modal', '$timeout', '$scope', '$rootScope', 'CurrentUser', 'siteSettings', 'recrutoidPopup', 'VacanciesDAO',
             'paginationSupport', 'gettext', '$filter', '$window', MyVacanciesCtrl]);
})();
