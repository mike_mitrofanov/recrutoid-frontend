(function ()
{
    'use strict';

    function cvCheckUpCtrl($location, $modal, $q, $scope, Authenticator, CurrentUser, Notification,
                                    paginationSupport, recrutoidPopup, UserDAO, CvfilesDAO, gettext)
    {
        var ctrl = this;
        var refresh;


        var defaultFilter = {
            email: null,
            timestamp: null,
            filename: null,
            size: 20,
            from: 0,
        };
        this.filter = angular.copy(defaultFilter);


        this.doActionOnSelected = function (action)
        {
            if (-1 === ['remove', 'ban', 'unban'].indexOf(action)) {
                return;
            }

            var ids = [], promises = [];

            var title = gettext('Warning');
            var text = gettext('Are you sure to ' + action + ' selected?');
            var confirm = gettext('popup_yes_i_am_button');
            recrutoidPopup({
                title: title,
                text: text,
                html: true,
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: confirm,
                closeOnConfirm: true
            }, function (isConfirm)
            {
                if (isConfirm) {
                    angular.forEach(ctrl.results, function (elem)
                    {
                        if (elem.selected) {
                            ids.push(elem.id);
                        }
                        delete elem.selected;
                    });

                    ctrl.allSelected = false;

                    angular.forEach(ids, function (id)
                    {
                        promises.push(cvfilesDAO[action](id));
                    });

                    if (promises.length) {
                        $q.all(promises).then(function ()
                        {
                            refresh();
                            var text;
                            if ('remove' === action) {
                                text = gettext('Selected users have been removed.');
                            } else if ('ban' === action) {
                                text = gettext('Selected users have been banned.');
                            } else if ('unban' === action) {
                                text = gettext('Selected users have been unbanned.');
                            }
                            Notification.success(text);
                        });
                    }
                }
            });
        };

        this.selectAll = function () {
            ctrl.results.forEach(function (vacancy){
                vacancy.selected = ctrl.allSelected;
            });
        };

        this.download = function (fileurl) {
       

            //this trick will generate a temp <a /> tag
            var link = document.createElement("a");
            link.href = fileurl;

            //set the visibility hidden so it will not effect on your web-layout
            link.style = "visibility:hidden";
            link.download = fileurl; //this is an example file to download, use yours

            //this part will append the anchor tag and remove it after automatic click
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);

        };


        this.remove = function (user)
        {
            var title = gettext('Warning');
            var text = gettext('message_are_you_sure');
            var confirm = gettext('popup_yes_i_am_button');
            recrutoidPopup({
                title: title,
                text: text,
                html: true,
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: confirm,
                closeOnConfirm: true
            }, function (isConfirm)
            {
                if (isConfirm) {
                    CvfilesDAO.remove(user.id).then(function ()
                    {
                        refresh();
                        var text = gettext('User_removed');
                        Notification.success(text);
                    });
                }
            });
        };

        function copySearchQueryToFilter()
        {
            var search = $location.search();
            angular.forEach(ctrl.filter, function (value, key)
            {
                var searchValue = search[key];
                if (null != searchValue && '' !== searchValue) {
                    ctrl.filter[key] = searchValue;
                }
            });
        }

         function watchSearchQuery()
        {
            $scope.$watch(function ()
            {
                return $location.search();
            }, copySearchQueryToFilter, true);
        }

        function watchFilterAndUpdateSearchQuery()
        {
            $scope.$watch(function ()
            {
                return ctrl.filter;
            }, function ()
            {
                var filter = angular.copy(ctrl.filter);
                angular.forEach(defaultFilter, function (value, key)
                {
                    if (filter[key] === value) {
                        delete filter[key];
                    } else if ('' === filter[key]) {
                        delete filter[key];
                    }
                });
                $location.search(filter);
            }, true);
        }
        
        (function init()
        {
            var promise;
            var promises = [];
            copySearchQueryToFilter();
            watchFilterAndUpdateSearchQuery();
            watchSearchQuery();

            $q.all(promises).then(function ()
            {
                refresh = paginationSupport(ctrl, function (callback)
                {
                    CvfilesDAO.query(ctrl.filter).then(function (results)
                    {
                        ctrl.results = results.results;
                        ctrl.total = results.total;
                        ctrl.showTotal = true;
                        callback(results.total);
                    });
                });
                refresh();
            });
        })();
    }

    angular.module('recrutoid.vacancy').controller('cvCheckUpCtrl',
            ['$location', '$modal', '$q', '$scope', 'Authenticator', 'CurrentUser', 'Notification', 'paginationSupport',
             'recrutoidPopup', 'UserDAO', 'CvfilesDAO', 'gettext', cvCheckUpCtrl]);
})();
