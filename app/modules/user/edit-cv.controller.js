(function ()
{
    'use strict';

    function EditCvController($filter, $location, $window, $rootScope, $scope, $q, CommonDAO, CurrentUser, cvMechanic, gettext, lookForChanges, recrutoidPopup,
                              unsavedChangesPopup, uploadAvatar, UserResumeDAO, UserDAO, UserSettingsChangePage, profileChecker)
    {
        var ctrl = this;
        var initialCvData;
        var propertiesToIgnoreWhenLookingForChanges = ['thumbnail'];
        ctrl.infoEdit = false;

        cvMechanic.attachData(this);

        this.step = 0;

        this.showCvOnHomeChange = function ()
        {
            CurrentUser.require().then(function (user)
            {
                UserResumeDAO.showCvOnHome(user.id, ctrl.showCvOnHome).then(function ()
                {
                    recrutoidPopup({
                        title: gettext('Success!'),
                        text: gettext('Show CV on home setting changed'),
                        type: 'success',
                        confirmButtonText: 'Ok',
                        closeOnConfirm: true
                    });
                });
            });
        };

        function submitForm(showSuccessPopup, remotelyInvokedSave)
        {
            var cv = ctrl.cvMechanic.prepareCvToSubmit();
            return UserResumeDAO.save(cv).then(function ()
            {
                initialCvData = angular.copy(ctrl.cv);
                var text = '';

                if (showSuccessPopup) {
                    text = gettext('Message_CV_Is_Now_Active_On_Site');
                }else{
                    text = gettext('Message_CV_Is_Not_Active_On_Site');
                }

                if (initialCvData.userData.email !== ctrl.cv.userData.email) {
                    text = showSuccessPopup?gettext('Message_CV_Is_Now_Active_On_Site_New_Email'):gettext('Message_CV_Is_Not_Active_On_Site_New_Email');
                    ctrl.cv.userData.email = initialCvData.userData.email;
                }

                if(text) {
                    recrutoidPopup({
                        title: gettext(showSuccessPopup?'Success':'Warning!'),
                        text: text,
                        type: showSuccessPopup?'success':'warning',
                        confirmButtonText: 'Ok',
                        closeOnConfirm: true
                    }, function (isConfirm)
                    {
                        if (isConfirm && !remotelyInvokedSave) {
                            ctrl.cancelLookForChanges = true;
                            ctrl.setActiveSite('cv-page');
                        }
                    });
                }else{
                    if(!remotelyInvokedSave)
                        ctrl.cancelLookForChanges = true;
                        ctrl.setActiveSite('cv-page');
                }
            });
        }

        this.changeStep = function (desiredStep, remotelyInvokedSave)
        {
            var errorMessage = gettext('Message_Some_fields_require_your_attention');
            var isValid = ctrl.cvMechanic.validate.main();
            // if (!formProblemMessage) {
            //     formProblemMessage = ctrl.cvMechanic.validate.education();
            // }
            // if (!formProblemMessage) {
            //     formProblemMessage = ctrl.cvMechanic.validate.workExperience();
            // }
            // if (!formProblemMessage) {
            //     formProblemMessage = ctrl.cvMechanic.validate.languages();
            // }

            if (isValid) {
                recrutoidPopup('Wait!', errorMessage, 'warning');
                if (remotelyInvokedSave) {
                    remotelyInvokedSave.reject();
                }
                return false;
            }

            ctrl.savingData = true;

            var user = angular.copy(ctrl.cv.userData);
            user.resume = ctrl.cv;
            user.role = 'normal';

            submitForm(profileChecker.isActive(user), remotelyInvokedSave).then(function ()
            {
                $scope.$emit('event:userDataHasChanged', ctrl.cv.userData);
                $scope.$emit('event:userCvHasChanged', ctrl.cv);

                if (remotelyInvokedSave) {
                    remotelyInvokedSave.resolve(false);
                }
                else {
                    // if now step is equal 1 it means that education is valid and cv state is lighted as active
                    // if (1 === ctrl.step) {
                    //     setEducationValidity(true);
                    // }
                    ctrl.savingData = false;
                    fetchAndPrepareCV();
                }
            }).catch(function (error)
            {
                if(409 === error.status) {
                    recrutoidPopup('', gettext('Message_Email_Already_Registered'), 'warning');
                }
                ctrl.savingData = false;
                if (remotelyInvokedSave) {
                    remotelyInvokedSave.reject();
                }
            });

            return true;
        };

        this.back = function ()
        {
            ctrl.step = 0 > --ctrl.step ? 0 : ctrl.step;
        };

        this.uploadAvatar = function (files)
        {
            if (!files || !files.length) {
                return;
            }
            ctrl.uploading = true;
            uploadAvatar.upload(files).then(function (url)
            {
                ctrl.cv.userData.thumbnail = url;
            }).finally(function ()
            {
                ctrl.uploading = false;
            });
        };

        this.formatSalary = function ()
        {
            ctrl.cv.desiredSalary.value = $filter('formatSalary')(ctrl.cv.desiredSalary.value);
        };

        this.showIncorrectFields = function ()
        {
            var step, message = ctrl.cvMechanic.validate.main();

            if(message) {
                step = 0;
            }
            else {
                message = ctrl.cvMechanic.validate.education();
                if (message) {
                    step = 1;
                }
            }

            if (message) {
                recrutoidPopup('', message, 'warning');
                ctrl.step = step;
            }
        };

        this.getCvStatus = function ()
        {
            var cvActive = ctrl.cv.title && ctrl.cv.title.length && ctrl.cv.userData.categories.length && ctrl.cv.userData.city &&
                ctrl.cv.userData.firstName && ctrl.cv.userData.firstName.length &&
                ctrl.cv.userData.lastName && ctrl.cv.userData.lastName.length && ctrl.educationValidity;

            return cvActive;
        };



        var unregisterChangeLocationListener = $rootScope.$on('$locationChangeStart', function ($e, newUrl)
        {
            if (!ctrl.cancelLookForChanges && lookForChanges(ctrl.cv, initialCvData, propertiesToIgnoreWhenLookingForChanges)) {
                $e.preventDefault();
                unsavedChangesPopup.open(function (saving)
                {
                    if (saving) {
                        ctrl.cvMechanic.closeAllEditBoxes();

                        var remotelyInvokedSave = $q.defer();
                        ctrl.changeStep(null, remotelyInvokedSave);
                        remotelyInvokedSave.promise.then(function (showSuccessPopup)
                        {
                            if (showSuccessPopup) {
                                unsavedChangesPopup.success();
                            }
                            unregisterChangeLocationListener();
                            $location.href = newUrl;
                        });
                    }
                    else {
                        unregisterChangeLocationListener();
                        $location.href = newUrl;
                    }
                });
            }
            else {
                unregisterChangeLocationListener();
            }
        });

        UserSettingsChangePage.callback = function ()
        {
            var defer = $q.defer();

            if (!ctrl.cancelLookForChanges && lookForChanges(ctrl.cv, initialCvData, propertiesToIgnoreWhenLookingForChanges)) {
                unsavedChangesPopup.open(function (saving)
                {
                    if (saving) {
                        ctrl.cvMechanic.closeAllEditBoxes();

                        var remotelyInvokedSave = $q.defer();
                        ctrl.changeStep(null, remotelyInvokedSave);
                        remotelyInvokedSave.promise.then(function (showSuccessPopup)
                        {
                            if (showSuccessPopup) {
                                unsavedChangesPopup.success();
                            }
                            unregisterChangeLocationListener();
                            defer.resolve();
                        }).catch(function ()
                        {
                            defer.reject();
                        });
                    }
                    else {
                        unregisterChangeLocationListener();
                        defer.resolve();
                    }
                });
            }
            else {
                unregisterChangeLocationListener();
                defer.resolve();
            }

            return defer.promise;
        };

        function checkInitialEducationValidity()
        {
            // later education will be filled up with empty object by cvMechanic if empty
            ctrl.educationValidity = !!ctrl.cv.education.length;
        }

        function setEducationValidity(educationValidity)
        {
            ctrl.educationValidity = educationValidity;
        }

        function fetchAndPrepareCV(cv)
        {
            var promise;
            if (cv) {
                var defer = $q.defer();
                defer.resolve(cv);
                promise = defer.promise;
            }
            else {
                promise = CurrentUser.require().then(function (user)
                {
                    return UserResumeDAO.get(user.id);
                });
            }

            return promise.then(function (cv)
            {
                ctrl.cv = cv;
                if (!ctrl.cv.userData.categories) {
                    ctrl.cv.userData.categories = [];
                }

                if (!ctrl.cv.desiredSalary) {
                    ctrl.cv.desiredSalary = {};
                }
                else if (ctrl.cv.desiredSalary.hasOwnProperty('value')) {
                    ctrl.cv.desiredSalary.value = ctrl.cv.desiredSalary.value.toString();
                    ctrl.formatSalary();
                }
                if (!ctrl.cv.desiredSalary.currency) {
                    ctrl.cv.desiredSalary.currency = 'KZT';
                }

                checkInitialEducationValidity();

                ctrl.cvMechanic = cvMechanic.getInstance(ctrl);

                ctrl.cv.userId = ctrl.cv.userData.id;
                initialCvData = angular.copy(ctrl.cv);

                prepareWorkExperience();
                prepareEducation();


            });
        }

        function prepareWorkExperience() {
            var today = new Date();
            ctrl.cv.workExperience.forEach(function(exp){
                exp.rawEndDate = exp.endDate;
                if (exp.currentWork) {
                    exp.rawEndDate = today;
                }
            });
            ctrl.cv.workExperience = $filter('orderBy')(ctrl.cv.workExperience, '-startDate');
            ctrl.cv.workExperience = $filter('orderBy')(ctrl.cv.workExperience, '-endDate');
        }

        function prepareEducation() {
            ctrl.cv.education = $filter('orderBy')(ctrl.cv.education, '-startDate');
            ctrl.cv.education = $filter('orderBy')(ctrl.cv.education, '-endDate');
        }

        function parseCategoriesToArrays(categories) {
            var parsedCategories = [];
            categories.forEach(function (category){
                parsedCategories.push([category]);
            });

            return parsedCategories;
        }

        (function init()
        {
            ctrl.setActiveSite = $scope.$parent.$parent.settings.setActiveSite;
            UserDAO.getMyProfile().then(function (profile) {
                ctrl.profile = profile;
            });
            CommonDAO.getEditResumeData().then(function (data)
            {
                ctrl.categories = parseCategoriesToArrays(data.categories);
                ctrl.salary_ranges = data.salary_ranges;
                ctrl.cities = data.cities;

                if (data.cv.additionalInfo) {
                    data.cv.additionalInfoTmp = data.cv.additionalInfo;
                }

                ctrl.languages = data.languages.results.map(function (lang)
                {
                    return {name: lang.language, val: lang.language};
                });

                fetchAndPrepareCV(data.cv).then(function ()
                {
                    ctrl.showCvOnHome = ctrl.cv.userData.showOnHomePage;
                    ctrl.viewReady = true;
                    $rootScope.siteLoaded = true;
                });
            });
        })();
    }

    angular.module('recrutoid.user').controller('EditCvController',
            ['$filter',
             '$location',
             '$window',
             '$rootScope',
             '$scope',
             '$q',
             'CommonDAO',
             'CurrentUser',
             'cvMechanic',
             'gettext',
             'lookForChanges',
             'recrutoidPopup',
             'unsavedChangesPopup',
             'uploadAvatar',
             'UserResumeDAO',
             'UserDAO',
             'UserSettingsChangePage',
             'profileChecker',
             EditCvController]);
})();
