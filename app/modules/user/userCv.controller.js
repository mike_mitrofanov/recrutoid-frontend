(function ()
{
    'use strict';

    function UserCVCtrl($location, $modal, $routeParams, $scope, CurrentUser, recrutoidPopup, UserResumeDAO, VacanciesDAO, UserDAO, gettext)
    {
        var ctrl = this;
        
        this.languageLevels = ['Basic', 'Intermediate', 'Advanced'];

        function swal(type, text)
        {
            recrutoidPopup({
                title: type,
                text: text,
                type: type.toLowerCase(),
                confirmButtonText: 'Ok',
                closeOnConfirm: true
            });
        }

        this.returnToTheListByEmployer = function ()
        {
            if ($location.search().hasOwnProperty('appliers')) {
                $location.path("/user/settings/myVacanciesApplications");
            }else{
                if ($scope.locationHistory.length > 1) {
                    $scope.locationHistory.pop();
                    $location.url($scope.locationHistory.pop());
                    $location.replace();
                } else {
                    $location.path('/users');
                }
            }
        };

        this.callForInterview = function ()
        {
            function openModal(vacancies)
            {
                return $modal.open({
                    size: 'md',
                    controller: 'CallForInterview as callForInterview',
                    templateUrl: 'modules/user/call-for-interview/callForInterview.modal.tpl.html',
                    windowClass: 'call-for-interview-window',
                    resolve: {
                        data: function ()
                        {
                            return {
                                vacancies: vacancies,
                                cvId: ctrl.profile.id,
                                user: ctrl.profile
                            };
                        }
                    }
                }).result;
            }

            VacanciesDAO.query({my: true, userId: $routeParams.id, publishMode: 'website'}).then(function (vacancies)
            {
                vacancies = vacancies.results;
                var text;
                if(vacancies.length) {
                    var callResult = openModal(vacancies);

                    callResult.then(function ()
                    {
                        refresh();
                        text = gettext('Message_Your_Offer_Was_Sent_To_This_Job_Seeker');
                        swal('Success', text);
                    }).catch(function (error)
                    {
                        if(error) {
                            text = gettext('Message_Your_Offer_Was_Not_Sent_To_This_Job_Seeker');
                            swal('Warning', text);
                        }
                    });
                }
                else {
                    text = gettext('Message_You_Dont_Have_Any_Vacancies_Or_You_Have_Already_Send');
                    swal('Warning', text);
                }
            });
        };

        this.edit = function (type, name)
        {
            if(-1 === ['education', 'workExperience', 'languages', 'additionalInfo'].indexOf(type)) {
                return;
            }

            var modalResult = $modal.open({
                size: 'lg',
                backdrop: 'static',
                controller: 'EditCvModalController as editCv',
                templateUrl: 'modules/user/edit-cv-modal/editCvModal.tpl.html',
                resolve: {
                    modalSettings: function ()
                    {
                        return {
                            partType: type,
                            partName: name
                        };
                    }
                }
            }).result;

            modalResult.then(refresh);
        };

        this.getPdf = function (siteUrl) {
            window.open(siteUrl+'/api/users/'+ctrl.profile.id+'/resume/pdf');
        };

        function refresh()
        {
            return UserResumeDAO.get($routeParams.id).then(function (profile)
            {
                ctrl.resume = profile;
                ctrl.profile = profile.userData;
                delete ctrl.resume.userData;
                var workExperience = moment.duration(ctrl.profile.workExperience);
                
                angular.forEach(ctrl.resume.languages, function (elem)
                {
                    elem.level = parseInt(elem.level, 10);
                });

                ctrl.totalWorkExperience = workExperience._data.years + ' years ' + workExperience._data.months + ' months';

				return profile;
            });
        }

		function visitedUsers(user)
		{
			var data = {
				visitedUserId: $routeParams.id,
				userId: user.id,
				userEmail: user.email
			};

			UserDAO.isVisited(data);
        }


        (function init()
        {
            ctrl.forPDF = $location.search().hasOwnProperty('pdf');
            CurrentUser.resolve().then(function (user)
            {
                ctrl.myCv = $routeParams.id === user.id;

                // Yes, yes, this condition sucks..
                if(!ctrl.forPDF && (!$routeParams.id || ('normal' === user.role && !ctrl.myCv))) {
                    $location.path('/');
                }
                else {
                    refresh().then(function(data) {
						visitedUsers(user);
					});
                }
            });
        })();
    }

    angular.module('recrutoid.user').controller('UserCVCtrl',
            ['$location', '$modal', '$routeParams', '$scope', 'CurrentUser', 'recrutoidPopup', 
				'UserResumeDAO', 'VacanciesDAO', 'UserDAO', 'gettext', UserCVCtrl]);
})();
