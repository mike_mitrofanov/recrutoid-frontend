(function ()
{
    'use strict';

    function OfferListCtrl($location, $scope, UserOffersDAO, recrutoidPopup, paginationSupport)
    {
        var ctrl = this;

        ctrl.filter = {
            size: 3,
            from: 0
        };

        function refreshNewOffersViewsCount()
        {
            return UserOffersDAO.getNewOffersViewsCount().then(function (offersViewsCount)
            {
                ctrl.newOffersViews = offersViewsCount.count;
                $scope.$emit('event:interview-seen', offersViewsCount.count);
            });
        }

        this.showVacancyDetails = function(vacancy) {
            if(!vacancy.seen) {
                UserOffersDAO.markOfferSeen(vacancy.id);
            }
            $location.path('/vacancy/' + vacancy.id);
        };

        this.markAllAsSeen = function (){
            UserOffersDAO.markAllSeen().then(function() {
                ctrl.offers.forEach(function (offer){
                    offer.isNew = false;
                });
                refreshNewOffersViewsCount();
                refresh();
            });
        };

        this.remove = function remove(id)
        {
            recrutoidPopup({
                title: 'Warning',
                text: 'message_are_you_sure',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'popup_yes_i_am_button',
                closeOnConfirm: true
            }, function (isConfirm)
            {
                if (isConfirm) {
                    UserOffersDAO.remove(id).then(function ()
                    {
                        refresh();
                    }).catch(function (error)
                    {
                        recrutoidPopup('Error', error, 'error');
                    });
                }
            });
        };

        this.removeSelected = function removeSelected()
        {
            var deleted = 0;
            var toDelete = {
                ids: []
            };

            function toRemove(toDelete)
            {
                for (var i = 0; i < ctrl.offers.length; i++) {
                    if (true === ctrl.offers[i].checked) {
                        toDelete.push(ctrl.offers[i].offerId);
                        deleted++;
                    }
                }
            }

            toRemove(toDelete.ids);

            if (0 === toDelete.ids.length) {
                recrutoidPopup('Error', 'You haven\'t checked anything.', 'error');
            } else {
                recrutoidPopup({
                    title: 'Warning',
                    text: 'Are you sure you want to remove all selected offers?',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'popup_yes_i_am_button',
                    closeOnConfirm: true
                }, function (isConfirm)
                {
                    if (isConfirm) {
                        UserOffersDAO.removeArray(toDelete).then(function ()
                        {
                            recrutoidPopup('Success', 'You have deleted ' + deleted + ' offers.', 'success');
                            refresh();
                        }).catch(function (error)
                        {
                            recrutoidPopup('Error', error, 'error');
                        });
                    }
                });
            }
        };

        this.selectAll = function ()
        {
            ctrl.selectValue = !ctrl.selectValue;
            for (var i = 0; i < ctrl.offers.length; i++) {
                ctrl.offers[i].checked = ctrl.selectValue;
            }
        };

        function refresh()
        {
            paginationSupport(ctrl, function (callback)
            {
                UserOffersDAO.query(ctrl.filter).then(function (results)
                {
                    if (0 === results.length)
                    {
                        ctrl.message = 'You have 0 offers.';
                    }

                    ctrl.offers = results.results;

                    for (var i = 0; i < ctrl.offers.length; i++)
                    {
                        ctrl.offers[i].createDate = ctrl.offers[i].addDate;
                        ctrl.offers[i].isNew = !ctrl.offers[i].seen;
                    }

                    callback(results.total);
                    if(ctrl.allowScroll) {
                        $scope.$root.$emit('scrollTo', 'myOffersList');
                        delete ctrl.allowScroll;
                    }
                });
            })();
        }

        (function ()
        {
            refresh();
        })();
    }

    angular.module('recrutoid.user').controller('OfferListCtrl',
            ['$location', '$scope', 'UserOffersDAO', 'recrutoidPopup', 'paginationSupport', OfferListCtrl]);
})();
