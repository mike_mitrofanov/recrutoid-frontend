(function()
{
    'use strict';

    function ResumeViewsController($scope, CurrentUser, UserResumeDAO, paginationSupport)
    {
        var ctrl = this;
        var refresh;
        this.currentUserId = '';
        ctrl.filter = {
            size: 5,
            from: 0
        };


        function refreshNewResumeViewsCount()
        {
            return UserResumeDAO.getNewResumeViewsCount().then(function (resumeViewsCount)
            {
                ctrl.newResumeViews = resumeViewsCount.count;
                $scope.$emit('event:resume-views-seen', resumeViewsCount);
            });
        }

        this.markAllAsSeen = function (){
            UserResumeDAO.markAllSeen(ctrl.currentUserId, ctrl.views).then(function() {
                ctrl.views.forEach(function (view){
                    view.seen = true;
                });
                refreshNewResumeViewsCount();
            });
        };

        refresh = paginationSupport(ctrl, function (callback)
        {
            UserResumeDAO.queryViews(ctrl.currentUserId, ctrl.filter).then(function (data)
            {
                ctrl.views = data.results;
                for(var i=0; i<ctrl.views.length; i++)
                {
                    if(!ctrl.views[i].seen) {
                        refreshNewResumeViewsCount();
                        break;
                    }
                }

                callback(data.total);
            });
        });

        (function ()
        {

            var currentUser;
            CurrentUser.resolveOrReject().then(function (user)
            {
                currentUser = user;
                ctrl.currentUserId = user.id;
                refresh();
            });
        })();
    }

    angular.module('recrutoid.user').controller('ResumeViewsController', ['$scope', 'CurrentUser', 'UserResumeDAO', 'paginationSupport', ResumeViewsController])
})();
