(function()
{
    'use strict';

    function MyCvController($window, gettext, $scope, recrutoidPopup, $rootScope, CurrentUser, UserResumeDAO, profileChecker)
    {
        var ctrl = this;

        this.details = function ()
        {
            $window.open('/user/' + ctrl.currentUserId + '/profile');
        };

        this.cancelCvCheckup = function(isImpersonated) {

            if(isImpersonated && !ctrl.isCvActive()) {
                var text = gettext('Message_You_Have_To_Fill_Up_Users_Cv');
                var successDisplay = gettext('Warning');
                recrutoidPopup(successDisplay, text, 'warning');
                return;
            }

            UserResumeDAO.cancelCvCheckup(isImpersonated).then(function(){
                $scope.canceledCvCheckup = !isImpersonated;
                if(!isImpersonated){
                    refreshUser();
                    $scope.$emit('event:cv-uploaded', false);
                }else{
                    delete ctrl.user.uploadedResume.data;
                }
            },function(err){
                if (err.status == 412) {
                    var text = gettext('Message_Cancel_Time_Has_Been_Reached');
                    var successDisplay = gettext('Warning');
                    recrutoidPopup(successDisplay, text, 'warning');
                }
                refreshUser();
            });
        };

        this.showJobSeekerProfile = function (id)
        {
            var path = '/user/' + id + '/profile';

            if($rootScope.media == 'mobile'){
                $location.search({appliers: ctrl.vacancyId});
                $location.path(path);
            }else{
                path+='?appliers='+ctrl.vacancyId;
                $window.open(path, '_blank');
            }
        };

        this.isCvActive = function(){
            return profileChecker.isActive(ctrl.user);
        };

        function getWorkExperience(){
            if (ctrl.user.workExperience && typeof ctrl.user.workExperience === 'number'){
                var workExperience = moment.duration(ctrl.user.workExperience);
                ctrl.user.workExperience = workExperience._data.years + 'y ' + workExperience._data.months + 'm';
            }
        }

        function refreshUser() {
            CurrentUser.reload().then(function (user)
            {
                ctrl.user = user;
                ctrl.currentUserId = user.id;

                getWorkExperience();
            });
        }

        this.cancelTimeExpired = function(){
            return ctrl.user.cancelCvExpireTime < new Date().getTime();
        };

        function handleUploadedCv(event, added) {
            if(added) {
                ctrl.user.uploadedResume.data = {url:'fake'};
                var fiveMinutesLater = new Date();
                fiveMinutesLater.setMinutes(fiveMinutesLater.getMinutes() + 5);

                ctrl.user.cancelCvExpireTime = fiveMinutesLater.getTime();

                recrutoidPopup(gettext('Thank_You'), gettext('Message_Your_Cv_Will_Be_Moderated'), 'success');
            }
            else {
                ctrl.user.resume = {};
                delete ctrl.user.uploadedResume.data;
            }
        }

        function handleEditedCv(event, cv) {
            ctrl.user.resume = cv;
            refreshUser();
        }

        var destroyListener = {
            uploadedCvChange: $scope.$root.$on('event:cv-uploaded', handleUploadedCv),
            editedCvChange: $scope.$root.$on('event:userCvHasChanged', handleEditedCv)
        };
        $scope.$on('$destroy', function ()
        {
            destroyListener.uploadedCvChange();
        });

        (function ()
        {
            $rootScope.siteLoaded = true;
            CurrentUser.resolveOrReject().then(function (user)
            {
                ctrl.user = user;
                ctrl.currentUserId = user.id;

                getWorkExperience();
            });
        })();
    }

    angular.module('recrutoid.user').controller('MyCvController', ['$window', 'gettext', '$scope', 'recrutoidPopup', '$rootScope', 'CurrentUser', 'UserResumeDAO', 'profileChecker', MyCvController])
})();
