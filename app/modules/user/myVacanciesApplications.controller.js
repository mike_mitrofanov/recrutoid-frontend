(function ()
{
    'use strict';

    function MyVacanciesApplicationsCtrl($scope, $rootScope, VacanciesDAO, paginationSupport, gettext, recrutoidPopup, $window, $location)
    {
        var ctrl = this;
        var refresh;

        ctrl.filter = {
            myAll: true,
            size: 5,
            from: 0
        };

        var warning = gettext('Warning');
        var errorDisplay = gettext('Error');
        var successDisplay = gettext('Success');
        var confirm = gettext('popup_yes_i_am_button');

        $scope.$watch('myApplications.jobSeekers', function(items){
            var selectedItems = 0;
            angular.forEach(items, function(item){
                selectedItems += item.checked ? 1 : 0;
            });
            $scope.selectedItems = selectedItems;
        }, true);

        this.markAsSeen = function (id)
        {
            VacanciesDAO.markApplicationAsSeen(ctrl.vacancyId, [id]).then(function ()
            {
                refresh();
            });
        };

        this.remove = function (applicationId)
        {
            recrutoidPopup({
                title: 'Warning',
                text: 'message_are_you_sure',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'popup_yes_i_am_button',
                closeOnConfirm: true
            }, function (isConfirm)
            {
                if (isConfirm) {
                    VacanciesDAO.removeApplication(ctrl.vacancyId, [applicationId]).then(function ()
                    {
                        refresh();
                    });
                }
            });
        };

        this.showJobSeekerProfile = function (id)
        {
            ctrl.markAsSeen(id);
            var path = '/user/' + id + '/profile';

            if($rootScope.media == 'mobile'){
                $location.search({appliers: ctrl.vacancyId});
                $location.path(path);
            }else{
                path+='?appliers='+ctrl.vacancyId;
                $window.open(path, '_blank');
            }
        };

        this.markAsSeenSelected = function ()
        {
            var toMark = [];
            for (var i = 0; i < ctrl.jobSeekers.length; i++) {
                if (ctrl.jobSeekers[i].checked) {
                    toMark.push(ctrl.jobSeekers[i].id);
                }
            }

            if (0 === toMark.length) {
                recrutoidPopup('Error', 'You haven\'t checked anything.', 'error');
            } else {
                recrutoidPopup({
                    title: 'Warning',
                    text: gettext('Are_you_sure_you_want_to_mark_all_selected_offers_as_seen'),
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'popup_yes_i_am_button',
                    closeOnConfirm: true
                }, function (isConfirm)
                {
                    if (isConfirm) {
                        VacanciesDAO.markApplicationAsSeen(ctrl.vacancyId, toMark).then(function ()
                        {
                            recrutoidPopup('Success', 'You have marked ' + toMark.length + ' offers as seen.', 'success');
                            refresh();
                        }).catch(function (error)
                        {
                            recrutoidPopup('Error', error, 'error');
                        });
                    }
                });
            }
        };


        this.selectAll = function ()
        {
            ctrl.selectValue = !ctrl.selectValue;
            for (var i = 0; i < ctrl.jobSeekers.length; i++) {
                ctrl.jobSeekers[i].checked = ctrl.selectValue;
            }
        };

        this.removeSelected = function ()
        {
            var toDelete = [];
            for (var i = 0; i < ctrl.jobSeekers.length; i++) {
                if (ctrl.jobSeekers[i].checked) {
                    toDelete.push(ctrl.jobSeekers[i].id);
                }
            }

            if (0 === toDelete.length) {
                recrutoidPopup('Error', 'You haven\'t checked anything.', 'error');
            } else {
                recrutoidPopup({
                    title: 'Warning',
                    text: 'Are you sure you want to remove all selected offers?',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'popup_yes_i_am_button',
                    closeOnConfirm: true
                }, function (isConfirm)
                {
                    if (isConfirm) {
                        VacanciesDAO.removeApplication(ctrl.vacancyId, toDelete).then(function ()
                        {
                            recrutoidPopup('Success', 'You have deleted ' + toDelete.length + ' offers.', 'success');
                            refresh();
                        }).catch(function (error)
                        {
                            recrutoidPopup('Error', error, 'error');
                        });
                    }
                });
            }
        };

        refresh = paginationSupport(ctrl, function (callback)
        {
            VacanciesDAO.queryApplications(ctrl.vacancyId, ctrl.filter).then(function (data)
            {
                console.log(data);
                var newApplicationsCountRefreshed = false;
                ctrl.jobSeekers = data.results;
                for (var i = 0; i < ctrl.jobSeekers.length; i++) {
                    if (!newApplicationsCountRefreshed && !ctrl.jobSeekers[i].seen) {
                        $scope.$parent.settings.refreshNewApplicationsCount();
                        newApplicationsCountRefreshed = true;
                    }
                    var workExperience = moment.duration(ctrl.jobSeekers[i].workExperience);
                    ctrl.jobSeekers[i].workExperience = workExperience._data.years + 'y ' + workExperience._data.months + 'm';
                }

                callback(data.total);
                $rootScope.siteLoaded = true;
                ctrl.selectValue = false;
            });
        });

        (function init()
        {
            ctrl.vacancyId = $location.search().appliers;
            refresh();
        })();
    }

    angular.module('recrutoid.user').controller('MyVacanciesApplicationsCtrl',
            ['$scope', '$rootScope', 'VacanciesDAO', 'paginationSupport', 'gettext', 'recrutoidPopup', '$window', '$location', MyVacanciesApplicationsCtrl]);
})();
