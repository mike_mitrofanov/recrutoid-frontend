(function ()
{
    'use strict';

    function SearchUsersAsAdminCtrl($location, $modal, $q, $scope, Authenticator, CurrentUser,
                                    ExporterDAO, CategoriesDAO, CitiesDAO, SalaryRangesDAO, Notification,
                                    paginationSupport, recrutoidPopup, UserDAO, UsersPermissionsDAO, UserResumeDAO, gettext)
    {
        var ctrl = this;
        var refresh;


        var defaultFilter = {
            title: null,
            nameOrCvTitle: null,
            adminPanel: true,
            email: null,
            categories: null,
            city: null,
            verified: null,
            role: 'normal',
            banned: null,
            size: 50,
            from: 0,
            hidden: null,
            showOnHomePage: null,
            applicants: null,
            dbAccess: null,
            addVacancies: null,
            isSubscriber: null,
            appNotifications: null,
            cvActive: null,
            salaryRange: null,
            cvUploaded: null
        };


        this.filter = angular.copy(defaultFilter);

        this.clearPresetsEmployer = function ()
        {
            ctrl.filter.searchQuery = null;
            ctrl.filter.categories = null;
            ctrl.filter.city = null;
            ctrl.filter.verified = null;
            ctrl.filter.banned = null;
            ctrl.selectedCategory = null;
            ctrl.selectedCity = null;
            ctrl.filter.hidden = null;
            ctrl.filter.showOnHomePage = null;
            ctrl.filter.applicants = null;
            ctrl.filter.dbAccess = null;
            ctrl.filter.addVacancies = null;
        };

        this.clearPresets = function ()
        {
            ctrl.selectedSalaryRange = null;
            ctrl.selectedCity = null;
            ctrl.selectedCategory = null;
            
            ctrl.filter.nameOrCvTitle = null;
            ctrl.filter.email = null;
            ctrl.filter.categories = null;
            ctrl.filter.city = null;
            ctrl.filter.verified = null;
            ctrl.filter.banned = null;
            ctrl.filter.dbAccess = null;
            ctrl.filter.addVacancies = null;
            ctrl.filter.isSubscriber = null;
            ctrl.filter.appNotifications = null;
            ctrl.filter.cvActive = null;
            ctrl.filter.salaryRange = null;
            ctrl.filter.hidden = null;
        };

        this.clearPresetsAdmin = function ()
        {
            ctrl.filter.title = null;
            ctrl.filter.email = null;
        };

        this.cvActiveNotify = function (user) {
          UserResumeDAO.cvActiveNotify(user).then(function(){
              recrutoidPopup('Success', 'Message has been sent', 'success');
              refresh();
          });
        };

        this.newAdmin = function ()
        {
            $modal.open({
                size: 'sm',
                backdrop: 'static',
                controller: 'AddAdminController as addAdmin',
                templateUrl: 'modules/adminSettings/add-admin/addAdmin.tpl.html'
            }).result.then(function ()
            {
                recrutoidPopup('Success', 'New admin account created. Tell new person about email to activate account', 'success');
                refresh();
            }).catch(function (error)
            {
                if(error) {
                    recrutoidPopup('This is bad', error.data || 'Something went wrong', 'error');
                }
            });
        };

        this.downloadCv = function (fileurl) {


            //this trick will generate a temp <a /> tag
            var link = document.createElement("a");
            link.href = fileurl;

            //set the visibility hidden so it will not effect on your web-layout
            link.style = "visibility:hidden";
            link.download = fileurl; //this is an example file to download, use yours

            //this part will append the anchor tag and remove it after automatic click
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);

        };

        this.doActionOnSelected = function (action)
        {
            if (-1 === ['remove', 'ban', 'unban'].indexOf(action)) {
                return;
            }

            var ids = [], promises = [];

            var title = gettext('Warning');
            var text = gettext('Are you sure to ' + action + ' selected?');
            var confirm = gettext('popup_yes_i_am_button');
            recrutoidPopup({
                title: title,
                text: text,
                html: true,
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: confirm,
                closeOnConfirm: true
            }, function (isConfirm)
            {
                if (isConfirm) {
                    angular.forEach(ctrl.results, function (elem)
                    {
                        if (elem.selected) {
                            ids.push(elem.id);
                        }
                        delete elem.selected;
                    });

                    ctrl.allSelected = false;

                    angular.forEach(ids, function (id)
                    {
                        promises.push(UserDAO[action](id));
                    });

                    if (promises.length) {
                        $q.all(promises).then(function ()
                        {
                            refresh();
                            var text;
                            if ('remove' === action) {
                                text = gettext('Message_Selected_Users_Have_Been_Removed');
                            } else if ('ban' === action) {
                                text = gettext('Message_Selected_Users_Have_Been_Banned');
                            } else if ('unban' === action) {
                                text = gettext('Message_Selected_Users_Have_Been_Unbanned');
                            }
                            Notification.success(text);
                        });
                    }
                }
            });
        };

        this.selectAll = function () {
            ctrl.results.forEach(function (vacancy){
                vacancy.selected = ctrl.allSelected;
            });
        };

        this.changeShowOnHomePage = function (user)
        {
            var currentStateOfSOHP = user.showOnHomePage;

            var title = gettext('Warning!');

            if(!currentStateOfSOHP && ctrl.hpTotal >= 15) {
                recrutoidPopup({
                    title: title,
                    text: gettext('Message_Limit_of_15_users_shown_on_homepage_exceeded'),
                    type: 'warning'
                });
                return;
            }

            var text = 'Are you sure to ' + (currentStateOfSOHP ? '<b>hide</b>' : '<b>show</b>');
            text += ' user ' + user.email + (user.firstName ? ' (' + user.firstName + ')' : '') + '\'s ';
            text += 'logo on Home Page?';
            var confirm = gettext('popup_yes_i_am_button');

            recrutoidPopup({
                title: title,
                text: text,
                html: true,
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: confirm,
                closeOnConfirm: true
            }, function (isConfirm)
            {
                if (isConfirm) {
                    UserResumeDAO.showCvOnHome(user.id, !currentStateOfSOHP).then(function ()
                    {
                        refresh();
                        var text = gettext('Success!');
                        Notification.success(text);
                    });
                }
            });
        };

        this.changePermission = function (user, permissionType)
        {
            // only employers can have this property
            if (!user.hasOwnProperty('permissions')) {
                return;
            }

            var currentStateOfPermission = user.permissions[permissionType];
            var title = gettext('Warning');
            var text = 'Are you sure to ' + (currentStateOfPermission ? '<b>forbid</b>' : '<b>allow</b>');
            text += ' user ' + user.email + (user.firstName ? ' (' + user.firstName + ')' : '') + ' to ';
            switch (permissionType) {
                case 'vacancyAdding':
                    text += '<b>adding vacancies</b>?';
                    break;
                case 'cvSearching':
                    text += '<b>searching through CV database</b>?';
                    break;

                default:
                    return;
            }
            var confirm = gettext('popup_yes_i_am_button');

            recrutoidPopup({
                title: title,
                text: text,
                html: true,
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: confirm,
                closeOnConfirm: true
            }, function (isConfirm)
            {
                if (isConfirm) {
                    var promise = (currentStateOfPermission ?
                            UsersPermissionsDAO.deny(user.id, permissionType) :
                            UsersPermissionsDAO.grant(user.id, permissionType));
                    promise.then(function ()
                    {
                        refresh();
                        var text = gettext('Permission_changed');
                        Notification.success(text);
                    });
                }
            });
        };

        this.remove = function (user)
        {
            var title = gettext('Warning');
            var text = gettext('message_are_you_sure');
            var confirm = gettext('popup_yes_i_am_button');
            recrutoidPopup({
                title: title,
                text: text,
                html: true,
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: confirm,
                closeOnConfirm: true
            }, function (isConfirm)
            {
                if (isConfirm) {

                    UserDAO.remove(user.id).then(function ()
                    {
                        refresh();
                        var text = gettext('User_removed');
                        Notification.success(text);
                    });
                }
            });
        };

        this.ban = function (user)
        {
            UserDAO.ban(user.id).then(function ()
            {
                refresh();
                var text = gettext('User_banned');
                Notification.success(text);
            });
        };

        this.unban = function (user)
        {
            UserDAO.unban(user.id).then(function ()
            {
                refresh();
                var text = gettext('User_unbanned');
                Notification.success(text);
            });
        };


        this.downloadAsPDF = function() {
            ExporterDAO.query('users', 'pdf', ctrl.filter).then(function (data)
            {
                ExporterDAO.download(data, 'users.pdf');
            });
        };

        this.downloadAsXLS = function() {
            ExporterDAO.query('users', 'xls', ctrl.filter).then(function (data)
            {
                ExporterDAO.download(data, 'users.xlsx');
            });
        };

        this.resendRegistrationEmail = function (id)
        {
            UserDAO.resendRegistrationEmail(id).then(function ()
            {
                var text = gettext('Email_successfully_sent');
                Notification.success(text);
            });
        };

        this.verify = function (user)
        {
            UserDAO.verify(user.id).then(function ()
            {
                refresh();
                var text = gettext('User_verified_successfully');
                Notification.success(text);
            });
        };

        this.impersonate = function (userId)
        {
            Authenticator.impersonate(userId).then(function ()
            {
                $location.path('/user/settings/editProfile');
                var text = gettext('Successfully_impersonated');
                Notification.success(text);
                CurrentUser.reload();
            }).catch(function ()
            {
                var text = gettext('Impersonating_failed');
                Notification.success(text);
            });
        };


        this.setSearchTo = function (companyName)
        {
            ctrl.filter.emailOrTitle = companyName;
        };

        function onCategoryOrCityChange(newValue, oldValue)
        {
            if (newValue === oldValue) {
                return;
            }
            ctrl.filter.categories = ctrl.selectedCategory && ctrl.selectedCategory.id;
            ctrl.filter.city = ctrl.selectedCity && ctrl.selectedCity.id;
            ctrl.filter.salaryRange = ctrl.selectedSalaryRange && ctrl.selectedSalaryRange.id;
        }

        $scope.$watch(function ()
        {
            return ctrl.selectedCategory;
        }, onCategoryOrCityChange);

        $scope.$watch(function ()
        {
            return ctrl.selectedSalaryRange;
        }, onCategoryOrCityChange);

        $scope.$watch(function ()
        {
            return ctrl.selectedCity;
        }, onCategoryOrCityChange);

        function copySearchQueryToFilter()
        {
            var search = $location.search();

            if(search.role !== ctrl.lastRole) {
                ctrl.filter = angular.copy(defaultFilter);
                ctrl.filter.role = search.role;
                ctrl.lastRole = search.role;
                return;
            }

            angular.forEach(ctrl.filter, function (value, key)
            {
                var searchValue = search[key];
                if (null != searchValue && '' !== searchValue) {
                    ctrl.filter[key] = searchValue;
                }
            });

            ctrl.lastRole = search.role;
        }

         function watchSearchQuery()
        {
            $scope.$watch(function ()
            {
                return $location.search();
            }, copySearchQueryToFilter, true);
        }

        function watchFilterAndUpdateSearchQuery()
        {
            $scope.$watch(function ()
            {
                return ctrl.filter;
            }, function ()
            {
                var filter = angular.copy(ctrl.filter);
                angular.forEach(defaultFilter, function (value, key)
                {
                    if (filter[key] === value) {
                        delete filter[key];
                    } else if ('' === filter[key]) {
                        delete filter[key];
                    }
                });
                $location.search(filter);
            }, true);
        }

        function updateSelectedCategoryWithFilter()
        {
            if (null != ctrl.filter.categories) {
                angular.forEach(ctrl.categories, function (category)
                {
                    if (ctrl.filter.categories === category.id) {
                        ctrl.selectedCategory = category;
                    }
                });
            }
        }

        function updateSelectedCityWithFilter()
        {
            if (null != ctrl.filter.city) {
                angular.forEach(ctrl.cities, function (city)
                {
                    if (ctrl.filter.city === city.id) {
                        ctrl.selectedCity = city;
                    }
                });
            }
        }

        (function init()
        {
            var promise;
            var promises = [];
            copySearchQueryToFilter();
            watchFilterAndUpdateSearchQuery();
            watchSearchQuery();

            promise = CategoriesDAO.query().then(function (data)
            {
                ctrl.categories = data;
            });
            promises.push(promise);

            promise = CitiesDAO.query().then(function (data)
            {
                ctrl.cities = data;
            });
            promises.push(promise);

            promise = SalaryRangesDAO.query().then(function (data)
            {
                ctrl.salaryRanges = data;
            });
            promises.push(promise);

            $q.all(promises).then(function ()
            {
                updateSelectedCategoryWithFilter();
                updateSelectedCityWithFilter();
                refresh = paginationSupport(ctrl, function (callback)
                {
                    UserDAO.query(ctrl.filter).then(function (results)
                    {
                        ctrl.results = results.results;
                        ctrl.total = results.total;
                        ctrl.hpTotal = results.hp_count;
                        ctrl.showTotal = true;
                        callback(results.total);
                    });
                });
                refresh();
            });
        })();
    }

    angular.module('recrutoid.vacancy').controller('SearchUsersAsAdminCtrl',
            ['$location', '$modal', '$q', '$scope', 'Authenticator', 'CurrentUser', 'ExporterDAO', 'CategoriesDAO', 'CitiesDAO', 'SalaryRangesDAO', 'Notification', 'paginationSupport',
             'recrutoidPopup', 'UserDAO', 'UsersPermissionsDAO', 'UserResumeDAO', 'gettext', SearchUsersAsAdminCtrl]);
})();
