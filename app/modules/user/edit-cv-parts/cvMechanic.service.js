(function ()
{
    'use strict';

    function cvMechanic($filter, $timeout, gettext, recrutoidPopup, $rootScope)
    {
        var publicData = {
            months: [
                {name: 'January', val: '01'},
                {name: 'February', val: '02'},
                {name: 'March', val: '03'},
                {name: 'April', val: '04'},
                {name: 'May', val: '05'},
                {name: 'June', val: '06'},
                {name: 'July', val: '07'},
                {name: 'August', val: '08'},
                {name: 'September', val: '09'},
                {name: 'October', val: '10'},
                {name: 'November', val: '11'},
                {name: 'December', val: '12'}
            ],
            currencies: ['KZT', 'USD', 'EUR'],
            years: (function ()
            {
                var years = [];
                var currentYear = new Date().getFullYear();
                for (var i = currentYear; i > currentYear - 100; i--) {
                    years.push({name: i, val: i});
                }
                return years;
            })(),
            taOptions: [
                ['ul', 'ol', 'bold', 'insertLink']
            ],
            degrees: [
                {name: 'Bachelors', val: 'Bachelors'},
                {name: 'Masters', val: 'Masters'},
                {name: 'PhD', val: 'PhD'}
            ],
            languageLevels: [
                {name: 'Basic', val: '0'},
                {name: 'Intermediate', val: '1'},
                {name: 'Advanced', val: '2'}
            ]
        };

        var errorMessage = gettext('Message_Some_fields_require_your_attention');
        var requiredMessage = gettext('Please_fill_out_this_field');
        var valueTooHighMessage = gettext('This_value_is_too_high');

        var allowedCvPartsTypes = ['education', 'workExperience', 'languages'];

        var currentItemModel = {};
        currentItemModel[allowedCvPartsTypes[0]] = {filter: {from: 0, size: 1}};
        currentItemModel[allowedCvPartsTypes[1]] = {filter: {from: 0, size: 1}};
        currentItemModel[allowedCvPartsTypes[2]] = {filter: {from: 0, size: 1}};

        var cvPartsTemplatesModel = {};
        cvPartsTemplatesModel[allowedCvPartsTypes[0]] = {};
        cvPartsTemplatesModel[allowedCvPartsTypes[1]] = {start: {}, end: {}};
        cvPartsTemplatesModel[allowedCvPartsTypes[2]] = {};

        var momentResetObj = {
            millisecond: 0,
            second: 0,
            minute: 0,
            hour: 0,
            date: 0,
            month: 0
        };

        function cleanPastedText(obj, prop)
        {
            $timeout(function ()
            {
                obj[prop] = $filter('cleanHtml')(obj[prop]);
            });
        }

        function CvMechanic(invoker)
        {
            var ctx = this;
            
            var CV = invoker.cv;
            var currentItem = angular.copy(currentItemModel);
            var cvPartsTemplates = angular.copy(cvPartsTemplatesModel);
            invoker.addNew = {};
            invoker.showEditor = {};
            invoker.hideOptions = {};
            invoker.backupItem = {};
            invoker.hideAddButtom = {};
            invoker.errors = {};

            function add(type)
            {
                invoker.errors = {};
                invoker.infoEdit = true;
                if(type == 'languages' && CV.languages.length >= 4) {
                    return;
                }

                invoker.addNew[type] = true;
                invoker.hideAddButtom[type] = true;
                $timeout(function(){
                    invoker.cv[type].push({hidden: true});
                    currentItem[type] = invoker.cv[type][invoker.cv[type].length - 1];
                    invoker.showEditor[type] = true;
                },210);
            }

            function remove(type, index)
            {
                if (!isNaN(index)){
                    CV[type].splice(index, 1);
                }

                invoker.addNew[type] = false;
                invoker.showEditor[type] = false;
            }

            function prepareCvToEdit()
            {
                angular.forEach(CV.workExperience, function (elem)
                {
                    if (elem.startDate) {
                        elem.start = {
                            year: moment(elem.startDate).get('year'),
                            month: publicData.months[moment(elem.startDate).get('month')].val   // get('month') returns number form 1 - it reflects ctrl.months array
                        };

                        if (elem.endDate) {
                            elem.end = {
                                year: moment(elem.endDate).get('year'),
                                month: publicData.months[moment(elem.endDate).get('month')].val   // get('month') returns number form 1 - it reflects ctrl.months array
                            };
                        }
                    }

                    elem.start = elem.start || {};
                    elem.end = elem.end || {};
                });
                angular.forEach(CV.education, function (elem)
                {
                    if (elem.startDate) {
                        elem.startDate = moment(elem.startDate).get('year');

                        if (elem.endDate) {
                            elem.endDate = moment(elem.endDate).get('year');
                        }
                    }
                });
            }

            this.validate = {
                main: function ()
                {
                    var isInvalid = false;
                    if (!CV.title || !CV.title.length) {
                        invoker.errors.title = requiredMessage;
                        isInvalid = true;
                    }
                    if(!CV.userData.categories.length) {
                        invoker.errors.categories = requiredMessage;
                        isInvalid = true;
                    }
                    if(!CV.userData.city) {
                        invoker.errors.city = requiredMessage;
                        isInvalid = true;
                    }
                    if(!CV.desiredSalaryRange) {
                        invoker.errors.desiredSalaryRange = requiredMessage;
                        isInvalid = true;
                    }
                    if(!CV.userData.firstName || !CV.userData.firstName.length) {
                        invoker.errors.firstName = requiredMessage;
                        isInvalid = true;
                    }
                    if(!CV.userData.lastName || !CV.userData.lastName.length) {
                        invoker.errors.lastName = requiredMessage;
                        isInvalid = true;
                    }
                    if(!CV.userData.phone || !CV.userData.phone.length) {
                        invoker.errors.phone = requiredMessage;
                        isInvalid = true;
                    }
                    if(!CV.userData.email || !CV.userData.email.length) {
                        invoker.errors.email = requiredMessage;
                        isInvalid = true;
                    }

                    return isInvalid;
                },
                education: function ()
                {
                    var isInvalid = false;
                    if(!currentItem.education.school || !currentItem.education.school.length) {
                        invoker.errors.school = requiredMessage;
                        isInvalid = true;
                    }
                    if(!currentItem.education.fieldOfStudy || !currentItem.education.fieldOfStudy.length) {
                        invoker.errors.fieldOfStudy = requiredMessage;
                        isInvalid = true;
                    }
                    if(!currentItem.education.city || !currentItem.education.city.length) {
                        invoker.errors.city = requiredMessage;
                        isInvalid = true;
                    }
                    if (!currentItem.education.degree) {
                        invoker.errors.degree = requiredMessage;
                        isInvalid = true;
                    }
                    if(!currentItem.education.startDate || !currentItem.education.endDate) {
                        invoker.errors.startDate = requiredMessage;
                        isInvalid = true;
                    }
                    if(!currentItem.education.endDate || !currentItem.education.endDate) {
                        invoker.errors.endDate = requiredMessage;
                        isInvalid = true;
                    }
                    if(currentItem.education.startDate.val > currentItem.education.endDate.val) {
                        invoker.errors.startDate = valueTooHighMessage;
                        isInvalid = true;
                    }

                    return isInvalid;
                },
                workExperience: function ()
                {
                    var isInvalid = false;

                    if(!currentItem.workExperience.title || !currentItem.workExperience.title.length) {
                        invoker.errors.jobTitle = requiredMessage;
                        isInvalid = true;
                    }

                    if(!currentItem.workExperience.company || !currentItem.workExperience.company.length) {
                        invoker.errors.jobCompany = requiredMessage;
                        isInvalid = true;
                    }

                    if(!currentItem.workExperience.city || !currentItem.workExperience.city.length) {
                        invoker.errors.jobCity = requiredMessage;
                        isInvalid = true;
                    }

                    if (!currentItem.workExperience.start) {
                        invoker.errors.workStartYear = requiredMessage;
                        invoker.errors.workStartMonth = requiredMessage;
                        isInvalid = true;
                    }else{
                        if(!currentItem.workExperience.start.year) {
                            invoker.errors.workStartYear = requiredMessage;
                            isInvalid = true;
                        }

                        if(!currentItem.workExperience.start.month) {
                            invoker.errors.workStartMonth = requiredMessage;
                            isInvalid = true;
                        }
                    }

                    //If it is current work, stop checking endDate
                    if (currentItem.workExperience.currentWork) {
                        return isInvalid;
                    }

                    if (!currentItem.workExperience.end) {
                        invoker.errors.workEndYear = requiredMessage;
                        invoker.errors.workEndMonth = requiredMessage;
                        isInvalid = true;
                    }else{
                        if(!currentItem.workExperience.end.year) {
                            invoker.errors.workEndYear = requiredMessage;
                            isInvalid = true;
                        }

                        if(!currentItem.workExperience.end.month) {
                            invoker.errors.workEndMonth = requiredMessage;
                            isInvalid = true;
                        }
                    }
                    return isInvalid;
                },
                languages: function ()
                {
                    var isInvalid = false;
                    if (!currentItem.languages.level) {
                        invoker.errors.langLevel = requiredMessage;
                        isInvalid = true;
                    }

                    if(!currentItem.languages.name) {
                        invoker.errors.langName = requiredMessage;
                        isInvalid = true;
                    }

                    if ($filter('filter')(invoker.cv.languages, {name: currentItem.languages.name.name }).length > 1) {
                        invoker.errors.langName = gettext('You_have_already_added_this_language');
                        isInvalid = true;
                    }

                    return isInvalid;
                }
            };

            function checkCorrectnessOf(type)
            {
                var isInvalid = ctx.validate[type] ? ctx.validate[type]() : false;
                if (isInvalid) {
                    recrutoidPopup('Wait!', errorMessage, 'warning');
                }

                return !isInvalid;
            }

            this.prepareCvToSubmit = function ()
            {
                var cv = angular.copy(CV);

                if (invoker.profile.email !==  CV.userData.email) {
                    CV.userData.newEmail = CV.userData.email;
                }

                if (cv.desiredSalary.value) {
                    cv.desiredSalary.value = $filter('cleanSalary')(cv.desiredSalary.value);
                }

                angular.forEach(cv.workExperience, function (elem, index)
                {
                    for (var prop in elem) {
                        if (elem.hasOwnProperty(prop) && !cvPartsTemplates.workExperience.hasOwnProperty(prop) && elem[prop]) {
                            if (elem.start.year) {
                                elem.startDate = new Date(elem.start.month +'.01.'+ elem.start.year);

                                if(!elem.currentWork) {
                                    if (elem.end.year) {
                                        elem.endDate = new Date(elem.end.month +'.01.'+ elem.end.year);                                    }
                                }
                            }
                            delete elem.start;
                            delete elem.end;

                            return;   // ends current iteration (delete will not be performed)
                        }
                    }

                    cv.workExperience.splice(index, 1);
                });
                angular.forEach(cv.education, function (elem, index)
                {
                    for (var prop in elem) {
                        if (elem.hasOwnProperty(prop) && !cvPartsTemplates.education.hasOwnProperty(prop) && elem[prop]) {
                            if (elem.startDate) {
                                elem.startDate = moment().set(momentResetObj).set({
                                    year: elem.startDate
                                }).valueOf();

                                if (elem.endDate) {
                                    elem.endDate = moment().set(momentResetObj).set({
                                        year: elem.endDate
                                    }).valueOf();
                                }
                            }

                            return;   // ends current iteration (delete will not be performed)
                        }
                    }

                    cv.education.splice(index, 1);
                });
                angular.forEach(cv.languages, function (elem, index)
                {
                    for (var prop in elem) {
                        if (elem.hasOwnProperty(prop) && !cvPartsTemplates.education.hasOwnProperty(prop) && elem[prop]) {
                            return;   // ends current iteration (delete will not be performed)
                        }
                    }

                    cv.languages.splice(index, 1);
                });

                return cv;
            };

            function editItem(type, item) {
                hideEditor(type, 'cancel');
                invoker.infoEdit = true;
                invoker.errors = {};
                $timeout(function(){
                    currentItem[type] = item;
                    invoker.backupItem[type] = angular.copy(currentItem[type]);

                    invoker.addNew[type] = false;
                    invoker.hideAddButtom[type] = true;

                    invoker.hideOptions[type] = true;
                    $timeout(function(){
                        parseItem(type, currentItem[type]);
                        invoker.showEditor[type] = true;
                    },200);
                },200);
            }

            function parseItem(type, item, save){
                switch(type){
                    case 'education':
                        if(!save) {
                            item.startDate = getByVal(publicData.years, item.startDate);
                            item.endDate = getByVal(publicData.years, item.endDate);
                            item.degree = getByVal(publicData.degrees, item.degree);
                        }else{
                            item.startDate = item.startDate.val;
                            item.endDate = item.endDate.val;
                            item.degree = item.degree.val;
                        }
                        prepareEducation();
                        break;
                    case 'workExperience':
                        if(!save) {
                            item.start.month = getByVal(publicData.months, item.start.month);
                            item.start.year = getByVal(publicData.years, item.start.year);
                            item.end.month = getByVal(publicData.months, item.end.month);
                            item.end.year = getByVal(publicData.years, item.end.year);
                        }else{
                            item.start.month = item.start.month.val;
                            item.start.year = item.start.year.val;
                            if(!item.currentWork){
                                item.end.month = item.end.month.val;
                                item.end.year = item.end.year.val;
                            }
                            item.startDate = new Date(item.start.month +'.01.'+ item.start.year).valueOf();
                            prepareWorkExperience();
                        }
                        break;
                    case 'languages':
                        if(!save) {
                            item.name = getByVal(invoker.languages, item.name);
                            item.level = getByVal(publicData.languageLevels, item.level);
                        }else{
                            item.name = item.name.val;
                            item.level = item.level.val;
                        }
                        break;
                }
            }

            function prepareWorkExperience() {
                var today = new Date();
                invoker.cv.workExperience.forEach(function(exp){
                    exp.rawEndDate = exp.endDate;
                    if (exp.currentWork) {
                        exp.rawEndDate = today;
                    }
                });
                invoker.cv.workExperience = $filter('orderBy')(invoker.cv.workExperience, '-startDate');
                invoker.cv.workExperience = $filter('orderBy')(invoker.cv.workExperience, '-endDate');
            }

            function prepareEducation() {
                invoker.cv.education = $filter('orderBy')(invoker.cv.education, '-startDate');
                invoker.cv.education = $filter('orderBy')(invoker.cv.education, '-endDate');
            }

            function hideEditor(type, operation) {
                switch(operation) {
                    case 'save':
                        if(checkCorrectnessOf(type)){
                            parseItem(type, currentItem[type], true);
                            hide();
                        }
                        break;
                    case 'cancel':

                        if (invoker.addNew[type]){
                            invoker.cv[type].splice(invoker.cv[type].length - 1, 1);
                        }else{
                            angular.forEach(invoker.backupItem[type], function(value, key){
                                invoker.currentItem[type][key] = value;
                            });
                        }

                        hide();
                        break;
                }

                function hide(){
                    invoker.cv[type].forEach(function (item) {
                        item.hidden = false;
                    });

                    invoker.infoEdit = false;
                    invoker.showEditor[type] = false;
                    $timeout(function(){
                        invoker.hideAddButtom[type] = false;
                        invoker.addNew[type] = false;
                        invoker.hideOptions[type] = false;
                    },200);
                }
            }

            function getByVal(array, val){
                return $filter('filter')(array, {val: val}, false)[0];
            }

            this.closeAllEditBoxes = function() {
                invoker.hideEditor('education', 'cancel');
                invoker.hideEditor('workExperience', 'cancel');
                invoker.hideEditor('languages', 'cancel');
            };
            
            (function init()
            {
                invoker.cleanPastedText = cleanPastedText;
                invoker.currentItem = currentItem;
                invoker.add = add;
                invoker.remove = remove;
                invoker.hideEditor = hideEditor;
                invoker.editItem = editItem;

                prepareCvToEdit();
            })();
        }

        return {
            attachData: function (context)
            {
                angular.extend(context, publicData);
            },
            getInstance: function (cvData)
            {
                return new CvMechanic(cvData);
            }
        };
    }

    angular.module('recrutoid.user').service('cvMechanic', ['$filter', '$timeout', 'gettext', 'recrutoidPopup', '$rootScope', cvMechanic]);
})();
