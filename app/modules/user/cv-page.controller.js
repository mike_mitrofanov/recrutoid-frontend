(function ()
{
    'use strict';

    function CvPageController($timeout, $location, $scope, $rootScope, UserResumeDAO, uploadCvModal)
    {
        var ctrl = this;
        ctrl.activeTab = 'my_cv';
        ctrl.changedTab = 'my_cv';

        this.setActiveTab = function (tab, cvEmpty) {
            if(cvEmpty){
                return;
            }
            ctrl.activeTab = tab;
            $timeout(function(){
                ctrl.changedTab = tab;
                $location.search({tab:tab});
            },210);
        };

        this.openUploadCvModal = function()
        {
            uploadCvModal.open();
        };

        var listener = $rootScope.$watch(function() {
                return $location.search();
            },
            function(a){
                var basePath = '/user/settings/cv-page';
                if(ctrl.activeTab !== a.tab && !!a.tab && $location.path() == basePath)
                    ctrl.setActiveTab(a.tab);
            });

        $scope.$on('$destroy', function ()
        {
            listener();
        });


        (function init()
        {
            var search = $location.search();
            if(!!search.tab && ctrl.activeTab !== search.tab)
                ctrl.setActiveTab(search.tab);
        })();
    }

    angular.module('recrutoid.user').controller('CvPageController',
            ['$timeout',
             '$location',
             '$scope',
             '$rootScope',
             'UserResumeDAO',
             'uploadCvModal',
             CvPageController]);
})();
