(function ()
{
    'use strict';

    function addNewVacancyCtrl($filter, $timeout, $q, CategoriesDAO, CitiesDAO, SalaryRangesDAO, gettext, lookForChanges, recrutoidPopup,
								unsavedChangesPopup, VacanciesDAO, $rootScope, $scope, $location)
    {
        var ctrl = this;
		ctrl.errors = {};
        var text;
        var currencies = ['KZT', 'USD', 'EUR'];
        var changeid = '';    
        var vacancyDataInitially = {
			companyName: $scope.$parent.settings.user.firstName,
			email: $scope.$parent.settings.user.email,
            publishOnWebsite: true,
            publishViaEmail: true,
            showEmailOnWebsite: true,
            salary: {
                currency: currencies[0]
            }
        };
		
        changeid = $location.$$search.id;

        this.newVacancy = angular.copy(vacancyDataInitially);

        this.taOptions = [
            ['bold', 'italics', 'underline', 'strikeThrough', 'ul', 'ol', 'redo', 'undo', 'clear'],
            ['justifyLeft', 'justifyCenter', 'justifyRight', 'indent', 'outdent']
        ];

//        this.close = function ()
//        {
//            if (lookForChanges(ctrl.newVacancy, vacancyDataInitially)) {
//                unsavedChangesPopup.open(function (saving)
//                {
//                    if (saving) {
//                        var remotelyInvokedSave = $q.defer();
//                        ctrl.add(remotelyInvokedSave);
//                        remotelyInvokedSave.promise.then(function (showSuccessPopup)
//                        {
//                            // actions to be done after published
//                        });
//                    }
//                    else {
//						$location.path('/user/settings/myVacancies');
//                    }
//                });
//            }
//            else {
//                $location.path('/user/settings/myVacancies');
//            }
//        };

        this.cleanPastedText = function ()
        {
            $timeout(function ()
            {
                ctrl.newVacancy.details = $filter('cleanHtml')(ctrl.newVacancy.details);
            });
        };

        this.formatSalary = function ()
        {
            ctrl.newVacancy.salary.value = $filter('formatSalary')(ctrl.newVacancy.salary.value);
        };

        this.changeCurrency = function ()
        {
            ctrl.newVacancy.salary.currency = currencies[(currencies.indexOf(ctrl.newVacancy.salary.currency) + 1) % 3];
        };

        this.add = function (remotelyInvokedSave)
        {
            function publish()
            {
                ctrl.newVacancy.categories = ctrl.newVacancy.categories.id;
                ctrl.newVacancy.cityId = ctrl.newVacancy.cityId.id;
                ctrl.newVacancy.salaryRangeId = ctrl.newVacancy.salaryRangeId.id;
                VacanciesDAO.update(ctrl.newVacancy).then(function ()
                {
                    if (remotelyInvokedSave) {
                        remotelyInvokedSave.resolve();
                    }

                    $scope.$parent.settings.setActiveSite('myVacancies');
                    //$modalInstance.close();
                }).catch(function ()
                {
                    text = gettext('Message_Somethings_Went_Wrong');
                    recrutoidPopup('Warning', text, 'warning');
                    if (remotelyInvokedSave) {
                        remotelyInvokedSave.reject();
                    }
                });
            }


			if (validateForm()) {
                publish();
                return;
            }

            if (remotelyInvokedSave) {
                remotelyInvokedSave.reject();
            }
        };
		

		function validateForm() {
            var isValid = true;
            var requiredMessage = "Please_fill_out_this_field";


            if (!ctrl.newVacancy.title) {
                ctrl.errors.title = requiredMessage;
                isValid = false;
            }
			if (!ctrl.newVacancy.categories) {
				ctrl.errors.categories = requiredMessage;
				isValid = false;
            }
            if (!ctrl.newVacancy.cityId) {
                ctrl.errors.cityId = requiredMessage;
                isValid = false;
            }
			if (!ctrl.newVacancy.salaryRangeId) {
                ctrl.errors.salaryRangeId = requiredMessage;
                isValid = false;
            }
            if (!ctrl.newVacancy.details) {
                ctrl.errors.details = requiredMessage;
                isValid = false;
            }

            if (!isValid) {
                recrutoidPopup(gettext('Warning!'), gettext('Message_Some_fields_require_your_attention'), 'warning');
            }

            return isValid;
        }


        (function init()
        {
            CategoriesDAO.query().then(function (data)
            {
                ctrl.categories = data;

                if (changeid) {
                    ctrl.editing = true;
                    VacanciesDAO.get(changeid).then(function (vacancy)
                    {
                        for (var i = 0; i < ctrl.categories.length; i++) {
                            if (vacancy.categories[0].id === ctrl.categories[i].id) {
                                vacancy.categories = ctrl.categories[i];
                                break;
                            }
                        }
                        vacancyDataInitially = vacancy;

                        ctrl.newVacancy = angular.copy(vacancyDataInitially);
                    });
                }

                return CitiesDAO.query();
            }).then(function (data)
            {
                ctrl.cities = data;
                return SalaryRangesDAO.query();
            }).then(function (data)
            {
                ctrl.salaryRanges = data;
                $rootScope.siteLoaded = true;
            });
        })();
    }

    angular.module('recrutoid.user').controller('addNewVacancyCtrl',
            ['$filter',
             '$timeout',
             '$q',
             'CategoriesDAO',
             'CitiesDAO',
              'SalaryRangesDAO',
             'gettext',
             'lookForChanges',
             'recrutoidPopup',
             'unsavedChangesPopup',
             'VacanciesDAO',
             '$rootScope',
             '$scope',
             '$location',
             addNewVacancyCtrl]);
})();
