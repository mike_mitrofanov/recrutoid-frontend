(function ()
{
    'use strict';

    function EditCvModalController($q, CommonDAO, CurrentUser, cvMechanic, gettext, lookForChanges, recrutoidPopup,
                                   unsavedChangesPopup, UserResumeDAO, modalSettings, $modalInstance)
    {
        var ctrl = this;
        var initialCvData;

        cvMechanic.attachData(this);

        function fetchAndPrepareCV(cv)
        {
            var promise;
            if (cv) {
                var defer = $q.defer();
                defer.resolve(cv);
                promise = defer.promise;
            }
            else {
                promise = CurrentUser.require().then(function (user)
                {
                    return UserResumeDAO.get(user.id);
                });
            }

            return promise.then(function (cv)
            {
                ctrl.cv = cv;

                ctrl.cvMechanic = cvMechanic.getInstance(ctrl);

                ctrl.cv.userId = ctrl.cv.userData.id;
                initialCvData = angular.copy(ctrl.cv);
            });
        }

        this.close = function ()
        {
            if (lookForChanges(ctrl.cv, initialCvData)) {
                unsavedChangesPopup.open(function (saving)
                {
                    if (saving) {
                        var remotelyInvokedSave = $q.defer();
                        ctrl.submitOnePart(remotelyInvokedSave);
                        remotelyInvokedSave.promise.then(function ()
                        {
                            $modalInstance.close();
                        });
                    }
                    else {
                        $modalInstance.dismiss();
                    }
                });
            }
            else {
                $modalInstance.dismiss();
            }
        };

        this.submitOnePart = function (remotelyInvokedSave)
        {
            var message;
            if (ctrl.cvMechanic.validate[modalSettings.partType]) {
                message = ctrl.cvMechanic.validate[modalSettings.partType]();
            }
            if (message) {
                recrutoidPopup('Warning!', message, 'warning');
                return;
            }

            UserResumeDAO.save(ctrl.cvMechanic.prepareCvToSubmit()).then(function ()
            {
                recrutoidPopup('Success!', gettext('Message_Successfully_Updated'), 'success');
                if (remotelyInvokedSave) {
                    remotelyInvokedSave.resolve();
                }
                $modalInstance.close();
            }).catch(function ()
            {
                if (remotelyInvokedSave) {
                    remotelyInvokedSave.reject();
                }
            });
        };

        (function init()
        {
            ctrl.partType = modalSettings.partType;
            ctrl.partName = modalSettings.partName;

            CommonDAO.getEditResumeData().then(function (data)
            {
                ctrl.categories = data.categories;
                ctrl.cities = data.cities;
                ctrl.languages = data.languages.results.map(function (lang)
                {
                    return {name: lang.language, val: lang.language};
                });
                ctrl.languages.unshift({name: 'Language'});

                fetchAndPrepareCV(data.cv).then(function ()
                {
                    ctrl.showCvOnHome = ctrl.cv.userData.showOnHomePage;
                    ctrl.viewReady = true;
                });
            });
        })();
    }

    angular.module('recrutoid.user').controller('EditCvModalController',
            ['$q',
             'CommonDAO',
             'CurrentUser',
             'cvMechanic',
             'gettext',
             'lookForChanges',
             'recrutoidPopup',
             'unsavedChangesPopup',
             'UserResumeDAO',
             'modalSettings',
             '$modalInstance',
             EditCvModalController]);
})();
