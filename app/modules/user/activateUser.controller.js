(function ()
{

    'use strict';

    function ActivateUserCtrl($routeParams, $location, DistributorsDAO, recrutoidPopup, gettext)
    {
        var title = gettext('Congratulations');
        var text = gettext('Message_You_Have_Successfully_Confirmed_Your_Account');
        var confirm = gettext('Message_Ok_Thanks');
        DistributorsDAO.confirm($routeParams.token).then(function ()
        {
            recrutoidPopup({
                title: title,
                text: text,
                html: true,
                type: 'success',
                showCancelButton: false,
                confirmButtonText: confirm,
                closeOnConfirm: true
            }, function (isConfirm)
            {
                if (isConfirm) {
                    $location.path('#/');
                }
            });
        });
    }

    angular.module('recrutoid.user').controller('ActivateUserCtrl',
            ['$routeParams', '$location', 'DistributorsDAO', 'recrutoidPopup', 'gettext', ActivateUserCtrl]);
})();
