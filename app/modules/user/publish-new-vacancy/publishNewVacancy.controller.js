(function ()
{
    'use strict';

    function PublishNewVacancyController($filter, $modalInstance, $timeout, $q, CategoriesDAO, CitiesDAO, SalaryRangesDAO, gettext, lookForChanges, recrutoidPopup,
                                         unsavedChangesPopup, VacanciesDAO, vacancyData)
    {
        var ctrl = this;
        var text;
        var currencies = ['KZT', 'USD', 'EUR'];

        var vacancyDataInitially = {
            companyName: vacancyData.companyName,
            email: vacancyData.email,
            publishOnWebsite: true,
            publishViaEmail: false,
            showEmailOnWebsite: true,
            salary: {
                currency: currencies[0]
            }
        };

        this.newVacancy = angular.copy(vacancyDataInitially);

        this.taOptions = [
            ['bold', 'italics', 'underline', 'strikeThrough', 'ul', 'ol', 'redo', 'undo', 'clear'],
            ['justifyLeft', 'justifyCenter', 'justifyRight', 'indent', 'outdent']
        ];

        this.close = function ()
        {
            if (lookForChanges(ctrl.newVacancy, vacancyDataInitially)) {
                unsavedChangesPopup.open(function (saving)
                {
                    if (saving) {
                        var remotelyInvokedSave = $q.defer();
                        ctrl.add(remotelyInvokedSave);
                        remotelyInvokedSave.promise.then(function (showSuccessPopup)
                        {
                            // actions to be done after published
                        });
                    }
                    else {
                        $modalInstance.dismiss();
                    }
                });
            }
            else {
                $modalInstance.dismiss();
            }
        };

        this.cleanPastedText = function ()
        {
            $timeout(function ()
            {
                ctrl.newVacancy.details = $filter('cleanHtml')(ctrl.newVacancy.details);
            });
        };

        this.formatSalary = function ()
        {
            ctrl.newVacancy.salary.value = $filter('formatSalary')(ctrl.newVacancy.salary.value);
        };

        this.changeCurrency = function ()
        {
            ctrl.newVacancy.salary.currency = currencies[(currencies.indexOf(ctrl.newVacancy.salary.currency) + 1) % 3];
        };

        this.add = function (remotelyInvokedSave)
        {
            function publish()
            {
                ctrl.newVacancy.categories = [
                    ctrl.newVacancy.categories.id
                ];
                ctrl.newVacancy.cityId = ctrl.newVacancy.cityId.id;
                ctrl.newVacancy.salaryRangeId = ctrl.newVacancy.salaryRangeId.id;
                VacanciesDAO.update(ctrl.newVacancy).then(function ()
                {
                    if (remotelyInvokedSave) {
                        remotelyInvokedSave.resolve();
                    }
                    $modalInstance.close();
                }).catch(function ()
                {
                    text = gettext('Message_Somethings_Went_Wrong');
                    recrutoidPopup('Warning', text, 'warning');
                    if (remotelyInvokedSave) {
                        remotelyInvokedSave.reject();
                    }
                });
            }

            if (!ctrl.newVacancy.title) {
                text = gettext('Message_Title_Field_Is_Incorrect');
                recrutoidPopup('Warning', text, 'warning');
            }
            else if (!ctrl.newVacancy.categories) {
                text = gettext('Message_Category_Field_Is_Incorrect');
                recrutoidPopup('Warning', text, 'warning');
            }
            else if (!ctrl.newVacancy.cityId) {
                text = gettext('Message_City_Field_Is_Incorrect');
                recrutoidPopup('Warning', text, 'warning');
            }
            else if (!ctrl.newVacancy.details) {
                text = gettext('Message_Details_Field_Is_Incorrect');
                recrutoidPopup('Warning', text, 'warning');
            }
            else {
                publish();
                return;
            }

            if (remotelyInvokedSave) {
                remotelyInvokedSave.reject();
            }
        };

        (function init()
        {
            CategoriesDAO.query().then(function (data)
            {
                ctrl.categories = data;

                if (vacancyData.id) {
                    ctrl.editing = true;
                    VacanciesDAO.get(vacancyData.id).then(function (vacancy)
                    {
                        for (var i = 0; i < ctrl.categories.length; i++) {
                            if (vacancy.categories[0].id === ctrl.categories[i].id) {
                                vacancy.categories = ctrl.categories[i];
                                break;
                            }
                        }
                        vacancyDataInitially = vacancy;
                        ctrl.newVacancy = angular.copy(vacancyDataInitially);
                    });
                }

                return CitiesDAO.query();
            }).then(function (data)
            {
                ctrl.cities = data;
                return SalaryRangesDAO.query();
            }).then(function (data)
            {
                ctrl.salaryRanges = data;
            });
        })();
    }

    angular.module('recrutoid.user').controller('PublishNewVacancyController',
            ['$filter',
             '$modalInstance',
             '$timeout',
             '$q',
             'CategoriesDAO',
             'CitiesDAO',
              'SalaryRangesDAO',
             'gettext',
             'lookForChanges',
             'recrutoidPopup',
             'unsavedChangesPopup',
             'VacanciesDAO',
             'vacancyData',
             PublishNewVacancyController]);
})();
