(function ()
{
    'use strict';

    function UserSubscriptions($q, $scope, $rootScope, $location, $routeParams, CitiesDAO, SalaryRangesDAO, CurrentUser, lookForChanges, recrutoidModals,
                               UserDAO, UserSettingsChangePage, uploadAvatar, recrutoidPopup, unsavedChangesPopup, gettext, CategoriesDAO)
    {
        var ctrl = this;

        this.password = {};
        var text;
        var warningDisplay = gettext('Warning');
        var successDisplay = gettext('Success');

        var initialProfileData;
        var shouldLookForChanges = true;

        this.save = function (remotelyInvokedSave)
        {
            var payload = {profile:{}};

            payload.profile.id = ctrl.profile.id;
            payload.profile.subscription = ctrl.profile.subscription;

            UserDAO.saveMyProfile(payload).then(function ()
            {
                shouldLookForChanges = true;
                ctrl.saved = true;
                $scope.$emit('event:userDataHasChanged', payload.profile);
                if (initialProfileData.email !== ctrl.profile.email) {
                    text =
                        gettext('Message_Profile_Successfully_Updated_The_Confirmation_Link_Was_Sent_To_Your_New_Email');
                }
                else {
                    text = gettext('Message_Profile_Successfully_Updated');
                }
                initialProfileData = angular.copy(ctrl.profile);
                recrutoidPopup(successDisplay, text, 'success');
                if(remotelyInvokedSave) {
                    remotelyInvokedSave.resolve();   // invoking save method from popup about unsaved changes
                }
            }).catch(function (error)
            {
                if (error.status) {
                    text = gettext('Message_Something_Went_Wrong');
                }
                recrutoidPopup(warningDisplay, text, 'warning');
                if(remotelyInvokedSave) {
                    remotelyInvokedSave.reject();
                }
            });
        };


        this.deleteCategory = function(index){
            ctrl.profile.subscription.categories.splice(index,1);
        };

        this.resend = function() {

        };

        $scope.$watch(function() {
            return ctrl.profile;
        }, function() {
            ctrl.saved = false;
        }, true);

        (function init()
        {
            CitiesDAO.query().then(function (cities)
            {
                ctrl.cities = cities;
                ctrl.citiesSubscription = angular.copy(cities);
                ctrl.citiesSubscription.unshift({
                    name: {
                        ru: 'Все города',
                        en: 'All cities'
                    },
                    id: null
                });
                return CategoriesDAO.query();
            }).then(function (categories)
            {
                ctrl.categories = categories;
                return SalaryRangesDAO.query();
            }).then(function (salaryRanges)
            {
                ctrl.salaryRanges = salaryRanges;
                ctrl.salaryRanges.unshift({id: null, name: {en: 'All salary ranges', ru: 'Все диапазоны окладов'}});
                return UserDAO.getMyProfile();
            }).then(function (profile)
            {
                ctrl.profile = profile;

                if (ctrl.profile.subscription) {
                    if (null === ctrl.profile.subscription.city) {
                        ctrl.profile.subscription.city = ctrl.citiesSubscription[0];
                    }
                    if (null === ctrl.profile.subscription.desiredSalaryRange) {
                        ctrl.profile.subscription.desiredSalaryRange = ctrl.salaryRanges[0];
                    }
                }

                if ('employer' === ctrl.profile.role) {
                    ctrl.isEmployer = true;
                }
                ctrl.loaded = true;
                delete ctrl.profile.$promise;
                initialProfileData = angular.copy(ctrl.profile);
                $rootScope.siteLoaded = true;
            }).catch(function ()
            {
                $location.path('/');
            });
        })();
    }

    angular.module('recrutoid.user').controller('UserSubscriptions',
        ['$q', '$scope', '$rootScope', '$location', '$routeParams', 'CitiesDAO', 'SalaryRangesDAO', 'CurrentUser', 'lookForChanges', 'recrutoidModals',
            'UserDAO', 'UserSettingsChangePage', 'uploadAvatar', 'recrutoidPopup', 'unsavedChangesPopup', 'gettext', 'CategoriesDAO', UserSubscriptions]);
})();
