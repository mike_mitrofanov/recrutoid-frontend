(function ()
{
    'use strict';

    function ExtendProfileController($location, $routeParams, recrutoidPopup, UserDAO, gettext)
    {
        var text = gettext('Message_Your_Profile_Publication_Has_Been_Successfully_Extended');
        var successDisplay = gettext('Success');
        UserDAO.extendProfile($routeParams.token).then(function ()
        {
            recrutoidPopup(successDisplay, text, 'success');
            $location.path('/');
        });
    }

    angular.module('recrutoid.user').controller('ExtendProfileController',
            ['$location', '$routeParams', 'recrutoidPopup', 'UserDAO', 'gettext', ExtendProfileController]);
})();
