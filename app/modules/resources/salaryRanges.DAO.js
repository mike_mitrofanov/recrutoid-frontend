(function () {
    'use strict';

    function SalaryRangesDAO($resource) {
        var api = $resource('/api/salary-ranges/:a/:b', null, {
            query: {method: 'GET', isArray: true},
            update: {method: 'POST'},
            remove: {method: 'DELETE'}
        });
        return {
            query: function () {
                return api.query().$promise;
            },
            update: function(newName) {
                return api.update(newName).$promise;
            },
            remove: function(id, newId) {
                return api.remove({a: id, b: newId}).$promise;
            }
        };
    }

    angular.module('recrutoid.resources').factory('SalaryRangesDAO', ['$resource', SalaryRangesDAO]);
})();
