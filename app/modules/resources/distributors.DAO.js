angular.module('recrutoid.resources').factory('DistributorsDAO',
        ['$resource', function ($resource)
        {
            'use strict';
            var api = $resource('/api/distributors/:a/:b', null, {
                query: {method: 'GET', isArray: false},
                changeState: {method: 'POST'}

            });

            return {
                query: function ()
                {
                    return api.query().$promise;
                },
                verify: function (id)
                {
                    return api.save({user: id}).$promise;
                },
                resendEmail: function (id)
                {
                    return api.save({a: 'resend'}, {id: id}).$promise;
                },
                remove: function (id)
                {
                    return api.remove({a: id}).$promise;
                },
                confirm: function (token)
                {
                    return api.changeState({a: token, b: 'verifyByToken'}, {}).$promise;
                }
            };
        }]);
