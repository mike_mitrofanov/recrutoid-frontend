(function ()
{
    'use strict';

    function CategoriesDAO($resource)
    {
        var api = $resource('/api/categories/:a/:b', null, {
            query: {method: 'GET', isArray: true},
            update: {method: 'POST'},
            remove: {method: 'DELETE'},
            getObject: {method: 'GET', isArray: false}
        });
        return {
            query: function ()
            {
                return api.query().$promise;
            },
            update: function (newName)
            {
                return api.update(newName).$promise;
            },
            remove: function (id, newId)
            {
                return api.remove({a: id, b: newId}).$promise;
            },
            getAllCategoriesWithCount: function ()
            {
                return api.getObject({a: 'count'}).$promise;
            }
        };
    }

    angular.module('recrutoid.resources').factory('CategoriesDAO', ['$resource', CategoriesDAO]);
})();
