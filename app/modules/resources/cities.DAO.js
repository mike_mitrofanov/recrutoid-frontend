(function () {
    'use strict';

    function CitiesDAO($resource) {
        var api = $resource('/api/cities/:a/:b', null, {
            query: {method: 'GET', isArray: true},
            update: {method: 'POST'},
            remove: {method: 'DELETE'}
        });
        return {
            query: function (showOilGasCamp) {
                return api.query({a: showOilGasCamp?true:false}).$promise;
            },
            update: function(newName) {
                return api.update(newName).$promise;
            },
            remove: function(id, newId) {
                return api.remove({a: id, b: newId}).$promise;
            }
        };
    }

    angular.module('recrutoid.resources').factory('CitiesDAO', ['$resource', CitiesDAO]);
})();
