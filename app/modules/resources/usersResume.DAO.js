(function ()
{
    'use strict';

    function UserResumeDAO($resource, Upload)
    {
        var api = $resource('/api/users/:a/:b/:c', null, {
            get: {method: 'GET', params: {b: 'resume'}},
            save: {method: 'POST', params: {b: 'resume'}},
            markAllSeen: {method: 'POST', params: {b: 'resume', c: 'mark-seen'}},
            callForInterview: {method: 'POST', params: {b: 'call-for-interview'}},
            find: {method: 'GET', params: {a: 'find'}},
            isNew: {method: 'POST', params: {a: 'is-new'}},
            cancelCvCheckup: {method: 'POST', params: {a: 'resume', b: 'cancel'}},
            cvActiveNotify:  {method: 'POST', params: {a: 'resume', b: 'notify-active'}},
            views: {method: 'GET', params: {b: 'resume', c: 'views'}, isA8awrray: false}
        });

        return {
            get: function (id)
            {
                return api.get({a: id}).$promise;
            },
            save: function (resume)
            {
                return api.save({a: resume.userId}, resume).$promise;
            },
            callForInterview: function (resumeId, vacancyId)
            {
                return api.callForInterview({a: resumeId}, {vacancyId: vacancyId}).$promise;
            },
            queryViews: function (resumeId, filter)
            {
                filter.a = resumeId;
                return api.views(filter).$promise;
            },
            markAllSeen: function (userId, views)
            {
                return api.markAllSeen({a: userId}, views).$promise;
            },
            uploadCv: function (file, data, fileExtension)
            {
                return Upload.upload({
                    url: '/api/users/resume/upload', file: file, fields: {cv: data, fileExtension: fileExtension}
                });
            },
            cancelCvCheckup: function (isImpersonated)
            {
                return api.cancelCvCheckup({is_impersonated: isImpersonated}).$promise;
            },
            cvActiveNotify: function (userData)
            {
                return api.cvActiveNotify(userData).$promise;
            },
            getNewResumeViewsCount: function (userId)
            {
                return api.get({b: 'fresh-resume-views'}).$promise;
            },
            showCvOnHome: function (resumeId, showCvOnHome)
            {
                return api.save({a: resumeId, b: 'resume', c: 'show-on-home'}, {showCvOnHome: showCvOnHome}).$promise;
            },
            forwardCv: function (email)
            {
                return api.save({c: 'forward'}, {email: email}).$promise;
            },
            find: function (filter)
            {
                return api.find(filter).$promise;
            },
            isNew: function (ids)
            {
                return api.isNew(ids).$promise;
            }
        };
    }

    angular.module('recrutoid.resources').factory('UserResumeDAO', ['$resource', 'Upload', UserResumeDAO]);
})();
