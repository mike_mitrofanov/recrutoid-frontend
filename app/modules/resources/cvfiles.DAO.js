(function ()
{
    'use strict';

    function CvfilesDAO($resource, Upload)
    {
        var api = $resource('/api/cvfiles/:a/:b/:c', null, {
            query: {method: 'GET', isArray: false}
        });

        return {
            query: function (filter)
            {
                return api.query(filter).$promise;
            },
            remove: function (id)
            {
                return api.remove({a: id}).$promise;
            }
        };
    }

    angular.module('recrutoid.resources').factory('CvfilesDAO', ['$resource', 'Upload', CvfilesDAO]);
})();
