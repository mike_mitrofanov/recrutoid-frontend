(function ()
{
    'use strict';

    function SubscriptionsDAO($resource)
    {
        var api = $resource('/api/subscriptions/:a/:b', null, {
            changeState: {method: 'POST'},
            update: {method: 'POST'},
            remove: {method: 'DELETE'},
            query: {method: 'GET', isArray: false}
        });
        return {
            get: function (token)
            {
                return api.get({a: token}).$promise;
            },
            confirm: function (token)
            {
                return api.changeState({a: token, b: 'confirm'}, {}).$promise;
            },
            verify: function (id)
            {
                return api.changeState({a: id, b: 'verify'}, {}).$promise;
            },
            ban: function (id)
            {
                return api.changeState({a: id, b: 'ban'}, {}).$promise;
            },
            unban: function (id)
            {
                return api.changeState({a: id, b: 'un-ban'}, {}).$promise;
            },
            remove: function (id)
            {
                return api.remove({a: id, b: 'remove'}).$promise;
            },
            unsubscribe: function (id)
            {
                return api.remove({a: id, b: 'unsubscribe'}).$promise;
            },
            update: function (subscriberData)
            {
                return api.update(subscriberData).$promise;
            },
            query: function (filter)
            {
                return api.query(filter).$promise;
            }
        };
    }

    angular.module('recrutoid.resources').factory('SubscriptionsDAO', ['$resource', SubscriptionsDAO]);
})();
