(function ()
{
    'use strict';

    function BlacklistedDomainsDAO($resource)
    {
        var api = $resource('/api/banned-domains/:a', null, {
            query: {method: 'GET'},
            update: {method: 'POST'},
            remove: {method: 'DELETE'},
            checkEmailInBlackListed: {method: 'POST', params: {a: 'check-email'}}
        });
        return {
            query: function (filter)
            {
                return api.query(filter).$promise;
            },
            update: function (newName)
            {
                return api.update(newName).$promise;
            },
            remove: function (id)
            {
                return api.remove({a: id}).$promise;
            },
            checkEmailInBlackListed: function (email)
            {
                return api.checkEmailInBlackListed(email).$promise;
            }
        };
    }

    angular.module('recrutoid.resources').factory('BlacklistedDomainsDAO', ['$resource', BlacklistedDomainsDAO]);
})();
