(function ()
{
    'use strict';

    function CommonDAO($resource)
    {
        var api = $resource('/api/:a/:b');

        return {
            getHomeData: function() {
                return api.get({a: 'home'}).$promise;
            },
            getNavBarData: function ()
            {
                return api.get({a: 'nav-bar'}).$promise;
            },
            isProfileActive: function ()
            {
                return api.get({a: 'is-profile-active'}).$promise;
            },
            getEditResumeData: function ()
            {
                return api.get({a: 'edit-resume'}).$promise;
            },
            sendEmail: function (emailBody)
            {
                return api.save({a: 'email', b: 'send'}, emailBody).$promise;
            },
            getSearchVacanciesData: function ()
            {
                return api.get({a: 'search-vacancies'}).$promise;
            },
            getSearchUsersData: function ()
            {
                return api.get({a: 'search-users'}).$promise;
            }
        };
    }

    angular.module('recrutoid.resources').factory('CommonDAO', ['$resource', CommonDAO]);

})();
