(function ()
{
    'use strict';

    function UsersPermissionsDAO($resource)
    {
        var api = $resource('/api/users/:id/:a/:b', null, {});

        return {
            grant: function (userId, permission)
            {
                return api.save({id: userId, a: 'grant', b: permission}, {}).$promise;
            },
            deny: function (userId, permission) {
                return api.save({id: userId, a: 'deny', b: permission}, {}).$promise;
            }
        };
    }

    angular.module('recrutoid.resources').factory('UsersPermissionsDAO', ['$resource', UsersPermissionsDAO]);
})();
