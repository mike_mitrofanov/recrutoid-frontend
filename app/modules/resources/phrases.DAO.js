(function () {
    'use strict';

    function PhrasesDAO($resource) {
        var api = $resource('/api/phrases/:a/:b/:c', null, {
            query: {method: 'GET'},
            update: {method: 'POST'},
            remove: {method: 'DELETE'},
            add: {method: 'POST', params: {a: 'add'}}

        });
        return {
            query: function (filter)
            {
                return api.query(filter).$promise;
            },
            update: function(data) {
                return api.update(data).$promise;
            },
            remove: function (id)
            {
                return api.remove({a: id}).$promise;
            },
            add: function(data) {
                return api.add(data).$promise;
            }
        };
    }

    angular.module('recrutoid.resources').factory('PhrasesDAO', ['$resource', PhrasesDAO]);
})();
