(function ()
{
    'use strict';

    function UserDAO($resource, Upload)
    {
        var api = $resource('/api/users/:a/:b/:c', null, {
            authenticate: {method: 'POST', params: {a: 'auth'}},
            impersonate: {method: 'POST', params: {a: 'impersonate'}},
            ban: {method: 'POST', params: {b: 'ban'}},
            unban: {method: 'POST', params: {b: 'unban'}},
            verify: {method: 'POST', params: {b: 'verify'}},
            resendRegistrationEmail: {method: 'POST', params: {b: 'resend-registration-email'}},
            me: {method: 'GET', params: {a: 'me'}},
            meOrNull: {method: 'GET', params: {a: 'me', b: 'optional'}},
            register: {method: 'POST', params: {a: 'register'}},
            searchVisited: {method: 'POST', params: {a: 'search-visited'}},
            activate: {method: 'POST', params: {a: 'activate'}},
            check: {method: 'POST', params: {a: 'check'}},
            checkEmailAvailability: {method: 'POST', params: {a: 'check-email'}},
            query: {method: 'GET', isArray: false},
            resetPassword: {method: 'POST', params: {a: 'init-password-reset'}},
            setPassword: {method: 'POST', params: {a: 'set-password'}},
            checkResetPasswordToken: {method: 'GET', params: {a: 'token'}},
            extendProfile: {method: 'POST', params: {a: 'extend'}},
            carousel: {method: 'GET', params: {a: 'carousel'}},
            getFiveMostActiveOnSiteEmployers: {method: 'GET', isArray: true},
			isVisited: {method: 'POST', params: {a: 'is-visited'}},
            isVisitedList: {method: 'POST', params: {a: 'is-visited-list'}}
        });

        return {
            authenticate: function (email, password)
            {
                return api.authenticate({email: email, password: password}).$promise;
            },
            impersonate: function (userId)
            {
                return api.impersonate({userId: userId}).$promise;
            },
            ban: function (id)
            {
                return api.ban({a: id}, {}).$promise;
            },
            unban: function (id)
            {
                return api.unban({a: id}, {}).$promise;
            },
            verify: function (id)
            {
                return api.verify({a: id}, {}).$promise;
            },
            getMe: function ()
            {
                return api.me().$promise;
            },
            getMeOrNull: function ()
            {
                return api.meOrNull().$promise;
            },
            check: function (emailOrNickName)
            {
                return api.check({emailOrNickName: emailOrNickName}).$promise;
            },
            checkEmailAvailability: function (email)
            {
                return api.checkEmailAvailability({email: email}).$promise;
            },
            query: function (filter)
            {
                return api.query(filter).$promise;
            },
            register: function (user)
            {
                return api.register(user).$promise;
            },
            searchVisited: function (user)
            {
                return api.searchVisited(user).$promise;
            },
            resendRegistrationEmail: function (id)
            {
                return api.resendRegistrationEmail({a: id}, {}).$promise;
            },
            getMyProfile: function ()
            {
                return api.get({a: 'my-profile'}).$promise;
            },
            saveMyProfile: function (data)
            {
                return api.save({a: 'my-profile'}, data).$promise;
            },
            save: function (banner)
            {
                return api.save(banner).$promise;
            },
            uploadAvatar: function (data)
            {
                return Upload.upload({
                    url: '/api/users/avatar', file: data
                });
            },
            remove: function (id)
            {
                return api.remove({a: id}).$promise;
            },
            unremove: function(id)
            {
                return api.remove({a: id, b:'revert'}).$promise;
            },
            activate: function (token)
            {
                return api.activate(token).$promise;
            },
            resetPassword: function (email)
            {
                return api.resetPassword(email).$promise;
            },
            setPassword: function (newPasswordAndToken)
            {
                return api.setPassword(newPasswordAndToken).$promise;
            },
            checkResetPasswordToken: function (token)
            {
                return api.checkResetPasswordToken({b: token}).$promise;
            },
            extendProfile: function (token)
            {
                return api.extendProfile({b: token}, {}).$promise;
            },
            applicationsNotifications: function (value)
            {
                return api.save({a: 'applications-notifications'}, {applicationsNotifications: value}).$promise;
            },
            carousel: function ()
            {
                return api.carousel().$promise;
            },
            confirmEmail: function (token)
            {
                return api.save({a: 'confirm-email'}, token).$promise;
            },
            getFiveMostActiveOnSiteEmployers: function ()
            {
                return api.getFiveMostActiveOnSiteEmployers({a: 'five-employers'}).$promise;
            },
            newAdmin: function (newAdmin)
            {
                return api.save({a: 'admin', b: 'new'}, newAdmin).$promise;
            },
			isVisitedList: function (ids)
            {
                return api.isVisitedList(ids).$promise;
            },
            isVisited: function (data)
            {
                return api.isVisited(data).$promise;
            }
        };
    }

    angular.module('recrutoid.resources').factory('UserDAO', ['$resource', 'Upload', UserDAO]);
})();
