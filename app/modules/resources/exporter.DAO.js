(function () {
    'use strict';

    function ExporterDAO($resource, $http) {
        var api = $resource('/api/exporter/:a/:b', null, {
            query: {
                method: 'POST',
                responseType : 'arraybuffer',
                transformResponse: function(data, headersGetter) {
                    // Stores the ArrayBuffer object in a property called "data"
                    return { data : data };
                }
            }
        });
        return {
            query: function (exporterData, exporterType, originalFilters) {
                var filters = (JSON.parse(JSON.stringify(originalFilters))); // clone the object

                filters.size = 'all'; // always retrieve all records for xls and pdf generation

                if ('website' === filters.publishMode) {
                    filters.publishOnWebsite = true;
                } else if ('email' === filters.publishMode) {
                    filters.publishViaEmail = true;
                }

                delete filters.from;
                delete filters.adminPanel;
                delete filters.publishMode;

                angular.forEach(filters, function (val, key) {
                    if (null === val || '' === val) {
                        delete filters[key];
                    }
                    if ('false' === val) {
                        filters[key] = false;
                    }
                    if ('true' === val) {
                        filters[key] = true;
                    }
                });
                
                return api.query({a: exporterData, b: exporterType}, filters).$promise;
            },
            download: function (data, filename) {
                var file = new Blob([ data.data ], {
                    type : 'application/csv'
                });
                //trick to download store a file having its URL
                var fileURL = URL.createObjectURL(file);
                var a         = document.createElement('a');
                a.href        = fileURL;
                a.target      = '_blank';
                a.download    = filename;
                document.body.appendChild(a);
                a.click();
            }
        };
    }

    angular.module('recrutoid.resources').factory('ExporterDAO', ['$resource', '$http', ExporterDAO]);
})();
