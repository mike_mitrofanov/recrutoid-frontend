angular.module('recrutoid.resources').factory('EmailDAO',
        ['$resource', function ($resource)
        {
            'use strict';
            var api = $resource('/api/email/:a', null, {
                sendEmail: {method: 'POST'}
            });
            return {
                sendEmailToActiveSubscribe: function (id)
                {
                    return api.sendEmail({a: 'subscribe'}, {id: id}).$promise;
                },
                sendEmailToActiveVacancy: function (id)
                {
                    return api.sendEmail({a: 'vacancy'}, {id: id}).$promise;
                }
            };
        }]);
