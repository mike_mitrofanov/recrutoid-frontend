(function ()
{
    'use strict';

    function BannersDAO($resource, Upload)
    {
        var api = $resource('/api/banners/:a/:b', null, {
            query: {isArray: false},
            activate: {params: {b: 'activate'}, method: 'POST'},
            deactivate: {params: {b: 'deactivate'}, method: 'POST'},
            getPublished: {params: {a: 'published'}, method: 'GET'}
        });

        return {
            query: function (filter)
            {
                return api.query(filter).$promise;
            },
            save: function (banner)
            {
                return api.save(banner).$promise;
            },
            remove: function (bannerId)
            {
                return api.remove({a: bannerId}).$promise;
            },
            get: function(bannerId) {
                return api.get({a: bannerId}).$promise;
            },
            upload: function (file, banner)
            {
                return Upload.upload({
                    url: '/api/banners/upload',
                    fields: banner,
                    file: file
                });
            },
            activate: function(banner)
            {
                return api.activate({a: banner.id}, {location: banner.location}).$promise;
            },
            deactivate: function(id)
            {
                return api.deactivate({a: id}, {}).$promise;
            },
            getPublished: function()
            {
                return api.getPublished().$promise;
            }
        };
    }

    angular.module('recrutoid.resources').factory('BannersDAO', ['$resource', 'Upload', BannersDAO]);

})();
