(function ()
{
    'use strict';

    function VacanciesDAO($resource)
    {
        var api = $resource('/api/vacancies/:a/:b/:c', null, {
            changeState: {method: 'POST'},
            isNew: {method: 'POST', params: {a: 'is-new'}},
            isNotVisited: {method: 'POST', params: {a: 'is-visited'}},
            isNotVisitedList: {method: 'POST', params: {a: 'is-visited-list'}},
            dispatchVacancies: {method: 'POST', params: {a: 'dispatch'}},
            update: {method: 'POST'},
            remove: {method: 'DELETE'},
            markAsSeen: {method: 'POST', params: {b: 'applications'}},
            applyTo: {method: 'POST', params: {b: 'apply'}},
            application: {method: 'GET', params: {b: 'applications'}},
            query: {method: 'GET', isArray: false},
            find: {method: 'GET', isArray: false, params: {a: 'find'}},
            extendPublication: {method: 'POST', params: {a: 'extend-publication'}}
        });
        return {
            get: function (token)
            {
                return api.get({a: token}).$promise;
            },
            confirm: function (confirmData)
            {
                return api.changeState({a: confirmData.token, b: 'confirm'}, confirmData).$promise;
            },
            dispatchVacancies: function ()
            {
                return api.dispatchVacancies().$promise;
            },
            isNew: function (ids)
            {
                return api.isNew(ids).$promise;
            },
            isNotVisitedList: function (ids)
            {
                return api.isNotVisitedList(ids).$promise;
            },
            isNotVisited: function (data)
            {
                return api.isNotVisited(data).$promise;
            },
            verify: function (id)
            {
                return api.changeState({a: id, b: 'verify'}, {}).$promise;
            },
            verifyAll: function (id)
            {
                return api.changeState({a: id, b: 'verifyAll'}, {}).$promise;
            },
            approve: function (id)
            {
                return api.changeState({a: id, b: 'approve'}, {}).$promise;
            },
            ban: function (id)
            {
                return api.changeState({a: id, b: 'ban'}, {}).$promise;
            },
            unban: function (id)
            {
                return api.changeState({a: id, b: 'unban'}, {}).$promise;
            },
            remove: function (id)
            {
                return api.remove({a: id}).$promise;
            },
            removeArray: function (array)
            {
                return api.remove({ids: array}).$promise;
            },
            update: function (vacancyData)
            {
                return api.update(vacancyData).$promise;
            },
            query: function (filter)
            {
                return api.query(filter).$promise;
            },
            applyTo: function (id)
            {
                return api.applyTo({a: id}, {}).$promise;
            },
            queryApplications: function (id, filter)
            {
                filter.a = id;
                return api.application(filter).$promise;
            },
            removeApplication: function (vacancyId, applications)
            {
                return api.remove({a: vacancyId, b: 'applications', ids: applications}).$promise;
            },
            markApplicationAsSeen: function (vacancyId, applications)
            {
                return api.markAsSeen({a: vacancyId, b: 'applications'}, {ids: applications}).$promise;
            },
            extendPublication: function (token)
            {
                return api.extendPublication({b: token}, {}).$promise;
            },
            getNewApplicationsCount: function ()
            {
                return api.get({a: 'new-applications-count'}).$promise;
            },
            find: function (filter)
            {
                var filterCopy = JSON.parse(JSON.stringify(filter));
                
                if(filterCopy.location ) { 
                    filterCopy.city = filterCopy.location;
                    delete filterCopy.location;
                }
                 
                return api.find(filterCopy).$promise;
            }
        };
    }

    angular.module('recrutoid.resources').factory('VacanciesDAO', ['$resource', VacanciesDAO]);
})();
