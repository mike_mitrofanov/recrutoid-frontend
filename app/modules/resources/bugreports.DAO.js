(function () {
    'use strict';

    function BugreportsDAO($resource) {
        var api = $resource('/api/bugreports/:a', null, {
            query: {method: 'GET'},
            update: {method: 'POST'},
            remove: {method: 'DELETE'}
        });
        return {
            query: function (filter)
            {
                return api.query(filter).$promise;
            },
            update: function(data) {
                return api.update(data).$promise;
            },
            remove: function(id) {
                return api.remove({a: id}).$promise;
            }
        };
    }

    angular.module('recrutoid.resources').factory('BugreportsDAO', ['$resource', BugreportsDAO]);
})();
