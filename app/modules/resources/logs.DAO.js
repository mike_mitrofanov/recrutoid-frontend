(function ()
{
    'use strict';

    function LogsDAO($resource)
    {
        var api = $resource('/api//errors/email:a', null, {
            getAll: {method: 'GET', isArray: false},
            remove: {method: 'DELETE'},
            get: {method: 'GET'}
        });

        return {
            query: function (filter)
            {
                return api.getAll(filter).$promise;
            },
            remove: function (pageId)
            {
                return api.remove({a: pageId}).$promise;
            },
            get: function(slug) {
                return api.get({a: slug}).$promise;
            }
        };
    }

    angular.module('recrutoid.resources').factory('LogsDAO', ['$resource', LogsDAO]);

})();
