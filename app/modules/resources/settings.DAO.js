(function ()
{
    'use strict';

    function SettingsDAO($resource)
    {
        var api = $resource('/api/settings/:a', null, {
            getUserExpirationTime: {method: 'GET', params: {a: 'user-expiration'}},
            query: {isArray: false}
        });

        return {
            getUserExpirationTime: function ()
            {
                return api.getUserExpirationTime().$promise;
            },
            query: function ()
            {
                return api.query().$promise;
            },
            save: function (settings)
            {
                return api.save(settings).$promise;
            },
            siteSettings: function ()
            {
                return api.query({a: 'site-settings'}).$promise;
            },
            statistics: function ()
            {
                return api.get({a: 'statistics'}).$promise;
            }
        };
    }

    angular.module('recrutoid.resources').factory('SettingsDAO', ['$resource', SettingsDAO]);

})();
