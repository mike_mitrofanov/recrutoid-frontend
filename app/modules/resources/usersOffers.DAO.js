(function ()
{
    'use strict';

    function UserOffersDAO($resource)
    {
        var api = $resource('/api/users/offers/:a/:b', null, {
            get: {method: 'GET'},
            markAllSeen: {method: 'GET'},
            remove: {method: 'DELETE'},
            query: {method: 'GET', isArray: false}
        });

        return {
            get: function ()
            {
                return api.get().$promise;
            },
            remove: function(offerId) {
                return api.remove({a: offerId}).$promise;
            },
            query: function (filter)
            {
                return api.query(filter).$promise;
            },
            removeArray: function (array)
            {
                return api.remove(array, {}).$promise;
            },
            markOfferSeen: function (offerId)
            {
                return api.save({a: offerId, b: 'seen'}, {}).$promise;
            },
            markAllSeen: function ()
            {
                return api.markAllSeen({a: 'mark-all-seen'}).$promise;
            },
            getNewOffersViewsCount: function ()
            {
                return api.get({a: 'fresh-offers-views'}).$promise;
            }
        };
    }

    angular.module('recrutoid.resources').factory('UserOffersDAO', ['$resource', UserOffersDAO]);
})();
