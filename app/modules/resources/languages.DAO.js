angular.module('recrutoid.resources').factory('LanguagesDAO',
        ['$resource', function ($resource)
        {
            'use strict';
            var api = $resource('/api/languages/:a', null, {
                query: {method: 'GET', isArray: false},
                save: {method: 'POST'},
                remove: {method: 'DELETE'}
            });

            return {
                query: function ()
                {
                    return api.query().$promise;
                },
                save: function (language)
                {
                    return api.save(language).$promise;
                },
                remove: function (languageId)
                {
                    return api.remove({a: languageId}).$promise;
                }
            };
        }]);
