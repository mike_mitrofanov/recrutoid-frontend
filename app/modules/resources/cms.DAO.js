(function ()
{
    'use strict';

    function CmsDAO($resource, Upload)
    {
        var api = $resource('/api/cms/:a', null, {
            getAll: {method: 'GET', isArray: false},
            save: {method: 'POST'},
            remove: {method: 'DELETE'},
            get: {method: 'GET'}
        });

        return {
            getList: function ()
            {
                return api.query({a: 'list'}).$promise;
            },
            getNews: function ()
            {
                return api.query({a: 'news'}).$promise;
            },
            query: function (filter)
            {
                return api.getAll(filter).$promise;
            },
            save: function (page)
            {
                return api.save(page).$promise;
            },
            remove: function (pageId)
            {
                return api.remove({a: pageId}).$promise;
            },
            get: function(slug) {
                return api.get({a: slug}).$promise;
            },
            uploadNewsImage: function (file, page)
            {
                return Upload.upload({
                    url: '/api/cms/news/image', file: file, fields: {page: page}
                });
            }
        };
    }

    angular.module('recrutoid.resources').factory('CmsDAO', ['$resource', 'Upload', CmsDAO]);

})();
