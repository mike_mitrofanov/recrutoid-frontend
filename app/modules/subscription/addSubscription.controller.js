(function ()
{

    'use strict';

    function AddSubscriptionCtrl($scope, SubscriptionsDAO, CategoriesDAO, CitiesDAO, recrutoidPopup, 
								Authenticator, CurrentUser, gettext, emailValidator)
    {
        var ctrl = this;
        ctrl.showEmail = true;
        var text;
        var warningDisplay = gettext('Warning!');
        var successDisplay = gettext('Congratulations!');
        this.errors = {};

        function init()
        {
            CurrentUser.resolve().then(function (user)
            {
                if(user.email) {
                    ctrl.showEmail = false;
                }
            });
        }

        this.subscription = {
            interval: 'daily'
        };

        this.selectedCategories = [];
        this.addSubscriber = function ()
        {
            if(!ctrl.isFormValid()) {
                return handleErrors();
            }

            var promise;

            ctrl.subscription.categories = ctrl.selectedCategories.map(function (elem)
            {
                return elem.id;
            });

            ctrl.subscription.city = ctrl.selectedCity.id || null;
            ctrl.subscription.desiredSalaryRange = ctrl.selectedSalaryRange.id || null;

            if (Authenticator.isAuthenticated()) {
                promise = CurrentUser.resolveOrReject().then(function (data)
                {
                    ctrl.subscription.email = data.email;
                    return SubscriptionsDAO.update(ctrl.subscription);
                });
            } else {
                promise = SubscriptionsDAO.update(ctrl.subscription);
            }

            promise.then(function ()
            {
                clearForm();

                var message;
                if (Authenticator.isAuthenticated()) {
                    message = gettext('Message_We_Have_Added_Your_Subscription');
                } else {
                    message = gettext('Message_You_Are_One_Step_Away_From_Completing_Subscription');
                }
                recrutoidPopup(successDisplay, message, 'success');
            });

        };

        function handleErrors() {
            ctrl.errors = {};

            if (0 >= ctrl.selectedCategories.length) {
                ctrl.errors.category = true;
            }

            if (!ctrl.selectedCity) {
                ctrl.errors.city = true;
            }

            if (!ctrl.selectedSalaryRange) {
                ctrl.errors.salaryRange = true;
            }

            if (!Authenticator.isAuthenticated() && !emailValidator.isValid(ctrl.subscription.email)) {
                ctrl.errors.email = true;
            }
        }

        this.isFormValid = function() {
            return ctrl.isEmailValid() && ctrl.selectedSalaryRange && ctrl.selectedCity && ctrl.selectedCategories.length;
        };

		this.isEmailValid = function() {
			return emailValidator.isValid(ctrl.subscription.email) || Authenticator.isAuthenticated();
		};


		function clearForm()
		{
			ctrl.subscription.email = '';
			ctrl.selectedCategories = [];
			ctrl.selectedCity = undefined;
			ctrl.selectedSalaryRange = undefined;
		}

        $scope.$watch(function() {
            return ctrl.selectedCategories;
        }, function() {
            ctrl.errors = {};
        }, true);
		
        init();

        var destroyLogOutListener = $scope.$root.$on('event:auth-loggedOut', function ()
        {
            ctrl.showEmail = true;
        });

        var destroyLogInListener = $scope.$root.$on('event:auth-loggedIn', function ()
        {
            ctrl.showEmail = false;
        });

		$scope.$on('event:auth-loggedOut', clearForm);

        $scope.$on('$destroy', function ()
        {
            destroyLogOutListener();
            destroyLogInListener();
        });
    }

    angular.module('recrutoid.subscription').controller('AddSubscriptionCtrl',
            ['$scope', 'SubscriptionsDAO', 'CategoriesDAO', 'CitiesDAO', 'recrutoidPopup', 
				'Authenticator', 'CurrentUser', 'gettext', 'emailValidator', AddSubscriptionCtrl]);
})();
