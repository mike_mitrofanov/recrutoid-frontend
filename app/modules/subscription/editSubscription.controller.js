(function ()
{

    'use strict';

    function EditSubscriptionCtrl($routeParams, $location, $timeout, SubscriptionsDAO, CategoriesDAO, SalaryRangesDAO, CitiesDAO, recrutoidPopup, gettext)
    {
        var ctrl = this;
        this.id = $routeParams.id;
        this.subscription = {categories: []};
        this.allCategories = {name: {en: 'All categories', ru: 'Все категории'}};
        this.allCity = {name: {en: 'All cities', ru: 'Все города'}};
        this.allSalaryRanges = {name: {en: 'All salary ranges', ru: 'Все диапазоны окладов'}};
        var text;
        var successDisplay = gettext('Success');
        function init()
        {
            CitiesDAO.query().then(function (result)
            {
                result.unshift(ctrl.allCity);
                ctrl.cities = result;
                return CategoriesDAO.query();
            }).then(function (result)
            {
                ctrl.categories = result;
                return SalaryRangesDAO.query();
            }).then(function (result)
            {
                result.unshift(ctrl.allSalaryRanges);
                ctrl.salaryRanges = result;
                return SubscriptionsDAO.get(ctrl.id);
            }).then(function (data)
            {
                if(null === data.city) {
                    data.city = ctrl.allCity;
                }
                if(null === data.categories[0]) {
                    data.categories = [];
                }
                if(null === data.desiredSalaryRange) {
                    data.desiredSalaryRange = ctrl.allSalaryRanges;
                }

                console.log(data);

                ctrl.subscription = data;

                if($location.search().hasOwnProperty('unsubscribe')) {
                    ctrl.resign();
                }
            });

            $timeout(function(){
                $('.categories-select').find('.ui-select-match').prepend('<i class="fa fa-caret2 fa-angle-down caret pull-right" ng-click="$select.toggle($event)"></i>');
            },0);

        }

        this.saveSubscriber = function ()
        {
            var subscription = angular.copy(ctrl.subscription);
            if(null == subscription.categories[0]) {
                subscription.categories = [null];
            }
            else {
                subscription.categories = subscription.categories.map(function (elem)
                {
                    return elem.id;
                });
            }
            subscription.city = subscription.city.id || null;
            subscription.desiredSalaryRange = subscription.desiredSalaryRange.id || null;
            SubscriptionsDAO.update(subscription).then(function ()
            {
                ctrl.savedSubscription = angular.copy(ctrl.subscription);
                // text = gettext('Message_Your_Subscription_Has_Been_Changed');
                // recrutoidPopup(successDisplay, text, 'success');
                // $location.path('/');
            });
        };

        this.isSaved = function() {
            return angular.equals(ctrl.savedSubscription, ctrl.subscription);
        };

        this.deleteCategory = function(index){
            ctrl.subscription.categories.splice(index,1);
        };

        this.resign = function ()
        {
            var title = gettext('message_are_you_sure');
            var confirm = gettext('Message_Yes_I_Am');
            text = gettext('Message_Your_Subscription_Will_Be_Deleted');
            recrutoidPopup({
                title: title,
                type: 'warning',
                text: text,
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: confirm,
                closeOnConfirm: false
            }, function (isConfirm)
            {
                if (isConfirm) {
                    SubscriptionsDAO.unsubscribe($routeParams.id).then(function ()
                    {
                        text = gettext('Message_We_Have_Deleted_Your_Subscription');
                        recrutoidPopup(successDisplay, text, 'success');
                        $location.path('/');
                    });
                }
            });
        };

        init();
    }

    angular.module('recrutoid.subscription').controller('EditSubscriptionCtrl',
            ['$routeParams', '$location', '$timeout', 'SubscriptionsDAO', 'CategoriesDAO', 'SalaryRangesDAO', 'CitiesDAO', 'recrutoidPopup', 'gettext', EditSubscriptionCtrl]);
})();
