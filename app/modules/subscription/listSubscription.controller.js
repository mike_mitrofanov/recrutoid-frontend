(function ()
{

    'use strict';

    function ListSubscriptionCtrl($location, $q, $scope, EmailDAO, ExporterDAO, SubscriptionsDAO,
                                CategoriesDAO, CitiesDAO, Notification, paginationSupport, recrutoidPopup,
                                gettext, SalaryRangesDAO)
    {
        var ctrl = this;
        var refresh;

        var text;
        var warningDisplay = gettext('Warning');

        this.allCity = {name: {en: 'All cities', ru: 'Все города'}};
        var defaultFilter = {
            from: 0,
            size: 50,
            verified: null,
            banned: null,
            interval: null,
            categories: null,
            city: null,
            desiredSalaryRange: null,
            email: null,
            subscribed: null
        };
        this.filter = angular.copy(defaultFilter);

        this.manualVerifying = function (id)
        {
            SubscriptionsDAO.verify(id).then(function ()
            {
                refresh();
                text = gettext('Message_Success_Subscriber_Verified');
                Notification.success(text);
            });
        };

        this.selectAll = function () {
            ctrl.subscriberList.forEach(function (subscriber){
                subscriber.selected = ctrl.allSelected;
            });
        };
        
        this.doActionOnSelected = function(action)
        {
            if(-1 === ['remove', 'ban', 'unban', 'resend'].indexOf(action)) {
                return;
            }

            var ids = [], promises = [];

            var title = gettext('Warning');
            var text = gettext('Are you sure to ' + action + ' selected?');
            if(action == 'resend') {
                text = gettext('Message_Are_You_Sure_To_Resend_Confirmation_Links_To_Selected');
            }
            var confirm = gettext('popup_yes_i_am_button');
            recrutoidPopup({
                title: title,
                text: text,
                html: true,
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: confirm,
                closeOnConfirm: true
            }, function (isConfirm)
            {
                if (isConfirm) {
                    angular.forEach(ctrl.subscriberList, function (elem)
                    {
                        if(elem.selected) {
                            ids.push(elem.id);
                        }
                        delete elem.selected;
                    });

                    ctrl.allSelected = false;

                    angular.forEach(ids, function (id)
                    {
                        if(action == 'resend') {
                            promises.push(EmailDAO.sendEmailToActiveSubscribe(id))
                        }else{
                            promises.push(SubscriptionsDAO[action](id));
                        }
                    });

                    if(promises.length) {
                        $q.all(promises).then(function ()
                        {
                            refresh();
                            var text;
                            if('remove' === action) {
                                text = gettext('Message_Selected_Subscriptions_Have_Been_Removed');
                            }
                            else if('ban' === action) {
                                text = gettext('Message_Selected_Subscriptions_Have_Been_Banned');
                            }
                            else if('unban' === action) {
                                text = gettext('Message_Selected_Subscriptions_Have_Been_Unbanned.');
                            } else if ('resend' === action) {
                                text = gettext('Emails has been sent to selected subscriptions');
                            }
                            Notification.success(text);
                        });
                    }
                }
            });
        };

        this.deleteUser = function (id)
        {
            text = gettext('message_are_you_sure');
            var confirm = gettext('popup_yes_i_am_button');
            recrutoidPopup({
                title: warningDisplay,
                text: text,
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: confirm,
                closeOnConfirm: true
            }, function (isConfirm)
            {
                if (isConfirm) {
                    SubscriptionsDAO.remove(id).then(function ()
                    {
                        refresh();
                        text = gettext('Message_Success_User_Deleted');
                        Notification.success(text);
                    });
                }
            });
        };

        this.resendEmail = function (subscription)
        {
            EmailDAO.sendEmailToActiveSubscribe(subscription.id).then(refresh);
            text = gettext('Message_Success_Mail_Sent');
            Notification.success(text);
        };

        function onCategoryOrCityChange(newValue, oldValue)
        {
            if (newValue === oldValue) {
                return;
            }
            ctrl.filter.categories = ctrl.selectedCategory && ctrl.selectedCategory.id;
            ctrl.filter.city = ctrl.selectedCity && ctrl.selectedCity.id;
            ctrl.filter.desiredSalaryRange = ctrl.selectedSalaryRange && ctrl.selectedSalaryRange.id;
        }

        $scope.$watch(function ()
        {
            return ctrl.selectedCategory;
        }, onCategoryOrCityChange);

        $scope.$watch(function ()
        {
            return ctrl.selectedCity;
        }, onCategoryOrCityChange);

        $scope.$watch(function ()
        {
            return ctrl.selectedSalaryRange;
        }, onCategoryOrCityChange);

        function copySearchQueryToFilter()
        {
            var search = $location.search();
            angular.forEach(ctrl.filter, function (value, key)
            {
                var searchValue = search[key];
                if (null != searchValue && '' !== searchValue) {
                    ctrl.filter[key] = searchValue;
                }
            });
        }

        function updateSelectedSalaryRangeWithFilter()
        {
            if (null != ctrl.filter.salaryRanges) {
                angular.forEach(ctrl.salaryRanges, function (salaryRange)
                {
                    if (ctrl.filter.salaryRange === salaryRange.id) {
                        ctrl.selectedSalaryRange = salaryRange;
                    }
                });
            }
        }

        function watchFilterAndUpdateSearchQuery()
        {
            $scope.$watch(function ()
            {
                return ctrl.filter;
            }, function ()
            {
                var filter = angular.copy(ctrl.filter);
                angular.forEach(defaultFilter, function (value, key)
                {
                    if (filter[key] === value) {
                        delete filter[key];
                    } else if ('' === filter[key]) {
                        delete filter[key];
                    }
                });
                $location.search(filter);
                ctrl.allSelected = false;
            }, true);
        }

        function updateSelectedCategoryWithFilter()
        {
            if (null != ctrl.filter.categories) {
                angular.forEach(ctrl.categories, function (category)
                {
                    if (ctrl.filter.categories === category.id) {
                        ctrl.selectedCategory = category;
                    }
                });
            }
        }

        function updateSelectedCityWithFilter()
        {
            if (null != ctrl.filter.city) {
                angular.forEach(ctrl.cities, function (city)
                {
                    if (ctrl.filter.city === city.id) {
                        ctrl.selectedCity = city;
                    }
                });
            }
        }

        ctrl.clearPresets = function ()
        {
            ctrl.filter = angular.copy(defaultFilter);
            ctrl.selectedCity = null;
            ctrl.selectedCategory = null;
            ctrl.selectedSalaryRange = null;
        };

        this.banned = function banned(id)
        {
            SubscriptionsDAO.ban(id).then(refresh);
        };
        this.unban = function unban(id)
        {
            SubscriptionsDAO.unban(id).then(refresh);
        };

        this.downloadAsPDF = function() {
            ExporterDAO.query('subscriptions', 'pdf', ctrl.filter).then(function (data)
            {
                ExporterDAO.download(data, 'subscriptions.pdf');
            });
        };

        this.downloadAsXLS = function() {
            ExporterDAO.query('subscriptions', 'xls', ctrl.filter).then(function (data)
            {
                ExporterDAO.download(data, 'subscriptions.xlsx');
            });
        };

        (function init()
        {
            copySearchQueryToFilter();
            watchFilterAndUpdateSearchQuery();
            
            var promise, promises = [];
            promise = CategoriesDAO.query().then(function (data)
            {
                ctrl.categories = data;
                ctrl.categories.unshift({
                    id: 'all',
                    name: {
                        en: "All",
                        ru: "все"
                    }
                });
            });
            promises.push(promise);
            promise = CitiesDAO.query().then(function (data)
            {
                ctrl.cities = data;
                ctrl.cities.unshift({
                    id: 'all',
                    name: {
                        en: "All",
                        ru: "все"
                    }
                });
            });
            promises.push(promise);
            promise = SalaryRangesDAO.query().then(function (data)
            {
                ctrl.salaryRanges = data;
                ctrl.salaryRanges.unshift({
                    id: 'all',
                    name: {
                        en: "All",
                        ru: "все"
                    }
                });
            });
            promises.push(promise);
            $q.all(promises).then(function ()
            {
                updateSelectedCategoryWithFilter();
                updateSelectedCityWithFilter();
                updateSelectedSalaryRangeWithFilter();
                refresh = paginationSupport(ctrl, function (callback)
                {
                    SubscriptionsDAO.query(ctrl.filter).then(function (data)
                    {
                        ctrl.subscriberList = data.results;
                        ctrl.total = data.total;
                        ctrl.showTotal = true;
                        callback(data.total);
                    });
                });
                refresh();
            });

        })();
    }

    angular.module('recrutoid.subscription').controller('ListSubscriptionCtrl',
            ['$location',
             '$q',
             '$scope',
             'EmailDAO',
             'ExporterDAO',
             'SubscriptionsDAO',
             'CategoriesDAO',
             'CitiesDAO',
             'Notification',
             'paginationSupport',
             'recrutoidPopup',
             'gettext',
             'SalaryRangesDAO',
             ListSubscriptionCtrl]);
})();
