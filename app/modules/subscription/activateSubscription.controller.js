(function ()
{
    'use strict';

    function ActivateSubscriptionCtrl($routeParams, SubscriptionsDAO, subscriptionDetails, gettext)
    {
        SubscriptionsDAO.confirm($routeParams.token).then(function (data)
        {
            console.log(data);
            subscriptionDetails.open({message: gettext('Message_You_Have_Successfully_Confirmed_Your_Subscription'), subscription: data});
        });
    }


    angular.module('recrutoid.subscription').controller('ActivateSubscriptionCtrl',
            ['$routeParams', 'SubscriptionsDAO', 'subscriptionDetails', 'gettext', ActivateSubscriptionCtrl]);
})();
