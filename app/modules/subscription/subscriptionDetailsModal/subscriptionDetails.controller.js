(function ()
{
    'use strict';

    function SubscriptionDetailsController($modalInstance, $location, subscriptionData)
    {
        var ctrl = this;

        this.message = subscriptionData.message;
        this.subscription = subscriptionData.subscription;
        subscriptionData.subscription.categories[0] = subscriptionData.subscription.categories[0] || {name: {en: 'All categories', ru: 'Все категории'}};
        this.allCity = {name: {en: 'All cities', ru: 'Все города'}};
        this.allSalaryRanges = {name: {en: 'All salary ranges', ru: 'Все диапазоны окладов'}};

        this.edit = function ()
        {
            $modalInstance.dismiss();
            $location.path('/subscription/edit/' + ctrl.subscription.token);
        };

        this.close = function ()
        {
            $modalInstance.dismiss();
            $location.path('/');
        };
        function init()
        {
            $location.path('/');
        }

        init();
    }

    angular.module('recrutoid.vacancy').controller('SubscriptionDetailsController',
            ['$modalInstance', '$location', 'subscriptionData', SubscriptionDetailsController]);
})();
