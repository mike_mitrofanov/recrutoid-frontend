(function ()
{
    'use strict';

    function subscriptionDetails($modal)
    {
        function open(subscriptionData)
        {
            return $modal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: 'modules/subscription/subscriptionDetailsModal/subscriptionDetails.tpl.html',
                controller: 'SubscriptionDetailsController as ctrl',
                windowClass: 'subscription-details-window',
                size: 'md',
                resolve: {
                    subscriptionData: function() {
                        return subscriptionData;
                    }
                }
            }).result;
        }

        return {
            open: open
        };
    }

    angular.module('recrutoid.subscription').service('subscriptionDetails', ['$modal', subscriptionDetails]);

})();
