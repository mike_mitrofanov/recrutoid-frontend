(function ()
{
    'use strict';

    function searchColumnMover()
    {

        var padding = 10;

        var minOffset;
        var maxOffset;
        var vacancyPreview;
        var filtersBox;

        var lastVacancyOffset = 0;
        var lastOffset = 0;
        var initHeight = 557;
        var scrollingTop = false;
        var itemContainer;
        var lastBoxOffset;
        var containerHeight;

        function updateSizes() {
            var leftHeight = $('#desktop-filters').height();
            itemContainer = $('#search-items-container');
            
            if( itemContainer.height()<leftHeight) {
             
                 itemContainer.height(leftHeight);
                 
            } else {
                itemContainer.css('height', 'auto');
            }
            
            containerHeight = itemContainer.height();
            
            
            lastBoxOffset = itemContainer.offset().top;
            minOffset = itemContainer.offset().top;
            maxOffset = itemContainer.offset().top + itemContainer.height() - vacancyPreview.height();
        }

        function init() {
            itemContainer = $('#search-items-container');
            vacancyPreview = $('#vacancy-preview-box');
            filtersBox = $('#desktop-filters');

            filtersBox.unbind('hover');
            $(".job-seeker-box", document.body).hover(function (event) {
                updateSizes();
                handleStickOnHover($(this));
            });


            $(window).off('scroll.search');
            containerHeight = itemContainer.height();
            var lastScroll = 0;
            $(window).on('scroll.search', function () {
                
                var scroll = $(this).scrollTop();
                scrollingTop = lastScroll > scroll;
                lastScroll = scroll;
                updateSizes();
                handleStickOnScroll(containerHeight);
            });

            lastBoxOffset = itemContainer.offset().top;
            minOffset = itemContainer.offset().top;
            maxOffset = itemContainer.offset().top + itemContainer.height() - vacancyPreview.height();
            updateSizes();
            if (vacancyPreview.offset()) {

                vacancyPreview.offset({top: vacancyPreview.offset().top});

                resetAll();

                $(".job-seeker-box").first().trigger('hover')
            }

        }

        function resetAll() {
             updateSizes();
            lastVacancyOffset = lastBoxOffset;

            vacancyPreview.height(initHeight);
            vacancyPreview.removeClass('stickTop');
            vacancyPreview.removeClass('stickBottom');
            vacancyPreview.offset({top: minOffset});

            filtersBox.removeClass('stickTop');
            filtersBox.removeClass('stickBottom');
            filtersBox.offset({top: minOffset});
        }

        function handleStickOnHover(seekerBox) {

            var offsetTop = seekerBox.offset().top;
            var desiredOffset = offsetTop - vacancyPreview.height() / 2;
            lastOffset = desiredOffset;

            setTimeout(function () {
                lastVacancyOffset = desiredOffset;

                //check if hovered item is above center of preview box
                var isAboveCenter = getWindowOffset(seekerBox).top <= getWindowOffset(vacancyPreview).top + (vacancyPreview.height() * 0.3);

                //check if hovered item is under center of preview box
                var isUnderCenter = getWindowOffset(seekerBox).bottom <= getWindowOffset(vacancyPreview).bottom + (vacancyPreview.height() * 0.3);

                if (isAboveCenter || isUnderCenter) {
                    if (desiredOffset == lastOffset) {

                        resetAndRemoveSticky();
                        movePreviewBox(desiredOffset, true);
                    }
                }
            }, 250);
        }

        function handleStickOnScroll(containerHeight) {

            if (!vacancyPreview.offset()) {
                return;
            }

            var desiredOffset;
            var windowTopOffset = vacancyPreview.offset().top - $(document).scrollTop();
            var animated = false;
            desiredOffset = vacancyPreview.offset().top - windowTopOffset + padding;

            var desiredWindowBottomOffset = $(window).height() - (vacancyPreview.offset().top - $(document).scrollTop()) - vacancyPreview.height();

            if (desiredOffset <= lastVacancyOffset) {
                if (desiredWindowBottomOffset > 150) {
                    animated = true;
                }

                resetAndRemoveSticky();
                desiredOffset = lastVacancyOffset;
            }



            if (containerHeight > vacancyPreview.height()) {
                movePreviewBox(desiredOffset, animated);
            } else {
                movePreviewBox(0, animated);
            }

//            if (containerHeight > filtersBox.height()) {
//                moveFiltersBox();
//            }
              moveFiltersBox();

        }

        function movePreviewBox(desiredOffset, isAnimated) {
            updateSizes();
            var desiredWindowTopOffset = desiredOffset - $(document).scrollTop();
            var desiredWindowBottomOffset = $(window).height() - (desiredOffset - $(document).scrollTop()) - vacancyPreview.height();
            var valid = true;

            //if want to go too way up
            if (desiredWindowTopOffset < padding) {
                desiredOffset = desiredOffset - desiredWindowTopOffset + padding;
            }

            //if want to go too way down
            if (desiredWindowBottomOffset < padding) {

                desiredOffset = desiredOffset + desiredWindowBottomOffset - padding;
                
            }

            //if want to go underneath container
            if (desiredOffset > maxOffset) {
                desiredOffset = maxOffset;
                valid = false;
            }

            //if want to go above container
            if (desiredOffset < minOffset) {
                desiredOffset = minOffset;
                valid = false;
            }

            if (!valid) {
                removeSticky();
            }

            resizeBoxIfNeeded(desiredOffset);

            if (isAnimated) {
                vacancyPreview.animate({
                    top: (desiredOffset - minOffset) + padding
                }, 'fast', function () {
                    makeSticky(valid, desiredOffset);
                });
            } else {
                vacancyPreview.stop();
                vacancyPreview.offset({top: desiredOffset});
                makeSticky(valid, desiredOffset);
            }



        }

        function makeSticky(valid, desiredOffset) {
            if (getWindowOffset(vacancyPreview, desiredOffset).top <= 10 && valid) {
                vacancyPreview.removeClass('stickBottom');
                vacancyPreview.addClass('stickTop');
            } else if (getWindowOffset(vacancyPreview, desiredOffset).bottom <= 10 && valid) {
                vacancyPreview.removeClass('stickTop');
                vacancyPreview.addClass('stickBottom');
            }
        }

        function removeSticky() {
            vacancyPreview.removeClass('stickTop');
            vacancyPreview.removeClass('stickBottom');
        }

        function resetAndRemoveSticky() {
            if (vacancyPreview.hasClass('stickTop')) {
                removeSticky();
                vacancyPreview.offset({top: $(document).scrollTop() + padding});
            }

            if (vacancyPreview.hasClass('stickBottom')) {
                removeSticky();
                vacancyPreview.offset({top: $(document).scrollTop() + $(window).height() - vacancyPreview.height() - padding});
            }
        }
        function moveFiltersBox() {
            
            var maxOffset = itemContainer.offset().top + itemContainer.height() - filtersBox.height();
            var isHigherThanWindow = filtersBox.height() + padding > $(window).height();
            var windowDifference = isHigherThanWindow ? padding : $(window).height() - filtersBox.height() - padding;

            lastBoxOffset = filtersBox.offset().top;

            if (getWindowOffset(itemContainer).top - padding <= 0 && getWindowOffset(itemContainer).bottom - windowDifference < 0) {
                if (isHigherThanWindow) {
                    if (!scrollingTop) {
                        //stick to bottom
                        if (filtersBox.hasClass('stickTop')) {
                            filtersBox.removeClass('stickTop');
                            filtersBox.offset({top: lastBoxOffset});
                        }

                        if (getWindowOffset(filtersBox).top < getWindowOffset(filtersBox).bottom && getWindowOffset(filtersBox).bottom >= padding) {
                            if (!filtersBox.hasClass('stickBottom')) {
                                filtersBox.addClass('stickBottom');
                            }
                        }
                    } else {
                        //stick to top
                        if (filtersBox.hasClass('stickBottom')) {
                            filtersBox.removeClass('stickBottom');
                            filtersBox.offset({top: lastBoxOffset});
                        }

                        if (getWindowOffset(filtersBox).top > getWindowOffset(filtersBox).bottom && getWindowOffset(filtersBox).top >= padding) {
                            if (!filtersBox.hasClass('stickTop')) {
                                filtersBox.addClass('stickTop');
                            }
                        }
                    }
                } else {
                    //always stick to top if there is nothing to scroll
                    filtersBox.removeClass('stickBottom');

                    if (filtersBox.hasClass('stickBottom')) {
                        filtersBox.removeClass('stickBottom');
                        filtersBox.offset({top: lastBoxOffset});
                    }

                    if (!filtersBox.hasClass('stickTop')) {
                        filtersBox.addClass('stickTop');
                    }
                }
            } else {
                //stop sticking if out of bounds
                lastBoxOffset = getWindowOffset(itemContainer).top - padding <= 0 ? maxOffset : minOffset;

                filtersBox.removeClass('stickTop');
                filtersBox.removeClass('stickBottom');
                filtersBox.offset({top: lastBoxOffset});
            }

            if (parseInt(filtersBox.css('top'))<0) {
                filtersBox.css('top', 0);
            }
            
        }

        function resizeBoxIfNeeded(desiredOffset) {
            var desiredTopWindowOffset = desiredOffset - $(document).scrollTop();
            var desiredWindowBottomOffset = $(window).height() - (desiredOffset - $(document).scrollTop()) - initHeight;
            var desiredCssTopOffset = 0;
            var desiredCssBottomOffset = 0;

            if (desiredTopWindowOffset < padding) {
                desiredCssTopOffset = (-desiredTopWindowOffset + padding);
            }

            if (desiredWindowBottomOffset < padding) {
                desiredCssBottomOffset = (-desiredWindowBottomOffset + padding);
            }

            // console.log(desiredCssTopOffset, desiredCssBottomOffset, desiredTopWindowOffset, desiredWindowBottomOffset);
            if (desiredWindowBottomOffset > 300) {
                return;
            }
            vacancyPreview.find('.popover-preview').css({top: desiredCssTopOffset});
            vacancyPreview.find('.popover-preview').css({bottom: desiredCssBottomOffset});

        }

        function getWindowOffset(item, offset) {
            var desiredOffset = offset ? offset : item.offset().top;
            return {
                top: desiredOffset - $(document).scrollTop(),
                bottom: $(window).height() - (desiredOffset - $(document).scrollTop()) - item.height()
            }
        }

        function destroy() {
            $(".job-seeker-box").unbind('hover');
            filtersBox.unbind('hover');
            $(window).off('scroll.search');
        }

        return {
            init: init,
            destroy: destroy,
            resetAll: resetAll
        };
    }

    angular.module('recrutoid.common').service('searchColumnMover',
            [searchColumnMover]);
})();