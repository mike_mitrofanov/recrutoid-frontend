(function ()
{
    'use strict';

    function RegisterController($location, $rootScope, $routeParams, Authenticator, UserDAO, recrutoidPopup, gettext)
    {
        var ctrl = this;
        this.employer = {email: null, password: null, confirmPassword: null};
        this.user = {email: null, password: null, confirmPassword: null};
        this.token = $routeParams.token;
        if (ctrl.token) {
            this.employer = {};
        }

        function validatePasswords()
        {
            if(ctrl.user.password !== ctrl.user.confirmPassword) {
                recrutoidPopup({
                    title: 'Warning',
                    text: gettext('Message_Passwords_Are_Not_Equal'),
                    type: 'warning'
                });
                return false;
            }

            return true;
        }

        this.registerEmployer = function registerEmployer()
        {
            if(!validatePasswords()) {
                return;
            }

            ctrl.employer.employer = true;
            ctrl.employer.fromVacancy = ctrl.token;
            UserDAO.register(ctrl.employer).then(function (results)
            {
                if (!ctrl.token) {
                    $location.path('/register/check-email');
                } else {
                    Authenticator.setToken(results.token);
                    //refresh app ctrl and set property
                    $rootScope.$emit('event:logIn');
                    $location.path('/vacancies/edit/' + results.results);
                }
            }).catch(function (error)
            {
                recrutoidPopup({
                    title: 'Sorry',
                    text: error.data,
                    type: 'error'
                }, function ()
                {
                    ctrl.employer.email = null;
                });
            });
        };

        this.registerUser = function registerUser()
        {
            if(!validatePasswords()) {
                return;
            }

            UserDAO.register(ctrl.user).then(function ()
            {
                $location.path('/register/check-email');
            }).catch(function (error)
            {
                recrutoidPopup({
                    title: gettext('Sorry'),
                    text: error.data,
                    type: 'error'
                }, function ()
                {
                    ctrl.user.email = null;
                });
            });
        };
    }

    angular.module('recrutoid.common').controller('RegisterController',
            ['$location', '$rootScope', '$routeParams', 'Authenticator', 'UserDAO', 'recrutoidPopup', 'gettext', RegisterController]);

})();
