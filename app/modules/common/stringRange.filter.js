(function ()
{
  'use strict';

  function stringRange()
  {
    return function(input, begin, limit) {
      return input.slice(begin, begin + limit);
    };
  }

  angular.module('recrutoid.common').filter('stringRange', [stringRange]);
})();
