(function ()
{
    'use strict';

    function emailValidator()
    {
        function isValid(email) {
			var EMAIL_REGEXP = /[a-z0-9!#$%&'*+=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/g;
            return EMAIL_REGEXP.test(email) ? true : false;
        }

        return {
            isValid: isValid
        };
    }

    angular.module('recrutoid.common').service('emailValidator', [emailValidator]);
})();


