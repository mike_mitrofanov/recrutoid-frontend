(function ()
{

    'use strict';

    function NavBarCtrl($location, $route, $scope, VacanciesDAO, Notification, gettext)
    {
        var ctrl = this;
        var text;
        this.dispatchVacancies = function ()
        {
            VacanciesDAO.dispatchVacancies().then(function (data)
            {
                text = gettext('Vacancies dispatched. ' + data.sent + ' email(s) sent.');
                Notification.success(text);
                $route.reload();
            }).catch(function ()
            {
                text = gettext('Message_Something_Went_Wrong');
                Notification.error(text);
            });

        };

        (function init()
        {
            var search = $location.search();
            if(!search.publishMode) {
                ctrl.showingType = 'All';
            }
            else if('email' === search.publishMode) {
                if (search.expired == "true") {
                    ctrl.showingType = 'Archived Distributed';
                }else{
                    ctrl.showingType = 'Daily sending mode';
                }
            }
            else if('website' === search.publishMode) {
                if (search.expired == "true") {
                    ctrl.showingType = 'Archived Published';
                }else{
                    ctrl.showingType = 'Published and weekly sending mode';
                }
            }
        })();

        $scope.$root.$broadcast('event:viewTypeChanged', 'admin');
    }

    angular.module('recrutoid.common').controller('NavBarCtrl', ['$location', '$route', '$scope', 'VacanciesDAO', 'Notification',
        'gettext', NavBarCtrl]);
})();
