(function()
{
    'use strict';

    function unsavedChangesPopup(gettext, recrutoidPopup)
    {
        function open(callback)
        {
            recrutoidPopup({
                title: gettext('Warning'),
                html: true,
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: gettext('Save'),
                cancelButtonText: gettext('Leave'),
                closeOnConfirm: true,
                text: gettext('Message_Do_You_Want_To_Save_Your_Changes?')
            }, callback);
        }
        
        function success()
        {
            recrutoidPopup({
                title: gettext('Success'),
                type: 'success',
                text: gettext('Message_Changes_Successfully_Saved')
            });
        }
        
        return {
            open: open,
            success: success
        };
    }

    angular.module('recrutoid.common').service('unsavedChangesPopup', ['gettext', 'recrutoidPopup', unsavedChangesPopup]);
})();
