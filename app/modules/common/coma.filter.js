(function ()
{
    'use strict';


    function coma()
    {
        return function (collection, propertyName)
        {
            if (collection instanceof Array) {
                if (propertyName) {
                    collection = collection.map(function (item)
                    {
                        return item[propertyName];
                    });
                }
                return collection.join(', ');
            } else if (collection instanceof Object) {
                var values = [];
                angular.forEach(collection, function (value)
                {
                    if (null != value && '' !== value) {
                        values.push(value);
                    }
                });
                return values.join(', ');
            }
            return collection;
        };
    }

    angular.module('recrutoid.common').filter('coma', coma);
})();
