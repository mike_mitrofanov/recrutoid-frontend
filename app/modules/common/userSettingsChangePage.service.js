(function()
{
    'use strict';

    function UserSettingsChangePage()
    {
        var ctx = this;

        this.isCallback = function ()
        {
            return ctx.callback && ctx.callback instanceof Function;
        };

        this.clearCallback = function ()
        {
            delete ctx.callback;
        };
    }

    angular.module('recrutoid.common').service('UserSettingsChangePage', [UserSettingsChangePage]);
})();
