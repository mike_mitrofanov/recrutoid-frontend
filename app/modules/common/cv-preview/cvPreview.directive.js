(function()
{
    'use strict';

    function cvPreview(CurrentUser)
    {
        function link(scope)
        {
        }

        return {
            restrict: 'E',
            scope: {
                redirectMethod: '&',
                seekerData: '='
            },
            link: link,
            templateUrl: 'modules/common/cv-preview/cvPreview.tpl.html'
        };
    }

    angular.module('recrutoid.common').directive('cvPreview', cvPreview);
})();

