(function ()
{
    'use strict';

    function translateObject(language, object, propertyName)
    {
        object = angular.copy(object);
        if (object[propertyName] && object[propertyName][language]) {
            object[propertyName] = object[propertyName][language];
        }
        return object;
    }

    function translateDomain(gettextCatalog)
    {
        return function (collection, propertyName)
        {
            if (null == collection) {
                return collection;
            }
            var language = gettextCatalog.getCurrentLanguage();
            if (collection instanceof Array) {
                return collection.map(function (item)
                {
                    return translateObject(language, item, propertyName);
                });
            } else {
                if (propertyName) {
                    return translateObject(language, collection, propertyName);
                } else {
                    return collection && collection[language] || collection;
                }
            }
        };
    }

    angular.module('recrutoid.common').filter('translateDomain', ['gettextCatalog', translateDomain]);
})();
