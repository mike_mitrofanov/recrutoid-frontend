(function ()
{
    'use strict';

    function feedbackModal($modal)
    {
        function open()
        {
            return $modal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: 'modules/common/feedback-modal/feedbackModal.tpl.html',
                controller: 'feedbackModalController as ctrl',
                windowClass: 'recrutoid-feedback-window'
            }).result;
        }

        return {
            open: open
        };
    }

    angular.module('recrutoid.common').service('feedbackModal', ['$modal', feedbackModal]);

})();
