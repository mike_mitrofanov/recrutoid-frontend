(function ()
{
    'use strict';

    function feedbackModalController($rootScope, $filter, $location, $modalInstance, $timeout, Authenticator,
									CurrentUser, gettext, siteSettings, recrutoidPopup, BugreportsDAO, emailValidator)
    {
        var ctrl = this;
        var warning = gettext('Warning');

		ctrl.feedback = {};

        this.close = function ()
        {
			$modalInstance.dismiss();
        };
        

        this.send = function ()
        {
			if (!ctrl.isFormValid()) {
                return handleErrors();
            }


			BugreportsDAO.update(ctrl.feedback).then(function ()
			{
				$modalInstance.close();

				var title = gettext('Feedback_has_been_sent');
				var confirm = gettext('OK');
				var text = gettext('Thank_you_for_your_feedback_We_will_review_immediately');
				var swalOptions = {
					title: title,
					text: text,
					html: true,
					type: 'success',
					showCancelButton: false,
					confirmButtonText: confirm,
					closeOnConfirm: true
				};

				recrutoidPopup(swalOptions, function (isConfirm)
				{
					//
				});
				
			}).catch(function (e)
			{
				if (e && !e.hasOwnProperty('status')) {
					return;
				}

				var text;
				if (409 === e.status) {
					text = gettext('Message_Validation_Failed.');
					recrutoidPopup(warning, text, 'warning');
				} else {
					text = gettext('Message_Technical_Error_Occured_Please_Contact_Support');
					recrutoidPopup(text, 'error');
				}
			});

        };


		function handleErrors() {
            ctrl.errors = {};

            if (!ctrl.feedback.message) {
                ctrl.errors.message = true;
            }

            if (!emailValidator.isValid(ctrl.feedback.email)) {
                ctrl.errors.email = true;
            }
        }

        this.isFormValid = function() {
            return ctrl.isEmailValid() && ctrl.feedback.message && ctrl.feedback.message.length;
        };

		this.isEmailValid = function() {
			return emailValidator.isValid(ctrl.feedback.email);
		};

		(function init()
        {
			CurrentUser.resolve().then(function (user)
            {
                if (user.email) {
                    ctrl.feedback.email = user.email;
                }
            });
        })();

    }

    angular.module('recrutoid.common').controller('feedbackModalController',
            ['$rootScope', '$filter', '$location', '$modalInstance', '$timeout', 'Authenticator',
				'CurrentUser', 'gettext', 'siteSettings', 'recrutoidPopup', 'BugreportsDAO',
				'emailValidator', feedbackModalController]);
})();
