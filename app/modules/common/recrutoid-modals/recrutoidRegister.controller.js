(function ()
{
    'use strict';

    function RecrutoidRegisterController($location, $scope, $modalInstance, $rootScope, gettext, options, 
									recrutoidPopup, UserDAO, emailValidator)
    {
        var ctrl = this;

        this.credentials = {};
		this.errors = {};
        $scope.modalStep = 1;


        this.userTypeSelect = [
            {name : 'Subscriber', icon : '&#xf007;'},
            {name : 'Distributer', icon : '&#xf007;'},
        ];

		$scope.inputTypePassword = 'password';
		$scope.hideShowPassword = function () {
			if ($scope.inputTypePassword == 'password') {
				$scope.inputTypePassword = 'text';
				$scope.colorEyePassword = 'blue-eye';
			} else {
				$scope.inputTypePassword = 'password';
				$scope.colorEyePassword = '';
			}
		};


		$scope.inputTypeConfirmPassword = 'password';
		$scope.hideShowConfirmPassword = function () {
			if ($scope.inputTypeConfirmPassword == 'password') {
				$scope.inputTypeConfirmPassword = 'text';
				$scope.colorEyeConfirmPassword = 'blue-eye';
			} else {
				$scope.inputTypeConfirmPassword = 'password';
				$scope.colorEyeConfirmPassword = '';
			}
		};


        function swal(type, title, content, callback)
        {
            recrutoidPopup({
                title: title,
                text: content,
                type: type
            }, callback ? callback() : void 0);
        }

        function validatePasswords()
        {			
            if (ctrl.credentials.password.length < 6) {
				ctrl.errors.password = gettext('Password_must_contain_at_least_6_characters');
                return false;
            }
			if (ctrl.credentials.password !== ctrl.credentials.confirmPassword) {
				ctrl.errors.password = gettext('Message_Passwords_Are_Not_Equal');
                return false;
            }
            if (ctrl.credentials.confirmPassword.length < 6) {
				ctrl.errors.confirmPassword = gettext('Password_must_contain_at_least_6_characters');
                return false;
            }

            return true;
        }

        function validateEmail()
        {
            if (!emailValidator.isValid(ctrl.credentials.email)) {
				ctrl.errors.email = gettext('Email_is_invalid');
                return;
            }

            return true;
        }

        function validateUserType()
        {
            if (!ctrl.disableUserTypeInput && !ctrl.credentials.userType) {
				ctrl.errors.userType = gettext('Required');
                return;
            }

            return true;
        }

		function isFormValid() {
			return validateEmail() && validatePasswords() && validateUserType();
		}

		this.isEmailValid = function() {
			return emailValidator.isValid(ctrl.credentials.email);
		};

        var register = function()
        {
			var credentials = angular.copy(ctrl.credentials);			

			if (!isFormValid()) {
                return;
            }

            if ('employer' === ctrl.credentials.userType.type) {
                credentials.employer = true;
                delete credentials.userType;
            }

			ctrl.errors = {};
            delete ctrl.credentials.password;
            delete ctrl.credentials.confirmPassword;

            UserDAO.register(credentials).then(function ()
            {
                $scope.modalStep = 2;
            }).catch(function (error)
            {
                if (460 === error.status) {
					ctrl.errors.email = gettext('Message_Please_Use_Your_Official_Company_Email');
                    //swal('warning', gettext('Warning'), gettext('Message_Please_Use_Your_Official_Company_Email'));
                }
                else if (412 === error.status) {
					ctrl.errors.email = gettext('Message_This_Email_Is_Already_Registered');
                    //swal('warning', gettext('Warning'), gettext('Message_This_Login_Is_Already_Registered'));
                }
                else {
					ctrl.errors.email = error.data;
                    //swal('warning', gettext('Sorry'), error.data);
                }
            });
        };

        var validateAndReturn = function ()
        {
			if (!isFormValid()) {
                delete ctrl.credentials.password;
				delete ctrl.credentials.confirmPassword;
                return;
            }

            $modalInstance.close({
                email: ctrl.credentials.email,
                password: ctrl.credentials.password
            });
        };

		

        this.submit = function()
        {
            if(options.returnData) {
                validateAndReturn();
            }
            else {
                register();
            }
        };

        this.cancel = function ()
        {
            $modalInstance.dismiss({});
        };


		$scope.$watch(function ()
        {
            return ctrl.credentials.email;
        }, function() {
			if (ctrl.credentials.email && !emailValidator.isValid(ctrl.credentials.email)) {
				ctrl.errors.email = gettext('Email');
            }
		});

        (function init()
        {
            if(options.initialEmail) {
				// email field should be editable
                //ctrl.disableEmailInput = true;
                ctrl.credentials.email = options.initialEmail;
            }

            if(options.initialUserType) {
                ctrl.credentials.userType = options.initialUserType;
                ctrl.disableUserTypeInput = true;
            }

            if (options.hideUserType || $rootScope.media == 'mobile') {
                ctrl.hideUserTypeInput = true;
            }
        })();
    }

    angular.module('recrutoid.common').controller('RecrutoidRegisterController',
        ['$location', '$scope', '$modalInstance', '$rootScope', 'gettext', 'options', 'recrutoidPopup', 
			'UserDAO', 'emailValidator', RecrutoidRegisterController]);
})();
