(function ()
{
    'use strict';

    function recrutoidModals($modal, $q)
    {
        var alreadyOpened = {};
        function register()
        {
            function open(options)
            {
                options = options || {};

                return $modal.open({
                    size: 'sm',
                    templateUrl: 'modules/common/recrutoid-modals/recrutoidRegister.tpl.html',
                    controller: 'RecrutoidRegisterController as registerModal',
                    windowClass: 'recrutoid-register-window',
                    resolve: {
                        options: function()
                        {
                            return options;
                        }
                    }
                }).result;
            }

            return {
                open: open
            };
        }

        function login()
        {
            function open(options)
            {
                if(alreadyOpened.login) {
                    var defer = $q.defer();
                    defer.resolve();
                    return defer.promise;
                }

                alreadyOpened.login = true;

                options = options || {};

                return $modal.open({
                    size: 'sm',
                    templateUrl: 'modules/common/recrutoid-modals/recrutoidLogin.tpl.html',
                    controller: 'RecrutoidLoginController as login',
                    windowClass: 'recrutoid-login-window',
                    resolve: {
                        options: function()
                        {
                            return options;
                        }
                    }
                }).result.then(function (data)
                {
                    delete alreadyOpened.login;
                    return data;
                }).catch(function (error)
                {
                    delete alreadyOpened.login;
                    var defer = $q.defer();
                    defer.reject(error);
                    return defer.promise;
                });
            }

            return {
                open: open
            };
        }

        function resetPassword()
        {
            function open(options)
            {
                options = options || {};

                return $modal.open({
                    size: 'sm',
                    templateUrl: 'modules/common/recrutoid-modals/recrutoidResetPassword.tpl.html',
                    controller: 'RecrutoidResetPasswordController as reset',
                    windowClass: 'recrutoid-reset-password-window',
                    resolve: {
                        options: function()
                        {
                            return options;
                        }
                    }
                }).result;
            }

            return {
                open: open
            };
        }

        function entireFlow(options)
        {
            options = options || {};
            return login().open(options.login).catch(function (errorMessage)
            {
                if('createAccount' === errorMessage) {
                    return register().open(options.register);
                }
                else if('resetPassword' === errorMessage) {
                    return resetPassword().open(options.resetPassword);
                }

                var defer = $q.defer();
                defer.reject();
                return defer.promise;
            });
        }

        return {
            register: register(),
            login: login(),
            resetPassword: resetPassword(),
            entireFlow: entireFlow
        };
    }

    angular.module('recrutoid.common').service('recrutoidModals',
        ['$modal', '$q', recrutoidModals]);
})();
