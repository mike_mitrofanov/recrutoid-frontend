(function ()
{
    'use strict';

    function RecrutoidLoginController($scope, $rootScope, $location, $modalInstance, $timeout, Authenticator, CurrentUser, options,
                                      recrutoidPopup, gettext, emailValidator, UserDAO)
    {
        var ctrl = this;
		this.errors = {};


        // when user deleting account and gets unauthenticated
        var customErrorMessage;

        this.credentials = {};

        function swal(type, title, content, callback)
        {
            recrutoidPopup({
                title: title,
                text: content,
                type: type
            }, callback ? callback() : void 0);
        }

        $scope.inputType = 'password';
		$scope.hideShowPassword = function () {
			if ($scope.inputType == 'password') {
				$scope.inputType = 'text';
				$scope.colorEye = 'blue-eye';
			} else {
				$scope.inputType = 'password';
				$scope.colorEye = '';
			}
		};


		function validateEmail()
        {
            if (!emailValidator.isValid(ctrl.credentials.email)) {
				ctrl.errors.email = gettext('Email_is_invalid');
                return;
            }

            return true;
        }

		function isFormValid() {
			return validateEmail();
		}

        this.submit = function()
        {			
            var credentials = angular.extend({}, ctrl.credentials);

			if (!isFormValid()) {
                return;
            }


            if(!ctrl.disableEmailInput) {
                //delete ctrl.credentials.email;
            }
            delete ctrl.credentials.password;

            Authenticator.authenticate(credentials.email, credentials.password)
                .then(function ()
                {
                    CurrentUser.reload().then(function (user)
                    {
                        $modalInstance.close(user);
                        if(options.enableReload){
                            window.location.reload();
                        }
                    });
                })
                .catch(function (error)
                {
                    if(customErrorMessage && customErrorMessage.error === error.status) {
                        $timeout(function ()
                        {
							ctrl.errors.email = customErrorMessage.message;
                            //recrutoidPopup(gettext('Failure'), customErrorMessage.message, 'error');
                            //$modalInstance.dismiss({});
                        });
                    }
                    else if (412 === error.status) {
						ctrl.errors.email = gettext('Message_confirmed_Please_Confirm_It_Using_Activation_Link_Sent_To_You_Via_Email');
//                        swal('warning', gettext('Warning'), gettext('Message_This_Email_Is_Already_Registered' +
//                            'Message_confirmed_Please_Confirm_It_Using_Activation_Link_Sent_To_You_Via_Email'));
                    }
                    else if (400 === error.status) {
						ctrl.errors.email = gettext('Email_is_not_registered');
                    }
                    else if (401 === error.status) {
						ctrl.errors.password = gettext('The_password_that_you_ve_entered_is_incorrect');
                    }
                    else if (405 === error.status) {
                        //swal('warning', gettext('Banned'), gettext('Message_You_Have_Been_Banned'));
                        if(error.data == 'deleted')
						    ctrl.errors.email = gettext('Message_Account_Has_Been_Deleted');
                        else
                            ctrl.errors.email = gettext('Message_You_Have_Been_Banned');
                    }
                    else if (409 === error.status) {
                        //swal('warning', gettext('Warning'), gettext('Message_Registered_Using_Social_Site'));
						ctrl.errors.email = gettext('Message_Registered_Using_Social_Site');
                    }
                    else if (error.data) {
                        //swal('warning', gettext('Warning'), error.data);
						ctrl.errors.email = error.data;
                    }
                });
        };

        this.oAuth = function(provider)
        {
            if(this.oAuthInProgress) {
                return;
            }
            this.oAuthInProgress = true;
            Authenticator.oAuth(provider).then(function ()
            {
                CurrentUser.reload().then(function (user)
                {
                    if(user.firstLogin){
                        $location.url('/user/settings/cv-page?tab=my_cv');
                    }

                    $modalInstance.close(user);
                });
            }).catch(function (error)
            {
                if(412 === error.status) {
                    recrutoidPopup('Error', 'Message_Cant_Get_Informations_About_Your_Social_Account', 'error');
                }
                else if (405 === error.status) {
					ctrl.errors.email = gettext('Message_You_Have_Been_Banned');
                    //swal('warning', gettext('Banned'), gettext('Message_You_Have_Been_Banned'));
                }
            }).finally(function ()
            {
                ctrl.oAuthInProgress = false;
            });
        };

        this.cancel = function (message)
        {
            $modalInstance.dismiss(message || {});
        };



//		$scope.$watch(function ()
//        {
//            return ctrl.credentials.email;
//        }, isEmailValid());

        (function init()
        {
            if(options.initialEmail) {
                ctrl.disableEmailInput = true;
                ctrl.credentials.email = options.initialEmail;
            }

            ctrl.disableSocialOptions = options.disableSocialOptions || false;
            ctrl.disableFooterOptions = options.disableFooterOptions || false;
            ctrl.disableCreateAccountOptions = options.disableCreateAccountOptions || false;
            customErrorMessage = options.customErrorMessage;
        })();
    }

    angular.module('recrutoid.common').controller('RecrutoidLoginController',
        ['$scope', '$rootScope', '$location', '$modalInstance', '$timeout', 'Authenticator', 'CurrentUser', 'options', 'recrutoidPopup',
            'gettext', 'emailValidator', 'UserDAO', RecrutoidLoginController]);
})();
