(function ()
{
    'use strict';

    function RecrutoidResetPasswordController($modalInstance, $rootScope, UserDAO, options, recrutoidPopup, gettext)
    {
        var ctrl = this;

        this.credentials = {};

        function swal(type, title, content, callback)
        {
            recrutoidPopup({
                title: title,
                text: content,
                type: type
            }, callback ? callback() : void 0);
        }

        this.submit = function()
        {
            var credentials = angular.extend({}, ctrl.credentials);
            if (!ctrl.disableEmailInput) {
                delete ctrl.credentials.email;
            }

            UserDAO.resetPassword({email: credentials.email}).then(function ()
            {
                swal('success', gettext('Success'), gettext('Message_To_Reset_Password_Use_Link_Sent_To_You_In_Email'));
                $modalInstance.close();
            }).catch(function (error)
            {
                if(400 === error.status) {
					ctrl.errors.email = gettext('Message_This_Email_Is_Not_Registered');
                    //swal('warning', gettext('Warning'), gettext('Message_This_Email_Is_Not_Registered'));
                }
                else if(412 === error.status) {
					ctrl.errors.email = gettext('Message_Email_Was_Sent');
                    //swal('warning', gettext('Warning'), gettext('Message_Email_Was_Sent'));
                }
                else {
					ctrl.errors.email = gettext('Message_Something_Went_Wrong');
                    //swal('error', gettext('Error'), gettext('Message_Something_Went_Wrong'));
                }
            });
        };

        this.cancel = function ()
        {
            $modalInstance.dismiss({});
        };

        (function init()
        {
            if(options.initialEmail) {
                ctrl.disableEmailInput = true;
                ctrl.credentials.email = options.initialEmail;
            }
        })();
    }

    angular.module('recrutoid.common').controller('RecrutoidResetPasswordController',
        ['$modalInstance', '$rootScope', 'UserDAO', 'options', 'recrutoidPopup', 'gettext', RecrutoidResetPasswordController]);
})();
