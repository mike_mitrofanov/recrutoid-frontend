(function()
{
    'use strict';

    function siteSettings($q, recrutoidPopup, SettingsDAO)
    {
        var settings = {};
        var settingsEmpty = true;

        function reload()
        {
            return SettingsDAO.siteSettings().then(function (data)
            {
                angular.extend(settings, data);
            });
        }

        function get()
        {
            if(settingsEmpty) {
                reload();
            }

            return settings;
        }

        function checkPermission(user, permission)
        {
            var defer = $q.defer();
            if(!settings.paymentModeEnabled || (user && user.hasOwnProperty('permissions') && user.permissions[permission])) {
                defer.resolve(user);
            }
            else {
                recrutoidPopup({
                    title: 'Warning!',
                    text: '<p>Please pay to resume the access. Details in <a href="/page/payment">payment instructions</a></p>',
                    html: true,
                    type: 'warning',
                    closeOnLocationChange: true
                });
                defer.reject();
            }
            return defer.promise;
        }

        function requireVacancyPermission(user)
        {
            return checkPermission(user, 'vacancyAdding');
        }

        function requireCvSearchingPermission(user)
        {
            return checkPermission(user, 'cvSearching');
        }

        return {
            reload: reload,
            get: get,
            requireVacancyPermission: requireVacancyPermission,
            requireCvSearchingPermission: requireCvSearchingPermission
        };
    }

    angular.module('recrutoid.common').factory('siteSettings', ['$q', 'recrutoidPopup', 'SettingsDAO', siteSettings]);
})();

