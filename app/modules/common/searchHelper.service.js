(function ()
{
    'use strict';

    function SearchHelper($location, $rootScope, SearchSuggestions) {
        var ctrl, refreshCallback, availableFilters, dealWithQueryString, filtersHandledBySelect2;

        function init(iCtrl, iRefresh, idealWithQueryString, iAvailableFilters, iFiltersHandledBySelect2) {
            ctrl = iCtrl;
            refreshCallback = iRefresh;
            availableFilters = iAvailableFilters;
            dealWithQueryString = idealWithQueryString;
            filtersHandledBySelect2 = iFiltersHandledBySelect2;

            $rootScope.backCallback = function () {

                locationToFilters();
                
                refreshCallback();
                

            };

        }

        function findSelect2Item(id, items)
        {
            for (var i in items) {
                if (items[i].id == id) {
                    return items[i];
                }
            }

            return null;
        }

        function select2NameToItems(name) {
            var nameSelectMap = {
                category: ctrl.categories,
                city: ctrl.cities
            };

            return nameSelectMap[name];
        }

        function locationToFilters() {
            var loc = $location.search();
            for (var i in availableFilters) {
                var item = availableFilters[i];
                ctrl.filter[item] = loc[item];
                if (filtersHandledBySelect2.indexOf(item) !== -1) {

                    var items = select2NameToItems(item);
                    var selItem = findSelect2Item(loc[item], items);
                    ctrl.select2Models[item] = selItem;

                }
            }

            SearchSuggestions.updateShortened();
        }

        function isSelect2Cleared(fieldName) {

            if (!ctrl.select2Models[fieldName]) {
                ctrl.dealWithFilters();
                ctrl.search();
            }
        }

        function isInputCleared(fieldName) {
            if (!ctrl.filter[fieldName]) {
                ctrl.dealWithFilters();
                ctrl.search();
            }
        }

        return {
            init: init,
            locationToFilters: locationToFilters,
            isSelect2Cleared: isSelect2Cleared,
            isInputCleared: isInputCleared
        };
    }

    angular.module('recrutoid.common').service('SearchHelper', ['$location', '$rootScope', 'SearchSuggestions',
        SearchHelper]);
})();
