(function()
{
    'use strict';

    function cropEmail($timeout)
    {
        function link(scope, elem)
        {
            var email = scope.cropEmail;
            scope.emailParts = [
                email.slice(0, email.indexOf('@')),
                email.slice(email.indexOf('@'))
            ];

            $timeout(function ()
            {
                var totalWidth = elem.width();
                var lastPartWidth = elem.find('.email-last-part').width() + 1;
                var firstPartContentWidth = elem.find('.email-first-part-content').width();
                var initialEmailWidth = lastPartWidth + firstPartContentWidth;
                var restWidth = totalWidth - lastPartWidth;
                var desiredFirstPartWidth = firstPartContentWidth < restWidth ? firstPartContentWidth : restWidth;
                elem.find('.email-first-part').css('width', desiredFirstPartWidth);
                
                if(initialEmailWidth > totalWidth) {
                    scope.tooltipContent = scope.cropEmail;
                }
            });
        }

        return {
            restrict: 'A',
            scope: {
                cropEmail: '='
            },
            link: link,
            templateUrl: 'modules/common/crop-email/cropEmail.tpl.html'
        };
    }

    angular.module('recrutoid.common').directive('cropEmail', ['$timeout', cropEmail]);
})();
