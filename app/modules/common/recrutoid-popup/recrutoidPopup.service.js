(function ()
{
    'use strict';

    function recrutoidPopup($modal)
    {
        var typeEnum = ['none', 'success', 'warning', 'error'];
        var defaults = {
            title: '',
            text: '',
            type: typeEnum[0],
            showCancelButton: false,
            cancelButtonText: 'Cancel',
            confirmButtonText: 'OK',
            closeOnConfirm: true,
            html: false,
            closeOnLocationChange: false
        };

        function check(text, type)
        {
            if (!text ||
                    'string' !==
                    typeof text ||
                    !type ||
                    'string' !==
                    typeof type ||
                    -1 ===
                    typeEnum.indexOf(type)) {
                throw new Error('No required parameters!');
            }
        }

        function extendOptions(options, passedOpts)
        {
            for (var prop in options) {
                if (options.hasOwnProperty(prop) && passedOpts.hasOwnProperty(prop)) {
                    options[prop] = passedOpts[prop];
                }
            }
        }

        return function ()
        {
            var callback = function () {};
            var options = angular.copy(defaults);
            if ('string' === typeof arguments[0]) {
                arguments[2] = arguments[2] || typeEnum[0];
                check(arguments[1], arguments[2]);
                options.title = arguments[0];
                options.text = arguments[1];
                options.type = arguments[2];
            }
            else {
                arguments[0].type = arguments[0].type || typeEnum[0];
                check(arguments[0].text, arguments[0].type);
                extendOptions(options, arguments[0]);
                if ('function' === typeof arguments[1]) {
                    callback = arguments[1];
                }
            }

            var modalResult = $modal.open({
                size: 'sm',
                backdrop: 'static',
                controller: 'RecrutoidPopupController as popup',
                templateUrl: 'modules/common/recrutoid-popup/recrutoidPopup.tpl.html',
                windowClass: 'recrutoid-popup-window',
                resolve: {
                    options: function ()
                    {
                        return options;
                    }
                }
            }).result;

            modalResult.then(function ()
            {
                callback(true);
            }).catch(function ()
            {
                callback(false);
            });
        };
    }

    angular.module('recrutoid.common').service('recrutoidPopup',
            ['$modal', recrutoidPopup]);
})();
