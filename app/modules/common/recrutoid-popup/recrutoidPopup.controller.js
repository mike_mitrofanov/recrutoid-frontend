(function()
{
    'use strict';

    function RecrutoidPopupController($modalInstance, $scope, options)
    {
        this.options = options;

        this.close = function(result)
        {
            if(result) {
                $modalInstance.close();
            }
            else {
                $modalInstance.dismiss();
            }
        };

        if(options.closeOnLocationChange) {
            var destroy = $scope.$root.$on('$locationChangeSuccess', function ()
            {
                $modalInstance.dismiss();
                destroy();
            });
        }
    }

    angular.module('recrutoid.common').controller('RecrutoidPopupController', ['$modalInstance', '$scope', 'options', RecrutoidPopupController]);
})();
