(function ()
{
    'use strict';

    function scroller($window)
    {
        function link(scope, appBody)
        {
            scope.$root.$on('scrollTo', function (e, place)
            {
                var target = appBody.find('[data-scroll-to="' + place + '"]');
                if (target.length) {
                    $window.scrollTo(0, target.offset().top);
                }
            });
        }

        return {
            restrict: 'A',
            link: link
        };
    }

    angular.module('recrutoid.common').directive('scroller', ['$window', scroller]);
})();
