(function ()
{
    'use strict';

    function cleanHtml()
    {
        return function (value)
        {
            var text = value.replace(
                new RegExp('<' +
                    '(?!h1)(?!\/h1)(?!h2)(?!\/h2)(?!h3)(?!\/h3)(?!h4)(?!\/h4)(?!h5)(?!\/h5)(?!h6)(?!\/h6)' +
                    '(?!b)(?!\/b)(?!blockquote)(?!\/blockquote)(?!ul)(?!\/ul)(?!ol)(?!\/ol)(?!li)(?!\/li)' +
                    '(?!u)(?!\/u)(?!strike)(?!\/strike)(?!p)(?!\/p)' +
                    '([^>]*)>', 'g'), ''
            ).replace(/(\S+)=["']?((?:.(?!["']?\s+(?:\S+)=|[>"']))+.)["']?/g, '').replace(/<h([1-6])/ig,"<p").replace(/<\/h([1-6])/ig,"</p");
            return replaceURLWithHTMLLinks(text);
        };

        function replaceURLWithHTMLLinks(text) {
            var re = /(\(.*?)?\b((?:https?|ftp|file):\/\/[-a-z0-9+&@#\/%?=~_()|!:,.;]*[-a-z0-9+&@#\/%=~_()|])/ig;
            return text.replace(re, function(match, lParens, url) {
                var rParens = '';
                lParens = lParens || '';

                // Try to strip the same number of right parens from url
                // as there are left parens.  Here, lParenCounter must be
                // a RegExp object.  You cannot use a literal
                //     while (/\(/g.exec(lParens)) { ... }
                // because an object is needed to store the lastIndex state.
                var lParenCounter = /\(/g;
                while (lParenCounter.exec(lParens)) {
                    var m;
                    // We want m[1] to be greedy, unless a period precedes the
                    // right parenthesis.  These tests cannot be simplified as
                    //     /(.*)(\.?\).*)/.exec(url)
                    // because if (.*) is greedy then \.? never gets a chance.
                    if (m = /(.*)(\.\).*)/.exec(url) ||
                            /(.*)(\).*)/.exec(url)) {
                        url = m[1];
                        rParens = m[2] + rParens;
                    }
                }
                return lParens + "<a href='" + url + "'>" + url + "</a>" + rParens;
            });
        }
    }

    angular.module('recrutoid.common').filter('cleanHtml', [cleanHtml]);
})();
