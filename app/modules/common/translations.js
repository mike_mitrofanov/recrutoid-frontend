angular.module('recrutoid').run(['gettextCatalog', 'TranslationsDAO', 'LanguagesDAO', '$rootScope', function (gettextCatalog, TranslationsDAO, LanguagesDAO, $rootScope) {
/* jshint -W100 */
    var ctrl = this;
    this.translations = [];
    this.translationStrings = [];
    this.languages = [];

    TranslationsDAO.query().then(function (data)
    {
        ctrl.translations = data.results;
        return LanguagesDAO.query();
    }).then(function (data)
    {
        var translation;
        
        data.results.forEach(function(language){
            translation = {};
            translation.code = language.code;
            translation.actions = {};

            ctrl.translations.forEach(function(translationData){
                if (language.code in translationData.translation) {
                    translation.actions[translationData.action] = translationData.translation[language.code];
                }else{
                    translation.actions[translationData.action] = translationData.action;
                }
            });
            ctrl.translationStrings.push(translation);
        });
       return ctrl.translationStrings;
    }).then(function(translationStrings){
        translationStrings.forEach(function(translation){
            gettextCatalog.setStrings(translation.code, translation.actions);
        });
    });
/* jshint +W100 */
}]);