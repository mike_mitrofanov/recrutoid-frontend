(function ()
{
    'use strict';

    function SearchSuggestions($http) {
        var filterName;
        var endPoint;
        var ctrl;
        var ctrlField;
        var shorten;
        var focused = false;
        var measuringCanvas;

        function setContext(cCtrl, cEndPoint, cFilterName)
        {
            ctrl = cCtrl;
            endPoint = cEndPoint;
            filterName = cFilterName;
            ctrlField = 'filter';

        }

        function fetchSuggestions(text, overrideEndpoint)
        {
            var eP = endPoint;

            if (overrideEndpoint) {
                eP = overrideEndpoint;
            }

            return $http.get(eP + text, {
                ignoreLoadingBar: true
            }).then(function (response) {
                var data = response.data;
                if (data.indexOf(text) === -1) {
                    data.unshift(text);
                }
                return data;
            });
        }


        function fetchVacancySuggestions(text)
        {
            return  fetchSuggestions(text, '/api/autocomplete/vacancies?q=');
        }

        function fetchCVSuggestions(text)
        {
            return  fetchSuggestions(text, '/api/autocomplete/cv?q=');
        }

        function keyPress(event)
        {
            if (event.which === 13) {
                ctrl.search();
            }
        }

        function onSelect(event)
        {

        }


        function shouldShorten()
        {
            return shorten;
        }

        function onBlur(event)
        {
            shorten = true;
            updateShortened();
            focused = false;

        }

        function onFocus(event)
        {

            shorten = false;
            focused = true;


            setTimeout(function () {
                angular.element('.shortened-field').trigger('focus');
                angular.element('.shortened-field').trigger('click');

            }, 100);
        }

        function setCtrlField(sCtrlField)
        {
            ctrlField = sCtrlField;
        }


        function calcTextWidth(text)
        {
            if (!measuringCanvas) {
                measuringCanvas = document.createElement("canvas");
            }

            var context = measuringCanvas.getContext("2d");
            context.font = 'normal 14px sans-serif';
            var metrics = context.measureText(text);

            return metrics.width;
        }

        function cutTextToWidth(text, width) {
            var w = 0;
            var tmpText = "";
            var tl = text.length;
            var i = 0;

            width -= calcTextWidth("...") * 2;
            
            console.log(calcTextWidth(text), width);
            
            if (calcTextWidth(text) <= width) {
                return text;
            }

            var previousText = "";

            while (w < width && i <= tl) {

                previousText = tmpText;

                tmpText += text[i];

                w = calcTextWidth(tmpText);
                i++;
            }
            
            console.log(previousText);
            
            return previousText + "...";
        }


        function refocus()
        {
            onFocus();
            onBlur();
        }

        function updateShortened(fieldOverride)
        {
            var filterFieldName = filterName;

            if (!ctrl[ctrlField][filterFieldName]) {
                ctrl.shortenedTitle = '';
                return;
            }

            ctrl.shortenedTitle = ctrl[ctrlField][filterFieldName];

            var sf = $('.typeahead-container input.shorting-field');
            var elWidth;
            if ($('.typeahead-container').hasClass('home-typeahead-container')) {
                elWidth = $('.typeahead-container').width() - 70;
            } else {
                
                var pW = $('.typeahead-container').width();
                
                var pWidth = (pW / 100) * 1000;
                var w = $('.typeahead-container input.shorting-field').width();
                
                
                if(w<=0) {
                    w = $('.typeahead-container input.shortened-field').width();
                }

                elWidth = (w / 100) * pWidth;
                elWidth -= parseInt($(sf).css('padding-left')) + parseInt($(sf).css('padding-right'));
            }

            if(elWidth<=0) {
                return;
            }

           

            ctrl.shortenedTitle = cutTextToWidth(ctrl.shortenedTitle, elWidth);

        }

        function clear(fieldName, filterName)
        {
            if (filterName) {
                ctrl[filterName][fieldName] = '';
            } else {
                ctrl.filter[fieldName] = '';
            }


            ctrl.shortenedTitle = '';
        }

        function isFocused()
        {
            return focused;
        }


        return {
            setContext: setContext,
            fetchSuggestions: fetchSuggestions,
            fetchVacancySuggestions: fetchVacancySuggestions,
            fetchCVSuggestions: fetchCVSuggestions,
            keyPress: keyPress,
            onSelect: onSelect,
            shouldShorten: shouldShorten,
            onFocus: onFocus,
            onBlur: onBlur,
            updateShortened: updateShortened,
            setCtrlField: setCtrlField,
            clear: clear,
            isFocused: isFocused,
            refocus: refocus,
        };
    }

    angular.module('recrutoid.common').service('SearchSuggestions', ['$http', SearchSuggestions]);
})();
