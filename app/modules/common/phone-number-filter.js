(function()
{
    'use strict';

    function formatPhone()
    {
        return function (tel)
        {
            if(tel[0]==='+' || tel.length<11) {
                return tel; 
            }
            
            var fmt = '+'+tel[0]+' ('+tel.substring(1,4)+') '+tel.substring(4,7)+'-'
            + tel.substring(7,9)+'-'+tel.substring(9,11) 
            ;
            
            return fmt;
            
        };
    }

    angular.module('recrutoid.common').filter('phone_number', [formatPhone]);
})();