(function()
{
    'use strict';

    function userEmail($rootScope, $timeout)
    {
		function link(scope) {
			scope.media = $rootScope.media;
            scope.initialProfile = angular.copy(scope.profile);
		}


        return {
            restrict: 'E',
            scope: {
                profile: '=',
                errors: '='
            },
			link: link,
            templateUrl: 'modules/common/userEmail/userEmail.tpl.html',
            controller: ["$scope", "recrutoidPopup", "gettext", "$timeout", "UserDAO", "$interval", function($scope, recrutoidPopup, gettext, $timeout, UserDAO) {
                $scope.resendRegistrationEmail = function () {
                    UserDAO.resendRegistrationEmail($scope.profile.id).then(function(){
                        var successDisplay = gettext('Success');
                        var text = gettext('Confirmation_Link_Was_Sent_To_Your_New_Email');
                        recrutoidPopup(successDisplay, text, 'success');
                    });
                };
            }]
        };
    }

    angular.module('recrutoid.common').directive('userEmail', userEmail);
})();