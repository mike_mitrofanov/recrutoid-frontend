(function()
{
    'use strict';

    function cleanSalary()
    {
        return function (value)
        {
            return value.replace(/([^0-9]*)$/g, '').replace(/,*/g, '').replace(/\s*/g, '');
        };
    }

    angular.module('recrutoid.common').filter('cleanSalary', [cleanSalary]);
})();
