(function ()
{
    'use strict';

    function UserMenuController($location, $scope, $rootScope, CommonDAO, CurrentUser, siteSettings)
    {
        var ctrl = this;
        ctrl.newApplicationsOrCvViews = null;

        this.goProfiles = function ()
        {
            CurrentUser.resolveOrReject().then(siteSettings.requireCvSearchingPermission).then(function ()
            {
                $location.path('/users');
            });
        };

        $scope.goToLocation = function(link) {
            window.location = link;
            location.reload();
        };

        function refreshNewApplicationsCount($e, applicationsCount)
        {
            ctrl.newApplicationsOrCvViews = applicationsCount.count;
        }

        function refreshNewResumeViewsCount($e, resumeViewsCount)
        {
            ctrl.newApplicationsOrCvViews = resumeViewsCount.count;
        }

        function refreshOffersCount($e, offersCount)
        {
            ctrl.newOffers = offersCount;
        }

        function refreshCvStatus($e, cvHidden){
            $scope.hidden_cv = cvHidden;
        }

        function refreshEmailStatus($e, unconfirmedEmail){
            $scope.unconfirmed_email = unconfirmedEmail;
        }

        function refresh()
        {
            CommonDAO.getNavBarData().then(function (data)
            {
                ctrl.pages = data.pages;
                ctrl.newApplicationsOrCvViews = data.newApplicationsOrCvViews;
                ctrl.newOffers = data.newOffers;

                if (data.notifications){
                    $rootScope.daysLeftToDeletion = data.notifications.daysLeft;
                    $scope.hidden_cv = data.notifications.hidden_cv;
                    $scope.unconfirmed_email = data.notifications.unconfirmed_email;
                }
            });
        }

        (function init()
        {
            refresh();
        })();

        var destroyListener = {
            newApplicationsCount: $scope.$root.$on('event:applications-seen', refreshNewApplicationsCount),
            newResumeViewsCount: $scope.$root.$on('event:resume-views-seen', refreshNewResumeViewsCount),
            interviewSeen: $scope.$root.$on('event:interview-seen', refreshOffersCount),
            cvActive: $scope.$root.$on('event:cv-active', refreshCvStatus),
            unconfirmedEmail: $scope.$root.$on('event:unconfirmed-email', refreshEmailStatus)
        };

        $rootScope.$on('event:auth-loggedIn', refresh);

        $scope.$on('$destroy', function ()
        {
            destroyListener.newApplicationsCount();
            destroyListener.newResumeViewsCount();
            destroyListener.interviewSeen();
        });

        $rootScope.$broadcast('event:viewTypeChanged', 'normal');
    }

    angular.module('recrutoid.common')
            .controller('UserMenuController',
                    ['$location', '$scope', '$rootScope', 'CommonDAO', 'CurrentUser', 'siteSettings', UserMenuController]);
})();

