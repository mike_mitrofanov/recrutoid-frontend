(function()
{
    'use strict';
    
    function recrutoidInput()
    {
        function link(scope)
        {
            scope.checkIfMax = function() {
                return scope.max && scope.ngModel && scope.ngModel.length > scope.max;
            };
        }
        
        return {
            restrict: 'E',
            scope: {
                type: '@',
                ngModel: '=',
                required: '=',
                max: '=',
                error: '=',
                placeholder: '@',
                icon: '@',
                uiMask: '@'
            },
            link: link,
            templateUrl: 'modules/common/recrutoidFields/recrutoidInput.tpl.html'
        };
    }

    angular.module('recrutoid.common').directive('recrutoidInput', recrutoidInput);
})();
