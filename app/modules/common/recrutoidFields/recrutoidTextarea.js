(function()
{
    'use strict';
    
    function recrutoidTextarea()
    {
        function link(scope)
        {
            scope.checkIfMax = function() {
                return scope.max && scope.ngModel && scope.ngModel.length > scope.max;
            };
        }
        
        return {
            restrict: 'E',
            scope: {
                ngModel: '=',
                required: '=',
                max: '=',
                error: '=',
                placeholder: '@',
                icon: '@',
                uiMask: '@'
            },
            link: link,
            templateUrl: 'modules/common/recrutoidFields/recrutoidTextarea.tpl.html'
        };
    }

    angular.module('recrutoid.common').directive('recrutoidTextarea', recrutoidTextarea);
})();
