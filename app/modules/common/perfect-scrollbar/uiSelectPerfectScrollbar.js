function uiSelectPerfectScrollbar()
{
    return {
        restrict: 'A',
        link: function (scope, element)
        {

            element.addClass('with-perfect-scrollbar');
            function initializePerfectScrollbar()
            {
                var scrollArea = element.find('.ui-select-choices');
                scrollArea.perfectScrollbar({
                    wheelSpeed: 2,
                    wheelPropagation: true,
                    minScrollbarLength: 20,
                    suppressScrollX: true
                });

                element.click(function(){
                    scrollArea.scrollTop(0);
                    scrollArea.perfectScrollbar('update');

                    setTimeout(function(){
                        scrollArea.perfectScrollbar('update');
                    },50);
                })
            }

            initializePerfectScrollbar();

            scope.initializePerfectScrollbar = initializePerfectScrollbar;
        }
    };
}

angular.module('recrutoid.home').directive('uiSelectPerfectScrollbar', uiSelectPerfectScrollbar);