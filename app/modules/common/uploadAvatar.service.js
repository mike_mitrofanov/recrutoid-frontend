(function ()
{
    'use strict';

    function uploadAvatar($modal, $rootScope, UserDAO)
    {
        return (function ()
        {
            function upload(files)
            {
                if (!files || !files.length) {
                    return;
                }
                var templateUrl;
                var windowClass;

                if ($rootScope.media == 'mobile') {
                    templateUrl = 'modules/user/crop-image-modal/mobileCropImage.modal.tpl.html';
                    windowClass = 'recrutoid-image-crop-window-mobile';
                } else {
                    templateUrl = 'modules/user/crop-image-modal/cropImage.modal.tpl.html';
                    windowClass = 'recrutoid-image-crop-window';
                }

                var modalResult = $modal.open({
                    size: 'md',
                    backdrop: 'static',
                    templateUrl: templateUrl,
                    controller: 'CropImageController as cropImage',
                    windowClass: windowClass,
                    resolve: {
                        file: function ()
                        {
                            return files[0];
                        }
                    }
                }).result;

                return modalResult.then(function (newFile)
                {
                    return UserDAO.uploadAvatar(newFile);
                }).then(function (data)
                {
                    $rootScope.$broadcast('event:reloadAvatar', data.data.url);
                    return data.data.url;
                });
            }



            return {
                upload: upload
            };
        })();
    }

    angular.module('recrutoid.common').service('uploadAvatar', ['$modal', '$rootScope', 'UserDAO', uploadAvatar]);
})();
