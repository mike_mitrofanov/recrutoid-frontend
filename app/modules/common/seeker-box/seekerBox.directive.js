(function ()
{
    'use strict';

    function seekerBox($rootScope, $location, $filter)
    {
        function link(scope)
        {
            scope.forListing = 'true' === scope.forListing;
            scope.seeker = scope.seekerBox;
            scope.twoLastJobs = [];
            scope.degree = "";
            scope.languages = [];
            scope.media = $rootScope.media;

            scope.calculateLanguages = function () {
                var languages = [];
                if (scope.seeker.resume.languages.length) {
                    scope.seeker.resume.languages.forEach(function (item) {
                        if (item.code) {
                            languages.push(item.code.toUpperCase());
                        }
                    });
                    scope.languages = languages.join(', ');

                    return true;
                }
            };

            scope.calculateEducation = function () {
                var degrees = [];
                if (scope.seeker.resume.education.length) {
                    scope.seeker.resume.education.forEach(function (education) {
                        degrees.push(education.degree);
                    });

                    if (degrees.indexOf('PhD') !== -1) {
                        scope.degree = 'PhD';
                        return true;
                    } else if (degrees.indexOf('Masters') !== -1) {
                        scope.degree = 'Masters';
                        return true;
                    } else if (degrees.indexOf('Bachelors') !== -1) {
                        scope.degree = 'Bachelors';
                        return true;
                    }
                }
                return false;
            };

            scope.calculateEducation();
            scope.calculateLanguages();

            scope.details = function ()
            {
                var url = '/user/' + scope.seeker.id + '/profile';

                if ($rootScope.media !== 'desktop') {
                    $location.path(url).search({});
                } else {
                    window.open(url);
                }

            };

            scope.edit = function () {
                scope.settings.setActiveSite('edit-cv');
            };

            prepareWorkExperience(scope);


        }

        function prepareWorkExperience(scope) {
            var today = new Date();
            scope.seeker.resume.workExperience.forEach(function(exp){
                if (exp.currentWork) {
                    exp.endDate = today;
                }
            });
            scope.seeker.resume.workExperience = $filter('orderBy')(scope.seeker.resume.workExperience, '-endDate');

            var workExperienceLength = scope.seeker.resume.workExperience.length;
            if (workExperienceLength) {
                scope.twoLastJobs = scope.seeker.resume.workExperience.slice(0,2)
            }
        }

        return {
            restrict: 'A',
            scope: {
                seekerBox: '=',
                actions: '=',
                settings: '=',
                active: '=',
                forListing: '@'
            },
            link: link,
            templateUrl: 'modules/common/seeker-box/seekerBox.tpl.html'
        };
    }

    angular.module('recrutoid.common').directive('seekerBox', ['$rootScope', '$location', '$filter', seekerBox]);
})();
