(function()
{
    'use strict';

    function subVacancyFilters($window)
    {
        return {
            restrict: 'E',
            scope: {
                mode: '@',
                hasSelectedFilters: '&',
                isFilterSelected: '&',
                isFilterSet: '&',
                isSalaryFilterSet: '&',
                resetFilters: '&',
                setFilter: '&',
                setSalaryFilter: '&',
                salaryRanges: '=',
                companyFilterData: '=',
                locationFilterData: '='
            },
            templateUrl: 'modules/common/sub-vacancy-filters/subVacancyFilters.tpl.html',
            controller: ["$scope", "$rootScope", "$interval", function($scope, $rootScope) {
                $scope.rootScope = $rootScope;
            }]
        };
    }

    angular.module('recrutoid.common').directive('subVacancyFilters', subVacancyFilters);
})();