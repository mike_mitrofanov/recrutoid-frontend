(function()
{
    'use strict';

    function floatingButton($window)
    {
        return {
            restrict: 'E',
            scope: {
                icon: '@',
                href: '@',
                offsetTop: '=',
                half: '='
            },
            templateUrl: 'modules/common/floatingButton/floatingButton.tpl.html',
            controller: ["$scope", "$rootScope", "$interval", function($scope, $rootScope) {
                $scope.rootScope = $rootScope;
            }]
        };
    }

    angular.module('recrutoid.common').directive('floatingButton', floatingButton);
})();