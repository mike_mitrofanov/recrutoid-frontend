(function()
{
    'use strict';

    function vacancyPreview(CurrentUser)
    {
        function link(scope)
        {
            CurrentUser.resolve().then(function (user)
            {
                scope.showApplyBtn = user && 'normal' === user.role;
            });
        }

        return {
            restrict: 'E',
            scope: {
                createDate: '=',
                redirectMethod: '&',
                title: '@',
                city: '@',
                vacancy: '=',
                categories: '@',
                details: '@',
                applied: '=',
                applyMethod: '&'
            },
            link: link,
            templateUrl: 'modules/common/vacancy-preview/vacancyPreview.tpl.html'
        };
    }

    angular.module('recrutoid.common').directive('vacancyPreview', vacancyPreview);
})();

