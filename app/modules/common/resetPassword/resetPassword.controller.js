(function ()
{
    'use strict';

    function ResetPasswordController($location, $timeout, $routeParams, UserDAO)
    {
        var ctrl = this;
        this.token = $routeParams.token;
        function validate(password, confirmPassword)
        {
            return password === confirmPassword && 6 <= password.length;
        }

        this.setPassword = function (password, confirmPassword)
        {
            if (validate(password, confirmPassword)) {
                UserDAO.setPassword({password: password, confirmPassword: confirmPassword, token: ctrl.token}).then(function ()
                {
                    $location.path('/').search('login');
                });
            } else {
                $timeout(function ()
                {
                    ctrl.error = false;
                }, 3000);
                ctrl.error = true;
            }
        };
        function init()
        {
            if (ctrl.token) {
                UserDAO.checkResetPasswordToken(ctrl.token).catch(function ()
                {
                    $location.path('/');
                });
            }
        }

        init();
    }

    angular.module('recrutoid.common').controller('ResetPasswordController',
            ['$location', '$timeout', '$routeParams', 'UserDAO', ResetPasswordController]);
})();
