function dropdownCheckbox($timeout)
{
    return {
        restrict: 'A',
        templateUrl: 'modules/common/dropdownCheckbox/dropdown-checkbox.html',
        scope: {
            max: '@',
            placeholder: '@',
            selectName: '@',
            ngModel: '=',
            allItems: '='
        },
        link: function (scope, element)
        {            
            element.addClass('dropdown-checkbox');
            var valueElement = element.find('.value');
            var innerValueElement = element.find('.inner-value');
            var initialFontSize = parseInt(valueElement.css('fontSize'), 10);
            var allCategoriesSelected = false;

            function adjustFontSizeAndPadding()
            {
                var currentFontSize = initialFontSize;
                valueElement.css({fontSize: currentFontSize + 'px'});
                function doAdjustFontSizeAndPadding()
                {
                    var valueInnerHeight = valueElement.innerHeight();
                    if (innerValueElement.height() <= valueInnerHeight) {
                        var padding = (valueInnerHeight - innerValueElement.height()) / 2;
                        valueElement.css({paddingTop: padding + 'px', paddingBottom: padding + 'px'});
                    } else {
                        currentFontSize -= 2;
                        if (currentFontSize > 0) {
                            valueElement.css({fontSize: currentFontSize + 'px'});
                            $timeout(doAdjustFontSizeAndPadding);
                        }
                    }
                }

                $timeout(doAdjustFontSizeAndPadding);
            }

            scope.toggle = function ($event)
            {

            };
            scope.toggleItem = function (item, $event)
            {
                scope.ngModel = scope.ngModel || [];
                var indexOf = scope.ngModel.indexOf(item);
                if (-1 === indexOf) {
                    if('All categories' === item.name.en) {
                        allCategoriesSelected = true;
                        item.selected = true;
                        scope.ngModel = [item];
						scope.open = false;
                    }
                    else if (scope.max > scope.ngModel.length) {
                        if(allCategoriesSelected) {
                            scope.ngModel.splice(0);
                            allCategoriesSelected = false;
                        }
                        item.selected = true;
                        scope.ngModel.push(item);
                    }
                } else {
                    item.selected = false;
                    scope.ngModel.splice(indexOf, 1);
                }
                $event.stopPropagation();

                adjustFontSizeAndPadding();
            };

            scope.$watch('ngModel', function (newValue, oldValue)
            {
                if (null == newValue && null == oldValue) {
                    return;
                }
                angular.forEach(scope.allItems, function (item)
                {
                    item.selected = -1 !== newValue.indexOf(item);
                });

                if (scope.max == scope.ngModel.length) {
                    scope.open = false;
                }
            }, true);

            adjustFontSizeAndPadding();
        }
    };
}

angular.module('recrutoid.home').directive('dropdownCheckbox', dropdownCheckbox);