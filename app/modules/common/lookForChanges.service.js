(function()
{
    'use strict';
    
    function lookForChanges()
    {
        function check(obj, copy, excludes)
        {
            excludes = excludes || [];
            for (var prop in obj) {
                if (!obj.hasOwnProperty(prop) || '$' === prop[0] || -1 < excludes.indexOf(prop)) {
                    continue;
                }

                if (copy[prop] instanceof Object) {
                    if (check(obj[prop], copy[prop], excludes)) {
                        return true;
                    }
                }
                else if (copy[prop] !== obj[prop]) {
                    return true;
                }
            }
            return false;
        }
        return check;
    }
    
    angular.module('recrutoid.common').service('lookForChanges', [lookForChanges]);
})();
