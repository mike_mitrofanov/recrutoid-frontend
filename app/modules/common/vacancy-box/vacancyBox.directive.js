(function ()
{
    'use strict';

    function vacancyBox($rootScope, $location, CurrentUser)
    {
        function link(scope)
        {
            var allowedActions = ['edit', 'remove', 'checkbox'];

            scope.vacancy = scope.vacancyBox;
            scope.showLogo = 'true' === scope.showLogo;
            scope.showSeen = 'true' === scope.showSeen;
            scope.showSalary = 'true' === scope.showSalary;
            scope.showCreateDate = 'true' === scope.showCreateDate;
            scope.showCategoriesView = 'true' === scope.showCategoriesView;
            scope.showCategoriesListing = 'true' === scope.showCategoriesListing;
            scope.showCompanyName = 'true' === scope.showCompanyName;
            scope.showApplyButton = 'true' === scope.showApplyButton;
            scope.showApplicants = 'true' === scope.showApplicants;
            scope.showActionsSide = 'true' === scope.showActionsSide;
            scope.showContactInfo = 'true' === scope.showContactInfo;
            scope.showClickableTitle = 'true' === scope.showClickableTitle;
            scope.clickableBox = 'true' === scope.clickableBox;
            scope.showInviteButton = 'true' === scope.showInviteButton;
            scope.hideCreationDate = 'true' === scope.hideCreationDate;
            scope.vacancy.createDateHumanized = moment.duration(moment(scope.vacancy.createDate).diff()).humanize(true);
            scope.forProfile = 'true' === scope.forProfile;

            if (scope.vacancy.applicants) {
                for (var i = 0; i < scope.vacancy.applicants.length; i++)
                {
                    if (!scope.vacancy.applicants[i].seen) {
                        scope.freshApplicants = true;
                        break;
                    }
                }
            }

            if (scope.userOffers) {
                if (scope.userOffers.indexOf(scope.vacancy.id) !== -1) {
                    scope.vacancy.invited = true;
                }
            }

            if (scope.actions) {
                scope.numOfActions = 0;
                angular.forEach(scope.actions, function (elem, prop)
                {
                    if (-1 < allowedActions.indexOf(prop)) {
                        scope.numOfActions++;
                    }
                });
                if (!scope.numOfActions) {
                    delete scope.actions;
                }
            }

            scope.clickBox = function (vacancy) {
                console.log(scope.clickableBox);
                if (scope.clickableBox) {
                    scope.showDetails(vacancy);
                }
            };

            scope.showDetails = function (vacancy) {
                vacancy.isNotVisited = false;
                vacancy.isNew = false;
                if ($rootScope.media !== 'desktop') {
                    $location.path('/vacancy/' + vacancy.id).search({});
                } else {
                    window.open('/vacancy/' + vacancy.id);
                }

            };

            CurrentUser.resolve().then(function (user)
            {
                scope.userSearchVisited = user.searchVisited;
                scope.isJobSeeker = user && 'normal' === user.role;
                scope.isEmployer = user && 'employer' === user.role;
                if (scope.vacancy.userId)
                    scope.myVacancy = user.id === scope.vacancy.userId._id;
            });
        }

        return {
            restrict: 'A',
            scope: {
                vacancyBox: '=',
                userOffers: '=',
                showLogo: '@',
                showSeen: '@',
                showSalary: '@',
                showCreateDate: '@',
                showCategoriesView: '@',
                showCategoriesListing: '@',
                showCompanyName: '@',
                showApplyButton: '@',
                showApplicants: '@',
                showActionsSide: '@',
                showContactInfo: '@',
                showClickableTitle: '@',
                hideCreationDate: '@',
                clickableBox: '@',
                showInviteButton: '@',
                inviteMethod: '&',
                actions: '=',
                daysToDeletion: '@',
                applyMethod: '&',
                goApplicantsMethod: '&',
                visited: '=',
                rawCreationDate: '@',
                forProfile: '@'
            },
            link: link,
            templateUrl: 'modules/common/vacancy-box/vacancyBox.tpl.html'
        };
    }

    angular.module('recrutoid.common').directive('vacancyBox', ['$rootScope', '$location', 'CurrentUser', vacancyBox]);
})();
