(function()
{
    'use strict';

    function subCvFilters($window)
    {
        return {
            restrict: 'E',
            scope: {
                mode: '@',
                hasSelectedFilters: '&',
                isFilterSelected: '&',
                isFilterSet: '&',
                isSalaryFilterSet: '&',
                resetFilters: '&',
                setFilter: '&',
                setMultipleFilter: '&',
                setSalaryFilter: '&',
                isWorkExperienceFilterSet: '&',
                setWorkExperienceFilter: '&',
                toggleLanguage: '&',
                langProficiencyChanged: '&',
                educationOptions: '=',
                languagesOptions: '=',
                jobTitlesData: '=',
                selectedLanguages: '=',
                languageProficiencies: '=',
                filterData: '='
            },
            templateUrl: 'modules/common/sub-cv-filters/subCvFilters.tpl.html',
            controller: ["$scope", "$rootScope", "$interval", function($scope, $rootScope) {
                $scope.rootScope = $rootScope;
            }]
        };
    }

    angular.module('recrutoid.common').directive('subCvFilters', subCvFilters);
})();