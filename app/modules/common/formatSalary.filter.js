(function ()
{
    'use strict';

    function formatSalary($filter, gettextCatalog)
    {
        return function (value) {
            if('string' !== typeof value) {
                value = String(value);
            }
            if('0' === value[0]) {
                return '0';
            }
            var separator = 'en' === gettextCatalog.getCurrentLanguage() ? ',' : ' ';
            value = $filter('cleanSalary')(value);
            for(var i=value.length-3; i>-1; i-=3)
            {
                if(0<i) {
                    value = value.slice(0, i) + separator + value.slice(i);
                }
            }
            return value;
        };
    }

    angular.module('recrutoid.common').filter('formatSalary', ['$filter', 'gettextCatalog', formatSalary]);
})();
