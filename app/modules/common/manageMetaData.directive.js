(function ()
{
    'use strict';

    function manageMetaData($rootScope)
    {
        return {
            restrict: 'EA',
            link: function ()
            {
                var title = jQuery('head title'),
                    keywords = jQuery('head meta[name=keywords]'),
                    description = jQuery('head meta[name=description]');

                // if (0 === title.size()) {
                //     title = jQuery('<title></title>');
                //     jQuery('head').append(title);
                // }
                // if (0 === keywords.size()) {
                //     keywords = jQuery('<meta name="keywords">');
                //     jQuery('head').append(keywords);
                // }
                // if (0 === description.size()) {
                //     description = jQuery('<meta name="description">');
                //     jQuery('head').append(description);
                // }

                $rootScope.$on('event:setMetaData', function (e, data)
                {
                    title.html(data.pageTitle);
                    keywords.attr('content', data.keywords);
                    description.attr('content', data.description);
                });

                $rootScope.$on('$locationChangeStart', function ()
                {
                    title.html('Recrutoid');
                    keywords.attr('content', '');
                    description.attr('content', '');
                });
            }
        };
    }

    angular.module('recrutoid.common').directive('manageMetaData', ['$rootScope', manageMetaData]);
})();
