(function()
{
    'use strict';

    function windowHandler($window, $rootScope)
    {
        var scrollOffset = $window.innerHeight;
        getMedia();

        function getMedia() {
            if($window.innerWidth < 768) {
                $rootScope.media = 'mobile'
            }
            if($window.innerWidth >= 768) {
                $rootScope.media = 'tablet'
            }
            if($window.innerWidth >= 1024) {
                $rootScope.media = 'desktop'
            }
        }

        return function() {
            angular.element($window).bind('scroll', function() {
                if(this.pageYOffset >= scrollOffset && !$rootScope.scrolledDown){
                    $rootScope.scrolledDown = true;
                }else if(this.pageYOffset < scrollOffset && $rootScope.scrolledDown) {
                    $rootScope.scrolledDown = false;
                }

                if(this.pageYOffset >= (scrollOffset / 2) && !$rootScope.halfScrolledDown){
                    $rootScope.halfScrolledDown = true;
                }else if(this.pageYOffset < (scrollOffset / 2) && $rootScope.halfScrolledDown) {
                    $rootScope.halfScrolledDown = false;
                }
                $rootScope.$apply();

            });

            angular.element($window).bind('resize', function() {
                getMedia();
                $rootScope.$apply();
            });
        };
    }

    angular.module('recrutoid.common').directive('windowHandler', windowHandler);
})();

