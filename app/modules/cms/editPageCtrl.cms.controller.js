(function ()
{
    'use strict';

    function EditPageCMSCtrl($location, $routeParams, CmsDAO, recrutoidPopup)
    {
        var ctrl = this;

        this.save = function()
        {
            if(!ctrl.newsBoxesMode) {
                if(ctrl.page.slug.split('/').length !== 1) {
                    recrutoidPopup('Error!', 'Url slug can\'t contain additional slash', 'error');
                    return;
                }
                else if(!ctrl.disableContent && (!ctrl.page.content.en || !ctrl.page.content.ru)) {
                    recrutoidPopup('Error!', 'Content can\'t be empty', 'error');
                    return;
                }
            }

            ctrl.page.newsBox = ctrl.newsBoxesMode;

            CmsDAO.save(ctrl.page).then(function (data)
            {
                if(ctrl.image) {
                    return CmsDAO.uploadNewsImage(ctrl.image, data);
                }
            }).then(function ()
            {
                recrutoidPopup('Success!', 'You have added a new page.', 'success');
                $location.path('/admin/cms/pages-list');
            }).catch(function (error)
            {
                recrutoidPopup('Error!', error.data, 'error');
            });
        };

        this.photo = function (files)
        {
            if(files && files.length) {
                ctrl.image = files[0];
            }
        };

        this.removeImage = function ()
        {
            delete ctrl.image;
        };

        this.taOptions = [
            ['h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'p', 'pre', 'quote'],
            ['bold', 'italics', 'underline', 'strikeThrough', 'ul', 'ol', 'redo', 'undo', 'clear'],
            ['justifyLeft', 'justifyCenter', 'justifyRight', 'indent', 'outdent'],
            ['html', 'insertImage','insertLink', 'insertVideo', 'wordcount', 'charcount']
        ];

        (function init()
        {
            ctrl.newsBoxesMode = $location.search().hasOwnProperty('news-boxes');
            if('create' === $routeParams.slug) {
                ctrl.page = {
                    title: {},
                    //slug: {},
                    keywords: {},
                    metadescription: {},
                    content: {}
                };
            }
            else {
                CmsDAO.get($routeParams.slug).then(function (result)
                {
                    ctrl.page = result;
                });
            }

            if(-1 < ['home', 'contact'].indexOf($routeParams.slug)) {
                ctrl.limited = true;
                if('home' === $routeParams.slug) {
                    ctrl.disableContent = true;
                }
            }
        })();
    }

    angular.module('recrutoid.cms').controller('EditPageCMSCtrl',
            ['$location', '$routeParams', 'CmsDAO', 'recrutoidPopup', EditPageCMSCtrl]);
})();
