(function ()
{
    'use strict';

    function PagesListCMSCtrl($location, CmsDAO, recrutoidPopup, Notification, paginationSupport)
    {
        var ctrl = this;
        this.filter = {size: 20, from: 0, query: ''};

        this.remove = function(pageId) {
            recrutoidPopup({
                title: 'Danger',
                text: 'Are you <b>sure</b>?',
                html: true,
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, I\'m',
                closeOnConfirm: true
            }, function (isConfirm)
            {
                if (isConfirm) {
                    CmsDAO.remove(pageId).then(function ()
                    {
                        refresh();
                        Notification.success('Success, page deleted.');
                    });
                }
            });
        };

        this.edit = function(pageSlug) {
            $location.path('/admin/cms/edit-page/' + pageSlug).search(ctrl.newsBoxesMode ? 'news-boxes' : '');
        };


        var refresh = paginationSupport(ctrl, function (callback)
        {
            CmsDAO.query(ctrl.filter).then(function (results)
            {
                ctrl.pages = results.results;
                for(var i = 0; i < ctrl.pages.length;i++) {
                    ctrl.pages[i].createDate = moment(ctrl.pages[i].createDate).fromNow();
                }
                callback(results.total);
            });
        });


        this.search = refresh;

        (function init()
        {
            ctrl.filter.newsBoxes = ctrl.newsBoxesMode = $location.search().hasOwnProperty('news-boxes');
            refresh();
        })();
    }

    angular.module('recrutoid.cms').controller('PagesListCMSCtrl',
            ['$location', 'CmsDAO', 'recrutoidPopup', 'Notification', 'paginationSupport', PagesListCMSCtrl]);
})();
