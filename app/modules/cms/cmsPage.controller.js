(function ()
{
    'use strict';

    function CmsPageController($filter, $rootScope, $routeParams, CmsDAO)
    {
        var ctrl = this;

        ctrl.page = $routeParams.slug;

        (function init()
        {
            CmsDAO.get(ctrl.page).then(function (slug)
            {
                ctrl.content = slug.content;

                $rootScope.$emit('event:setMetaData', {
                    pageTitle: $filter('translateDomain')(slug.title),
                    keywords: $filter('translateDomain')(slug.keywords),
                    description: $filter('translateDomain')(slug.metadescription)
                });
            });
        })();
    }

    angular.module('recrutoid.cms').controller('CmsPageController', ['$filter', '$rootScope', '$routeParams', 'CmsDAO',
        CmsPageController]);
})();
