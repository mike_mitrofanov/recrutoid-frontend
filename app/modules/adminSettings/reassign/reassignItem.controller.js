(function ()
{
    'use strict';

    function ReassignItemController(data)
    {
        this.data = data.data;
        this.type = data.type;
        this.toRemove = data.toRemove;
    }

    angular.module('recrutoid.adminSettings').controller('ReassignItemController', ['data', ReassignItemController]);
})();
