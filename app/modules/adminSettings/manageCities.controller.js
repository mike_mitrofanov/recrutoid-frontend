(function ()
{

    'use strict';

    function ManageCitiesCtrl($modal, CitiesDAO, recrutoidPopup, Notification, gettext)
    {
        var ctrl = this;
        var text;
        var successDisplay = gettext('Success');

        function refresh() {
            CitiesDAO.query().then(function (data)
            {
                ctrl.cities = data;
            });
        }

        this.delete = function (city)
        {
            $modal.open({
                size: 'md',
                controller: 'ReassignItemController as reassign',
                templateUrl: 'modules/adminSettings/reassign/reassignItem.modal.tpl.html',
                resolve: {
                    data: function()
                    {
                        return {
                            data: ctrl.cities,
                            type: 'city',
                            toRemove: city
                        };
                    }
                }
            }).result.then(function (itemId)
            {
                recrutoidPopup({
                    title: gettext('Danger'),
                    text: gettext('message_are_you_sure'),
                    html: true,
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: gettext('Message_Yes_I_Am'),
                    closeOnConfirm: true
                }, function (isConfirm)
                {
                    if (isConfirm) {
                        CitiesDAO.remove(city.id, itemId).then( function() {
                            refresh();
                            text = gettext('Message_Success_City_Deleted');
                            Notification.success(text);
                        });
                    }
                });
            });
        };

        this.addCity = function() {
            var newName = {
                name: ctrl.newName,
                oilGasCamp: ctrl.oilGasCamp?true:false
            };
            CitiesDAO.update(newName).then( function() {
                refresh();
                ctrl.newName = '';
                ctrl.oilGasCamp = false;
                text = gettext('Message_You_Have_Added_New_City_To_The_List');
                recrutoidPopup(successDisplay, text, 'success');
            });
        };

        this.editCity = function(id, nameEn, nameRu, gasCamp) {
            CitiesDAO.update({id: id, name: {en: nameEn, ru: nameRu}, oilGasCamp: gasCamp?true:false}).then( function() {
                refresh();
                text = gettext('Message_Success_Category_Edited');
                Notification.success(text);
            });
        };

        refresh();
    }

    angular.module('recrutoid.adminSettings').controller('ManageCitiesCtrl', ['$modal', 'CitiesDAO', 'recrutoidPopup', 'Notification', 'gettext', ManageCitiesCtrl]);
})();
