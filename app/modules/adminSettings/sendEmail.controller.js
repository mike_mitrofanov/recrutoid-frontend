(function()
{
    'use strict';

    function SendEmailController($location, $timeout, $filter, $q, CommonDAO, gettext, recrutoidPopup, UserDAO, SubscriptionsDAO)
    {

        var refresh;
        var ctrl = this;
        this.emailBody = {};
        this.selectedAddresses = {};
        this.addresseesEnum = [];
        this.roles = ['normal', 'subscriber', 'employer', 'admin'];

        this.send = function ()
        {
            var emailBody = angular.copy(ctrl.emailBody);
            emailBody.addressees = ctrl.selectedAddresses.role;
            
            CommonDAO.sendEmail(emailBody).then(function ()
            {
                recrutoidPopup(gettext('Success'), gettext('Message_Messages_Successfully_Sent'), 'success');
            });
        };

        function getUserData(){
            var promises = [];
            var promise;
            ctrl.roles.forEach(function(role){
                var defaultFilter = {
                    banned: false,
                    hidden: false,
                    verified: true,
                    role: role,
                    size: -1
                };
                promise = role != 'subscriber'?UserDAO.query(defaultFilter):SubscriptionsDAO.query({size: -1, verified: true, banned: false});
                promises.push(promise);
            });

            return $q.all(promises);
        }

        (function init()
        {

            getUserData().then(function(data){
                var role_array  =  [];
                var totalCount = 0;

                ctrl.roles.forEach(function(role, index){
                    var items = data[index];
                    if(items.total){
                        role_array.push({
                            role: role,
                            name: role == 'normal'?'jobseekers':role+'s',
                            count: items.total
                        });
                        totalCount += items.total;
                    }
                });

                role_array.push({
                    role: 'all',
                    count: totalCount
                });

                ctrl.addresseesEnum = role_array;
                if($location.search().to){
                    ctrl.selectedAddresses = $filter('filter')(role_array, {role: $location.search().to})[0];
                } else {
                    ctrl.selectedAddresses = role_array[role_array.length - 1];
                }
            });
        })();

        this.emailBodyhtml = [
            ['h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'p', 'pre', 'quote'],
            ['bold', 'italics', 'underline', 'strikeThrough', 'ul', 'ol', 'redo', 'undo', 'clear'],
            ['justifyLeft', 'justifyCenter', 'justifyRight', 'indent', 'outdent'],
            ['html', 'insertImage','insertLink', 'insertVideo', 'wordcount', 'charcount']
        ];



         

                    



    }



    angular.module('recrutoid.adminSettings').controller('SendEmailController', ['$location', '$timeout', '$filter', '$q', 'CommonDAO', 'gettext', 'recrutoidPopup', 'UserDAO', 'SubscriptionsDAO', SendEmailController]);
})();
