(function ()
{

    'use strict';

    function ManageCategoriesCtrl($modal, CategoriesDAO, recrutoidPopup, Notification, gettext)
    {
        var ctrl = this;

        var text;
        var successDisplay = gettext('Success!');

        function refresh()
        {
            CategoriesDAO.query().then(function (data)
            {
                ctrl.categories = data;
            });
        }

        this.delete = function (category)
        {
            $modal.open({
                size: 'md',
                controller: 'ReassignItemController as reassign',
                templateUrl: 'modules/adminSettings/reassign/reassignItem.modal.tpl.html',
                resolve: {
                    data: function()
                    {
                        return {
                            data: ctrl.categories,
                            type: 'category',
                            toRemove: category
                        };
                    }
                }
            }).result.then(function (itemId)
            {
                recrutoidPopup({
                    title: gettext('Danger'),
                    text: gettext('message_are_you_sure'),
                    html: true,
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: gettext('popup_yes_i_am_button'),
                    closeOnConfirm: true
                }, function (isConfirm)
                {
                    if (isConfirm) {
                        CategoriesDAO.remove(category.id, itemId).then(function ()
                        {
                            refresh();
                            text = gettext('Message_Success_Category_Deleted');
                            Notification.success(text);
                        });
                    }
                });
            });
        };

        this.addCategory = function ()
        {
            var newName = {
                name: ctrl.newName
            };
            CategoriesDAO.update(newName).then(function ()
            {
                refresh();
                ctrl.newName = '';
                text = gettext('Message_You_Have_Added_New_Category_To_The_List');
                recrutoidPopup(successDisplay, text, 'success');
            });
        };

        this.editCategory = function (id, nameEn, nameRu)
        {
            CategoriesDAO.update({id: id, name: {en: nameEn, ru: nameRu}}).then(function ()
            {
                refresh();
                text = gettext('Message_Success_Category_Edited');
                Notification.success(text);
            });
        };

        refresh();
    }

    angular.module('recrutoid.adminSettings').controller('ManageCategoriesCtrl',
            ['$modal', 'CategoriesDAO', 'recrutoidPopup', 'Notification', 'gettext', ManageCategoriesCtrl]);
})();
