(function ()
{
    'use strict';

    function ManageTranslationsCtrl($scope, $timeout, LanguagesDAO, paginationSupport, TranslationsDAO, gettext, Notification)
    {
        var ctrl = this;
        var refresh = angular.noop;
        this.currentTranslation = null;
        var text;

        var defaultFilter = {
            action: null,
            size: 15,
            from: 0
        };
        this.filter = angular.copy(defaultFilter);

        $scope.$watch(function () {
            return ctrl.currentTranslation;
        }, function () {
            if (!!ctrl.currentTranslation) {
                $timeout(function(){
                    ctrl.showEditor = true;
                },300);
            }
        });

        this.edit = function(translation) {
            ctrl.currentTranslation = angular.copy(translation);

            ctrl.results.forEach(function(trans){
                if ('selected' in trans) {
                    delete trans.selected;
                }
            });
            translation.selected = true;
        };

        this.save = function() {
            TranslationsDAO.update(ctrl.currentTranslation).then(function ()
            {
                refresh();
                text = gettext('Message_Success_Translation_Edited');
                Notification.success(text);
            });
        };

        (function init()
        {
            LanguagesDAO.query().then(function (data)
            {
                ctrl.languages = data.results;
            });
            
            refresh = paginationSupport(ctrl, function (callback)
            {
                TranslationsDAO.query(ctrl.filter).then(function (results)
                {
                    ctrl.results = results.results;
                    ctrl.total = results.total;
                    ctrl.showTotal = true;
                    angular.forEach(ctrl.results, function (value)
                    {
                        value.createDateHumanized = moment.duration(moment(value.createDate).diff()).humanize(true);
                    });
                    callback(results.total);
                });
            });
            refresh();

        })();
    }

    angular.module('recrutoid.vacancy').controller('ManageTranslationsCtrl',
      ['$scope', '$timeout', 'LanguagesDAO', 'paginationSupport', 'TranslationsDAO', 'gettext', 'Notification', ManageTranslationsCtrl]);
})();
