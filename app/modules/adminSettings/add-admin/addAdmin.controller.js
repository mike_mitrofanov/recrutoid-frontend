(function()
{
    'use strict';
    function AddAdminController($modalInstance, UserDAO)
    {
        var ctrl = this;

        this.addAdmin = function ()
        {
            UserDAO.newAdmin({email: ctrl.newAdminEmail, password: ctrl.newAdminPassword}).then(function ()
            {
                $modalInstance.close();
            }).catch(function (error)
            {
                $modalInstance.dismiss(error);
            });
        };
    }

    angular.module('recrutoid.adminSettings').controller('AddAdminController', ['$modalInstance', 'UserDAO', AddAdminController]);
})();
