angular.module('recrutoid.adminSettings').controller('ManageLanguagesCtrl',
        ['LanguagesDAO',
         'paginationSupport',
         'Notification',
         'recrutoidPopup',
         'gettext',
         function (LanguagesDAO, paginationSupport, Notification, recrutoidPopup, gettext)
         {
             'use strict';

             var ctrl = this;
             this.languages = {filter: {size: 10, from: 0}};

             var text;
             var successDisplay = gettext('Success');

             var refresh = paginationSupport(ctrl.languages, function (callback)
             {
                 ctrl.newLang = '';
                 LanguagesDAO.query(ctrl.languages.filter).then(function (results)
                 {
                     ctrl.languages.list = results.results;
                     callback(results.total);
                 });
             });

             this.edit = function (id, language, code)
             {
                 LanguagesDAO.save({id: id, language: language, code: code}).then(function ()
                 {
                     refresh();
                     text = gettext('Message_Success_Language_Edited');
                     Notification.success(text);
                 });
             };

             this.addLanguage = function ()
             {
                 LanguagesDAO.save(ctrl.newLang).then(function ()
                 {
                     refresh();
                     text = gettext('Message_You_Have_Added_New_Language_To_The_List');
                     recrutoidPopup(successDisplay, text, 'success');
                 });
             };

             this.remove = function (languageId)
             {
                 recrutoidPopup({
                     title: gettext('Danger'),
                     text: gettext('message_are_you_sure'),
                     html: true,
                     type: 'warning',
                     showCancelButton: true,
                     confirmButtonText: gettext('Message_Yes_I_Am'),
                     closeOnConfirm: true
                 }, function (isConfirm)
                 {
                     if (isConfirm) {
                         LanguagesDAO.remove(languageId).then(function ()
                         {
                             refresh();
                             text = gettext('Message_Success_Language_Deleted');
                             Notification.success(text);
                         });
                     }
                 });
             };

             refresh();
         }]);
