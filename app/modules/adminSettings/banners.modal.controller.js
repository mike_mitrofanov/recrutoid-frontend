(function ()
{
    'use strict';

    function BannersModalController(banner, BannersDAO, gettext, recrutoidPopup)
    {
        var ctrl = this;
        var urlIterator = 0;

        this.banner = banner;

        this.upload = function (files)
        {
            BannersDAO.upload(files[0], {
                id: banner.id,
                location: banner.location
            }).then(function (newBanner)
            {
                ctrl.banner = newBanner;
                ctrl.banner.url += (urlIterator++).toString();
                var text = gettext('Message_Banner_Image_Successfully_Updated');
                recrutoidPopup('Success', text, 'success');
            });
        };
    }

    angular.module('recrutoid.adminSettings').controller('BannersModalController', ['banner', 'BannersDAO', 'gettext', 'recrutoidPopup', BannersModalController]);
})();
