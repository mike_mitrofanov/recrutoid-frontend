(function ()
{
    'use strict';

    function ManagePhrasesCtrl($scope, $timeout, LanguagesDAO, paginationSupport, PhrasesDAO, gettext, Notification, recrutoidPopup)
    {
        var ctrl = this;
        var refresh = angular.noop;
        this.currentPhrase = null;
        var text;

        var defaultFilter = {
            action: null,
            size: 15,
            from: 0
        };
        this.filter = angular.copy(defaultFilter);

        $scope.$watch(function () {
            return ctrl.currentPhrase;
        }, function () {
            if (!!ctrl.currentPhrase) {
                $timeout(function(){
                    ctrl.showEditor = true;
                    
                },300);
            }
        });


        ctrl.addNew = true;
        this.edit = function(translation) {
            ctrl.currentPhrase = angular.copy(translation);

            ctrl.results.forEach(function(trans){
                if ('selected' in trans) {
                    delete trans.selected;
                }
            });
            translation.selected = true;
        };

        this.save = function() {
            PhrasesDAO.update(ctrl.currentPhrase).then(function ()
            {
                refresh();
                text = gettext('Message_Success_Translation_Edited');
                Notification.success(text);
            });
        };

        this.addNew = function() {
            PhrasesDAO.add(ctrl.newPhrase).then(function ()
            {
                refresh();
                text = gettext('Message_Success_Translation_Edited');
                Notification.success(text);
            });
        };


        this.remove = function (phrase)
        {
            var title = gettext('Warning');
            var text = gettext('message_are_you_sure');
            var confirm = gettext('popup_yes_i_am_button');
            recrutoidPopup({
                title: title,
                text: text,
                html: true,
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: confirm,
                closeOnConfirm: true
            }, function (isConfirm)
            {
                if (isConfirm) {

                    PhrasesDAO.remove(phrase).then(function ()
                    {
                        refresh();
                        var text = gettext('Phrase removed');
                        Notification.success(text);
                    });
                }
            });
        };

        (function init()
        {
            LanguagesDAO.query().then(function (data)
            {
                ctrl.languages = data.results;
            });
            
            refresh = paginationSupport(ctrl, function (callback)
            {
                PhrasesDAO.query(ctrl.filter).then(function (results)
                {
                    ctrl.results = results.results;
                    ctrl.total = results.total;
                    ctrl.showTotal = true;
                    angular.forEach(ctrl.results, function (value)
                    {
                        value.createDateHumanized = moment.duration(moment(value.createDate).diff()).humanize(true);
                    });
                    callback(results.total);
                });
            });
            refresh();

        })();
    }

    angular.module('recrutoid.vacancy').controller('ManagePhrasesCtrl',
      ['$scope', '$timeout', 'LanguagesDAO', 'paginationSupport', 'PhrasesDAO', 'gettext', 'Notification', 'recrutoidPopup', ManagePhrasesCtrl]);
})();
