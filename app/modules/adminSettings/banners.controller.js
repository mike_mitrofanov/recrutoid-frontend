(function ()
{
    'use strict';

    function BannersListController($modal, $scope, BannersDAO, gettext, paginationSupport, recrutoidPopup)
    {
        var ctrl = this;

        ctrl.filter = {
            from: 0, size: 10
        };

        ctrl.newBanner = {
            location: 0
        };

        ctrl.bannerPreview = function (banner)
        {
            $modal.open({
                size: 'md',
                templateUrl: 'modules/adminSettings/banners.modal.tpl.html',
                controller: 'BannersModalController as modal',
                resolve: {
                    banner: function ()
                    {
                        return banner;
                    }
                }
            });
        };

        ctrl.updateDescription = function(banner)
        {
            banner = {id: banner.id, description: banner.description};
            BannersDAO.save(banner).then(function ()
            {
                var text = gettext('Descriptions successfully updated');
                recrutoidPopup('Success', text, 'success');
            });
        };

        ctrl.remove = function(banner)
        {
            var title = gettext('message_are_you_sure');
            var confirm = gettext('Message_Yes_I_Am');
            var text = gettext('Message_Do_You_Really_Want_To_Delete_Banner "');

            recrutoidPopup({
                title: title,
                type: 'warning',
                text: (text + banner.description + '"?'),
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: confirm,
                closeOnConfirm: false
            }, function (confirmed)
            {
                if(confirmed) {
                    BannersDAO.remove(banner.id).then(function ()
                    {
                        refresh();
                        var text = gettext('Message_Banner_Successfully_Removed');
                        recrutoidPopup('Success', text, 'success');
                    });
                }
            });
        };

        ctrl.addBanner = function ()
        {
            if(!ctrl.newBannerFile) {
                var text = gettext('Message_Select_Banner_Image_To_Upload');
                recrutoidPopup('Warning', text, 'warning');
                return;
            }

            BannersDAO.save(ctrl.newBanner).then(function (data)
            {
                return BannersDAO.upload(ctrl.newBannerFile, {
                    id: data.id, location: data.location
                });
            }).then(function ()
            {
                ctrl.newBanner = {
                    location: 0
                };
                delete ctrl.newBannerFile;
                refresh();
                var text = gettext('Message_Banner_Successfully_Added');
                recrutoidPopup('Success', text, 'success');
            });
        };

        ctrl.setBannerImage = function (files)
        {
            if (!files) {
                return;
            }
            ctrl.newBannerFile = files[0];
        };

        ctrl.activateBanner = function(banner)
        {
            BannersDAO.activate(banner).then(function ()
            {
                refresh();
                var text = gettext('Message_Banner_Successfully_activated');
                recrutoidPopup('Success', text, 'success');
            });
        };

        ctrl.deactivateBanner = function(banner)
        {
            BannersDAO.deactivate(banner).then(function ()
            {
                refresh();
                var text = gettext('Message_Banner_Successfully_deactivated');
                recrutoidPopup('Success', text, 'success');
            });
        };

        var refresh = paginationSupport(ctrl, function (callback)
        {
            BannersDAO.query(ctrl.filter).then(function (banners)
            {
                ctrl.banners = banners.results;
                callback(banners.total);
            });
        });

        refresh();
    }

    angular.module('recrutoid.adminSettings').controller('BannersListController',
            ['$modal', '$scope', 'BannersDAO', 'gettext', 'paginationSupport', 'recrutoidPopup', BannersListController]);
})();
