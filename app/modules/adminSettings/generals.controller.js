(function ()
{
    'use strict';

    function GeneralsController(gettext, SettingsDAO, siteSettings, recrutoidPopup)
    {
        var ctrl = this;
        var oneDay = (24 * 3600000);

        this.save = function()
        {
            var settings = angular.copy(ctrl.settings);
            settings.userExpirationTime *= oneDay;

            SettingsDAO.save(settings).then(function ()
            {
                var text = gettext('Message_Settings_Successfully_Updated');
                recrutoidPopup('Success', text, 'success');
                siteSettings.reload();
            });
        };

        (function init()
        {
            SettingsDAO.query().then(function (result)
            {
                result.userExpirationTime /= oneDay;
                ctrl.settings = result;
            });
        })();
    }

    angular.module('recrutoid.adminSettings').controller('GeneralsController', ['gettext', 'SettingsDAO', 'siteSettings', 'recrutoidPopup', GeneralsController]);
})();
