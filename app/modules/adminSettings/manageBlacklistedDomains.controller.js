(function ()
{
    'use strict';

    function BlacklistedDomainsController($scope, BlacklistedDomainsDAO, Notification, paginationSupport, recrutoidPopup, gettext)
    {
        var ctrl = this;
        this.filter = {from: 0, size: 20};

        this.newName = '';

        var text;
        var warningDisplay = gettext('Warning!');
        var successDisplay = gettext('Success!');

        var refresh = paginationSupport(this, function (callback)
        {
            BlacklistedDomainsDAO.query(ctrl.filter).then(function (results)
            {
                ctrl.domainsList = results.results;
                callback(results.total);
            });
        });

        this.delete = function (id)
        {
            recrutoidPopup({
                title: gettext('Message_Danger'),
                text: gettext('message_are_you_sure'),
                html: true,
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: gettext('Message_Yes_I_Am'),
                closeOnConfirm: true
            }, function (isConfirm)
            {
                if (isConfirm) {
                    BlacklistedDomainsDAO.remove(id).then( function() {
                        refresh();
                        text = gettext('Message_Success_DomainDeleted');
                        Notification.success(text);
                    });
                }
            });
        };

        this.addDomain = function() {
            var newName = {
                name: ctrl.newName
            };
            BlacklistedDomainsDAO.update(newName).then(function()
            {
                refresh();
                ctrl.newName = '';
                text = gettext('Message_You_Have_Added_New_Domain_To_The_Blacklist');
                recrutoidPopup(successDisplay, text, 'success');
            }).catch(function (error)
            {
                if(409 === error.status) {
                    recrutoidPopup(warningDisplay, error.data, 'warning');
                }
            });
        };

        this.editDomain = function(id, name) {
            BlacklistedDomainsDAO.update({id: id, name: name}).then( function() {
                refresh();
                text = gettext('Success, domain edited.');
                Notification.success(text);
            });
        };

        refresh();
    }

    angular.module('recrutoid.adminSettings').controller('BlacklistedDomainsController',
            ['$scope', 'BlacklistedDomainsDAO', 'Notification', 'paginationSupport', 'recrutoidPopup', 'gettext', BlacklistedDomainsController]);
})();
