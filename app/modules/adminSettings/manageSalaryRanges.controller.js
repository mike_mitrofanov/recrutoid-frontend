(function ()
{

    'use strict';

    function ManageSalaryRangesCtrl($modal, SalaryRangesDAO, recrutoidPopup, Notification, gettext, $scope)
    {
        var ctrl = this;
        var text;
        var successDisplay = gettext('Success!');

        function refresh() {
            SalaryRangesDAO.query().then(function (data)
            {
                ctrl.salaryRanges = data;
            });
        }

        $scope.user = {
            status: [2, 3]
        };

        $scope.statuses = [
            {value: 1, text: 'status1'},
            {value: 2, text: 'status2'},
            {value: 3, text: 'status3'}
        ];

        $scope.showStatus = function() {
            var selected = [];
            angular.forEach($scope.statuses, function(s) {
                if ($scope.user.status.indexOf(s.value) >= 0) {
                    selected.push(s.text);
                }
            });
            return selected.length ? selected.join(', ') : 'Not set';
        };

        this.delete = function (salaryRange)
        {
            $modal.open({
                size: 'md',
                controller: 'ReassignItemController as reassign',
                templateUrl: 'modules/adminSettings/reassign/reassignItem.modal.tpl.html',
                resolve: {
                    data: function()
                    {
                        return {
                            data: ctrl.salaryRanges,
                            type: 'salary range',
                            toRemove: salaryRange
                        };
                    }
                }
            }).result.then(function (itemId)
            {
                recrutoidPopup({
                    title: gettext('Danger'),
                    text: gettext('message_are_you_sure'),
                    html: true,
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: gettext('Message_Yes_I_Am'),
                    closeOnConfirm: true
                }, function (isConfirm)
                {
                    if (isConfirm) {
                        SalaryRangesDAO.remove(salaryRange.id, itemId).then( function() {
                            refresh();
                            text = gettext('Message_Success_SalaryRange_Deleted');
                            Notification.success(text);
                        });
                    }
                });
            });
        };

        this.addSalaryRange = function() {
            var newName = {
                name: ctrl.newName
            };
            SalaryRangesDAO.update(newName).then( function() {
                refresh();
                ctrl.newName = '';
                text = gettext('Message_You_Have_Added_New_SalaryRange_To_The_List');
                recrutoidPopup(successDisplay, text, 'success');
            });
        };

        this.editSalaryRange = function(id, order, nameEn, nameRu) {
            SalaryRangesDAO.update({id: id, order: order, name: {en: nameEn, ru: nameRu}}).then( function() {
                refresh();
                text = gettext('Message_Success_Category_Edited');
                Notification.success(text);
            });
        };

        refresh();
    }

    angular.module('recrutoid.adminSettings').controller('ManageSalaryRangesCtrl', ['$modal', 'SalaryRangesDAO', 'recrutoidPopup', 'Notification', 'gettext', '$scope', ManageSalaryRangesCtrl]);
})();
