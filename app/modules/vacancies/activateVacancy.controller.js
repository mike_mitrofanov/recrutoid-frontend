(function ()
{

    'use strict';

    function ActivateVacancyCtrl($location, $routeParams, VacanciesDAO, recrutoidPopup, gettext)
    {
        VacanciesDAO.confirm($routeParams.token).then(function ()
        {
            var title = gettext('Congratulations');
            var text = gettext('Message_You_Have_Successfuly_Confirmed');
            var confirm = 'Message_Ok_Thanks';
            recrutoidPopup({
                title: title,
                text: text,
                html: true,
                type: 'success',
                showCancelButton: false,
                confirmButtonText: confirm,
                closeOnConfirm: true
            }, function (isConfirm)
            {
                if (isConfirm) {
                    $location.path('/');
                }
            });
        });
    }

    angular.module('recrutoid.vacancy').controller('ActivateVacancyCtrl', ['$location', '$routeParams', 'VacanciesDAO',
                                                                           'recrutoidPopup', 'gettext', ActivateVacancyCtrl]);
})();
