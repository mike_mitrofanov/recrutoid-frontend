(function ()
{

    'use strict';

    function EditVacancyCtrl($q, $route, $routeParams, $location, $timeout, VacanciesDAO, CategoriesDAO, SalaryRangesDAO, CitiesDAO,
                             CurrentUser, recrutoidModals, recrutoidPopup, gettext)
    {
        var ctrl = this;
        var initialStateOfPublishOnWebsite;

        this.categories = [];

        ctrl.taOptions = [
            ['bold', 'italics', 'underline', 'strikeThrough', 'ul', 'ol', 'redo', 'undo', 'clear'],
            ['justifyLeft', 'justifyCenter', 'justifyRight', 'indent', 'outdent']
        ];

        function activateVacancy(token)
        {
            VacanciesDAO.confirm({token: token}).then(function ()
            {
                var title = gettext('Congratulations');
                var confirm = gettext('Message_Ok_Thanks');
                var text = gettext('Message_You_Have_Successfuly_Confirmed');
                recrutoidPopup({
                    title: title,
                    text: text,
                    html: true,
                    type: 'success',
                    showCancelButton: false,
                    confirmButtonText: confirm,
                    closeOnConfirm: true
                });
            });
        }

        function init()
        {
            //TODO: Repair with data form backend
            ctrl.vacancy = {
                id: $routeParams.id, categoryId: {}, cityId: {}
            };

            CitiesDAO.query().then(function (result)
            {
                ctrl.cities = result;
                return SalaryRangesDAO.query();
            }).then(function (result)
            {
                ctrl.salaryRanges = result;
                return CategoriesDAO.query();
            }).then(function (result)
            {
                ctrl.categories = result;
                return VacanciesDAO.get(ctrl.vacancy.id);
            }).then(function (data)
            {
                ctrl.vacancy = data;
                initialStateOfPublishOnWebsite = ctrl.vacancy.publishOnWebsite;

                // verifying vacancy
                // property ctrl.vacancy.token decides if person is owner or just admin
                if (!ctrl.vacancy.verified && ctrl.vacancy.token) {
                    activateVacancy(ctrl.vacancy.token);
                }
            });
            CurrentUser.resolveOrReject().then(function (user)
            {
                ctrl.isAdmin = 'admin' === user.role;
            });
        }

        this.deleteVacancy = function ()
        {
            var title = gettext('message_are_you_sure');
            var confirm = gettext('Message_Yes_I_Am');
            var text = gettext('Message_You_Wont_Be_Able_To_Restore_This_Vacancy');

            recrutoidPopup({
                title: title,
                type: 'warning',
                text: text,
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: confirm,
                closeOnConfirm: false
            }, function (isConfirm)
            {
                if (isConfirm) {
                    VacanciesDAO.remove(ctrl.vacancy.id).then(function ()
                    {
                        $timeout(function ()
                        {
                            var title = gettext('Success');
                            var confirm = gettext('OK');
                            var text = gettext('Message_We_Have_Deleted_This_Vacancy');
                            recrutoidPopup({
                                title: title,
                                type: 'success',
                                text: text,
                                showCancelButton: false,
                                confirmButtonText: confirm,
                                closeOnConfirm: true
                            }, function (isConfirm)
                            {
                                if (isConfirm) {
                                    $location.path('/');
                                }
                            });
                        }, 100);
                    });
                }
            });
        };

        this.saveVacancy = function ()
        {
            function doSave()
            {
                var promise;

                if (false === initialStateOfPublishOnWebsite && true === ctrl.vacancy.publishOnWebsite) {
                    promise = recrutoidModals.entireFlow();
                }
                else {
                    var defer = $q.defer();
                    defer.resolve();
                    promise = defer.promise;
                }

                promise.then(function ()
                {
                    var vacancy = angular.copy(ctrl.vacancy);
                    vacancy.categories = vacancy.categories.map(function (category)
                    {
                        return category.id;
                    });
                    vacancy.cityId = vacancy.cityId.id;
                    VacanciesDAO.update(vacancy).then(function ()
                    {
                        $timeout(function ()
                        {
                            var title = gettext('Success');
                            var confirm = gettext('OK');
                            var text = gettext('Message_Your_Vacancy_Has_Been_Changed');
                            recrutoidPopup({
                                title: title,
                                type: 'success',
                                text: text,
                                showCancelButton: false,
                                confirmButtonText: confirm,
                                closeOnConfirm: true
                            }, function (isConfirm)
                            {
                                if (isConfirm) {
                                    if (!ctrl.isAdmin) {
                                        $location.path('/');
                                    }
                                }
                            });
                        }, 100);
                    });
                });
            }

            var warning = gettext('Warning');
            var text;
            if (!ctrl.vacancy.publishOnWebsite && !ctrl.vacancy.publishViaEmail) {
                text = gettext('Message_You_Must_Publish_The_Vacancy_At_Least_In_One_Of_Website_Or_Via_Emails');
                recrutoidPopup(warning, text, 'warning');
            } else if (!ctrl.vacancy.details) {
                text = gettext('Message_Vacancy_Description_Field_Is_Mandatory');
                recrutoidPopup(warning, text, 'warning');
            } else if (0 === ctrl.vacancy.categories.length) {
                text = gettext('Message_Category_Field_Is_Mandatory');
                recrutoidPopup(warning, text, 'warning');
            }
            else {
                doSave();
            }
        };

        init();
    }

    angular.module('recrutoid.vacancy').controller('EditVacancyCtrl',
            ['$q',
             '$route',
             '$routeParams',
             '$location',
             '$timeout',
             'VacanciesDAO',
             'CategoriesDAO',
             'SalaryRangesDAO',
             'CitiesDAO',
             'CurrentUser',
             'recrutoidModals',
             'recrutoidPopup',
             'gettext',
             EditVacancyCtrl]);
})();
