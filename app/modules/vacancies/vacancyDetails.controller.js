(function ()
{
    'use strict';

    function VacancyDetailsCtrl($filter, $location, $routeParams, $rootScope, $scope, CurrentUser, gettext, recrutoidModals,
                                recrutoidPopup, VacanciesDAO, CommonDAO)
    {
        var ctrl = this;

        this.viewUrl = $location.absUrl();

        if (!$routeParams.vacancyId) {
            $location.path('/404');
            return;
        }

        this.back = function ()
        {
            if ($location.search().hasOwnProperty('myVac')) {
                $location.search({});
                $location.path("/user/settings/myVacancies");
            }else{
                if ($scope.locationHistory.length > 1) {
                    $scope.locationHistory.pop();
                    $location.url($scope.locationHistory.pop());
                    $location.replace();
                } else {
                    $location.path('/vacancies');
                }
            }

        };

        this.returnToTheListByEmployer = function ()
        {
            if ($location.search().hasOwnProperty('appliers')) {
                $location.path("/user/settings/myVacancies");
            }else{
                $location.path("/users");
            }
        };

        function applyOrShowWarning(user)
        {
			CommonDAO.isProfileActive().then(function (data)
			{
				var warning, text;

				if (!data.isActive) {
					recrutoidPopup({
						title: gettext('Warning'),
						text: gettext('There_is_no_active_CV_on_the_website'),
						html: true,
						type: 'warning',
						showCancelButton: true,
						confirmButtonText: gettext('CV_Creation'),
						closeOnConfirm: true
					}, function (isConfirm)
					{
						if (isConfirm) {
							$location.url('/user/settings/edit-cv');
						}
					});
				}
				else if('normal' !== user.role) {
					warning = gettext('Warning');
					text = gettext('Message_To_Apply_For_This_Vacancy_Your_Account_Has_To_Be_Job_Seeker_Type');
					recrutoidPopup(warning, text, 'warning');
				}
				else if(ctrl.isApplied()) {
					warning = gettext('Warning');
					text = gettext('Message_You_Applied_To_This_Vacancy_Before');
					recrutoidPopup(warning, text, 'warning');
				}
				else {
					VacanciesDAO.applyTo(ctrl.vacancy.id).then(refresh);
				}
			});
        }

        this.apply = function ()
        {
            CurrentUser.resolve().then(function (user)
            {
                if(!user.id) {
                    recrutoidModals.entireFlow({
                        register: {
                            initialUserType: 'normal'
                        }
                    }).then(resolveCurrentUser).then(applyOrShowWarning);
                }
                else {
                    applyOrShowWarning(user);
                }
            });
        };

        this.isApplied = function ()
        {
            if (ctrl.vacancy && ctrl.currentUser) {
                return -1 !== ctrl.vacancy.applicants.indexOf(ctrl.currentUser.id);
            }
        };

        this.sendMail = function(fromWhom) {
            window.location.href = 'mailto:' + fromWhom;
        };

        function refresh()
        {
            return VacanciesDAO.get($routeParams.vacancyId).then(function (vacancy)
            {
                ctrl.vacancy = vacancy;
                vacancy.createDateHumanized = moment.duration(moment(vacancy.createDate).diff()).humanize(true);
                vacancy.applicants = vacancy.applicants.map(function (elem)
                {
                    return elem.user;
                });

                var description = 'Vacancy ' + vacancy.title + ' at ' + $filter('translateDomain')(vacancy.cityId.name);
                if(vacancy.salary) {
                    description += '. Salary: ' + vacancy.salary.value + ' ' + vacancy.salary.currency + '. ';
                }
                description += ('Job in ' + $filter('translateDomain')(vacancy.cityId.name) +
                ' Kazakhstan posted ' + moment(vacancy.createDate).format('DD.MM.YYYY'));
                $rootScope.$emit('event:setMetaData', {
                    pageTitle: 'Vacancy ' + vacancy.title + ' at ' + $filter('translateDomain')(vacancy.cityId.name),
                    keywords: 'Vacancy, ' + vacancy.title + ', ' + $filter('translateDomain')(vacancy.cityId.name) +
                        ', Kazakshstan, job',
                    description: description
                });

                return vacancy;
            });
        }
        function resolveCurrentUser(vacancy)
        {
            return CurrentUser.resolve().then(function (currentUser)
            {
                if(currentUser && currentUser.id) {
                    if('employer' === currentUser.role && currentUser.id !== vacancy.userId._id) {
                        $location.path('/');
                    }
                    ctrl.currentUser =  currentUser;
                }
                return currentUser;
            });
        }

        function visitedVacancies(xxx){
            var vacancyID = $routeParams.vacancyId;
            var userID;
            var userEmail;
            CurrentUser.resolveOrReject().then(function (user)
            {
                userID = user.id;
                userEmail = user.email;
                
            }).then(function (user){
                var data = {
                    vacancyId: vacancyID,
                    userId: userID,
                    userEmail: userEmail
                };

                VacanciesDAO.isNotVisited(data);

            });
            
        }

        (function init()
        {
            refresh().then(function(vacancy){
                resolveCurrentUser(vacancy);
            });
            visitedVacancies();

            
        })();

        $scope.$on('event:auth-loggedOut', resolveCurrentUser);
        $scope.$on('event:auth-loggedIn', resolveCurrentUser);
    }

    angular.module('recrutoid.vacancy').controller('VacancyDetailsCtrl',
            ['$filter', '$location', '$routeParams', '$rootScope', '$scope', 'CurrentUser', 'gettext', 
				'recrutoidModals', 'recrutoidPopup', 'VacanciesDAO', 'CommonDAO',
                VacancyDetailsCtrl]);
})();
