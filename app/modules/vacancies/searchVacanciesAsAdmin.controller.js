(function ()
{
    'use strict';

    function SearchVacanciesAsAdminCtrl($location, $modal, $http, $q, $scope, CategoriesDAO, CitiesDAO, SalaryRangesDAO, EmailDAO, ExporterDAO, Notification, paginationSupport, recrutoidPopup, VacanciesDAO,
                                        vacancyDetails, gettext)
    {
        var ctrl = this;
        var refresh = angular.noop;

        var defaultFilter = {
            adminPanel: true,
            emailOrTitle: null,
            categories: null,
            title: null,
            email: null,
            archived: $location.$$path.indexOf('waitingForApproval') == -1?false:null,
            companyName: null,
            city: null,
            salaryRange: null,
            approved: $location.$$path.indexOf('waitingForApproval') == -1,
            banned: null,
            registeredUser: null,
            publishMode: null,
            verified: null,
            size: 50,
            from: 0
        };

        this.filter = angular.copy(defaultFilter);

        this.clearPresets = function ()
        {
            ctrl.filter.title = null;
            ctrl.filter.email = null;
            ctrl.filter.companyName = null;
            ctrl.selectedCategory = null;
            ctrl.selectedCity = null;
            ctrl.selectedSalaryRange = null;
        };

        this.approve = function (vacancy)
        {
            VacanciesDAO.approve(vacancy.id).then(function ()
            {
                refresh();
                var text = gettext('Message_Vacancy_Approved');
                Notification.success(text);
            });
        };

        function vacancyModal(vacancyData, callback)
        {
            return $modal.open({
                size: 'lg',
                controller: 'PublishNewVacancyController as ctrl',
                templateUrl: 'modules/user/publish-new-vacancy/publishNewVacancy.modal.tpl.html',
                windowClass: 'publish-new-vacancy-modal',
                backdrop: 'static',
                resolve: {
                    vacancyData: function()
                    {
                        return vacancyData;
                    }
                }
            }).result.then(callback);
        }

        this.edit = function (vacancy)
        {
            vacancyModal({
                id: vacancy.id
            }, function ()
            {
                var text = gettext('Message_Changes_Successfully_Saved');
                recrutoidPopup('Success', text, 'success');
                refresh();
            });
        };

        this.verify = function (vacancy)
        {
            VacanciesDAO.verifyAll(vacancy.id).then(function ()
            {
                refresh();
                var text = gettext('Message_Vacancy_Verified');
                Notification.success(text);
            });

        };

        this.remove = function (vacancy)
        {
            var title = gettext('Warning');
            var text = gettext('message_are_you_sure');
            var confirm = gettext('popup_yes_i_am_button');
            recrutoidPopup({
                title: title,
                text: text,
                html: true,
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: confirm,
                closeOnConfirm: true
            }, function (isConfirm)
            {
                if (isConfirm) {
                    VacanciesDAO.remove(vacancy.id).then(function ()
                    {
                        refresh();
                        var text = gettext('Message_Vacancy_Deleted');
                        Notification.success(text);
                    });
                }
            });
        };

        this.doActionOnSelected = function(action)
        {
            if(-1 === ['remove', 'approve', 'resend'].indexOf(action)) {
                return;
            }

            var ids = [], promises = [];

            var title = gettext('Warning');
            var text = gettext('Are you sure to ' + action + ' selected?');
            if(action == 'resend') {
                text = gettext('Message_Are_You_Sure_To_Resend_Confirmation_Links_To_Selected');
            }
            var confirm = gettext('popup_yes_i_am_button');
            recrutoidPopup({
                title: title,
                text: text,
                html: true,
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: confirm,
                closeOnConfirm: true
            }, function (isConfirm)
            {
                if (isConfirm) {
                    angular.forEach(ctrl.results, function (elem)
                    {
                        if(elem.selected) {
                            ids.push(elem.id);
                        }
                        delete elem.selected;
                    });

                    ctrl.allSelected = false;

                    angular.forEach(ids, function (id)
                    {
                        if(action == 'resend') {
                            promises.push(EmailDAO.sendEmailToActiveVacancy(id))
                        }else{
                            promises.push(VacanciesDAO[action](id));
                        }
                        
                    });

                    if(promises.length) {
                        $q.all(promises).then(function ()
                        {
                            refresh();
                            var text;
                            if ('remove' === action) {
                                text = gettext('Message_Selected_Vacancies_Have_Been_Removed');
                            } else if ('approve' === action) {
                                text = gettext('Message_Selected_Vacancies_Have_Been_Approved');
                            } else if ('resend' === action) {
                                text = gettext('Message_Emails_Has_Been_Sent_To_Selected_Vacancies');
                            }
                            Notification.success(text);
                        });
                    }
                }
            });
        };

        this.setSearchTo = function (companyName)
        {
            ctrl.filter.companyName = companyName;
        };

        this.selectAll = function () {
            ctrl.results.forEach(function (vacancy){
                vacancy.selected = ctrl.allSelected;
            });
        };

        function onCategoryOrCityChange(newValue, oldValue)
        {
            if (newValue === oldValue) {
                return;
            }
            ctrl.filter.categories = ctrl.selectedCategory && ctrl.selectedCategory.id;
            ctrl.filter.city = ctrl.selectedCity && ctrl.selectedCity.id;
            ctrl.filter.salaryRange = ctrl.selectedSalaryRange && ctrl.selectedSalaryRange.id;
        }

        $scope.$watch(function ()
        {
            return ctrl.selectedCategory;
        }, onCategoryOrCityChange);

        $scope.$watch(function ()
        {
            return ctrl.selectedCity;
        }, onCategoryOrCityChange);

        $scope.$watch(function ()
        {
            return ctrl.selectedSalaryRange;
        }, onCategoryOrCityChange);

        function copySearchQueryToFilter()
        {

            var search = $location.search();
            angular.forEach(ctrl.filter, function (value, key)
            {
                var searchValue = search[key];
                if (null != searchValue && '' !== searchValue) {
                    ctrl.filter[key] = searchValue;
                }
            });

            if(!search.hasOwnProperty('publishMode')) {
                ctrl.filter.publishMode = null;
            }
        }

        function watchSearchQuery()
        {
            $scope.$watch(function ()
            {
                return $location.search();
            }, copySearchQueryToFilter, true);
        }

        function watchFilterAndUpdateSearchQuery()
        {
            $scope.$watch(function ()
            {
                return ctrl.filter;
            }, function ()
            {
                if (ctrl.lastLocation != $location.$$path){
                    ctrl.filter = angular.copy(defaultFilter);
                    copySearchQueryToFilter();
                }

                var filter = angular.copy(ctrl.filter);
                angular.forEach(defaultFilter, function (value, key)
                {
                    if (filter[key] === value) {
                        delete filter[key];
                    } else if ('' === filter[key]) {
                        delete filter[key];
                    }
                });
                $location.search(filter);
                ctrl.allSelected = false;
                ctrl.lastLocation = $location.$$path;
            }, true);
        }

        function updateSelectedCategoryWithFilter()
        {
            if (null != ctrl.filter.categories) {
                angular.forEach(ctrl.categories, function (category)
                {
                    if (ctrl.filter.categories === category.id) {
                        ctrl.selectedCategory = category;
                    }
                });
            }
        }

        function updateSelectedCityWithFilter()
        {
            if (null != ctrl.filter.city) {
                angular.forEach(ctrl.cities, function (city)
                {
                    if (ctrl.filter.city === city.id) {
                        ctrl.selectedCity = city;
                    }
                });
            }
        }

        function updateSelectedSalaryRangeWithFilter()
        {
            if (null != ctrl.filter.salaryRanges) {
                angular.forEach(ctrl.salaryRanges, function (salaryRange)
                {
                    if (ctrl.filter.salaryRange === salaryRange.id) {
                        ctrl.selectedSalaryRange = salaryRange;
                    }
                });
            }
        }
        
        this.downloadAsPDF = function() {
            ExporterDAO.query('vacancies', 'pdf', ctrl.filter).then(function (data)
            {
                ExporterDAO.download(data, 'vacancies.pdf');
            });
        };

        this.downloadAsXLS = function() {
            ExporterDAO.query('vacancies', 'xls', ctrl.filter).then(function (data)
            {
                ExporterDAO.download(data, 'vacancies.xlsx');
            });
        };

        (function init()
        {
            var promise;
            var promises = [];
            copySearchQueryToFilter();
            watchFilterAndUpdateSearchQuery();
            watchSearchQuery();

            promise = CategoriesDAO.query().then(function (data)
            {
                ctrl.categories = data;
                ctrl.categories.unshift({
                    id: 'all',
                    name: {
                        en: "All",
                        ru: "все"
                    }
                });
            });
            promises.push(promise);

            promise = CitiesDAO.query().then(function (data)
            {
                ctrl.cities = data;
                ctrl.cities.unshift({
                    id: 'all',
                    name: {
                        en: "All",
                        ru: "все"
                    }
                });
            });
            promises.push(promise);

            promise = SalaryRangesDAO.query().then(function (data)
            {
                ctrl.salaryRanges = data;
                ctrl.salaryRanges.unshift({
                    id: 'all',
                    name: {
                        en: "All",
                        ru: "все"
                    }
                });
            });
            promises.push(promise);

            $q.all(promises).then(function ()
            {
                updateSelectedCategoryWithFilter();
                updateSelectedCityWithFilter();
                updateSelectedSalaryRangeWithFilter();
                refresh = paginationSupport(ctrl, function (callback)
                {
                    VacanciesDAO.query(ctrl.filter).then(function (results)
                    {
                        ctrl.results = results.results;
                        ctrl.total = results.total;
                        ctrl.showTotal = true;
                        angular.forEach(ctrl.results, function (value)
                        {
                            value.createDateHumanized = moment.duration(moment(value.createDate).diff()).humanize(true);
                        });
                        callback(results.total);
                    });
                });
                refresh();
            });

        })();
    }

    angular.module('recrutoid.vacancy').controller('SearchVacanciesAsAdminCtrl',
            ['$location', '$modal', '$http', '$q', '$scope', 'CategoriesDAO', 'CitiesDAO', 'SalaryRangesDAO', 'EmailDAO', 'ExporterDAO', 'Notification', 'paginationSupport', 'recrutoidPopup', 'VacanciesDAO',
             'vacancyDetails', 'gettext', SearchVacanciesAsAdminCtrl]);
})();
