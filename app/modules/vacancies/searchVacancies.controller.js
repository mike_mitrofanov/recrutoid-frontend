(function ()
{
    'use strict';

    function SearchVacanciesController($location, $filter, $routeParams, $window, $scope, $timeout, $http, $rootScope,
            CommonDAO, CurrentUser, gettext, paginationSupport, recrutoidModals, recrutoidPopup,
            VacanciesDAO, UserDAO, SalaryRangesDAO, searchColumnMover, SearchSuggestions, SearchHelper)
    {
        var ctrl = this;
        var refresh;

        var filtersHandledBySelect2 = [
            'category',
            'city'
        ];
        var availableFilters = [
            'vacancyTitle',
            'salaryRange',
            'days',
            'companyName',
            'location'
        ].concat(filtersHandledBySelect2);

        var clearableFilters = [
            'days',
            'salaryRange',
            'companyName',
            'location'

        ];

        var previousVacancyTitle = '';

        ctrl.filter = {
            size: 20,
            from: 0
        };

        ctrl.select2Models = {};

        ctrl.activeRow = 0;
        ctrl.mobileFilter = ctrl.filter;

        SearchSuggestions.setContext(ctrl, '/api/autocomplete/vacancies?q=', 'vacancyTitle');
        $scope.suggestionsHandler = SearchSuggestions;



        this.resetRequired = function () {
            return ctrl.filter.vacancyTitle !== previousVacancyTitle;
        };


        this.onOpenClose = function (isOpen) {
            //console.log('is-open=');
            //console.log(isOpen);
        };

        this.details = function (vacancy)
        {

            vacancy.isNotVisited = false;
            vacancy.isNew = false;
            if ($rootScope.media !== 'desktop') {
                $location.path('/vacancy/' + vacancy.id).search({});
            } else {
                window.open('/vacancy/' + vacancy.id);
            }

        };

        this.apply = function (vacancy)
        {
            function doApply(user)
            {
                CommonDAO.isProfileActive().then(function (data)
                {
                    var warning, text;

                    if (!data.isActive) {

                        if (data.isInModeration) {
                            recrutoidPopup({
                                title: gettext('Warning'),
                                text: gettext('Your_cv_is_in_moderation_you_cant_use_page'),
                                html: true,
                                type: 'warning',
                                showCancelButton: false,
                                confirmButtonText: gettext('OK'),
                                closeOnConfirm: true
                            }, function (isConfirm)
                            {
                                if (isConfirm) {
                                    $location.url('/user/settings/cv-page?tab=my_cv');
                                }
                            });
                        } else {
                            recrutoidPopup({
                                title: gettext('Warning'),
                                text: gettext('There_is_no_active_CV_on_the_website'),
                                html: true,
                                type: 'warning',
                                showCancelButton: true,
                                confirmButtonText: gettext('CV_Creation'),
                                closeOnConfirm: true
                            }, function (isConfirm)
                            {
                                if (isConfirm) {
                                    $location.url('/user/settings/edit-cv');
                                }
                            });
                        }


                    } else if ('normal' !== user.role) {
                        warning = gettext('Warning');
                        text = gettext('Message_To_Apply_For_This_Vacancy_Your_Account_Has_To_Be_Job_Seeker_Type');
                        recrutoidPopup(warning, text, 'warning');
                    } else if (-1 !== vacancy.applicants.indexOf(user.id)) {
                        warning = gettext('Warning');
                        text = gettext('Message_You_Applied_To_This_Vacancy_Before');
                        recrutoidPopup(warning, text, 'warning');
                    } else {
                        VacanciesDAO.applyTo(vacancy.id).then(function ()
                        {
                            vacancy.applied = true;
                        });
                    }

                });
            }

            CurrentUser.resolveOrReject().then(doApply).catch(function ()
            {
                return recrutoidModals.entireFlow({
                    register: {
                        initialUserType: {
                            type: 'normal'
                        },
                        hideUserType: true
                    }
                }).then(function ()
                {
                    return CurrentUser.resolve().then(doApply).then(function () {
                        ctrl.doNotReset = true;
                        refresh();
                    });
                });
            });
        };

        function dealWithFilters()
        {



            if (ctrl.filter.vacancyTitle !== $location.search().vacancyTitle) {
                $location.search('vacancyTitle', ctrl.filter.vacancyTitle || null);
            }
            if (ctrl.filter.category !== $location.search().category) {
                $location.search('category', ctrl.filter.category || null);
                if (!$location.search().category) {
                    ctrl.select2Models.category = null;
                }
            }

            if (ctrl.filter.location !== $location.search().location) {
                $location.search('location', ctrl.filter.location || null);
            }

            if (ctrl.filter.city !== $location.search().city) {
                $location.search('city', ctrl.filter.city || null);
                if (ctrl.filter.city) {
                    ctrl.select2Models.city = ctrl.cities.find(function (elem)
                    {
                        return elem.id === ctrl.filter.city;
                    });
                } else {
                    delete ctrl.select2Models.city;
                }
            }
            if (ctrl.filter.companyName !== $location.search().companyName) {
                $location.search('companyName', ctrl.filter.companyName || null);
            }
            if (ctrl.filter.days !== $location.search().days) {
                $location.search('days', ctrl.filter.days || null);
            }
            if (ctrl.filter.salaryRange !== $location.search().salaryRange) {
                $location.search('salaryRange', ctrl.filter.salaryRange || null);
            }


            if (ctrl.currentPage !== $location.search().currentPage) {

                if (ctrl.currentPage != 1) {
                    $location.search('currentPage', ctrl.currentPage);
                } else {
                    $location.search('currentPage', null);
                }
            }

            SearchSuggestions.updateShortened();


        }

        this.dealWithFilters = dealWithFilters;

        this.resetFilters = function (dontRefresh)
        {

            for (var i = 0; i < clearableFilters.length; i++) {
                var item = clearableFilters[i];
                delete ctrl.filter[item];
            }

            if (!dontRefresh) {
                dealWithFilters();
                refresh();
            }
        };

        this.applyMobileFilters = function ()
        {
            ctrl.filter = ctrl.mobileFilter;

            $scope.showMobileFilters = false;

            dealWithFilters();
            refresh();
        };


        this.hasSelectedFilters = function ()
        {
            for (var i = 0, len = clearableFilters.length; i < len; i++) {
                var item = clearableFilters[i];
                if (ctrl.filter[item] !== null && ctrl.filter[item] !== undefined) {
                    return true;
                }
            }

            return false;
        };

        this.isFilterSelected = function (item)
        {
            if (ctrl.filter[item] !== null && ctrl.filter[item] !== undefined) {
                return true;
            }

            return false;
        };

        this.isFilterSet = function (item, value)
        {
            if (ctrl.filter[item] !== null && ctrl.filter[item] !== undefined) {
                if (value && value === ctrl.filter[item]) {
                    return true;
                }
            }

            return false;
        };


        this.setFilter = function (type, value)
        {
            ctrl.filter[type] = value;

            if (!$scope.showMobileFilters) {
                dealWithFilters();
                refresh();
            }
        };

        this.search = function ()
        {
            ctrl.filter.category = ctrl.select2Models.category && ctrl.select2Models.category.id || null;
            ctrl.filter.city = ctrl.select2Models.city && ctrl.select2Models.city.id || null;

            if (this.resetRequired()) {
                this.resetFilters(true);
            }


            if (!$scope.showMobileFilters) {
                //delete ctrl.filter.companyName;
                dealWithFilters();
                refresh();
                previousVacancyTitle = ctrl.filter.vacancyTitle;
            }
        };

        this.setActiveRow = function (index, vacancy) {
            ctrl.activeRow = index;
            ctrl.activeVacancy = vacancy;
        };

        function goHomeEmployer()
        {
            CurrentUser.resolveOrReject().then(function (user)
            {
                if ('employer' === user.role) {
                    $location.path('/');
                }
            });
        }

        function captureCityFromParams()
        {
            if ($routeParams.cityName) {
                var city = $routeParams.cityName.toString().toLowerCase();
                for (var i = 0; i < ctrl.cities.length; i++) {
                    if (city === ctrl.cities[i].name.en.toLowerCase() || city === ctrl.cities[i].name.ru.toLowerCase()) {
                        ctrl.filter.city = ctrl.cities[i].id;
                        break;
                    }
                }
            }
        }

        function dealWithQueryString()
        {
            angular.forEach($location.search(), function (value, name)
            {
                var setOfSelect2DataToSearch;
                if (-1 < availableFilters.indexOf(name)) {
                    ctrl.filter[name] = value;

                    if (-1 < filtersHandledBySelect2.indexOf(name)) {
                        setOfSelect2DataToSearch = 'category' === name ? ctrl.categories : ctrl.cities;
                        for (var i = 0; i < setOfSelect2DataToSearch.length; i++) {
                            if (value === setOfSelect2DataToSearch[i].id) {
                                ctrl.select2Models[name] = setOfSelect2DataToSearch[i];
                            }
                        }
                    }
                }
            });


        }

        function getSalaryRanges() {
            return SalaryRangesDAO.query().then(function (data) {
                ctrl.salaryRanges = data;
            });
        }

        var notInited = true;

        refresh = paginationSupport(ctrl, function (callback)
        {
            if (ctrl.doNotReset !== true) {
                if (ctrl.lastPage == ctrl.currentPage) {
                    ctrl.filter.size = 20;
                    ctrl.filter.from = 0;
                    if (!ctrl.currentPage || !notInited) {
                        ctrl.currentPage = 1;
                    }

                }
                
                
                $window.scrollTo(0, 0);
            }



            return VacanciesDAO.find(ctrl.filter).then(function (results)
            {
                ctrl.companyFilterData = results.companyFilterData;
                // ctrl.locationFilterData = results.locationFilterData;

                if (!ctrl.filter.city) {
                    ctrl.locationFilterData = results.locationFilterData;
                } else {
                    ctrl.locationFilterData = null;
                }


                if (ctrl.locationFilterData) {
                    for (var i in ctrl.locationFilterData.data) {
                        if (ctrl.locationFilterData.data[i].cityId == ctrl.filter.city && i > 4) {
                            ctrl.locationFilterData.showMore = true;
                            break;
                        }
                    }
                }

                if (ctrl.companyFilterData) {
                    for (var n in ctrl.companyFilterData.data) {
                        if (ctrl.companyFilterData.data[n].name == ctrl.filter.companyName && n > 4) {
                            ctrl.companyFilterData.showMore = true;
                            break;
                        }
                    }
                }


                if (!ctrl.companyFilterData) {
                    delete ctrl.filter.companyName;
                }
                if (!ctrl.locationFilterData) {
                    delete ctrl.filter.location;
                }

                var foundOneL = true;
                var foundOneC = true;
                var needRefresh = false;

                if (ctrl.companyFilterData && ctrl.filter.companyName && foundOneL) {

                    foundOneC = false;
                    for (var i in ctrl.companyFilterData.data) {
                        var item = ctrl.companyFilterData.data[i];
                        if (ctrl.filter.companyName == item.name) {
                            foundOneC = true;
                        }
                    }

                    if (!foundOneC) {
                        delete ctrl.filter.companyName;
                        needRefresh = true;
                    }

                }



                if (needRefresh) {
                    refresh();
                    return;
                }


                dealWithFilters();
                ctrl.mobileFilter = ctrl.filter;

                ctrl.results = results.results;

                if (ctrl.doNotReset !== true) {
                    ctrl.setActiveRow(0);
                    ctrl.activeVacancy = ctrl.results[0];

                    ctrl.lastPage = ctrl.currentPage;

                    setTimeout(function () {
                        if($location.path() == '/vacancies')
                            searchColumnMover.init();
                    }, 500);
                }

                ctrl.doNotReset = false;
                callback(results.total);
                if (notInited) {
                    SearchSuggestions.refocus();
                    notInited = false;
                }
            }).then(function ()
            {
                markNewAndVisited();
            });
        });

        function markNewAndVisited() {
            CurrentUser.resolve().then(function (currentUser) {
                var idsToCheckIfNew = [];
                var vacanciesCopyForIsNewMarking = {};

                angular.forEach(ctrl.results, function (value)
                {
                    idsToCheckIfNew.push(value.id);
                    vacanciesCopyForIsNewMarking[value.id] = value;
                });

                if (!currentUser.id) {
                    return;
                }

                UserDAO.searchVisited(currentUser);
                var applicants;
                angular.forEach(ctrl.results, function (vacancy)
                {
                    applicants = vacancy.applicants.map(function (applicant)
                    {
                        return applicant.user;
                    });
                    vacancy.applied = -1 !== applicants.indexOf(currentUser.id);
                });

                if (idsToCheckIfNew.length) {
                    VacanciesDAO.isNew(idsToCheckIfNew).then(function (result)
                    {
                        angular.forEach(result, function (isNewValue, vacancyId)
                        {
                            if ('$' !== vacancyId[0] && vacanciesCopyForIsNewMarking[vacancyId]) {
                                vacanciesCopyForIsNewMarking[vacancyId].isNew = isNewValue;
                            }
                        });
                    });

                    VacanciesDAO.isNotVisitedList(idsToCheckIfNew).then(function (result)
                    {

                        angular.forEach(result, function (isNewValue, vacancyId)
                        {
                            if ('$' !== vacancyId[0] && vacanciesCopyForIsNewMarking[vacancyId]) {
                                vacanciesCopyForIsNewMarking[vacancyId].isNotVisited = isNewValue;
                            }
                        });

                    });

                }
            });
        }

        CurrentUser.onreload(function ()
        {
            refresh();
        });

        SearchHelper.init(ctrl, refresh, dealWithQueryString, availableFilters, filtersHandledBySelect2);
        $scope.searchHelper = SearchHelper;


        (function init()
        {
            goHomeEmployer();
            
            delete ctrl.resumeTitle;
            
            CommonDAO.getSearchVacanciesData().then(function (data)
            {
                ctrl.categories = data.categories;
                ctrl.cities = data.cities;



            })
                    .then(captureCityFromParams)
                    .then(dealWithQueryString)
                    .then(getSalaryRanges)
                    .then(function () {
                        if ($location.search().currentPage) {
                            ctrl.currentPage = $location.search().currentPage;
                        }
                    })
                    .then(refresh);
        })();

        var destroyEmployerHunter = $scope.$root.$on('event:auth-loggedIn', goHomeEmployer);

        $scope.$on('$destroy', function ()
        {
            destroyEmployerHunter();
            searchColumnMover.destroy();
        });
    }

    angular.module('recrutoid.vacancy')
            .controller('SearchVacanciesController',
                    ['$location',
                        '$filter',
                        '$routeParams',
                        '$window',
                        '$scope',
                        '$timeout',
                        '$http',
                        '$rootScope',
                        'CommonDAO',
                        'CurrentUser',
                        'gettext',
                        'paginationSupport',
                        'recrutoidModals',
                        'recrutoidPopup',
                        'VacanciesDAO',
                        'UserDAO',
                        'SalaryRangesDAO',
                        'searchColumnMover',
                        'SearchSuggestions',
                        'SearchHelper',
                        SearchVacanciesController]);
})();
