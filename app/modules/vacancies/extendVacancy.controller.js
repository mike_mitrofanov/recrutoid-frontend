(function ()
{
    'use strict';

    function ExtendVacancyController($location, $routeParams, recrutoidPopup, VacanciesDAO, gettext)
    {
        var text = gettext('Message_Your_Vacancy_Publication_Has_Been_Successfully_Extended');
        var successDisplay = gettext('Success');
        VacanciesDAO.extendPublication($routeParams.token).then(function ()
        {
            recrutoidPopup(successDisplay, text, 'success');
            $location.path('/');
        });
    }

    angular.module('recrutoid.user').controller('ExtendVacancyController',
            ['$location', '$routeParams', 'recrutoidPopup', 'VacanciesDAO', 'gettext', ExtendVacancyController]);
})();
