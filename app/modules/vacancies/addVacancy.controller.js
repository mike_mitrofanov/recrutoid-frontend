(function ()
{

    'use strict';

    function AddVacancyCtrl($rootScope, $q, $scope, addVacancyDetails, Authenticator, BlacklistedDomainsDAO, CategoriesDAO,
                            CitiesDAO, CurrentUser, recrutoidModals, siteSettings, recrutoidPopup, UserDAO, gettext,
							emailValidator)
    {
        var ctrl = this;
        ctrl.show = false;
        ctrl.showEmail = true;
        this.selectedCategories = [];

        ctrl.taOptions = [
            ['h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'p', 'pre', 'quote'],
            ['bold', 'italics', 'underline', 'strikeThrough', 'ul', 'ol', 'redo', 'undo', 'clear'],
            ['justifyLeft', 'justifyCenter', 'justifyRight', 'indent', 'outdent']
        ];

        function init()
        {
            ctrl.package = {
                vacancy: {
                    publishOnWebsite: false,
                    publishViaEmail: false,
                    showEmailOnWebsite: true
                }
            };

            CurrentUser.resolve().then(function (user)
            {
                if(user.email) {
                    ctrl.showEmail = false;
                }
            });
        }

        this.addVacancy = function ()
        {
            var text;
            var warning = gettext('Warning!');

            if ($rootScope.media == 'mobile'){
                text = gettext('Message_Please_Use_Desktop_Or_Tablet_To_Use_All_The_Features');
                recrutoidPopup({
                    type: 'warning',
                    title: warning,
                    text: text
                });
                return;
            }

            if(!ctrl.isFormValid()) {
                return handleErrors();
            }

            function reject(data)
            {
                var defered = $q.defer();
                defered.reject(data);
                return defered.promise;
            }

            function loginOrRegister()
            {
                return UserDAO.checkEmailAvailability(ctrl.package.vacancy.email).then(function ()
                {
                    return recrutoidModals.register.open({
                        initialEmail: ctrl.package.vacancy.email,
                        initialUserType: 'employer',
                        hideUserType: true,
                        returnData: true
                    });
                }).catch(function (error)
                {
                    if (460 === error.status) {
                        text = gettext('Message_Please_Use_Your_Official_Companys_Email');
                        if (error.status) {
                            recrutoidPopup({
                                type: 'warning',
                                title: warning,
                                text: text
                            });
                        }
                    }

                    if (412 === error.status) {
                        text = gettext('Message_This_Email_Is_Already_Registered_As_Not_Employer');
                        if (error.status) {
                            recrutoidPopup({
                                type: 'warning',
                                title: warning,
                                text: text
                            });
                        }
                    }

                    if (409 === error.status) {
                        return recrutoidModals.login.open({
                            initialEmail: ctrl.package.vacancy.email,
                            disableSocialOptions: true,
                            disableCreateAccountOptions: true
                        }).catch(function (errorMessage)
                        {
                            if('resetPassword' === errorMessage) {
                                return recrutoidModals.resetPassword.open({
                                    initialEmail: ctrl.package.vacancy.email
                                }).then(function ()
                                {
                                    return reject({});
                                }).catch(function ()
                                {
                                    return reject();
                                });
                            }
                            else {
                                return reject();
                            }
                        });
                    }

                    return reject();
                });
            }

            CurrentUser.resolve().then(function (user)
            {
				var warning = gettext('Warning');

				if ('normal' === user.role) {
                    var text = gettext('Message_To_Post_New_Vacancy_Your_Account_Has_To_Be_Employer_Type');
                    recrutoidPopup(warning, text, 'warning');
					return;
                }
                else if (!ctrl.selectedCity || 0 >= ctrl.selectedCategories.length) {
                    text = gettext('Message_Please_Select_City_And_Category');
                    recrutoidPopup(warning, text, 'warning');
                    return;
                }
                else if (2 < ctrl.selectedCategories.length) {
                    text = gettext('Message_You_Cant_Select_More_Than_Categories');
                    recrutoidPopup(warning, text, 'warning');
                    return;
                }
                else if (!Authenticator.isAuthenticated() && !ctrl.package.vacancy.email) {
                    text = gettext('Message_Please_Enter_Valid_Email');
                    recrutoidPopup(warning, text, 'warning');
                    return;
                }

                ctrl.package.vacancy.cityId = ctrl.selectedCity.id;
                ctrl.package.vacancy.salaryRangeId = ctrl.selectedSalaryRange.id;
                ctrl.package.vacancy.categories = ctrl.selectedCategories.map(function (elem)
                {
                    return elem.id;
                });


                var promise;
                if (Authenticator.isAuthenticated()) {
                    promise = CurrentUser.resolveOrReject().then(siteSettings.requireVacancyPermission).then(function (data)
                    {

                        ctrl.package.vacancy.email = data.email;
                        ctrl.package.vacancy.role = data.role;
                        ctrl.package.vacancy.isCompany = false;
                        if('employer' === data.role) {
                            ctrl.package.vacancy.companyName = data.firstName;
                            ctrl.package.vacancy.role = data.role;
                            ctrl.package.vacancy.isCompany = true;
                        }
                        return BlacklistedDomainsDAO.checkEmailInBlackListed({email: ctrl.package.vacancy.email});
                    });
                } else {
                    promise = BlacklistedDomainsDAO.checkEmailInBlackListed({email: ctrl.package.vacancy.email}).then(function ()
                    {
                        return loginOrRegister().then(function (userData)
                        {
                            // do not display payment popup if loginOrRegister registered new user
                            if(userData.hasOwnProperty('id')) {
                                return siteSettings.requireVacancyPermission(userData);
                            }

                            return userData;
                        }).then(function (userData)
                        {
                            if(userData && Authenticator.isAuthenticated()) {
                                ctrl.package.vacancy.companyName = userData.firstName;
                                ctrl.package.vacancy.isCompany = true;
                            }
                            else if(userData) {
                                ctrl.package.vacancy.email = userData.email;
                                ctrl.package.vacancy.role = userData.role;
                                ctrl.package.vacancy.password = userData.password;
                            }
                        });
                    });
                }

                promise.then(function ()
                {
                    //console.log(ctrl.package);
                    return addVacancyDetails.open(ctrl.package);
                }).then(function ()
                {
                    clearForm();
					
                }).catch( function(error) {
                    if(error && 460 === error.status) {
                        text = gettext('Message_Please_Use_Your_Official_Companys_Email');
                        recrutoidPopup(warning, text, 'warning');
                    } else if(error) {
                        text = gettext('Message_Something_Went_Wrong');
                        recrutoidPopup(warning, text, 'warning');
                    }
                });
            });
        };

        function handleErrors() {
            ctrl.errors = {};

            if (0 >= ctrl.selectedCategories.length) {
                ctrl.errors.category = true;
            }

            if (!ctrl.selectedCity) {
                ctrl.errors.city = true;
            }

            if (!ctrl.selectedSalaryRange) {
                ctrl.errors.salaryRange = true;
            }

            if (!Authenticator.isAuthenticated() && !emailValidator.isValid(ctrl.package.vacancy.email)) {
                ctrl.errors.email = true;
            }
        }

        this.isFormValid = function() {
            return ctrl.isEmailValid() && ctrl.selectedSalaryRange && ctrl.selectedCity && ctrl.selectedCategories.length;
        };

		this.isEmailValid = function() {
			return emailValidator.isValid(ctrl.package.vacancy.email) || Authenticator.isAuthenticated();
		};


		function clearForm()
		{
			ctrl.selectedCity = null;
			ctrl.selectedCategories = [];
			ctrl.selectedSalaryRange = undefined;
			if ( ! Authenticator.isAuthenticated()) {
				ctrl.package.vacancy.email = '';
			}
		}


        $scope.$watch(function() {
            return ctrl.selectedCategories;
        }, function() {
            ctrl.errors = {};
        }, true);

        init();

        $scope.$on('event:auth-loggedOut', function ()
        {
			clearForm();
            ctrl.showEmail = true;
        });

        $scope.$on('event:auth-loggedIn', function ()
        {
            ctrl.showEmail = false;
        });
    }

    angular.module('recrutoid.vacancy').controller('AddVacancyCtrl',
            ['$rootScope',
             '$q',
             '$scope',
             'addVacancyDetails',
             'Authenticator',
             'BlacklistedDomainsDAO',
             'CategoriesDAO',
             'CitiesDAO',
             'CurrentUser',
             'recrutoidModals',
             'siteSettings',
             'recrutoidPopup',
             'UserDAO',
             'gettext',
             'emailValidator',
             AddVacancyCtrl]);
})();
