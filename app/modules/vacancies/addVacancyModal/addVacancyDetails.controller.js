(function ()
{
    'use strict';

    function AddVacancyDetailsController($rootScope, $filter, $location, $modalInstance, $timeout, $q, Authenticator, salaryRangesDAO, CurrentUser, gettext, lookForChanges,
                                         siteSettings, recrutoidPopup, unsavedChangesPopup, vacancyData, VacanciesDAO)
    {
        var ctrl = this;
        ctrl.errors = {};

        var currencies = ['KZT', 'USD', 'EUR'];

        this.vacancy = {
            categories: vacancyData.vacancy.categories,
            cityId: vacancyData.vacancy.cityId,
            salaryRangeId: vacancyData.vacancy.salaryRangeId,
            email: vacancyData.vacancy.email,
            password: vacancyData.vacancy.password,   // password may not be set
            companyName: vacancyData.vacancy.companyName,
            isCompany: vacancyData.vacancy.isCompany,
            publishOnWebsite: true,
            publishViaEmail: true,
            showEmailOnWebsite: true,
            salary: {
                currency: currencies[0]
            }
        };
        this.role = vacancyData.vacancy.role;
        var initialVacancyData = angular.copy(this.vacancy);

        var warning = gettext('Warning!');

        this.close = function ()
        {
            if(lookForChanges(ctrl.vacancy, initialVacancyData)) {
                unsavedChangesPopup.open(function (saving)
                {
                    if (saving) {
                        var remotelyInvokedSave = $q.defer();
                        ctrl.publish(remotelyInvokedSave);
                        remotelyInvokedSave.promise.then(function (showSuccessPopup)
                        {
                            if(showSuccessPopup) {
                                unsavedChangesPopup.success();
                            }
                        });
                    }
                    else {
                        $modalInstance.dismiss();
                    }
                });
            }
            else {
                $modalInstance.dismiss();
            }
        };

        this.formatSalary = function ()
        {
            ctrl.vacancy.salary.value = $filter('formatSalary')(ctrl.vacancy.salary.value);
        };

        this.changeCurrency = function()
        {
            ctrl.vacancy.salary.currency = currencies[(currencies.indexOf(ctrl.vacancy.salary.currency)+1) % 3];
        };

        this.publish = function (remotelyInvokedSave)
        {
            var text;

            function doPublish()
            {
                if(ctrl.vacancy.salary && ctrl.vacancy.salary.value) {
                    ctrl.vacancy.salary.value = $filter('cleanSalary')(ctrl.vacancy.salary.value);
                }
                if (ctrl.selectedSalaryRange) {
                    ctrl.vacancy.salaryRangeId = ctrl.selectedSalaryRange.id;
                }

                VacanciesDAO.update(ctrl.vacancy).then(function ()
                {
                    var title = gettext('Congratulations');
                    var confirm = gettext('OK');
                    var swalOptions = {
                        title: title,
                        html: true,
                        type: 'success',
                        showCancelButton: false,
                        confirmButtonText: confirm,
                        closeOnConfirm: true
                    };
                    if(remotelyInvokedSave) {
                        remotelyInvokedSave.resolve();
                    }

                    CurrentUser.resolveOrReject().then(function ()
                    {
                        swalOptions.showCancelButton = true;
                        swalOptions.cancelButtonText = 'Vacancies';
                        if(ctrl.vacancy.publishOnWebsite){
                            swalOptions.text = gettext('Message_You_Have_Successfully_Distributed_And_Published_New_Vacancy');
                        }
                        else {
                            swalOptions.text = gettext('Message_Your_Vacancy_Will_Be_Sent_To_All_Relevant_Subscribers_Right_After_Our_Approval');
                        }

                        recrutoidPopup(swalOptions, function (isConfirm)
                        {
                            if (isConfirm) {
                                $modalInstance.close();
                            }
                            else {
                                $modalInstance.close();
                                $location.path('/user/settings/myVacancies');
                            }
                        });
                    }).catch(function ()
                    {
                        var siteSettingsObject = siteSettings.get();
                        swalOptions.text = 'You have successfully added new vacancy. To finalize the process, please confirm your ' +
                            '<b>email address</b>.';
                        if(siteSettingsObject.paymentModeEnabled) {
                            swalOptions.text += ' To make your vacancy activated you have to pay for access to ' +
                                    'adding vacancy also. Close this pop up to read more.';
                        }

                        recrutoidPopup(swalOptions);
                        if(siteSettingsObject.paymentModeEnabled) {
                            $location.path('/page/payment');
                        }

                        $modalInstance.close();
                    });
                }).catch(function (e)
                {
                    if(remotelyInvokedSave) {
                        remotelyInvokedSave.reject();
                    }
                    if (e && !e.hasOwnProperty('status')) {
                        return;
                    }

                    var text;
                    if (409 === e.status) {
                        text = gettext('Message_Validation_Failed.');
                        recrutoidPopup(warning, text, 'warning');
                    } else {
                        text = gettext('Message_Technical_Error_Occured_Please_Contact_Support');
                        recrutoidPopup(text, 'error');
                    }
                });
            }


            if(validateForm()) {
                doPublish();
                return;
            }

            if(remotelyInvokedSave) {
                remotelyInvokedSave.reject();
            }
        };

        function validateForm() {
            var isValid = true;
            var text = false;
            var requiredMessage = "Please_fill_out_this_field";

            if (!ctrl.vacancy.companyName) {
                if(ctrl.vacancy.isCompany) {
                    text = gettext('Message_Please_Complete_Your_Company_Name_In_Your_Profile_Settings');
                }else{
                    ctrl.errors.company = requiredMessage;
                }
                isValid = false;
            }

            if (!ctrl.vacancy.title) {
                ctrl.errors.title = requiredMessage;
                isValid = false;
            }

            if (!ctrl.vacancy.publishOnWebsite && !ctrl.vacancy.publishViaEmail) {
                text = gettext('Message_You_Must_Publish_The_Vacancy_At_Least_In_One_Of_Website_Or_Via_Emails.');
                isValid = false;
            }

            if (!ctrl.vacancy.details) {
                ctrl.errors.description = requiredMessage;
                isValid = false;
            }

            if(!isValid && !text){
                text = gettext('Message_Some_fields_require_your_attention');
            }

            if(text){
                recrutoidPopup(warning, text, 'warning');
            }

            return isValid;
        }

        function init() {
            salaryRangesDAO.query().then(function (data)
            {
                ctrl.salaryRanges = data;
            });
        }
        init();


        ctrl.isLoggedOut = !Authenticator.isAuthenticated();
    }

    angular.module('recrutoid.vacancy').controller('AddVacancyDetailsController',
            ['$rootScope', '$filter', '$location', '$modalInstance', '$timeout', '$q', 'Authenticator', 'SalaryRangesDAO', 'CurrentUser', 'gettext', 'lookForChanges', 'siteSettings',
                'recrutoidPopup', 'unsavedChangesPopup', 'vacancyData', 'VacanciesDAO', AddVacancyDetailsController]);
})();
