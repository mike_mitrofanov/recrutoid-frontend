(function ()
{
    'use strict';

    function addVacancyDetails($modal)
    {
        function open(vacancyData)
        {
            return $modal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: 'modules/vacancies/addVacancyModal/addVacancyDetails.tpl.html',
                controller: 'AddVacancyDetailsController as ctrl',
                windowClass: 'modal-large',
                /*size: 'lg', has no effect */
                resolve: {
                    vacancyData: function() {
                        return vacancyData;
                    }
                }
            }).result;
        }

        return {
            open: open
        };
    }

    angular.module('recrutoid.vacancy').service('addVacancyDetails', ['$modal', addVacancyDetails]);

})();
