(function ()
{
    'use strict';

    function VacancyDetailsController($modalInstance, vacancy)
    {
        var ctrl = this;
        this.vacancy = vacancy;
        this.vacancy.approveDateHumanized = moment.duration(moment(ctrl.vacancy.publishedDate).diff()).humanize(true);
        this.close = function ()
        {
            $modalInstance.close();
        };

        this.edit = function ()
        {
            $modalInstance.close('edit');
        };
    }

    angular.module('recrutoid.vacancy').controller('VacancyDetailsController',
            ['$modalInstance', 'vacancy', VacancyDetailsController]);
})();
