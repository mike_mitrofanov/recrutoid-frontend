(function ()
{
    'use strict';

    function vacancyDetails($modal)
    {
        function open(vacancy)
        {
            return $modal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: 'modules/vacancies/modal/vacancyDetails.tpl.html',
                controller: 'VacancyDetailsController as vacancyCtrl',
                size: 'lg',
                resolve: {
                    vacancy: function() {
                        return vacancy;
                    }
                }
            }).result;
        }

        return {
            open: open
        };
    }

    angular.module('recrutoid.vacancy').service('vacancyDetails', ['$modal', vacancyDetails]);

})();
