(function ()
{
    'use strict';

    function recrutoidConfig($authProvider, cfpLoadingBarProvider, localStorageServiceProvider, NotificationProvider, paginationSupportProvider, $compileProvider)
    {
        $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|mailto|file|tel):/);
        paginationSupportProvider.setDefaultConfig({maxResultsProperty: 'size', firstResultProperty: 'from', size: 20});
        NotificationProvider.setOptions({
            delay: 2000,
            startTop: 20,
            startRight: 10,
            verticalSpacing: 20,
            horizontalSpacing: 20,
            positionX: 'right',
            positionY: 'bottom',
            templateUrl: 'modules/common/notification/notification.html'
        });
        cfpLoadingBarProvider.includeSpinner = false;
        localStorageServiceProvider.setPrefix('recrutoid');

        $authProvider.httpInterceptor = false;   // disable setting authorization header
        $authProvider.loginRedirect = null;
        $authProvider.facebook({
            clientId: '1497076817284826',
            url: '/api/users/auth/facebook',
            authorizationEndpoint: 'https://www.facebook.com/v2.4/dialog/oauth',
            requiredUrlParams: ['display', 'scope'],
            scope: ['email'],
            scopeDelimiter: ',',
            display: 'popup',
            type: '2.0',
            popupOptions: { width: 580, height: 400 }
        });
        $authProvider.linkedin({
            clientId: '77bs62j1ut9zxa',
            url: '/api/users/auth/linkedin',
            authorizationEndpoint: 'https://www.linkedin.com/uas/oauth2/authorization',
            requiredUrlParams: ['state'],
            scope: ['r_emailaddress'],
            scopeDelimiter: ' ',
            state: 'STATE',
            type: '2.0',
            popupOptions: { width: 527, height: 582 }
        });
        tinyMCE.init({
            plugins: 'autoresize paste lists placeholder',
            toolbar: 'bold italic underline strikethrough bullist numlist undo redo | alignleft aligncenter alignright indent outdent',
            statusbar: false,
            menubar: false,
            paste_preprocess: function(plugin, args) {
                args.content = $filter('cleanHtml')(args.content);
            },
            content_style: "#tinymce {padding-bottom: 0 !important; min-height: 184px;}"
        });
    }

    function recrutoidRun(AsyncQueue, $location, $route, $rootScope, cfpLoadingBar, Authenticator)
    {
        AsyncQueue.configure({timeout: 500});

        $rootScope.bigLoader = true;
        $rootScope.$on('cfpLoadingBar:completed', function(event, message) {
            var scrollArea = $('.with-perfect-scrollbar').find('.ui-select-choices');
            $rootScope.bigLoader = false;
            scrollArea.perfectScrollbar({
                wheelSpeed: 2,
                wheelPropagation: true,
                minScrollbarLength: 20,
                suppressScrollX: true
            });

            $('.with-perfect-scrollbar').click(function(){
                scrollArea.scrollTop(0);
                scrollArea.perfectScrollbar('update');

                setTimeout(function(){
                    scrollArea.perfectScrollbar('update');
                },50);
            })
        });

        var original = $location.path;
        $location.path = function (path, reload) {
            if (reload === false) {
                var lastRoute = $route.current;
                var un = $rootScope.$on('$locationChangeSuccess', function () {
                    $route.current = lastRoute;
                    un();
                });
            }
            return original.apply($location, [path]);
        };

        $rootScope.$watch(function() {
                return $location.path();
            },
            function(a){
                if (!Authenticator.isImpersonated())
                    Authenticator.checkIfUserChanged();
            });
    }

    angular.module('recrutoid').config(['$authProvider', 'cfpLoadingBarProvider', 'localStorageServiceProvider', 'NotificationProvider',
                                        'paginationSupportProvider', '$compileProvider', recrutoidConfig]);
    angular.module('recrutoid').run(['AsyncQueue', '$location', '$route', '$rootScope', 'cfpLoadingBar', 'Authenticator', recrutoidRun]);
})();
