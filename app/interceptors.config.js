(function ()
{
    'use strict';

    function loggingInterceptorFactory($log)
    {
        return {
            request: function (config)
            {
                if (!config.url.match(/.*\.html/)) {
                    var message = config.method + ' ' + config.url + ' ' + JSON.stringify(config.headers);
                    if (null !== config.data) {
                        message += '\n' + JSON.stringify(config.data);
                    }
                    $log.debug(message);
                }
                return config;
            }
        };
    }

    function interceptor($injector, $rootScope, $location, $q, $log, preLoginRoute)
    {
        var forbidden = '403';

        function error(response)
        {
            /*jshint maxcomplexity:false*/
            //user $injector because exist circular dependency
            //$http <- Notification <- $http <- gettextCatalog
            var AuthenticatorService = $injector.get('Authenticator');
            var Notification = $injector.get('Notification');
            if('/' !== $location.path()) {
                preLoginRoute.route = $location.path();
            }
            var status = response.status;
            $rootScope.errorDate = new Date();
            delete $rootScope.validationError;
            switch (status) {
                case 0:
                    return $q.reject(response);
                case 400:
                    $log.error('Request cannot be fulfilled due to bad syntax!');
                    break;
                case 401:
                    $log.error(response.statusText);
                    AuthenticatorService.logout();
                    Notification.error(response.statusText);
                    $rootScope.$emit('event:auth-loginRequired');
                    $location.path('/').search('login');
                    return $q.reject(response);
                case 403:
                    $log.error('You don\'t have access to this page or resource!');
                    $location.path(forbidden);
                    break;
                case 404:
                    $location.path('/404');
                    break;
                case 405:
                    $log.error('Request method not supported!');
                    break;
                case 408:
                    $log.error('The server timed out waiting for the request!');
                    break;
                case 409:
                    $log.error('Validation error');
                    break;
                case 415:
                    $log.error('This media type is not supported!');
                    break;
                case 500:
                    $location.path('500');
                    break;
                case 503:
                    $log.error('The server is currently unavailable (overloaded or down)!');
                    break;
                case 505:
                    $log.error('The server does not support the HTTP protocol version used in the request!');
                    break;
                default:
                    $log.error(response.message ? response.message : 'Page Not Found. Our apologies but we could not find a page matching your request.');

            }
            return $q.reject(response);
        }

        return {
            responseError: error
        };
    }


    function interceptorsConfig($httpProvider)
    {
        $httpProvider.interceptors.push(['$log', loggingInterceptorFactory]);
        $httpProvider.interceptors.push(['$injector', '$rootScope', '$location', '$q', '$log', 'preLoginRoute', interceptor]);
    }

    angular.module('recrutoid').config(['$httpProvider', interceptorsConfig]);
})();
