(function ()
{
    'use strict';

    function routesConfig($routeProvider, $locationProvider)
    {
        var requiredAdmin = {
            requireAdmin: function ($location, CurrentUser)
            {
                return CurrentUser.reload().then(function (user)
                {
                    if ('admin' !== user.role) {
                        return $location.path('/403');
                    }
                });
            }
        };
        var requireEmployer = {
            requireEmployer: function ($location, CurrentUser, siteSettings)
            {
                return CurrentUser.reload().then(function (user)
                {
                    if ('normal' === user.role) {
                        return $location.path('/403');
                    }
                    else if('employer' === user.role) {
                        return siteSettings.requireCvSearchingPermission(user).catch(function ()
                        {
                            return $location.path('/');
                        });
                    }
                });
            }
        };

        $routeProvider.when('/', {
            templateUrl: 'modules/home/home.tpl.html',
            controller: 'HomeCtrl',
            controllerAs: 'home',
            reloadOnSearch: false
        })
        .when('/activate/:token',{
            templateUrl: 'modules/home/home.tpl.html',
            controller: 'HomeCtrl',
            controllerAs: 'home'
        })
        .when('/reset-password/:token',{
            templateUrl: 'modules/common/resetPassword/resetPassword.tpl.html',
            controller: 'ResetPasswordController',
            controllerAs: 'reset'
        })
        .when('/register', {
            templateUrl: 'modules/common/register/register.tpl.html',
            controller: 'RegisterController',
            controllerAs: 'register'
        })
        .when('/register/check-email', {
            templateUrl: 'modules/common/register/message.tpl.html'
        })
        .when('/register/:token', {
            templateUrl: 'modules/common/register/register.tpl.html',
            controller: 'RegisterController',
            controllerAs: 'register'
        })
        .when('/vacancies', {
            templateUrl: 'modules/vacancies/searchVacancies.tpl.html',
            controller: 'SearchVacanciesController',
            controllerAs: 'searchVacancies',
            reloadOnSearch: false
        })
        .when('/vacancies/city/:cityName', {
            templateUrl: 'modules/vacancies/searchVacancies.tpl.html',
            controller: 'SearchVacanciesController',
            controllerAs: 'searchVacancies',
            reloadOnSearch: false
        })
        .when('/admin/vacancies', {
            templateUrl: 'modules/vacancies/searchVacanciesAsAdmin.tpl.html',
            controller: 'SearchVacanciesAsAdminCtrl',
            controllerAs: 'searchVacancies',
            reloadOnSearch: false,
            resolve: requiredAdmin
        })
        .when('/vacancy/:vacancyId', {
            templateUrl: 'modules/vacancies/vacancyDetails.tpl.html',
            controller: 'VacancyDetailsCtrl',
            controllerAs: 'vacancyDetails'
        })
        .when('/vacancy/extend/:token', {
            templateUrl: 'modules/home/home.tpl.html',
            controller: 'ExtendVacancyController'
        })
        .when('/admin/vacancies/waitingForApproval', {
            templateUrl: 'modules/vacancies/vacanciesWaitingForApproval.tpl.html',
            controller: 'SearchVacanciesAsAdminCtrl',
            controllerAs: 'searchVacancies',
            reloadOnSearch: false,
            resolve: requiredAdmin
        })
        .when('/admin/vacancies/publishedViaEmail', {
            templateUrl: 'modules/vacancies/vacanciesPublishViaEmail.tpl.html',
            controller: 'ApprovedVacancyListCtrl',
            controllerAs: 'ApprovedVacancyListCtrl',
            resolve: requiredAdmin
        })
        .when('/admin/vacancies/archived', {
            templateUrl: 'modules/vacancies/vacanciesArchived.tpl.html',
            controller: 'ArchivedVacancyListCtrl',
            controllerAs: 'ArchivedVacancyListCtrl',
            resolve: requiredAdmin
        })
        .when('/admin/vacancies/publishedOnWebsite', {
            templateUrl: 'modules/vacancies/vacanciesPublishedOnWebsite.tpl.html',
            controller: 'PublishedVacancyListCtrl',
            controllerAs: 'PublishedVacancyListCtrl',
            resolve: requiredAdmin
        })
        .when('/admin/vacancies/notverified', {
            templateUrl: 'modules/vacancies/notVerifiedVacancy.tpl.html',
            controller: 'NotVerifiedVacancy',
            controllerAs: 'notVerifiedVacancy',
            resolve: requiredAdmin
        })
        .when('/admin/vacancies/vacanciesCreatedByAnonymous', {
            templateUrl: 'modules/vacancies/anonymousUsersWithVacancies.tpl.html',
            controller: 'AnonymousUsersWithVacancies',
            controllerAs: 'anonymous',
            resolve: requiredAdmin
        })
        .when('/admin/vacancies/distributors', {
            templateUrl: 'modules/vacancies/distributors.tpl.html',
            controller: 'Distributors',
            controllerAs: 'distributors',
            resolve: requiredAdmin
        })
        .when('/vacancies/activate/:token', {
            templateUrl: 'modules/home/home.tpl.html',
            controller: 'ActivateVacancyCtrl',
            controllerAs: 'ActivateVacancyCtrl'
        })
        .when('/vacancies/edit/:id', {
            templateUrl: 'modules/vacancies/editVacancy.tpl.html',
            controller: 'EditVacancyCtrl',
            controllerAs: 'EditVacancyCtrl'
        })
        .when('/subscription/edit/:id', {
            templateUrl: 'modules/subscription/editSubscription.tpl.html',
            controller: 'EditSubscriptionCtrl',
            controllerAs: 'EditSubscriptionCtrl'
        })
        .when('/subscription/list', {
            templateUrl: 'modules/subscription/listSubscription.tpl.html',
            controller: 'ListSubscriptionCtrl',
            controllerAs: 'ListSubscriptionCtrl',
            reloadOnSearch: false,
            resolve: requiredAdmin
        })
        .when('/subscription/activate/:token', {
            templateUrl: 'modules/home/home.tpl.html',
            controller: 'ActivateSubscriptionCtrl',
            controllerAs: 'ActivateSubscriptionCtrl'
        })
        .when('/admin/users', {
            templateUrl: 'modules/user/searchUsersAsAdmin.tpl.html',
            controller: 'SearchUsersAsAdminCtrl',
            controllerAs: 'searchUsers',
            reloadOnSearch: false,
            //resolve:requiredAdmin
        })
        .when('/admin/bugsreport', {
            templateUrl: 'modules/user/bugs-report/bugsReport.tpl.html',
            controller: 'BugReportsCtrl',
            controllerAs: 'searchUsers',
            reloadOnSearch: false,
            resolve:requiredAdmin
        })
          .when('/admin/email-logs', {
              templateUrl: 'modules/user/email-logs/emailLogs.tpl.html',
              controller: 'EmailLogsCtrl',
              controllerAs: 'searchUsers',
              reloadOnSearch: false,
              resolve:requiredAdmin
          })
        .when('/admin/cvcheckup', {
            templateUrl: 'modules/user/cvCheckUp/cvCheckUp.tpl.html',
            controller: 'cvCheckUpCtrl',
            controllerAs: 'cvCheckUp',
            reloadOnSearch: false,
            resolve:requiredAdmin
        })
        .when('/admin/cms/pages-list', {
            templateUrl: 'modules/cms/pages-list.cms.tpl.html',
            controller: 'PagesListCMSCtrl',
            controllerAs: 'list'
        })
        .when('/admin/cms/pages-list', {
            templateUrl: 'modules/cms/pages-list.cms.tpl.html',
            controller: 'PagesListCMSCtrl',
            controllerAs: 'list'
        })
        .when('/admin/cms/edit-page/:slug', {
            templateUrl: 'modules/cms/edit-page.cms.tpl.html',
            controller: 'EditPageCMSCtrl',
            controllerAs: 'edit'
        })
        .when('/page/:slug', {
            controller: 'CmsPageController',
            controllerAs: 'cmsCtrl',
            templateUrl: 'modules/cms/cmsPage.tpl.html'
        })
        .when('/users', {
            templateUrl: 'modules/user/searchUsers.tpl.html',
            controller: 'SearchUsersController',
            controllerAs: 'searchUsers',
            reloadOnSearch: false,
            resolve: requireEmployer
        })
        .when('/user/settings/:site', {
            templateUrl: 'modules/user/settings.tpl.html',
            controller: 'UserSettingsCtrl',
            controllerAs: 'settings',
            reloadOnSearch: false
        })
        .when('/user/:id/profile', {
            templateUrl: 'modules/user/userCv.tpl.html',
            controller: 'UserCVCtrl',
            controllerAs: 'cv'
        })
        .when('/user/activate/:token', {
            templateUrl: 'modules/home/home.tpl.html',
            controller: 'ActivateUserCtrl',
            controllerAs: 'ActivateUserCtrl'
        })
        .when('/user/confirm-email/:token', {
            templateUrl: 'modules/home/home.tpl.html',
            controller: 'HomeCtrl',
            controllerAs: 'home'
        })
        .when('/user/extend-profile/:token', {
            templateUrl: 'modules/home/home.tpl.html',
            controller: 'ExtendProfileController'
        })
        .when('/settings/generals', {
            templateUrl: 'modules/adminSettings/generals.tpl.html',
            controller: 'GeneralsController',
            controllerAs: 'generals',
            resolve: requiredAdmin
        })
        .when('/settings/cities', {
            templateUrl: 'modules/adminSettings/manageCities.tpl.html',
            controller: 'ManageCitiesCtrl',
            controllerAs: 'CitiesCtrl',
            resolve: requiredAdmin
        })
        .when('/settings/salary-ranges', {
            templateUrl: 'modules/adminSettings/manageSalaryRanges.tpl.html',
            controller: 'ManageSalaryRangesCtrl',
            controllerAs: 'SalaryRangesCtrl',
            resolve: requiredAdmin
        })
        .when('/settings/categories', {
            templateUrl: 'modules/adminSettings/manageCategories.tpl.html',
            controller: 'ManageCategoriesCtrl',
            controllerAs: 'CategoriesCtrl',
            resolve: requiredAdmin
        })
        .when('/settings/languages', {
            templateUrl: 'modules/adminSettings/manageLanguage.tpl.html',
            controller: 'ManageLanguagesCtrl',
            controllerAs: 'manageLang',
            resolve: requiredAdmin
        })
          .when('/settings/translations', {
              templateUrl: 'modules/adminSettings/manageTranslations.tpl.html',
              controller: 'ManageTranslationsCtrl',
              controllerAs: 'manageTrans',
              resolve: requiredAdmin
          })
          .when('/settings/phrases', {
              templateUrl: 'modules/adminSettings/managePhrases.tpl.html',
              controller: 'ManagePhrasesCtrl',
              controllerAs: 'managePhrases',
              resolve: requiredAdmin
          })
        .when('/settings/domains', {
            templateUrl: 'modules/adminSettings/manageBlacklistedDomains.tpl.html',
            controller: 'BlacklistedDomainsController',
            controllerAs: 'DomainsCtrl',
            resolve: requiredAdmin
        })
        .when('/settings/banners', {
            templateUrl: 'modules/adminSettings/bannersList.tpl.html',
            controller: 'BannersListController',
            controllerAs: 'bannersCtrl',
            resolve: requiredAdmin
        })
        .when('/settings/banners/:id/edit', {
            templateUrl: 'modules/adminSettings/bannersEdit.tpl.html',
            controller: 'BannersEditController',
            controllerAs: 'bannersCtrl',
            resolve: requiredAdmin
        })
        .when('/admin/send-email', {
            templateUrl: 'modules/adminSettings/sendEmail.tpl.html',
            controller: 'SendEmailController',
            controllerAs: 'sendEmail',
            resolve: requiredAdmin
        })
        .when('/upload-cv', {
            templateUrl: 'modules/common/upload-cv/uploadCv.tpl.html',
            controller: 'EditCvController',
            controllerAs: 'editCv'
        }).when('/403', {
            templateUrl: 'modules/common/errors/403.tpl.html'
        }).when('/404', {
            templateUrl: 'modules/common/errors/404.tpl.html'
        })
        .when('/500', {
            template: '<h1 class="text-center">500</h1>'
        }).otherwise({
            redirectTo: '/'
        });

        $locationProvider.html5Mode(true);
        $locationProvider.hashPrefix('!');
        window.prerenderReady = true;
    }

    angular.module('recrutoid').config(['$routeProvider', '$locationProvider', routesConfig]).value('preLoginRoute', {route: ''});
})();
