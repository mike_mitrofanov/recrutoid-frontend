(function ()
{
    'use strict';

    function CurrentUser($q, $rootScope, Authenticator, UserDAO)
    {
        var userPromise;
        var requirePromise;
        var onreloadCallbacks = [];

        function clear()
        {
            userPromise = null;
            requirePromise = null;
        }

        function onreload(callback, scope)
        {
            onreloadCallbacks.push(callback);
            if (scope) {
                var deregisterScopeDestroyListener = scope.$on('$destroy', function ()
                {
                    var indexOf = onreloadCallbacks.indexOf(callback);
                    if (-1 < indexOf) {
                        onreloadCallbacks.splice(indexOf, 1);
                    }
                    deregisterScopeDestroyListener();
                });
            }
            if (null != userPromise) {
                userPromise.then(callback);
            }
            return service;
        }

        function require()
        {
            if (null == requirePromise) {
                requirePromise = UserDAO.getMe();
            }
            return requirePromise;
        }

        function resolve()
        {
            if (null == userPromise) {
                userPromise = UserDAO.getMeOrNull().then(function (user)
                {
                    for (var i = 0; i < onreloadCallbacks.length; i++) {
                        var callback = onreloadCallbacks[i];
                        callback(user);
                    }
                    return user;
                });
            }
            return userPromise;
        }

        function resolveOrReject()
        {
            return resolve().then(function (user)
            {
                if (!user.email) {
                    var defer = $q.defer();
                    defer.reject();
                    return defer.promise;
                }
                return user;
            });
        }

        function logOut()
        {
            Authenticator.logout();
            service.reload();
        }

        var service = {
            clear: clear,
            onreload: onreload,
            reload: function ()
            {
                clear();
                return resolve();
            },
            require: require,
            resolve: resolve,
            resolveOrReject: resolveOrReject,
            logOut: logOut
        };

        $rootScope.$on('event:auth-loginRequired', service.reload);
        $rootScope.$on('event:auth-loggedIn', service.reload);

        return service;
    }

    angular.module('authentication').factory('CurrentUser', ['$q', '$rootScope', 'Authenticator', 'UserDAO', CurrentUser]);
})();
