(function ()
{
    'use strict';

    function stopPropagation()
    {
        return {
            restrict: 'A', link: function (scope, elem)
            {
                elem.on('click', function (e)
                {
                    e.stopPropagation();
                });
                elem.keypress(function (e)
                {
                    e.stopPropagation();
                });
            }
        };
    }

    angular.module('stopPropagation', []).directive('stopPropagation', [stopPropagation]);
})();
