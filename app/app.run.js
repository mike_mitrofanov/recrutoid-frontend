(function ()
{
    'use strict';

    function recrutoidRun($cookies, gettextCatalog)
    {
        var lang = $cookies.lang || 'en';
        $cookies.lang = lang;
        gettextCatalog.setCurrentLanguage(lang);
    }

    angular.module('recrutoid').run(['$cookies', 'gettextCatalog', recrutoidRun]);
})();
