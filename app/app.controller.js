(function ()
{

    'use strict';

    function AppCtrl($cookies, $templateCache, $location, $rootScope, $window, Authenticator, CurrentUser, gettextCatalog,
            gettext, recrutoidModals, siteSettings, recrutoidPopup, feedbackModal, UserSettingsChangePage,
            CommonDAO, $q, $sce, $filter, profileChecker)
    {
        var ctrl = this;
        this.location = $location.url();
        this.currentLanguage = gettextCatalog.getCurrentLanguage();
        this.navMenuActive = false;
        this.logged = false;
        $rootScope.locationHistory = [];

        $rootScope.trustAsHtml = function (string) {
            return $sce.trustAsHtml(string);
        };

        $rootScope.tinymceFull = {
            plugins: 'autoresize paste lists placeholder',
            toolbar: 'bold italic underline strikethrough bullist numlist undo redo | alignleft aligncenter alignright indent outdent',
            statusbar: false,
            menubar: false,
            paste_preprocess: function (plugin, args) {
                args.content = $filter('cleanHtml')(args.content);
            },
            content_style: "#tinymce {padding-bottom: 0 !important; min-height: 184px;}"
        };

        this.isEmployer = function ()
        {
            return this.currentUser && 'employer' === this.currentUser.role;
        };

        $rootScope.$on('$locationChangeStart', function ($e, next, current)
        {
            ctrl.navMenuActive = false;

            //if(ctrl.currentUser)
            //redirectEmployerToProfileIfNotFilled(ctrl.currentUser, $e);

            // if($e)
            //



            if (next.indexOf('editProfile') == -1 &&
                    next.indexOf('logout') == -1 &&
                    ctrl.currentUser &&
                    ctrl.currentUser.id &&
                    ctrl.currentUser.role === 'employer' &&
                    !profileChecker.isActive(ctrl.currentUser) &&
                    $rootScope.media !== 'mobile') {

                $e.preventDefault();
                employerEmptyProfilePopUp();
            }
        });



        $rootScope.backCallback = function () {}

        $rootScope.$on('$locationChangeSuccess', function ()
        {
            ctrl.reset = false;
            ctrl.location = $location.url();
            $rootScope.locationHistory.push($location.url());
            while (10 < $rootScope.locationHistory.length) {
                $rootScope.locationHistory.shift();
            }

            $rootScope.actualLocation = $location.absUrl();

        });


        $rootScope.$watch(function () {
            return $location.absUrl();
        }, function (newLocation, oldLocation) {
            if ($rootScope.actualLocation === newLocation) {
                $rootScope.backCallback();
            }
        });

        this.changeLanguage = function (lang)
        {
            ctrl.navMenuActive = false;
            gettextCatalog.setCurrentLanguage(lang);
            ctrl.currentLanguage = gettextCatalog.getCurrentLanguage();
            $cookies.lang = lang;
        };

        this.openLoginModal = function ()
        {
            ctrl.navMenuActive = false;
            recrutoidModals.entireFlow();
        };

        this.openFeedbackModal = function ()
        {
            feedbackModal.open();
        };

        this.logout = function ()
        {
            ctrl.navMenuActive = false;
            function doLogout()
            {
                UserSettingsChangePage.clearCallback();
                var impersonated = Authenticator.isImpersonated();
                Authenticator.logout();
                if (impersonated) {
                    $location.path('/admin/users');
                    CurrentUser.reload();
                } else {
                    CurrentUser.reload();
                    $location.path('/?logout');
                }
            }

            if (UserSettingsChangePage.isCallback()) {
                return UserSettingsChangePage.callback().then(doLogout);
            }

            doLogout();
        };

        this.isAdmin = function ()
        {
            return ctrl.currentUser && 'admin' === ctrl.currentUser.role;
        };

        this.isJobSeeker = function ()
        {
            return ctrl.currentUser && 'normal' === ctrl.currentUser.role;
        };

        this.isLoggedIn = function ()
        {
            return ctrl.currentUser ? true : false;
        };

        this.isImpersonated = function ()
        {
            return Authenticator.isImpersonated();
        };

        this.scrollTop = function ()
        {
            $window.scrollTo(0, 0);
        };

        CurrentUser.onreload(function (currentUser)
        {
            ctrl.currentUser = currentUser.id ? currentUser : null;
        });

        function employerEmptyProfilePopUp() {
            var text;
            text = gettext('Message_You_Cant_Use_Page_Until_You_Fill_Up_Your_Company_Profile');
            recrutoidPopup({
                type: 'warning',
                title: gettext('Warning!'),
                text: text
            }, function (confirmed)
            {
                if (confirmed) {
                    $location.path('/user/settings/editProfile');
                }
            });
        }

        function init()
        {
            CurrentUser.resolve().then(function (currentUser)
            {
                if (currentUser.id) {
                    ctrl.currentUser = currentUser;

                    redirectEmployerToProfileIfNotFilled(currentUser);
                } else {
                    // if token is kept in cookies then we have to remove it.
                    Authenticator.logout();
                    ctrl.currentUser = null;
                }
                ctrl.currentUser = currentUser.id ? currentUser : null;
            });

            ctrl.siteSettings = siteSettings.get();
        }

        $rootScope.$on('event:logIn', init);
        $rootScope.$on('event:auth-loggedIn', init);

        $rootScope.$on('event:reloadAvatar', function ($e, url)
        {
            if (ctrl.currentUser) {
                ctrl.currentUser.thumbnail = url;
            }
        });

        $rootScope.$on('event:userDataHasChanged', function ($e, user)
        {
            if (ctrl.currentUser) {
                ctrl.currentUser.firstName = user.firstName;
                ctrl.currentUser.lastName = user.lastName;
            }
        });

        $rootScope.$on('event:viewTypeChanged', function (e, viewType)
        {
            ctrl.appBackground = ('normal' === viewType);
        });


        function redirectEmployerToProfileIfNotFilled(currentUser) {
            if (currentUser.id && currentUser.role === 'employer' && !profileChecker.isActive(currentUser)) {
                var text;
                if ($rootScope.media !== 'mobile') {
                    employerEmptyProfilePopUp();
                } else {
                    text = gettext('Message_You_Cant_Use_Mobile_Version_Until_You_Fill_Up_Your_Company_Profile_Using_Desktop_Or_Tablet');
                    recrutoidPopup({
                        type: 'warning',
                        title: gettext('Warning!'),
                        text: text
                    });
                    ctrl.logout();
                }
            }
        }


        init();


        //ui-select theme override
        $templateCache.put("bootstrap/match.tpl.html",
                "<div class=\"ui-select-match\">" +
                "<i class=\"fa fa-caret2 fa-angle-down caret pull-right\" ng-click=\"$select.toggle($event)\"></i>" +
                "<div ng-hide=\"$select.open && $select.searchEnabled\" ng-disabled=\"$select.disabled\" ng-class=\"{\'btn-default-focus\':$select.focus}\">" +
                "<span tabindex=\"-1\" class=\"btn btn-default form-control ui-select-toggle\" aria-label=\"{{ $select.baseTitle }} activate\" ng-disabled=\"$select.disabled\" ng-click=\"$select.activate()\" style=\"outline: 0;\">" +
                "<span ng-show=\"$select.isEmpty()\" class=\"ui-select-placeholder text-muted\">{{$select.placeholder}}</span>" +
                "<span ng-hide=\"$select.isEmpty()\" class=\"ui-select-match-text pull-left\" ng-class=\"{\'ui-select-allow-clear\': $select.allowClear && !$select.isEmpty()}\" ng-transclude=\"\"></span>" +
                "<a ng-show=\"$select.allowClear && !$select.isEmpty() && ($select.disabled !== true)\" aria-label=\"{{ $select.baseTitle }} clear\" style=\"margin-right: 10px\" ng-click=\"$select.clear($event)\" class=\"btn btn-xs btn-link pull-right\"><i class=\"glyphicon glyphicon-remove\" aria-hidden=\"true\"></i></a>" +
                "</span>" +
                "</div>" +
                "</div>"
                );

    }

    angular.module('recrutoid').controller('AppCtrl',
            ['$cookies', '$templateCache', '$location', '$rootScope', '$window', 'Authenticator', 'CurrentUser', 'gettextCatalog', 'gettext',
                'recrutoidModals', 'siteSettings', 'recrutoidPopup', 'feedbackModal', 'UserSettingsChangePage',
                'CommonDAO', '$q', '$sce', '$filter', 'profileChecker', AppCtrl]);
})();
