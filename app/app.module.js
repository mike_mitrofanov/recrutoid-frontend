(function ()
{
    'use strict';
    angular.module('recrutoid', [

        'ui.bootstrap', 'ngResource', 'ngRoute', 'ngCookies',  'ui.select', 'ngAnimate', 'angular-loading-bar',
        'ui-notification', 'angulike', 'xeditable', 'textAngular', 'ui.tinymce', 'gettext', 'duScroll',
        'ngFileUpload', 'LocalStorageModule', 'gettext', 'satellizer', 'slickCarousel', 'ngImgCrop', 'ui.mask',
        'angular-ladda', 'perfect_scrollbar', 'angular-click-outside', 'hl.sticky', 'sticky', 'ngLocationUpdate',
        'dibari.angular-ellipsis','sprintf', 'monospaced.elastic', 'ngCroppie',
        // app modules
        'recrutoid.resources', 'recrutoid.common', 'recrutoid.subscription', 'recrutoid.vacancy', 'recrutoid.user',
        'recrutoid.adminSettings', 'recrutoid.cms', 'recrutoid.home','rzModule',

        // custom
        'stopPropagation', 'paginationSupport', 'authentication'
    ]);

})();
