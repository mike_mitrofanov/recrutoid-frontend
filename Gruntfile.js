/*global module,require,process*/
/*jshint camelcase:false*/
var LIVERELOAD_PORT, lrSnippet, mountFolder;

LIVERELOAD_PORT = 35728;

lrSnippet = require('connect-livereload')({
    port: LIVERELOAD_PORT
});

var modRewrite = require('connect-modrewrite');

/* var conf = require('./conf.'+process.env.NODE_ENV); */

mountFolder = function (connect, dir)
{
    'use strict';

    return connect['static'](require('path').resolve(dir));
};

module.exports = function (grunt)
{
    'use strict';

    var yeomanConfig;

    // required for Heroku
    require('grunt-connect-proxy');
    grunt.loadNpmTasks('grunt-connect-proxy');

    require('load-grunt-tasks')(grunt);
    grunt.loadNpmTasks('grunt-angular-gettext');

    /* configurable paths */
    yeomanConfig = {
        app: 'app', dist: 'dist'
    };
    try {
        yeomanConfig.app = require('./bower.json').appPath || yeomanConfig.app;
    } catch (_error) {
    }
    var backendHost = process.env.host || grunt.option('backend-host') || 'localhost';
    var backendPort = process.env.port || grunt.option('backend-port') || 3000;
    grunt.initConfig({
        yeoman: yeomanConfig,
        watch: {
            less: {
                files: ['<%= yeoman.app %>/**/*.less'],
                tasks: ['less'],
                options: {
                    spawn: false
                }
            },
            livereload: {
                options: {
                    livereload: LIVERELOAD_PORT
                },
                files: ['<%= yeoman.app %>/index.html',
                        '<%= yeoman.app %>/*.js',
                        '<%= yeoman.app %>/modules/**/*',
                        '<%= yeoman.app %>/vendor/**/*',
                        '<%= yeoman.app %>/images/**/*.{png,jpg,jpeg,gif,webp,svg}']
            }
        },
        connect: {
            options: {
                port: process.env.PORT || 9000,
                hostname: process.env.SERVER_HOST || 'localhost',
                livereload: LIVERELOAD_PORT
            },
            livereload: {
                options: {
                    open: true,
                    middleware: function (connect) {
                        return [
                            // in case of using html5Mode - makes accessible uris without hashbang but containing view's path
                            modRewrite(['!\\.html|\\.js|\\.svg|\\.css|\\.png|\\.gif|\\.jpg|\\.ttf|\\.woff|(\\api.+)$ /index.html [L]']),
                            connect.static(yeomanConfig.app),
                            require('grunt-connect-proxy/lib/utils').proxyRequest
                        ];
                    }
                }
            },
            proxies: [{
                context: ['/api'],
                host: backendHost,
                port: backendPort,
                changeOrigin: true,
                xforward: false,
                headers: {
                    host: backendHost + ':' + backendPort
                }
            }],
            dist: {
                options: {
                    middleware: function (connect)
                    {
                        return [lrSnippet,
                                mountFolder(connect, '.tmp'),
                                mountFolder(connect, yeomanConfig.dist),

                                connect().use('/bower_components', connect.static('./bower_components')),
                                require('grunt-connect-proxy/lib/utils').proxyRequest];
                    }
                }
            }
        },
        jshint: {
            options: {
                jshintrc: '.jshintrc'
            },
            all: ['**/*.js']
        },
        less: {
            compile: {
                options: {
                    paths: ['app'],
                    rootPath: 'app',
                    relativeUrls: true
                },
                files: {
                    'app/app.css': 'app/app.less'
                }
            }
        },
        // Automatically inject Bower components into the app
        wiredep: {
            app: {
                src: ['<%= yeoman.app %>/index.html'],
                ignorePath: /\.\.\//
            }
        },
        concurrent: {
            server: ['less'],
            dist: [
                'copy:styles'
            ]
        },
        // Add vendor prefixed styles
        autoprefixer: {
            options: {
                browsers: ['last 1 version']
            }, dist: {
                files: [{
                    expand: true,
                    cwd: '.tmp/styles/',
                    src: '{,*/}*.css',
                    dest: '<%= yeoman.dist %>/styles/'
                }]
            }
        },
        nggettext_extract: {
            pot: {
                options: {
                    startDelim: '{{',
                    endDelim: '}}'
                },
                files: {
                    'po/pl.pot': ['app/modules/**/*.html', 'app/modules/**/*.js']
                }
            }
        },
        nggettext_compile: {
            all: {
                options: {
                    module: 'recrutoid'
                },
                files: {
                    'app/modules/common/translations.js': ['po/*.po']
                }
            }
        },
        clean: {
            dist: {
                files: [{
                    dot: true, src: ['.tmp', '<%= yeoman.dist %>/{,*/}*', '!<%= yeoman.dist %>/.git{,*/}*']
                }]
            }
        },
        useminPrepare: {
            html: '<%= yeoman.app %>/index.html',
            options: {
                dest: '<%= yeoman.dist %>',
                flow: {
                    html: {
                        steps: {
                            js: ['concat'/*, 'uglifyjs'*/],
                            css: ['cssmin']
                        },
                        post: {}
                    }
                }
            }
        },
        usemin: {
            html: ['<%= yeoman.dist %>/index.html', '!<%= yeoman.dist %>/bower_components/**'],
            css: ['<%= yeoman.dist %>/styles/**/*.css'],
            options: {
                dirs: ['<%= yeoman.dist %>']
            }
        },
        htmlmin: {
            dist: {
                options: {
                    removeComments: true,
                    collapseWhitespace: true
                },
                files: [
                    {
                        expand: true,
                        cwd: '<%= yeoman.dist %>',
                        src: ['*.html', 'modules/**/*.html', 'vendor/**/*.html'],
                        dest: '<%= yeoman.dist %>'
                    }
                ]
            }
        },
        copy: {
            dist: {
                files: [
                    {
                        expand: true,
                        dot: true,
                        cwd: '<%= yeoman.app %>',
                        dest: '<%= yeoman.dist %>',
                        src: [
                            'favicon.ico',
                            'fonts/**/*',
                            'i18n/**/*',
                            'images/**/*',
                            'styles/fonts/**/*',
                            'styles/*.css',
                            'styles/img/**/*',
                            'styles/ui/images/*',
                            '*.html',
                            '*.css',
                            'modules/**/*.jpg',
                            'modules/**/*.png',
                            'modules/**/*.gif'
                        ]
                    }, {
                        expand: true,
                        cwd: '<%= yeoman.app %>',
                        dest: '<%= yeoman.dist %>',
                        src: ['styles/**', 'assets/**']
                    }, {
                        expand: true,
                        cwd: '.tmp/images',
                        dest: '<%= yeoman.dist %>/images',
                        src: ['generated/*']
                    },
                    {
                        expand: true,
                        cwd: '<%= yeoman.app %>/bower_components/font-awesome/fonts',
                        dest: '<%= yeoman.dist %>/fonts',
                        src: ['*']
                    },
                    {
                        expand: true,
                        cwd: '<%= yeoman.app %>/bower_components/bootstrap/fonts',
                        dest: '<%= yeoman.dist %>/fonts',
                        src: ['*']
                    },
                    {
                        expand: true,
                        cwd: '<%= yeoman.app %>/bower_components/slick-carousel/slick/fonts',
                        dest: '<%= yeoman.dist %>/styles/fonts',
                        src: ['*']
                    },
                    {
                        expand: true,
                        cwd: '<%= yeoman.app %>/bower_components/slick-carousel/slick',
                        dest: '<%= yeoman.dist %>/styles',
                        src: ['ajax-loader.gif']
                    }
                ]
            },
            styles: {
                expand: true,
                cwd: '<%= yeoman.app %>/styles',
                dest: '<%= yeoman.dist %>/styles/',
                src: '**/*.css'
            }
        },
        ngtemplates: {
            recrutoid: {
                cwd: '<%= yeoman.app %>',
                src: ['**/*.html', '!index.html', '!bower_components/**/*'],
                dest: '<%= yeoman.dist %>/scripts/templates.js'
            }
        },
        concat: {
            options: {
                separator: ';\n'
            },
            dist: {
                files: {
                    '.tmp/concat/scripts/app.js': ['<%= yeoman.app %>/*.js',
                                                       '<%= yeoman.app %>/modules/**/*.js',
                                                       '<%= yeoman.app %>/vendor/**/*.js',
                                                          '.tmp/concat/templates.js'],
                    '.tmp/concat/scripts/vendor.js': ['<%= yeoman.app %>/bower_components/**/*.js']
                }
            }
        },
        cssmin: {},
        cachebreaker: {
            dev: {
                options: {
                    match: ['app.js', 'vendor.js', 'templates.js', 'app.css', 'vendor.css'],
                    replacement: function ()
                    {
                        return new Date().getTime();
                    }

                },
                files: {
                    src: ['dist/index.html']
                }
            }
        },
        'string-replace': {
            dist: {
                files: {
                    '<%= yeoman.dist %>/index.html': '<%= yeoman.dist %>/index.html'
                },
                options: {
                    replacements: [
                        {
                            pattern: '<!-- templates -->',
                            replacement: '<script src="scripts/templates.js"></script>'
                        }]
                }
            }
        }
    });
    //grunt.registerTask('serve', function (target)
    //{
    //    if (target === 'dist') {
    //        return grunt.task.run(['build', 'configureProxies:server', 'connect:dist:keepalive']);
    //    }
    //    return grunt.task.run([ 'concurrent:server', 'configureProxies:server', 'connect:livereload', 'watch']);
    //});

    grunt.registerTask('serve', 'Compile then start a connect web server', [ 'concurrent:server', 'configureProxies:server', 'connect:livereload', 'watch']);

    // Heroku
    grunt.registerTask('heroku', 'Compile then start a connect web server', function ()
    {
        grunt.task.run(['wiredep', 'concurrent:server', 'configureProxies:server', 'autoprefixer']);
    });

    grunt.registerTask('build', [
        'less',
        'clean:dist',
        //'wiredep',
        'useminPrepare',
        //'concurrent:dist',
        'autoprefixer',
        'ngtemplates',
        'concat',
        //'ngAnnotate',
        'copy:dist',
        'cssmin',
        //'uglify',
        'usemin',
        'string-replace',
        'cachebreaker:dev'
        //'htmlmin'
    ]);

    return grunt.registerTask('default', ['serve']);
};
