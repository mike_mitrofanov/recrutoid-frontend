/*global require, process, __dirname, console*/
(function ()
{
    'use strict';

    var express = require('express');
    var proxy = require('http-proxy-middleware');
    var http = require('http');
    var auth = require('http-auth');

    //var basic = auth.basic({
    //    realm: "Simon Area.",
    //    file: __dirname + "/htpasswd"
    //});
    var app = express();

    var config = {
        backendUrl: process.env.PROXY || 'http://localhost:3000',
        port: process.env.PORT || 9000
    };

    app.use(require('prerender-node'));
    app.use('/api', proxy({target: config.backendUrl, changeOrigin: true}));
    app.use('/sitemap.xml', proxy({target: config.backendUrl, changeOrigin: true}));
    app.use('/email-static', proxy({target: config.backendUrl, changeOrigin: true}));
    //app.use(auth.connect(basic));

    app.use('/', express.static(__dirname + '/dist'));


    //TODO Add max expiration time for css i js with ?rel=
    // ~* \.(css|js)\?rel=.*$

    app.get('*', function (req, res) {
        res.sendFile(__dirname + '/dist/index.html');
    });

    var httpListener = http.createServer(app).listen(config.port, function() {
        console.info('listening on port', httpListener.address().port);
    });

    process.on('uncaughtException', function (err)
    {
        console.error('Uncaught exception:', err.stack);
    });
})();
